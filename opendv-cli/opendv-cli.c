#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <limits.h>
#include <pthread.h>

#include "json-c/json.h"
#include "curl/curl.h"
#include "common.h"
#include "opendv-cli.h"

#define NELEMS(x)   (sizeof(x) / sizeof((x)[0]))

static int next_id = 1;

typedef struct {
    CURL *curl;
    uint8_t *buf;
    uint8_t *ptr;
    uint8_t *end;
} HttpClient;

static size_t curl_write_ctrl(void *data, size_t size, size_t nmemb, void *userp)
{
    HttpClient *cli = userp;

    int blksize = size * nmemb;

    int bufsize = cli->ptr - cli->buf;

    int bufleft = cli->end - cli->ptr - 1;
    if (bufleft < blksize) {
        int newsize = bufsize + blksize + 1;    /* 1 for NULL teminator */
        cli->buf = realloc(cli->buf, newsize);
        cli->ptr = cli->buf + bufsize;
        cli->end = cli->buf + newsize;
    }

    memcpy(cli->ptr, data, blksize);
    cli->ptr += blksize;

//    printf("size: %ld, nmemb: %ld\n", size, nmemb);
#if 0
    char *reply = data;
    printf("reply: %s\n", data);
#endif

#if 0
    json_object **object = (json_object **)userp;

    *object = json_tokener_parse(data);
    if (!*object)
        return -1;
#endif

    return size * nmemb;
}

#if 0
static void dump_chars(char *str, int len)
{
    printf("%d: ", len);
    int i;
    for (i = 0; i < len; i++)
        putchar(str[i]);
    putchar('\n');
}
#endif

static int http_transfer(const char *url, uint8_t *in, int in_len, uint8_t **out, int *out_len)
{
    HttpClient tmp, *cli = &tmp;
    if (!cli)
        return -1;
    memset(cli, 0, sizeof(HttpClient));

    CURLcode res = CURLE_OK;

    cli->curl = curl_easy_init();
    if (!cli->curl) {
        fprintf(stderr, "curl null\n");
        return -1;
    }

    curl_easy_setopt(cli->curl, CURLOPT_URL, url);
    curl_easy_setopt(cli->curl, CURLOPT_POSTFIELDS, in);
    curl_easy_setopt(cli->curl, CURLOPT_POSTFIELDSIZE, in_len);
    curl_easy_setopt(cli->curl, CURLOPT_WRITEFUNCTION, curl_write_ctrl);
    curl_easy_setopt(cli->curl, CURLOPT_WRITEDATA, (void *)cli);

    res = curl_easy_perform(cli->curl);
    if(res != CURLE_OK) {
        curl_easy_cleanup(cli->curl);
        return -1;
    }

    curl_easy_cleanup(cli->curl);

    *out = cli->buf;
    *out_len = cli->ptr - cli->buf;

    if (cli->buf)
        cli->buf[*out_len] = '\0';

    return 0;
}

static json_object *new_request(const char *method, json_object *params, int id)
{
    json_object *request = json_object_new_object();
    if (!request)
        return NULL;

    json_object_object_add(request, "method", json_object_new_string(method));
    if (params)
        json_object_object_add(request, "params", params);
    json_object_object_add(request, "id", json_object_new_int(id));

    return request;
}

static int get_result(const char *url, const char *method, json_object *params, json_object **result, int *status)
{
    char *out;
    int out_len;

    int new_id = __sync_fetch_and_add(&next_id, 1);

    json_object *request = new_request(method, params, new_id);
    if (!request) {
        if (params)
            json_object_put(params);
        return -1;
    }

    const char *req_str = json_object_to_json_string_ext(request, JSON_C_TO_STRING_PRETTY);
    if (!req_str) {
        json_object_put(request);
        return -1;
    }

    if (http_transfer(url, (uint8_t *)req_str, strlen(req_str), (uint8_t **)&out, &out_len) < 0) {
        json_object_put(request);
        return -1;
    }

    json_object_put(request);

//    dump_chars(out, out_len);

    json_object *response = json_tokener_parse(out);
    free(out);

    if (!response)
        return -1;

    int id = 0;

    int found_id = 0;
    int found_status = 0;
    *result = NULL;

    json_object_object_foreach(response, key, val) {
        if (strcmp(key, "status") == 0) {
            *status = json_object_get_int(val);
            found_status = 1;
        } else if (strcmp(key, "result") == 0) {
            const char *result_str = json_object_to_json_string_ext(val, JSON_C_TO_STRING_PRETTY);
            *result = json_tokener_parse(result_str);
        } else if (strcmp(key, "id") == 0) {
            id = json_object_get_int(val);
            found_id = 1;
        }
    }

    if (!found_status || !found_id || new_id != id) {
        if (*result) {
            json_object_put(*result);
            *result = NULL;
        }

        json_object_put(response);
        printf("not a valid message\n");
        return -1;
    }

    json_object_put(response);

    return 0;
}

static char control_url[1024];

int OpenDV_Init(char *ipaddr, int port)
{
    snprintf(control_url, sizeof(control_url), "http://%s:%d/camera", ipaddr, port);
    return 0;
}

int OpenDV_Quit(void)
{
    return 0;
}

int OpenDV_StartUpgrade(const char *filename)
{
    json_object *params = json_object_new_object();

    json_object_object_add(params, "filename", json_object_new_string(filename));
   
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "StartUpgrade", params, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_SystemShutdown(void)
{
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "SystemShutdown", NULL, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_SystemReboot(void)
{
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "SystemReboot", NULL, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_SystemFactoryReset(int hard)
{
    json_object *params = json_object_new_object();

    json_object_object_add(params, "hard", json_object_new_int(hard));
   
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "SystemFactoryReset", params, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_GetStorageStatus(StorageStatus *stat)
{
    int status;
    json_object *result = NULL;
   
    if (get_result(control_url, "GetStorageStatus", NULL, &result, &status) < 0)
        return -1;

    json_object_object_foreach(result, key, val) {
        if (strcmp(key, "status") == 0)
            stat->status = json_object_get_int(val);
        else if (strcmp(key, "total_size") == 0)
            stat->total_size = json_object_get_int64(val);
        else if (strcmp(key, "free_size") == 0)
            stat->free_size = json_object_get_int64(val);
    }

    json_object_put(result);

    return status;
}

int OpenDV_FormatStorage(void)
{
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "FormatStorage", NULL, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_GetWifiConfiguration(WifiConfiguration *conf)
{
    int status;
    json_object *result = NULL;
   
    if (get_result(control_url, "GetWifiConfiguration", NULL, &result, &status) < 0)
        return -1;

    if (!result)
        return -1;

    json_object_object_foreach(result, key, val) {
        if (strcmp(key, "enable") == 0) {
            conf->enable = json_object_get_int(val);
        } else if (strcmp(key, "ssid") == 0) {
            strncpy(conf->ssid, json_object_get_string(val), sizeof(conf->ssid) - 1);
        } else if (strcmp(key, "password") == 0) {
            strncpy(conf->password, json_object_get_string(val), sizeof(conf->password) - 1);
        } else if (strcmp(key, "band") == 0) {
            conf->band = json_object_get_int(val);
        }
    }

    json_object_put(result);

    return status;
}

int OpenDV_SetWifiConfiguration(WifiConfiguration *conf)
{
    json_object *params = json_object_new_object();

    json_object_object_add(params, "enable",   json_object_new_int(conf->enable));
    json_object_object_add(params, "ssid",     json_object_new_string(conf->ssid));
    json_object_object_add(params, "password", json_object_new_string(conf->password));
    json_object_object_add(params, "band",     json_object_new_int(conf->band));

    int status;
    json_object *result = NULL;
    if (get_result(control_url, "SetWifiConfiguration", params, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_GetRecordSettings(RecordSettings *settings)
{
    int status;
    json_object *result = NULL;
   
    if (get_result(control_url, "GetRecordSettings", NULL, &result, &status) < 0)
        return -1;

    if (!result)
        return -1;

    const char *recfmt = NULL;
    int pre_record_time = -1;
    int max_record_size = -1;
    int circulate_record = -1;
    int duration = -1;

    json_object_object_foreach(result, key, val) {
        if (strcmp(key, "recfmt") == 0) {
            recfmt = json_object_get_string(val);
        } else if (strcmp(key, "pre_record_time") == 0) {
            pre_record_time = json_object_get_int(val);
        } else if (strcmp(key, "max_record_size") == 0) {
            max_record_size = json_object_get_int(val);
        } else if (strcmp(key, "circulate_record") == 0) {
            circulate_record = json_object_get_int(val);
        } else if (strcmp(key, "duration") == 0) {
            duration = json_object_get_int(val);
        }
    }

    if (!recfmt && pre_record_time < 0 && max_record_size < 0 && circulate_record < 0 && duration < 0)
        return -1;

    if (recfmt) {
        if (strcmp(recfmt, "MOV") == 0)
            settings->recfmt = REC_FMT_MOV;
        else if (strcmp(recfmt, "MP4") == 0)
            settings->recfmt = REC_FMT_MP4;
        else
            return -1;
    }

    if (pre_record_time >= 0)
        settings->pre_record_time = pre_record_time;

    if (max_record_size >= 0)
        settings->max_record_size = max_record_size;

    if (circulate_record >= 0)
        settings->circulate_record = circulate_record;

    if (duration >= 0)
        settings->duration = duration;

    json_object_put(result);

    return status;
}

int OpenDV_SetRecordSettings(RecordSettings *settings)
{
    json_object *params = json_object_new_object();

    switch (settings->recfmt) {
    case REC_FMT_MOV:
        json_object_object_add(params, "recfmt", json_object_new_string("MOV"));
        break;
    case REC_FMT_MP4:
        json_object_object_add(params, "recfmt", json_object_new_string("MP4"));
        break;
    default:
        json_object_put(params);
        return -1;
    }

    json_object_object_add(params, "pre_record_time", json_object_new_int(settings->pre_record_time));
    json_object_object_add(params, "max_record_size", json_object_new_int(settings->max_record_size));
    json_object_object_add(params, "circulate_record", json_object_new_int(settings->circulate_record));
    json_object_object_add(params, "duration", json_object_new_int(settings->duration));

    int status;
    json_object *result = NULL;
    if (get_result(control_url, "SetRecordSettings", params, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_GetPhotoTakingSettings(PhotoTakingSettings *settings)
{
    int status;
    json_object *result = NULL;
   
    if (get_result(control_url, "GetPhotoTakingSettings", NULL, &result, &status) < 0)
        return -1;

    json_object_object_foreach(result, key, val) {
        if (strcmp(key, "res_width") == 0) {
            settings->res_width = json_object_get_int(val);
        } else if (strcmp(key, "res_height") == 0) {
            settings->res_height = json_object_get_int(val);
        }
    }

    json_object_put(result);

    return status;
}

int OpenDV_SetPhotoTakingSettings(PhotoTakingSettings *settings)
{
    json_object *params = json_object_new_object();

    json_object_object_add(params, "res_width",  json_object_new_int(settings->res_width));
    json_object_object_add(params, "res_height", json_object_new_int(settings->res_height));

    int status;
    json_object *result = NULL;
    if (get_result(control_url, "SetPhotoTakingSettings", params, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_GetOsdSettings(OsdSettings *settings)
{
    int status;
    json_object *result = NULL;
   
    if (get_result(control_url, "GetOsdSettings", NULL, &result, &status) < 0)
        return -1;

    json_object_object_foreach(result, key, val) {
        if (strcmp(key, "time") == 0) {
            json_object_object_foreach(val, key1, val1) {
                if (strcmp(key1, "enable") == 0) {
                    settings->time.enable = json_object_get_int(val1);
                } else if (strcmp(key1, "font_size") == 0) {
                    settings->time.font_size = json_object_get_int(val1);
                } else if (strcmp(key1, "color") == 0) {
                    settings->time.color = json_object_get_int(val1);
                } else if (strcmp(key1, "alpha") == 0) {
                    settings->time.alpha = json_object_get_int(val1);
                } else if (strcmp(key1, "x") == 0) {
                    settings->time.x = json_object_get_int(val1);
                } else if (strcmp(key1, "y") == 0) {
                    settings->time.y = json_object_get_int(val1);
                } else if (strcmp(key1, "time_format") == 0) {
                    settings->time.time_format = json_object_get_int(val1);
                }
            }
        } else if (strcmp(key, "text") == 0) {
            json_object_object_foreach(val, key1, val1) {
                if (strcmp(key1, "enable") == 0) {
                    settings->text.enable = json_object_get_int(val1);
                } else if (strcmp(key1, "font_size") == 0) {
                    settings->text.font_size = json_object_get_int(val1);
                } else if (strcmp(key1, "color") == 0) {
                    settings->text.color = json_object_get_int(val1);
                } else if (strcmp(key1, "alpha") == 0) {
                    settings->text.alpha = json_object_get_int(val1);
                } else if (strcmp(key1, "x") == 0) {
                    settings->text.x = json_object_get_int(val1);
                } else if (strcmp(key1, "y") == 0) {
                    settings->text.y = json_object_get_int(val1);
                } else if (strcmp(key1, "text") == 0) {
                    strncpy(settings->text.text, json_object_get_string(val1), sizeof(settings->text.text) - 1);
                }
            }
        }
    }

    json_object_put(result);

    return status;
}

int OpenDV_SetOsdSettings(OsdSettings *settings)
{
    json_object *params = json_object_new_object();

    json_object *timeobj = json_object_new_object();

    json_object_object_add(timeobj, "enable",      json_object_new_int(settings->time.enable));
    json_object_object_add(timeobj, "font_size",   json_object_new_int(settings->time.font_size));
    json_object_object_add(timeobj, "color",       json_object_new_int(settings->time.color));
    json_object_object_add(timeobj, "alpha",       json_object_new_int(settings->time.alpha));
    json_object_object_add(timeobj, "x",           json_object_new_int(settings->time.x));
    json_object_object_add(timeobj, "y",           json_object_new_int(settings->time.y));
    json_object_object_add(timeobj, "time_format", json_object_new_int(settings->time.time_format));

    json_object *textobj = json_object_new_object();

    json_object_object_add(textobj, "enable",      json_object_new_int(settings->text.enable));
    json_object_object_add(textobj, "font_size",   json_object_new_int(settings->text.font_size));
    json_object_object_add(textobj, "color",       json_object_new_int(settings->text.color));
    json_object_object_add(textobj, "alpha",       json_object_new_int(settings->text.alpha));
    json_object_object_add(textobj, "x",           json_object_new_int(settings->text.x));
    json_object_object_add(textobj, "y",           json_object_new_int(settings->text.y));
    json_object_object_add(textobj, "text",        json_object_new_string(settings->text.text));

    json_object_object_add(params, "time", timeobj);
    json_object_object_add(params, "text", textobj);
   
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "SetOsdSettings", params, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_GetCameraMode(CameraMode *mode)
{
    int status;
    json_object *result = NULL;
   
    if (get_result(control_url, "GetCameraMode", NULL, &result, &status) < 0)
        return -1;

    const char *mode_str = NULL;

    json_object_object_foreach(result, key, val) {
        if (strcmp(key, "mode") == 0) {
            mode_str = json_object_get_string(val);
        }
    }

    if (!mode_str)
        goto fail;

    if (strcmp(mode_str, "record") == 0)
        *mode = CAMERA_MODE_RECORD;
    else if (strcmp(mode_str, "capture") == 0)
        *mode = CAMERA_MODE_CAPTURE;
    else if (strcmp(mode_str, "playback") == 0)
        *mode = CAMERA_MODE_PLAYBACK;
    else
        goto fail;

    json_object_put(result);
    return status;

fail:
    json_object_put(result);
    return -1;
}

int OpenDV_SetCameraMode(CameraMode mode)
{
    json_object *params = json_object_new_object();

    if (mode == CAMERA_MODE_RECORD)
        json_object_object_add(params, "mode", json_object_new_string("record"));
    else if (mode == CAMERA_MODE_CAPTURE)
        json_object_object_add(params, "mode", json_object_new_string("capture"));
    else if (mode == CAMERA_MODE_IDLE)
        json_object_object_add(params, "mode", json_object_new_string("idle"));
    else if (mode == CAMERA_MODE_PLAYBACK)
        json_object_object_add(params, "mode", json_object_new_string("playback"));
    else {
        json_object_put(params);
        return -1;
    }
   
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "SetCameraMode", params, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_StartRecord(void)
{
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "StartRecord", NULL, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_StopRecord(void)
{
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "StopRecord", NULL, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_StartSnapshot(void)
{
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "StartSnapshot", NULL, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_StopSnapshot(void)
{
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "StopSnapshot", NULL, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_GetFilesNum(const char *suffix, int *total)
{
    json_object *params = NULL;

    if (suffix) {
        params = json_object_new_object();
        json_object_object_add(params, "suffix", json_object_new_string(suffix));
    }
   
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "GetFilesNum", params, &result, &status) < 0)
        return -1;

    if (!result)
        return -1;

    *total = 0;

    json_object_object_foreach(result, key, val) {
        if (strcmp(key, "total") == 0) {
            *total = json_object_get_int(val);
        }
    }

    json_object_put(result);

    return status;
}

int OpenDV_DeleteFiles(char *files[128], int nb_files)
{
    json_object *filelist = json_object_new_array();
    if (!filelist)
        return -1;

    int i;
    for (i = 0; i < nb_files; i++)
        json_object_array_add(filelist, json_object_new_string(files[i]));

    json_object *params = json_object_new_object();

    json_object_object_add(params, "filelist", filelist);

    int status;
    json_object *result = NULL;
    if (get_result(control_url, "DeleteFiles", params, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

int OpenDV_GetFilesList(int index, const char *suffix, FileItem *files, int *nb_files)
{
    json_object *params = json_object_new_object();

    json_object_object_add(params, "index", json_object_new_int(index));

    if (suffix)
        json_object_object_add(params, "suffix", json_object_new_string(suffix));

    json_object_object_add(params, "numfiles", json_object_new_int(*nb_files));
   
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "GetFilesList", params, &result, &status) < 0)
        return -1;

    if (!result)
        return -1;

    json_object_object_foreach(result, key, val) {
        if (strcmp(key, "filelist") == 0) {
            int length = json_object_array_length(val);

            int i;
            for (i = 0; i < length; i++) {
                json_object *fileObj = json_object_array_get_idx(val, i);
                FileItem *fileItem = &files[i];

                json_object_object_foreach(fileObj, key1, val1) {
                    if (strcmp(key1, "index") == 0)
                        fileItem->index = json_object_get_int(val1);
                    else if (strcmp(key1, "name") == 0)
                        strncpy(fileItem->filename, json_object_get_string(val1), sizeof(fileItem->filename) - 1);
                    else if (strcmp(key1, "size") == 0)
                        fileItem->index = json_object_get_int64(val1);
                    else if (strcmp(key1, "time") == 0)
                        strncpy(fileItem->datetime, json_object_get_string(val1), sizeof(fileItem->datetime) - 1);
                }
            }

            *nb_files = length;
        }
    }

    json_object_put(result);

    return status;
}

int OpenDV_CreateEventSubscriber(int *session_id)
{
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "CreateEventSubscriber", NULL, &result, &status) < 0)
        return -1;

    if (!result)
        return -1;

    *session_id = 0;

    json_object_object_foreach(result, key, val) {
        if (strcmp(key, "session_id") == 0) {
            *session_id = json_object_get_int(val);
        }
    }

    json_object_put(result);

    return status;
}

int OpenDV_DeleteEventSubscriber(int session_id)
{
    json_object *params = json_object_new_object();

    json_object_object_add(params, "session_id", json_object_new_int(session_id));
   
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "DeleteEventSubscriber", params, &result, &status) < 0)
        return -1;

    if (result)
        json_object_put(result);

    return status;
}

typedef struct {
    int   type;
    char *name;
} EventMap;

static EventMap eventMaps[] = {
    { EVENT_TYPE_PHOTO_TAKING_STARTED,    "photo_taking_started"    },
    { EVENT_TYPE_PHOTO_TAKING_STOPPED,    "photo_taking_stopped"    },
    { EVENT_TYPE_PHOTO_TAKEN,             "photo_taken"             },
    { EVENT_TYPE_VIDEO_RECORDING_STARTED, "video_recording_started" },
    { EVENT_TYPE_VIDEO_RECORDING_STOPPED, "video_recording_stopped" },
    { EVENT_TYPE_UPGRADE_STARTED,         "upgrade_started"         },
    { EVENT_TYPE_UPGRADE_STOPPED,         "upgrade_stopped"         },
    { EVENT_TYPE_STORAGE_OUT_OF_SPACE,    "storage_out_of_space"    },
    { EVENT_TYPE_LOW_BATTERY_LEVEL,       "low_battery_level"       },
    { EVENT_TYPE_STORAGE_STATUS,          "storage_status"          },
    { EVENT_TYPE_STORAGE_ADDED,           "storage_added"           },
    { EVENT_TYPE_STORAGE_REMOVED,         "storage_removed"         }
};

static int get_event_type(const char *name)
{
    int i;
    for (i = 0; i < NELEMS(eventMaps); i++) {
        EventMap *m = &eventMaps[i];
        if (strcmp(m->name, name) == 0)
            return m->type;
    }

    return -1;
}

int OpenDV_GetEvents(int session_id, Event *events, int *num_events)
{
    json_object *params = json_object_new_object();

    json_object_object_add(params, "session_id", json_object_new_int(session_id));
    json_object_object_add(params, "num_events", json_object_new_int(*num_events));
   
    int status;
    json_object *result = NULL;
    if (get_result(control_url, "GetEvents", params, &result, &status) < 0)
        return -1;

    if (!result)
        return -1;

    json_object_object_foreach(result, key, val) {
        if (strcmp(key, "events") == 0) {
            int length = json_object_array_length(val);

            int count = 0;

            int i;
            for (i = 0; i < length; i++) {
                json_object *eventObj = json_object_array_get_idx(val, i);
                Event *event = &events[count];

                event->event_type = -1;

                const char *snapshot = NULL;
                const char *filename = NULL;
                const char *thumbnail = NULL;

                json_object_object_foreach(eventObj, key1, val1) {
                    if (strcmp(key1, "event_type") == 0) {
                        event->event_type = get_event_type(json_object_get_string(val1));
                    } else if (strcmp(key1, "snapshot") == 0)
                        snapshot = json_object_get_string(val1);
                    else if (strcmp(key1, "filename") == 0)
                        filename = json_object_get_string(val1);
                    else if (strcmp(key1, "thumbnail") == 0)
                        thumbnail = json_object_get_string(val1);
                }

                if (event->event_type == -1)
                    continue;

                switch (event->event_type) {
                case EVENT_TYPE_VIDEO_RECORDING_STARTED:
                    strcpy(event->event_data.recording_started.snapshot, snapshot);
                    break;
                case EVENT_TYPE_VIDEO_RECORDING_STOPPED:
                    if (filename)
                        strcpy(event->event_data.recording_stopped.filepath, filename);
                    if (thumbnail)
                        strcpy(event->event_data.recording_stopped.thumbnail, thumbnail);
                    break;
                default:
                    break;
                }

                count++;
            }

            *num_events = count;
        }
    }

    json_object_put(result);

    return status;
}

#if 0
int main(void)
{
    OpenDV_Init("127.0.0.1", 8888);

    CameraMode mode;
    int status = OpenDV_GetCameraMode(&mode);

    printf("status: %d, mode: %d\n", status, mode);

    return 0;
}
#endif

#if 0
    {
        printf("status: %d\n", status);
        const char *result_str = json_object_to_json_string_ext(result, JSON_C_TO_STRING_PRETTY);
        printf("result: %s\n", result_str);
    }
#endif
