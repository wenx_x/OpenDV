#ifndef __OPENDV_CLI_H__
#define __OPENDV_CLI_H__

int OpenDV_Init(char *ipaddr, int port);
int OpenDV_Quit(void);

int OpenDV_StartUpgrade(const char *filename);
int OpenDV_SystemShutdown(void);
int OpenDV_SystemReboot(void);
int OpenDV_SystemFactoryReset(int hard);

int OpenDV_GetStorageStatus(StorageStatus *stat);
int OpenDV_FormatStorage(void);

int OpenDV_GetWifiConfiguration(WifiConfiguration *conf);
int OpenDV_SetWifiConfiguration(WifiConfiguration *conf);

int OpenDV_GetRecordSettings(RecordSettings *settings);
int OpenDV_SetRecordSettings(RecordSettings *settings);

int OpenDV_GetPhotoTakingSettings(PhotoTakingSettings *settings);
int OpenDV_SetPhotoTakingSettings(PhotoTakingSettings *settings);

int OpenDV_GetOsdSettings(OsdSettings *settings);
int OpenDV_SetOsdSettings(OsdSettings *settings);

int OpenDV_GetCameraMode(CameraMode *mode);
int OpenDV_SetCameraMode(CameraMode mode);

int OpenDV_StartRecord(void);
int OpenDV_StopRecord(void);

int OpenDV_StartSnapshot(void);
int OpenDV_StopSnapshot(void);

int OpenDV_GetFilesNum(const char *suffix, int *total);
int OpenDV_DeleteFiles(char *files[128], int nb_files);
int OpenDV_GetFilesList(int index, const char *suffix, FileItem *files, int *nb_files);

int OpenDV_CreateEventSubscriber(int *session_id);
int OpenDV_DeleteEventSubscriber(int session_id);
int OpenDV_GetEvents(int session_id, Event *events, int *num_events);

#endif
