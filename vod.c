#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>

#include "vod.h"
#include "h264.h"
#include "logger.h"
#include "netsvc.h"
#include "config.h"
#include "params.h"
#include "rtspsvc.h"
#include "recmgr.h"
#include "globaldata.h"

static const char *TAG = "VOD";

struct VodFile {
    char filename[128];

    Record *rec;

    RecFrame frame;

    int64_t base_time;
    int64_t base_pts;

    RecInfo recinfo;

    float scale;

    double video_incr_pts;
    double audio_incr_pts;

    int64_t video_prev_pts;
    int64_t audio_prev_pts;
};

int VodFile_GetMediaDesc(char *filename, MediaDesc *descs, int nmemb)
{
    RecInfo info;
    if (RecMgr_GetRecInfo(filename, &info) < 0) {
        LogV(TAG, "get %s detail info failed!", filename);
        return -1;
    }

    memset(descs, 0, sizeof(MediaDesc) * nmemb);

    int count = 0;

    if (info.has_video) {
        VideoInfo *vinfo = &info.video_info;
        if (vinfo->codec == CODEC_TYPE_H264) {
            MediaDesc *vdesc = &descs[count++];
            vdesc->codec = CODEC_TYPE_H264;
            vdesc->sample_rate = 90000;
            vdesc->duration = vinfo->duration;

            vdesc->sps_len = h264_get_sps(vinfo->extradata, vinfo->extrasize, vdesc->sps, sizeof(vdesc->sps));
            vdesc->pps_len = h264_get_pps(vinfo->extradata, vinfo->extrasize, vdesc->pps, sizeof(vdesc->pps));
        } else {
            LogV(TAG, "video codec %d not support yet!", vinfo->codec);
        }
    }

    if (info.has_audio) {
        AudioInfo *ainfo = &info.audio_info;
        if (ainfo->codec == CODEC_TYPE_AAC) {
            MediaDesc *adesc = &descs[count++];
            adesc->codec = CODEC_TYPE_AAC;
            adesc->sample_rate = ainfo->sample_rate;
            adesc->duration = ainfo->duration;
            adesc->audio_channels = ainfo->nb_channels;
        } else {
            LogV(TAG, "audio codec %d not support yet!", ainfo->codec);
        }
    }

    return count;
}

VodFile *VodFile_Open(const char *filename)
{
    LogV(TAG, "start replay %s", filename);

    VodFile *vf = malloc(sizeof(VodFile));
    if (!vf)
        return NULL;
    memset(vf, 0, sizeof(VodFile));

    vf->scale = 1.0;
    vf->base_pts = 0;
    vf->video_incr_pts = vf->base_pts;

    strncpy(vf->filename, filename, sizeof(vf->filename) - 1);

    if (RecMgr_GetRecInfo(vf->filename, &vf->recinfo) < 0) {
        LogV(TAG, "get %s detail info failed!", vf->filename);
        free(vf);
        return NULL;
    }

    vf->rec = RecMgr_Open(vf->filename);
    if (!vf->rec) {
        LogV(TAG, "open record %s failed", vf->filename);
        free(vf);
        return NULL;
    }

//    RecMgr_DumpRecInfo(vf->rec);

    LogV(TAG, "start replay %s success", filename);

    return vf;
}

int VodFile_Close(VodFile *vf)
{
    if (vf->rec)
        RecMgr_Close(vf->rec);

    free(vf);

    return 0;
}

#define PTS_DIFF_TO_MS(pts1, pts2, time_base)     (((pts1) - (pts2)) * 1000 / (time_base))

static int64_t get_time(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (int64_t)tv.tv_sec * 1000 + (int64_t)tv.tv_usec / 1000;
}

int VodFile_ReadFrame(VodFile *vf, VodFrameInfo *info, uint8_t *data, size_t size)
{
    RecFrame *frame = &vf->frame;

    int ret = RecMgr_Read(vf->rec, frame);
    if (ret < 0) {
        LogV(TAG, "read %s failed", vf->filename);
        return ret;
    }

    if (size < frame->size) {
        LogV(TAG, "buffer size %d frame size %d", size, frame->size);
        return -1;
    }

    memset(info, 0, sizeof(VodFrameInfo));
    memcpy(data, frame->data, frame->size);

    info->codec = frame->codec;
    info->media = frame->media;

    if (frame->media == MEDIA_TYPE_VIDEO) {
        VideoInfo *vinfo = &vf->recinfo.video_info;
        info->width = frame->width;
        info->height = frame->height;

        int64_t current_pts = frame->pts;
        int time_base = vinfo->time_base;

        if (vf->base_time == 0) {
            vf->base_time = get_time();
            vf->base_pts = 0;
        }

        int64_t itv_ms = PTS_DIFF_TO_MS(current_pts, vf->video_prev_pts, time_base);

        if (current_pts < vf->video_prev_pts || itv_ms > 250) {
            vf->video_prev_pts = current_pts - vinfo->time_base / vinfo->framerate;
        }

        vf->video_incr_pts += (current_pts - vf->video_prev_pts) / vf->scale;
        vf->video_prev_pts = current_pts;

        info->pts = vf->video_incr_pts * 90000 / time_base;
        info->keyframe = frame->keyframe;

        /* speed control */
        int64_t itv = PTS_DIFF_TO_MS(vf->video_incr_pts, vf->base_pts, time_base);
        int64_t needwait_ms = vf->base_time + itv - get_time();
        if (needwait_ms > 0)
            usleep(needwait_ms * 1000);
    } else if (frame->media == MEDIA_TYPE_AUDIO) {
        AudioInfo *ainfo = &vf->recinfo.audio_info;
        int64_t current_pts = frame->pts;
        int time_base = ainfo->time_base;

        int64_t itv_ms = PTS_DIFF_TO_MS(current_pts, vf->audio_prev_pts, time_base);

        if (current_pts < vf->audio_prev_pts || itv_ms > 500) {
            vf->audio_prev_pts = current_pts;
        }

        vf->audio_incr_pts += (current_pts - vf->audio_prev_pts) / vf->scale;;
        vf->audio_prev_pts = current_pts;

        info->pts = vf->audio_incr_pts;
    }

    return frame->size;
}

int VodFile_SeekByTime(VodFile *vf, int off_ms)
{
    int ret = RecMgr_Seek(vf->rec, off_ms);
    if (ret < 0) {
        LogV(TAG, "seek %s to %dms failed", vf->filename, off_ms);
        return -1;
    }

    return ret;
}

int VodFile_SetScaleFactor(VodFile *vf, float scale)
{
    LogV(TAG, "change replay scale from %.3f to %.3f", vf->scale, scale);

    vf->scale = scale;

    return 0;
}

int VodFile_Resume(VodFile *vf)
{
    if (!vf)
        return -1;

    vf->base_time = get_time();
    vf->base_pts = vf->video_incr_pts;

    return 0;
}
