#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>

#include "vod.h"
#include "logger.h"
#include "netsvc.h"
#include "config.h"
#include "params.h"
#include "rtspsvc.h"
#include "bufchan.h"
#include "recmgr.h"
#include "mediaapi.h"
#include "simstream.h"
#include "globaldata.h"

static const char *TAG = "SimStream";

static pthread_t tid0;
static pthread_t tid1;

#define MAX_FRAME_SIZE  (1024 * 1024)

static int64_t get_time(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (int64_t)tv.tv_sec * 1000 + (int64_t)tv.tv_usec / 1000;
}

typedef struct {
    char stream_name[64];
    char filename[128];
} SimStream;

static void *stream_task(void *arg)
{
    SimStream *st = (SimStream *)arg;

    uint8_t *buf = malloc(MAX_FRAME_SIZE);

    long w_handle = BufChan_Open(st->stream_name, AV_WRONLY);
    if (!w_handle)
        return NULL;

    int64_t video_incr_pts = 0;
    int64_t video_prev_pts = 0;

    int64_t audio_incr_pts = 0;
    int64_t audio_prev_pts = 0;

    while (1) {
        VodFile *vf = VodFile_Open(st->filename);
        if (!vf) {
            LogV(TAG, "VodFile_Open %s failed", st->filename);
            return NULL;
        }

        while (1) {
            VodFrameInfo vinfo;
            int size = VodFile_ReadFrame(vf, &vinfo, buf, MAX_FRAME_SIZE);
            if (size < 0)
                break;

            StreamInfo sinfo;
            memset(&sinfo, 0, sizeof(StreamInfo));

            if (vinfo.codec == CODEC_TYPE_H264) {
                sinfo.media = MEDIA_TYPE_VIDEO;
                sinfo.codec = CODEC_TYPE_H264;

                if (vinfo.keyframe)
                    sinfo.frame_type = FRAME_TYPE_I;
                else
                    sinfo.frame_type = FRAME_TYPE_P;

                if (abs(vinfo.pts - video_prev_pts) > 90000) {
                    video_incr_pts += 3000;
                } else {
                    video_incr_pts += vinfo.pts - video_prev_pts;
                }

                sinfo.pts = video_incr_pts;
                video_prev_pts = vinfo.pts;
            } else if (vinfo.codec == CODEC_TYPE_AAC) {
                sinfo.media = MEDIA_TYPE_AUDIO;
                sinfo.codec = CODEC_TYPE_AAC;

                if (abs(vinfo.pts - audio_prev_pts) > 44100) {
                    audio_incr_pts += 1024;
                } else {
                    audio_incr_pts += vinfo.pts - audio_prev_pts;
                }

                sinfo.pts = audio_incr_pts;
                audio_prev_pts = vinfo.pts;
            }
            sinfo.time_ms = get_time();

            struct iovec iov[2];
            iov[0].iov_base = &sinfo;
            iov[0].iov_len = sizeof(StreamInfo);
            iov[1].iov_base = buf;
            iov[1].iov_len = size;

            BufChan_WriteV(w_handle, iov, 2);
        }

        VodFile_Close(vf);
    }

    BufChan_Close(w_handle);
    free(buf);

    return NULL;
}

static SimStream stream0 = {
    .stream_name = LIVESTREAM_LARGE,
    .filename = "M0005.mp4",
};

static SimStream stream1 = {
    .stream_name = LIVESTREAM_SMALL,
    .filename = "M0005_THM.mp4",
};

int SimStream_Start(void)
{
    pthread_create(&tid0, NULL, stream_task, &stream0);
    pthread_create(&tid1, NULL, stream_task, &stream1);

    return 0;
}

int SimStream_Stop(void)
{
    pthread_join(tid0, NULL);
    pthread_join(tid1, NULL);
    return 0;
}
