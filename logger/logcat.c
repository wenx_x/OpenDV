#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "netio.h"

int main(void)
{
    int sockfd = j_net_tcp_connect("127.0.0.1", 8848);
    if (sockfd < 0)
        return -1;

    char buf[32 * 1024] = {0};

    while (1) {
        int ret = j_net_wait_readable(sockfd, 1000);
        if (ret < 0)
            break;
        else if (ret == 0)
            continue;

        uint32_t size = 0;
        if (j_net_readn_timedwait(sockfd, &size, 4) != 4)
            break;

        if (sizeof(buf) + 1 < size)
            break;

        if (j_net_readn_timedwait(sockfd, buf, size) != size)
            break;

        buf[size] = '\0';

        printf("%s\n", buf);
    }

    close(sockfd);
    return 0;
}
