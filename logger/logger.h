#ifndef __LOGGER_H__
#define __LOGGER_H__

int Logger_Init(int tcpport, int bufsize);
int Logger_Quit(void);

#define LOG_LEVEL_VERBOSE   (0)
#define LOG_LEVEL_ERROR     (1)
#define LOG_LEVEL_INFO      (2)
#define LOG_LEVEL_DEBUG     (3)

int Logger_Write(int level, const char *tag, int line_num, const char *fmt, ...);

#define LogV(TAG, format, ...)  Logger_Write(LOG_LEVEL_VERBOSE, TAG, __LINE__, format, ##__VA_ARGS__)
#define LogE(TAG, format, ...)  Logger_Write(LOG_LEVEL_ERROR, TAG, __LINE__, format, ##__VA_ARGS__)

typedef struct LogReader LogReader;

LogReader *Logger_OpenForRead(int read_from_latest);
int Logger_CloseReader(LogReader *r);
ssize_t Logger_Read(LogReader *r, char *buf, size_t len, int milliseconds);

#endif
