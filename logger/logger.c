#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

#include "netio.h"
#include "logger.h"
#include "bufchan.h"

typedef struct {
    char logname[64];
    long w_handle;

    int server_fd;
    pthread_t server_tid;

    int abort;
} Logger;

static Logger *gbl_log = NULL;

typedef struct {
    int connfd;
    pthread_t tid;

    Logger *logger;
} TcpConn;

#define MAX_LOG_SIZE    (4 * 1024)

static void *handle_connection(void *arg)
{
    pthread_detach(pthread_self());

    TcpConn *conn = (TcpConn *)arg;

    long r_handle = BufChan_Open(conn->logger->logname, AV_RDONLY | AV_OLDEST);
    if (!r_handle)
        return NULL;

    char *buf = malloc(MAX_LOG_SIZE);
    if (!buf) {
        BufChan_Close(r_handle);
        return NULL;
    }

    while (!conn->logger->abort) {
        /* check if peer connection was closed */
        if (j_net_wait_readable(conn->connfd, 0) > 0)
            break;

        int len = BufChan_Read(r_handle, buf, MAX_LOG_SIZE, 1000);
        if (len <= 0)
            continue;

        uint32_t size = len;

        if (j_net_writen_timedwait(conn->connfd, &size, 4) <= 0)
            break;

        if (j_net_writen_timedwait(conn->connfd, buf, len) <= 0)
            break;
    }

    BufChan_Close(r_handle);
    free(buf);

    return NULL;
}

static void *log_service(void *arg)
{
    Logger *log = (Logger *)arg;

    while (!log->abort) {
        int connfd = j_net_tcp_accept(log->server_fd);
        if (connfd < 0)
            return NULL;

        TcpConn *conn = malloc(sizeof(TcpConn));
        if (!conn) {
            close(connfd);
            continue;
        }
        memset(conn, 0, sizeof(TcpConn));

        conn->logger = log;
        conn->connfd = connfd;

        pthread_create(&conn->tid, NULL, handle_connection, conn);
    }

    return NULL;
}

int Logger_Init(int tcpport, int bufsize)
{
    if (gbl_log)
        return 0;

    Logger *log = malloc(sizeof(Logger));
    if (!log)
        return -1;
    memset(log, 0, sizeof(Logger));

    snprintf(log->logname, sizeof(log->logname), "log%p", log);

    if (BufChan_MkFifo(log->logname, bufsize) < 0) {
        free(log);
        return -1;
    }

    log->w_handle = BufChan_Open(log->logname, AV_WRONLY);
    if (!log->w_handle) {
        BufChan_UnLink(log->logname);
        free(log);
        return -1;
    }

    log->server_fd = j_net_tcp_server(NULL, tcpport);
    if (log->server_fd < 0) {
        BufChan_Close(gbl_log->w_handle);
        BufChan_UnLink(log->logname);
        free(log);
        return -1;
    }

    pthread_create(&log->server_tid, NULL, log_service, log);

    gbl_log = log;

    return 0;
}

int Logger_Quit(void)
{
    gbl_log->abort = 1;
    pthread_join(gbl_log->server_tid, NULL);
    BufChan_Close(gbl_log->w_handle);
    BufChan_UnLink(gbl_log->logname);
    free(gbl_log);
    gbl_log = NULL;
    return 0;
}

static char *level_str(int level)
{
    switch (level) {
    case LOG_LEVEL_VERBOSE:
        return "VERBOSE";
    case LOG_LEVEL_ERROR:
        return "ERROR";
    case LOG_LEVEL_INFO:
        return "INFO";
    case LOG_LEVEL_DEBUG:
        return "DEBUG";
    default:
        return NULL;
    }
}

int Logger_Write(int level, const char *tag, int line_num, const char *fmt, ...)
{
    if (!gbl_log)
        return -1;

    char  buf[1024] = {0};
    char *ptr = buf;
    char *end = buf + sizeof(buf);

    time_t t = time(NULL);

    struct tm tm;
    localtime_r(&t, &tm);

    snprintf(buf, sizeof(buf), "[%04d-%02d-%02d %02d:%02d:%02d %s] %s(%d): ",
            tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
            tm.tm_hour, tm.tm_min, tm.tm_sec, level_str(level), tag, line_num);
    ptr += strlen(buf);

    va_list ap;
    va_start(ap, fmt);
    (void) vsnprintf(ptr, end - ptr, fmt, ap);
    va_end(ap);

    printf("%s\n", buf);

    return BufChan_Write(gbl_log->w_handle, buf, strlen(buf) + 1);
}

struct LogReader {
    long r_handle;
};

LogReader *Logger_OpenForRead(int read_from_latest)
{
    if (!gbl_log)
        return NULL;

    LogReader *r = malloc(sizeof(LogReader));
    if (!r)
        return NULL;
    memset(r, 0, sizeof(LogReader));

    int flags = read_from_latest ? AV_RDONLY : (AV_RDONLY | AV_OLDEST);

    r->r_handle = BufChan_Open(gbl_log->logname, flags);
    if (!r->r_handle) {
        free(r);
        return NULL;
    }

    return r;
}

int Logger_CloseReader(LogReader *r)
{
    BufChan_Close(r->r_handle);
    free(r);
    return 0;
}

ssize_t Logger_Read(LogReader *r, char *buf, size_t len, int milliseconds)
{
    return BufChan_Read(r->r_handle, buf, len, milliseconds);
}

#if 0
int main(void)
{
    Logger_Init(1024 * 1024);

    if (LogV("recorder", "record %s created", "REC_20170802T090028.MP4") < 0) {
        printf("write log failed\n");
        return -1;
    }

    LogReader *reader = Logger_OpenForRead(0);
    if (!reader)
        return -1;

    char buf[4096];
    int len = Logger_Read(reader, buf, sizeof(buf), 1000);
    if (len < 0)
        return -1;
    else if (len == 0) {
        printf("timeout\n");
        return 0;
    }

    printf("buf: %s\n", buf);

    while (1) {
        sleep(100);
    }

    Logger_CloseReader(reader);

    return 0;
}
#endif
