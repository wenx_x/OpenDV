#ifndef __DOWNLOADER_H__
#define __DOWNLOADER_H__

#include "httpsvc.h"

int DownLoader_Init(HttpSvc *httpsvc, char *path);

#endif /* __DOWNLOADER_H__ */
