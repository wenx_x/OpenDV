#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "utils.h"

#define min(x, y)	((x) < (y) ? (x) : (y))

size_t j_strlcpy(char *dst, const char *src, size_t size)
{
    size_t len = 0;
    while (++len < size && *src)
        *dst++ = *src++;
    if (len <= size)
        *dst = 0;
    return len + strlen(src) - 1;
}

void url_split(char *proto, int proto_size,
                  char *authorization, int authorization_size,
                  char *hostname, int hostname_size,
                  int *port_ptr,
                  char *path, int path_size,
                  const char *url)
{
    const char *p, *ls, *ls2, *at, *at2, *col, *brk;

    if (port_ptr)               *port_ptr = -1;
    if (proto_size > 0)         proto[0] = 0;
    if (authorization_size > 0) authorization[0] = 0;
    if (hostname_size > 0)      hostname[0] = 0;
    if (path_size > 0)          path[0] = 0;

    /* parse protocol */
    if ((p = strchr(url, ':'))) {
        j_strlcpy(proto, url, min(proto_size, p + 1 - url));
        p++; /* skip ':' */
        if (*p == '/') p++;
        if (*p == '/') p++;
    } else {
        /* no protocol means plain filename */
        j_strlcpy(path, url, path_size);
        return;
    }

	if (strncmp(proto, "scsi", 4) == 0) {
		j_strlcpy(hostname, p, hostname_size);
		return;
	}

    /* separate path from hostname */
    ls = strchr(p, '/');
    ls2 = strchr(p, '?');
    if(!ls)
        ls = ls2;
    else if (ls && ls2)
        ls = min(ls, ls2);
    if(ls)
        j_strlcpy(path, ls, path_size);
    else
        ls = &p[strlen(p)]; // XXX

    /* the rest is hostname, use that to parse auth/port */
    if (ls != p) {
        /* authorization (user[:pass]@hostname) */
        at2 = p;
        while ((at = strchr(p, '@')) && at < ls) {
            j_strlcpy(authorization, at2,
                       min(authorization_size, at + 1 - at2));
            p = at + 1; /* skip '@' */
        }

        if (*p == '[' && (brk = strchr(p, ']')) && brk < ls) {
            /* [host]:port */
            j_strlcpy(hostname, p + 1,
                       min(hostname_size, brk - p));
            if (brk[1] == ':' && port_ptr)
                *port_ptr = atoi(brk + 2);
        } else if ((col = strchr(p, ':')) && col < ls) {
            j_strlcpy(hostname, p,
                       min(col + 1 - p, hostname_size));
            if (port_ptr) *port_ptr = atoi(col + 1);
        } else
            j_strlcpy(hostname, p,
                       min(ls + 1 - p, hostname_size));
    }
}

static void skip_spaces(const char **pp)
{
    const char *p;
    p = *pp;
    while (*p == ' ' || *p == '\t')
        p++;
    *pp = p;
}

static void get_word(char *buf, int buf_size, const char **pp)
{
    const char *p;
    char *q;

    p = *pp;
    skip_spaces(&p);
    q = buf;
    while (!isspace(*p) && *p != '\0') {
        if ((q - buf) < buf_size - 1)
            *q++ = *p;
        p++;
    }
    if (buf_size > 0)
        *q = '\0';
    *pp = p;
}

int get_mount_point(const char *devname, char *mount_point, int len)
{
	FILE *fp = fopen("/proc/mounts", "r");
	if (!fp)
		return -1;

	char line[1024];
	while (fgets(line, 1024, fp) != NULL) {
		const char *p = line;

		char devname1[64];
		get_word(devname1, sizeof(devname1), &p);

		if (strcmp(devname1, devname) == 0) {
			get_word(mount_point, len, &p);
			return 0;
		}
	}

	return -1;
}
