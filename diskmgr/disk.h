#ifndef __DISK_H__
#define __DISK_H__

#include <stdint.h>

#include "diskmgr.h"

typedef struct {
	char type[32];

	int (*module_init)(void);
	int (*module_deinit)(void);

	int (*attach)(const char *url, int (*func)(const char *url, int action, void *ctx), void *ctx);
	int (*detach)(const char *url);

	int (*open)(DiskHndl *disk);
	int (*close)(DiskHndl *disk);

	int (*mkparts)(DiskHndl *disk, int label_type, int fs_type, int nparts);
	int (*mkfs)(PartHndl *disk, int fs_type);

	int (*mount)(PartHndl *part, const char *mount_point);
	int (*umount)(PartHndl *part);
} disk_core_t;

struct PartHndl {
	int fs_type;
	char devname[64];	/* /dev/xxx */
	int64_t totalsize;

	DiskHndl *disk;

	int mounted;
	char mount_point[256];
};

struct DiskHndl {
	char url[1024];     /** add by liudm */
	int online;
	int type;			/* DISK_YTPE_xxx */
	char devname[64];	/* /dev/xxx */
	char name[64];

	int64_t sector_size ;
	int64_t totalsize;

	pthread_mutex_t lock;
	JList *parts;

	disk_core_t *core;
};

#endif
