#ifndef __DISKMGR_H__
#define __DISKMGR_H__

#include <stdint.h>

#define DISK_OFFLINE		(0x00)
#define DISK_ONLINE			(0x01)

#define LABEL_TYPE_UNKNOWN	(0x00)
#define LABEL_TYPE_MSDOS	(0x01)
#define LABEL_TYPE_GPT		(0X02)

#define FS_TYPE_UNKNOWN		(0x00)
#define FS_TYPE_EXT2		(0x01)
#define FS_TYPE_EXT3		(0x02)
#define FS_TYPE_EXT4		(0x03)
#define FS_TYPE_FAT32		(0x04)

#define POWER_STATE_ACTIVE	(0)
#define POWER_STATE_SLEEP	(1)
#define POWER_STATE_STANDBY	(2)

#define MAX_PART_NUM	(16)

typedef struct DiskHndl DiskHndl;
typedef struct PartHndl PartHndl;

typedef struct {
	char devname[32];
	int fs_type;

	int mounted;
	char mount_point[256];

	int64_t totalsize;	/* in Byte */
} PartInfo;

typedef struct {
	int online;				/* 0 - offline, 1 - online */
	char devname[64];		/* device full path name   */

	int numparts;			/* number of partitions    */
	PartInfo parts[MAX_PART_NUM];

    int64_t totalsize;
} DiskInfo;

int DiskMgr_Init(void);
int DiskMgr_Quit(void);

/**
 * url:
 *     scsi://1:0:0:1
 *     mmc://mmcblk0
 */
int DiskMgr_AddWatcher(const char *url, int (*func)(const char *url, int action, void *ctx), void *ctx);
int DiskMgr_DelWatcher(const char *url);

DiskHndl *DiskMgr_OpenDisk(const char *url);
int DiskMgr_CloseDisk(DiskHndl *d);

int DiskMgr_MakeParts(DiskHndl *d, int label_type, int fs_type, int nparts);
int DiskMgr_GetDiskInfo(DiskHndl *d, DiskInfo *info);
int DiskMgr_GetPartNum(DiskHndl *d);
PartHndl *DiskMgr_GetPartById(DiskHndl *d, int id);
int DiskMgr_MkFs(PartHndl *part, int fs_type);
int DiskMgr_Mount(PartHndl *part, const char *mount_point);
int DiskMgr_Umount(PartHndl *part);
int DiskMgr_GetPartInfo(PartHndl *part, PartInfo *info);

#endif	/* __DISKMGR_H__ */
