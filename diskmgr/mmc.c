#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <pthread.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <sys/syscall.h>   /* For SYS_xxx definitions */
#include <sys/vfs.h>

#include <linux/types.h>
#include <linux/netlink.h>

#include <parted/debug.h>
#include <parted/parted.h>

#include "list.h"
#include "utils.h"
#include "diskmgr.h"
#include "disk.h"

typedef struct DiskWatcher {
	int online;
	char url[256];
	char type[32];
	char name[64];
	char devname[64];
	int (*func)(const char *url, int action, void *ctx);
	void *ctx;
} DiskWatcher;

static pthread_mutex_t glock;
static JList *watchers  = NULL;

static char *get_fs_type(int type)
{
	char *fs;

	switch (type) {
	case FS_TYPE_EXT2:
		fs = "ext2";
		break;
	case FS_TYPE_EXT3:
		fs = "ext3";
		break;
	case FS_TYPE_EXT4:
		fs = "ext4";
		break;
	case FS_TYPE_FAT32:
		fs = "fat32";
		break;
	default:
		printf("unknonw fs_type!\n");
		return NULL;
	}

	return fs;
}

static int get_fs_type_by_name(const char *name)
{
	if (strncmp(name, "ext2", 4) == 0)
		return FS_TYPE_EXT2;
	else if (strncmp(name, "ext3", 4) == 0)
		return FS_TYPE_EXT3;
	else if (strncmp(name, "ext4", 4) == 0)
		return FS_TYPE_EXT4;
	else if (strncmp(name, "fat32", 5) == 0)
		return FS_TYPE_FAT32;

	return -1;
}

static int check_online_once(DiskWatcher *w)
{
	char path[64];
	snprintf(path, sizeof(path), "/dev/%s", w->name);

	if (access(path, F_OK) != 0)
		return 0;

	if (w->func)
		w->func(w->url, DISK_ONLINE, w->ctx);

	w->online = 1;

	return 1;
}

static void check_online(DiskWatcher *w)
{
	if (w->online)
		return;

	int i;
	for (i = 0; i < 5; i++) {
		if (check_online_once(w) == 1)
			return;
		sleep(1);
	}
}

static void check_offline(DiskWatcher *w)
{
	int i;
	for (i = 0; i < 5; i++) {
		char path[1024];
		snprintf(path, sizeof(path), "/dev/%s", w->name);
		if (access(path, F_OK) != 0) {
			w->devname[0] = '\0';

			if (w->func)
				w->func(w->url, DISK_OFFLINE, w->ctx);

			w->online = 0;

			return;
		}

		sleep(1);
	}
}

static int get_scsi_id(const char *line, char *scsi_id, int len)
{
	char *ptr = strstr(line, "mmcblk");
	if (!ptr)
		return -1;

    if (strchr(ptr, '/') != NULL)
        return -1;

	strncpy(scsi_id, ptr, len - 1);

	return 0;
}

static int abort_service_request = 0;
static pthread_t hotplug_service_tid = 0;

static int open_hotplug_sock(void)
{
	struct sockaddr_nl snl;
	const int buffersize = 1 * 1024 * 1024;

	memset(&snl, 0x00, sizeof(struct sockaddr_nl));
	snl.nl_family = AF_NETLINK;
	snl.nl_pid = getpid();
	snl.nl_groups = 1;

	int sockfd = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
	if (sockfd < 0) {
		printf("error getting socket: %s\n", strerror(errno));
		return -1;
	}

	/* set receive buffersize */
	setsockopt(sockfd, SOL_SOCKET, SO_RCVBUFFORCE, &buffersize, sizeof(buffersize));

	if (bind(sockfd, (struct sockaddr *)&snl, sizeof(struct sockaddr_nl)) < 0) {
		printf("bind failed: %s\n", strerror(errno));
		close(sockfd);
		return -1;
	}

	return sockfd;
}

static DiskWatcher *get_watcher(const char *mmcblk)
{
	JListNode *n, *n_next;

	for (n = j_list_first(watchers); n; n = n_next) {
		n_next = j_list_next(watchers, n);

		DiskWatcher *w = n->data;
		if (!strcmp(w->name, mmcblk))
			return w;
	}

	return NULL;
}

#define UEVENT_BUFFER_SIZE      2048

static int wait_readable(int sockfd, int milliseconds)
{
	struct pollfd fds;

wait_again:
	memset(&fds, 0, sizeof(struct pollfd));

	fds.fd     = sockfd;
	fds.events = POLLIN;

	int ret = poll(&fds, 1, milliseconds);
	if (ret < 0 && (errno == EINTR))
		goto wait_again;	/* interrupted by signal, try again */

	return ret;
}

static void *hotplug_service(void *arg)
{
	int ret, nbyte;
	char line[UEVENT_BUFFER_SIZE * 2];

	int hotplug_sock = open_hotplug_sock();
	if (hotplug_sock < 0)
		return NULL;

	while (!abort_service_request) {
		ret = wait_readable(hotplug_sock, 1000);
		if (ret < 0) {
			break;
		} else if (ret == 0) {
			/* timeout */
			continue;
		}

		nbyte = recv(hotplug_sock, &line, sizeof(line), 0);
		if (nbyte <= 0) {
			if (nbyte < 0 && errno == EINTR)
				continue;
			break;
		}

		/*
		 * example:
         *     add@/devices/platform/ocp/48060000.mmc/mmc_host/mmc0/mmc0:0007/block/mmcblk0
		 */
		if (!strstr(line, "mmcblk"))
			continue;

		char mmcblk[32];
		if (get_scsi_id(line, mmcblk, 32) < 0)
			continue;

		printf("uevent: mmcblk %s\n", mmcblk);

		pthread_mutex_lock(&glock);
		DiskWatcher *w = get_watcher(mmcblk);
		if (w) {
			if (!strncmp(line, "add", 3))
				check_online(w);
			else if (!strncmp(line, "remove", 6))
				check_offline(w);
		}
		pthread_mutex_unlock(&glock);
	}

	close(hotplug_sock);

	return NULL;
}

static int start_hotplug_service(void)
{
	if (pthread_create(&hotplug_service_tid, NULL, hotplug_service, NULL) != 0)
		return -1;
	return 0;
}

static int stop_hotplug_service(void)
{
	abort_service_request = 1;
	if (hotplug_service_tid)
		pthread_join(hotplug_service_tid, NULL);
	abort_service_request = 0;

	return 0;
}

static int mmc_init(void)
{
	watchers = j_list_alloc();
	if (!watchers)
		return -1;

	pthread_mutex_init(&glock, NULL);

	start_hotplug_service();

	return 0;
}

static int mmc_deinit(void)
{
	if (watchers) {
		j_list_free(watchers);
		watchers = NULL;
	}

	stop_hotplug_service();
	pthread_mutex_destroy(&glock);

	return 0;
}

static int mmc_attach(const char *url, int (*func)(const char *url, int action, void *ctx), void *ctx)
{
	if (!url || !url[0] || !func)
		return -1;

	DiskWatcher *w = malloc(sizeof(DiskWatcher));
	if (!w)
		return -1;
	memset(w, 0, sizeof(DiskWatcher));

	url_split(w->type, sizeof(w->type), NULL, 0, w->name,
			sizeof(w->name), NULL, NULL, 0, url);

	strncpy(w->url, url, sizeof(w->url) - 1);
	w->func = func;
	w->ctx = ctx;

	check_online_once(w);

	pthread_mutex_lock(&glock);
	j_list_append(watchers, w);
	pthread_mutex_unlock(&glock);

	return 0;
}

static int mmc_detach(const char *url)
{
	if (!url || !url[0])
		return -1;

	JListNode *n, *n_next;

	pthread_mutex_lock(&glock);
	for (n = j_list_first(watchers); n; n = n_next) {
		n_next = j_list_next(watchers, n);
		DiskWatcher *w = n->data;

		if (strcmp(w->url, url) == 0) {
			j_list_remove(watchers, w);
			free(w);
			pthread_mutex_unlock(&glock);
			return 0;
		}
	}
	pthread_mutex_unlock(&glock);

	return -1;
}

static int free_parts(DiskHndl *d)
{
	JListNode *n, *n_next;

	pthread_mutex_lock(&d->lock);
	for (n = j_list_first(d->parts); n; n = n_next) {
		n_next = j_list_next(d->parts, n);
		PartHndl *p = n->data;
		j_list_remove(d->parts, p);
		free(p);
	}
	pthread_mutex_unlock(&d->lock);

	return 0;
}

static pthread_mutex_t pedlock = PTHREAD_MUTEX_INITIALIZER;

static int _scan_parts(DiskHndl *d)
{
	PedDevice *device = ped_device_get(d->devname);
	if (!device)
		return -1;

	d->sector_size = device->sector_size;
	d->totalsize   = device->length * device->sector_size;

	PedDisk *disk = ped_disk_new(device);
	if (!disk) {
		ped_device_destroy(device);
		return -1;
	}

	PedPartition *part = NULL;
	while ((part = ped_disk_next_partition(disk, part))) {
		if ((part->type & PED_PARTITION_METADATA)  ||
			(part->type & PED_PARTITION_FREESPACE) ||
			(part->type & PED_PARTITION_EXTENDED))
			continue;

		PartHndl *p = malloc(sizeof(PartHndl));
		if (!p)
			continue;
		memset(p, 0, sizeof(PartHndl));

		p->disk = d;

		j_strlcpy(p->devname, ped_partition_get_path(part), sizeof(p->devname));
		p->totalsize = part->geom.length * device->sector_size;
		if (part->fs_type) {
			printf("%s fs_type: %s\n", p->devname, part->fs_type->name);
			p->fs_type = get_fs_type_by_name(part->fs_type->name);
		}

		pthread_mutex_lock(&d->lock);
		j_list_append(d->parts, p);
		pthread_mutex_unlock(&d->lock);
	}

	ped_disk_destroy(disk);
	ped_device_destroy(device);

	return 0;
}

static int scan_parts(DiskHndl *d)
{
	pthread_mutex_lock(&pedlock);
	int ret = _scan_parts(d);
	pthread_mutex_unlock(&pedlock);

	return ret;
}

static int mmc_open(DiskHndl *d)
{
	snprintf(d->devname, sizeof(d->devname), "/dev/%s", d->name);

	if (access(d->devname, F_OK) != 0)
		return -1;

	scan_parts(d);

	d->online = 1;

	pthread_mutex_init(&d->lock, NULL);

	return 0;
}

static int mmc_close(DiskHndl *d)
{
	free_parts(d);
	pthread_mutex_destroy(&d->lock);

	return 0;
}

static int _erase_all(DiskHndl *d)
{
	PedDevice *device = NULL;
	PedDisk *disk = NULL;

	device = ped_device_get(d->devname);
	if (!device)
		return -1;

	disk = ped_disk_new(device);
	if (!disk)
		goto fail;

	if (ped_disk_delete_all(disk) == 0)
		goto fail;

	ped_disk_commit(disk);

	ped_disk_destroy(disk);
	ped_device_destroy(device);

	return 0;

fail:
	if (disk)
		ped_disk_destroy(disk);
	if (device)
		ped_device_destroy(device);

	return -1;
}

static int erase_all(DiskHndl *d)
{
	pthread_mutex_lock(&pedlock);
	int ret = _erase_all(d);
	pthread_mutex_unlock(&pedlock);

	return ret;
}

static char *get_label(int label_type)
{
	switch (label_type) {
	case LABEL_TYPE_MSDOS:
		return "msdos";
	case LABEL_TYPE_GPT:
		return "gpt";
	}

	return NULL;
}

static int make_parts(DiskHndl *d, int label_type, int fs_type, int nparts)
{
	char *fs = get_fs_type(fs_type);

	int offset = 100 / nparts;

	int i;
	char cmd[1024];

	char *label = get_label(label_type);
	if (!label)
		return -1;

	snprintf(cmd, sizeof(cmd), "parted -s %s mklabel %s",
			d->devname, label);

	if (system(cmd) == -1)
		return -1;

	for (i = 0; i < nparts; i++) {
		int start = (i + 0) * offset;
		int end   = (i + 1) * offset;
		if (i == nparts - 1)
			end = 100;

		snprintf(cmd, sizeof(cmd), "parted %s mkpart primary %s %d%% %d%%",
				d->devname, fs, start, end);

		if (system(cmd) == -1)
			return -1;
	}

	return 0;
}

static int mmc_mkparts(DiskHndl *d, int label_type, int fs_type, int nparts)
{
	if (!d || !d->online)
		return -1;

#if 0
	if (erase_all(d) < 0)
		return -1;
#else
	erase_all(d);
#endif

	if (make_parts(d, label_type, fs_type, nparts) < 0)
		return -1;

	if (free_parts(d) < 0)
		return -1;

	if (scan_parts(d) < 0)
		return -1;

	return 0;
}

static int mmc_mkfs(PartHndl *part, int fs_type)
{
	if (!part || !part->disk)
		return -1;

	char cmd[1024];

	switch (fs_type) {
	case FS_TYPE_EXT2:
		snprintf(cmd, sizeof(cmd), "mkfs.ext2 -m 1 %s", part->devname);
		break;
	case FS_TYPE_EXT3:
		snprintf(cmd, sizeof(cmd), "mkfs.ext3 -m 1 %s", part->devname);
		break;
	case FS_TYPE_EXT4:
		snprintf(cmd, sizeof(cmd), "mkfs.ext4 -m 1 %s", part->devname);
		break;
	case FS_TYPE_FAT32:
		snprintf(cmd, sizeof(cmd), "mkfs.vfat %s", part->devname);
		break;
	default:
		return -1;
	}

	if (system(cmd) < 0)
		return -1;

#if 0
	snprintf(cmd, sizeof(cmd), "tune2fs -c 0 -i 0 %s", part->devname);
	system(cmd);
#endif

	part->fs_type = fs_type;

	return 0;
}

static int mmc_mount(PartHndl *part, const char *mount_point)
{
	if (!part || !part->disk)
		return -1;

	/* check if mounted! */
	char mp[1024];
	if (get_mount_point(part->devname, mp, sizeof(mp)) == 0) {
        if (strcmp(mp, mount_point) == 0) {
            printf("%s allready mounted\n", part->devname);
            return 0;	/* allready mounted */
        }
	}

#if 0
	char *fs = get_fs_type(part->fs_type);
	if (!fs)
		return -1;

	int ret = mount(part->devname, mount_point, fs, 0, NULL);
	if (ret < 0) {
		fprintf(stderr, "mount %s failed: %s\n", part->devname, strerror(errno));
		return -1;
	}
#else
    char cmd[1024];
    snprintf(cmd, sizeof(cmd), "mount %s %s", part->devname, mount_point);

	if (system(cmd) < 0) {
		fprintf(stderr, "mount %s failed: %s\n", part->devname, strerror(errno));
		return -1;
	}
#endif

	return 0;
}

static int mmc_umount(PartHndl *part)
{
	if (!part || !part->disk)
		return -1;

	return umount(part->mount_point);
}

disk_core_t mmc_core = {
	.type    = "mmc",

	.module_init   = mmc_init,
	.module_deinit = mmc_deinit,

	.attach  = mmc_attach,
	.detach  = mmc_detach,

	.open    = mmc_open,
	.close   = mmc_close,

	.mkparts = mmc_mkparts,
	.mkfs    = mmc_mkfs,

	.mount   = mmc_mount,
	.umount  = mmc_umount,
};
