#ifndef __DISK_UTILS_H__
#define __DISK_UTILS_H__

size_t j_strlcpy(char *dst, const char *src, size_t size);

void url_split(char *proto, int proto_size,
                  char *authorization, int authorization_size,
                  char *hostname, int hostname_size,
                  int  *port_ptr,
                  char *path, int path_size,
                  const char *url);

int get_mount_point(const char *devname, char *mount_point, int len);

#endif /* __DISK_UTILS_H__ */
