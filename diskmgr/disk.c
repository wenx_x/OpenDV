#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <pthread.h>

#include "list.h"
#include "utils.h"
#include "disk.h"
#include "diskmgr.h"

#include "mmc.h"
#include "harddisk.h"

static disk_core_t *disk_core[] = {
	&mmc_core,
//	&harddisk_core,
};

static disk_core_t *get_core(const char *type)
{
	int size = sizeof(disk_core) / sizeof(disk_core_t *);

	int i;
	disk_core_t *core = NULL;
	for (i = 0; i < size; i++) {
		core = disk_core[i];
		if (!strcmp(type, core->type))
			return core;
	}

	return NULL;
}

int DiskMgr_Init(void)
{
	int size = sizeof(disk_core) / sizeof(disk_core_t *);

	int i;
	for (i = 0; i < size; i++) {
		disk_core_t *core = disk_core[i];

		if (core->module_init)
			core->module_init();
	}

	return 0;
}

int DiskMgr_Quit(void)
{
	int size = sizeof(disk_core) / sizeof(disk_core_t *);

	int i;
	for (i = 0; i < size; i++) {
		disk_core_t *core = disk_core[i];

		if (core->module_deinit)
			core->module_deinit();
	}

	return 0;
}

int DiskMgr_AddWatcher(const char *url, int (*func)(const char *url, int action, void *ctx), void *ctx)
{
	char type[32];
	url_split(type, sizeof(type), NULL, 0, NULL,
			0, NULL, NULL, 0, url);

	disk_core_t *core = get_core(type);
	if (!core)
		return -1;

	if (core->attach)
		core->attach(url, func, ctx);

	return 0;
}

int DiskMgr_DelWatcher(const char *url)
{
	char type[32];
	url_split(type, sizeof(type), NULL, 0, NULL,
			0, NULL, NULL, 0, url);

	disk_core_t *core = get_core(type);
	if (core == NULL)
		return -1;

	if (core->detach)
		core->detach(url);

	return 0;
}

DiskHndl *DiskMgr_OpenDisk(const char *url)
{
	DiskHndl *d = malloc(sizeof(DiskHndl));
	if (!d)
		return NULL;
	memset(d, 0, sizeof(DiskHndl));

	d->parts = j_list_alloc();
	if (!d->parts) {
		free(d);
		return NULL;
	}

	snprintf(d->url, sizeof(d->url), "%s", url);

	char type[32];
	url_split(type, sizeof(type), NULL, 0, d->name,
			sizeof(d->name), NULL, NULL, 0, url);

	d->core = get_core(type);
	if (d->core == NULL) {
		j_list_free(d->parts);
		free(d);
		return NULL;
	}

	if (d->core->open) {
		if (d->core->open(d) < 0) {
			j_list_free(d->parts);
			free(d);
			return NULL;
		}
	}

	return d;
}

int DiskMgr_CloseDisk(DiskHndl *d)
{
	if (!d)
		return -1;

	if (d->core->close)
		d->core->close(d);

	j_list_free(d->parts);
	free(d);

	return 0;
}

int DiskMgr_MakeParts(DiskHndl *d, int label_type, int fs_type, int nparts)
{
	if (!d || !d->online)
		return -1;

	disk_core_t *core = d->core;
	if (!core)
		return -1;

	if (!core->mkparts)
		return -1;

	return core->mkparts(d, label_type, fs_type, nparts);
}

int DiskMgr_GetDiskInfo(DiskHndl *d, DiskInfo *info)
{
	if (!d || !info)
		return -1;

	info->online    = d->online;
	info->numparts  = j_list_length(d->parts);
	info->totalsize = d->totalsize;

	j_strlcpy(info->devname, d->devname, sizeof(info->devname));

	int i = 0;
	JListNode *n;
	for (n = j_list_first(d->parts); n; n = j_list_next(d->parts, n)) {
		PartHndl *p = n->data;
		if (DiskMgr_GetPartInfo(p, &info->parts[i++]) < 0)
			return -1;

		if (i == MAX_PART_NUM)
			break;
	}

	info->numparts = i;

	return 0;
}

int DiskMgr_GetPartNum(DiskHndl *d)
{
	return j_list_length(d->parts);
}

PartHndl *DiskMgr_GetPartById(DiskHndl *d, int id)
{
	JListNode *node, *n_next;

	int i = 0;
	for (node = j_list_first(d->parts); node; node = n_next) {
		n_next = j_list_next(d->parts, node);
		if (i == id)
			return node->data;
		i++;
	}

	return NULL;
}

int DiskMgr_MkFs(PartHndl *part, int fs_type)
{
	if (!part || !part->disk || !part->disk->online)
		return -1;

	disk_core_t *core = part->disk->core;
	if (!core)
		return -1;

	if (!core->mkfs)
		return -1;

	return core->mkfs(part, fs_type);
}

int DiskMgr_Mount(PartHndl *part, const char *mount_point)
{
	if (!part || !part->disk || !part->disk->online)
		return -1;

	disk_core_t *core = part->disk->core;
	if (!core)
		return -1;

	if (!core->mount)
		return -1;

	if (core->mount(part, mount_point) < 0)
		return -1;

	part->mounted = 1;
	memset(part->mount_point, 0, sizeof(part->mount_point));
	strncpy(part->mount_point, mount_point, sizeof(part->mount_point) - 1);

	return 0;
}

int DiskMgr_Umount(PartHndl *part)
{
	if (!part || !part->disk || !part->disk->online)
		return -1;

	if (!part->mounted)
		return 0;

	disk_core_t *core = part->disk->core;
	if (!core)
		return -1;

	if (!core->umount)
		return -1;

	if (core->umount(part) < 0) {
		perror("umount");
		return -1;
	}

	part->mounted = 0;

	return 0;
}

int DiskMgr_GetPartInfo(PartHndl *part, PartInfo *info)
{
	if (!part || !info)
		return -1;

	memset(info, 0, sizeof(PartInfo));
	strncpy(info->devname, part->devname, sizeof(info->devname) - 1);
	info->fs_type   = part->fs_type;
	info->totalsize = part->totalsize;

	info->mounted   = part->mounted;
	if (info->mounted)
		strncpy(info->mount_point, part->mount_point, sizeof(info->mount_point) - 1);

	return 0;
}
