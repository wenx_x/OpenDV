/*
 * Feel free to use this example code in any way you see fit (Public Domain)
 */

#include <sys/types.h>
#ifndef _WIN32
#include <sys/select.h>
#include <sys/socket.h>
#else
#include <winsock2.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <microhttpd.h>

#include "list.h"
#include "httpsvc.h"

#define INIT_BUF_SIZE   (32 * 1024)

typedef struct {
    int method;
    char url[1024];

    HttpOps ops;
    void *priv_data;
} HttpRoute;

struct HttpConn {
    int method;     /* 0 - GET, 1 - POST */

    char *buf;
    char *end;

    HttpRoute *route;
    long handle;

    struct MHD_Connection *MHD_conn;
};

struct HttpSvc {
    int port;
    struct MHD_Daemon *daemon;

    JList *allroutes;
    JList *allconns;
};

/**
 * Information we keep per connection.
 */
const char *errorpage = "<html><body>This doesn't seem to be right.</body></html>";

static int send_page(struct MHD_Connection *connection, const char *page, int status_code)
{
    int ret;
    struct MHD_Response *response;

    response = MHD_create_response_from_buffer(strlen(page), (void *)page, MHD_RESPMEM_MUST_COPY);
    if (!response)
        return MHD_NO;

    MHD_add_response_header(response, MHD_HTTP_HEADER_CONTENT_TYPE, "text/html");
    ret = MHD_queue_response(connection, status_code, response);
    MHD_destroy_response(response);

    return ret;
}

static void
request_completed(void *cls,
                  struct MHD_Connection *connection,
                  void **con_cls,
                  enum MHD_RequestTerminationCode toe)
{
    HttpConn *conn = *con_cls;
    if (!conn)
        return;

    if (conn->route->ops.close)
        conn->route->ops.close(conn->handle);

    /* Upload finished */
    free(conn->buf);
    free(conn);

    *con_cls = NULL;
}

static HttpConn *new_connection(struct MHD_Connection *connection, const char *url, int method, HttpRoute *route)
{
    HttpConn *conn = malloc(sizeof(HttpConn));
    if (!conn)
        return NULL;
    memset(conn, 0, sizeof(HttpConn));

    conn->buf = malloc(INIT_BUF_SIZE);
    if (!conn->buf) {
        free(conn);
        return NULL;
    }

    conn->end = conn->buf + INIT_BUF_SIZE;
    conn->route = route;
    conn->method = method;
    conn->MHD_conn = connection;

    conn->handle = route->ops.open(conn, url, route->priv_data);
    if (!conn->handle) {
        free(conn->buf);
        free(conn);
        return NULL;
    }

    return conn;
}

static int get_method_id(const char *method)
{
    int method_id = -1;

    if (strcasecmp(method, MHD_HTTP_METHOD_POST) == 0)
        method_id = HTTP_METHOD_POST;
    else if (strcasecmp(method, MHD_HTTP_METHOD_GET) == 0)
        method_id = HTTP_METHOD_GET;

    return method_id;
}

static HttpRoute *get_route(HttpSvc *svc, const char *method, const char *url)
{
    JListNode *n, *n_next;
    for (n = j_list_first(svc->allroutes); n; n = n_next) {
        n_next = j_list_next(svc->allroutes, n);

        HttpRoute *r = n->data;

        if (strlen(url) < strlen(r->url))
            continue;

        if (r->method != get_method_id(method))
            continue;

        if (strcmp(r->url, url) == 0)
            return r;

        if (strncmp(r->url, url, strlen(r->url)) == 0) {
            char c = url[strlen(r->url) - 1];
            if (c == '/')
                return r;
            continue;
        }
    }

    return NULL;
}

static int httpconn_write(HttpConn *conn, uint8_t *buf, int len)
{
    if (conn->route->ops.write)
        return conn->route->ops.write(conn->handle, buf, len);
    return 0;
}

static int httpconn_read(HttpConn *conn, uint8_t *buf, int len)
{
    if (conn->route->ops.read)
        return conn->route->ops.read(conn->handle, buf, len);
    return 0;
}

static int httpconn_seek(HttpConn *conn, uint64_t pos)
{
    if (conn->route->ops.seek)
        return conn->route->ops.seek(conn->handle, pos);
    return 0;
}

static ssize_t
file_reader(void *cls, uint64_t pos, char *buf, size_t max)
{
    HttpConn *conn = cls;
    if (!conn)
        return -1;

    httpconn_seek(conn, pos);

    int n = httpconn_read(conn, (uint8_t *)buf, max);
    if (n < 0) {
        return MHD_CONTENT_READER_END_WITH_ERROR; /* Read at required position is not possible. */
    } else if (n == 0) {
        return MHD_CONTENT_READER_END_OF_STREAM;
    }

    return n;
}

const char *HttpConn_LookupConnectionValue(HttpConn *conn, ValueKind kind, const char *key)
{
    return MHD_lookup_connection_value(conn->MHD_conn, kind, key);
}

static int
handle_connection(void *cls,
                  struct MHD_Connection *connection,
                  const char *url,
                  const char *method,
                  const char *version,
                  const char *upload_data,
                  size_t     *upload_data_size,
                  void       **con_cls)
{
    HttpSvc *svc = cls;
    if (!cls)
        return MHD_NO;

    /* First call, setup data structures */
    HttpConn *conn = *con_cls;
    if (!conn) {
        HttpRoute *route = get_route(svc, method, url);
        if (!route)
            return MHD_NO;

        conn = new_connection(connection, url, get_method_id(method), route);
        if (!conn)
            return MHD_NO;

        j_list_append(svc->allconns, conn);
        *con_cls = conn;

        return MHD_YES;
    }

    if (conn->method == HTTP_METHOD_GET) {
        struct MHD_Response *response;
        response = MHD_create_response_from_callback(MHD_SIZE_UNKNOWN, INIT_BUF_SIZE, file_reader, conn, NULL);
        if (!response)
            return MHD_NO;

        int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
        MHD_destroy_response (response);
        return ret;
    } else if (conn->method == HTTP_METHOD_POST) {
        if (*upload_data_size != 0) {
            httpconn_write(conn, (uint8_t *)upload_data, *upload_data_size);
            *upload_data_size = 0;
            return MHD_YES;
        }

        /* end of write */
        httpconn_write(conn, NULL, 0);

        struct MHD_Response *response;
        response = MHD_create_response_from_callback(MHD_SIZE_UNKNOWN, INIT_BUF_SIZE, file_reader, conn, NULL);
        if (!response) {
            return MHD_NO;
        }

        int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
        MHD_destroy_response(response);

        return ret;
    }

    /* Note a GET or a POST, generate error */
    return send_page(connection, errorpage, MHD_HTTP_BAD_REQUEST);
}

HttpSvc *HttpSvc_Create(int port)
{
    HttpSvc *svc = malloc(sizeof(HttpSvc));
    if (!svc)
        return NULL;
    memset(svc, 0, sizeof(HttpSvc));

    svc->port = port;
    svc->allroutes = j_list_alloc();
    svc->allconns = j_list_alloc();

    if (!svc->allroutes || !svc->allconns)
        goto fail;

    svc->daemon = MHD_start_daemon(MHD_USE_INTERNAL_POLLING_THREAD,
                              port, NULL, NULL,
                              &handle_connection, svc,
                              MHD_OPTION_NOTIFY_COMPLETED,
                              &request_completed, svc,
                              MHD_OPTION_END);
    if (!svc->daemon)
        goto fail;

    return svc;

fail:
    if (svc->daemon)
        MHD_stop_daemon(svc->daemon);
    if (svc->allroutes)
        j_list_free(svc->allroutes);
    if (svc->allconns)
        j_list_free(svc->allconns);
    free(svc);
    return NULL;
}

int HttpSvc_Destroy(HttpSvc *svc)
{
    if (!svc)
        return -1;

    MHD_stop_daemon(svc->daemon);
    j_list_free(svc->allroutes);
    j_list_free(svc->allconns);
    free(svc);

    return 0;
}

int HttpSvc_AddRoute(HttpSvc *svc, HttpMethod method, const char *path, HttpOps *ops, void *priv_data)
{
    JListNode *n, *n_next;
    for (n = j_list_first(svc->allroutes); n; n = n_next) {
        n_next = j_list_next(svc->allroutes, n);

        HttpRoute *r = n->data;
        if ((r->method == method) && !strcmp(r->url, path))
            return -1;
    }

    HttpRoute *route = malloc(sizeof(HttpRoute));
    if (!route)
        return -1;
    memset(route, 0, sizeof(HttpRoute));

    strncpy(route->url, path, sizeof(route->url) - 1);
    route->method = method;
    route->ops = *ops;
    route->priv_data = priv_data;

    j_list_append(svc->allroutes, route);

    return 0;
}

int HttpSvc_DelRoute(HttpSvc *svc, HttpMethod method, const char *path)
{
    JListNode *n, *n_next;
    for (n = j_list_first(svc->allroutes); n; n = n_next) {
        n_next = j_list_next(svc->allroutes, n);

        HttpRoute *r = n->data;
        if ((r->method == method) && !strcmp(r->url, path)) {
            j_list_remove(svc->allroutes, r);
            free(r);
            return 0;
        }
    }

    return -1;
}

#if 0
typedef struct {
    int fd;
    int count;
} RecordCtx;

static long record_open(const char *url, void *priv_data)
{
    printf("%s.%d: %s\n", __func__, __LINE__, url);
    RecordCtx *ctx = malloc(sizeof(RecordCtx));
    if (!ctx)
        return 0;
    memset(ctx, 0, sizeof(RecordCtx));

    return (long)ctx;
}

static int record_close(long handle)
{
    RecordCtx *ctx = (RecordCtx *)handle;
    free(ctx);
    return 0;
}

static int record_seek(long handle, uint64_t pos)
{
    return pos;
}

static ssize_t record_read(long handle, uint8_t *buf, size_t size)
{
    RecordCtx *ctx = (RecordCtx *)handle;
    ctx->count++;
    if (ctx->count > 5)
        return 0;

    memset(buf, 'a' + ctx->count, 4 * 1024);
    return 4 * 1024;
}

static ssize_t record_write(long handle, uint8_t *buf, size_t size)
{
    char *str = (char *)buf;
    printf("%s.%d: %s\n", __func__, __LINE__, str);
    return size;
}

static HttpOps record_ops = {
    .open = record_open,
    .close = record_close,
    .seek = record_seek,

    .read = record_read,
    .write = record_write,
};

int main(void)
{
    HttpSvc *svc = HttpSvc_Create(8888);
    if (!svc) {
        fprintf(stderr, "Failed to start daemon\n");
        return 1;
    }

    HttpSvc_AddRoute(svc, HTTP_METHOD_GET, "/", &record_ops, NULL);
    HttpSvc_AddRoute(svc, HTTP_METHOD_POST, "/record/", &record_ops, NULL);

    while (1) {
        sleep(10);
    }

    HttpSvc_Destroy(svc);

    return 0;
}
#endif
