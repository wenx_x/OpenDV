#ifndef __HTTPSVC_H__
#define __HTTPSVC_H__

#include <stdint.h>

typedef enum {
    HTTP_METHOD_GET,
    HTTP_METHOD_POST
} HttpMethod;

typedef struct HttpConn HttpConn;

typedef struct {
    long (*open)(HttpConn *conn, const char *url, void *priv_data);
    int (*close)(long handle);

    int (*seek)(long handle, uint64_t pos);

    ssize_t (*read)(long handle, uint8_t *buf, size_t len);
    ssize_t (*write)(long handle, uint8_t *buf, size_t len);
} HttpOps;

typedef struct HttpSvc HttpSvc;

HttpSvc *HttpSvc_Create(int port);
int HttpSvc_Destroy(HttpSvc *svc);

int HttpSvc_AddRoute(HttpSvc *svc, HttpMethod method, const char *path, HttpOps *ops, void *priv_data);
int HttpSvc_DelRoute(HttpSvc *svc, HttpMethod method, const char *path);

typedef enum {
    /**
     * Response header
     * @deprecated
     */
    HTTP_VALUE_KIND_RESPONSE_HEADER = 0,

    /**
     * HTTP header (request/response).
     */
    HTTP_VALUE_KIND_HEADER = 1,

    /**
     * Cookies.  Note that the original HTTP header containing
     * the cookie(s) will still be available and intact.
     */
    HTTP_VALUE_KIND_COOKIE = 2,

    /**
     * POST data.  This is available only if a content encoding
     * supported by MHD is used (currently only URL encoding),
     * and only if the posted content fits within the available
     * memory pool.  Note that in that case, the upload data
     * given to the #HTTP_VALUE_KIND_AccessHandlerCallback will be
     * empty (since it has already been processed).
     */
    HTTP_VALUE_KIND_POSTDATA = 4,

    /**
     * GET (URI) arguments.
     */
    HTTP_VALUE_KIND_GET_ARGUMENT = 8,

    /**
     * HTTP footer (only for HTTP 1.1 chunked encodings).
     */
    HTTP_VALUE_KIND_FOOTER = 16
} ValueKind;

const char *HttpConn_LookupConnectionValue(HttpConn *conn, ValueKind kind, const char *key);

#endif
