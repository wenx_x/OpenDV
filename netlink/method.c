#include <stdio.h>
#include <stdint.h>
#include <strings.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <limits.h>

#include "method.h"

typedef struct JsonMethod JsonMethod;

struct JsonMethod {
    char method[128];
    int (*handler)(json_object *params, json_object *result);

    JsonMethod *next;
};

static JsonMethod *first_method = NULL;
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

static JsonMethod *find_method(char *method)
{
    JsonMethod *m;
    for (m = first_method; m; m = m->next) {
        if (strcasecmp(m->method, method) == 0)
            return m;
    }

    return NULL;
}

int JsonMethod_Register(char *method, int (*handler)(json_object *params, json_object *result))
{
    if (find_method(method))
        return -1;  /* method already exist */

    JsonMethod *m = malloc(sizeof(JsonMethod));
    if (!m)
        return -1;
    memset(m, 0, sizeof(JsonMethod));

    strncpy(m->method, method, sizeof(m->method) - 1);
    m->handler = handler;

    pthread_mutex_lock(&lock);
    m->next = first_method;
    first_method = m;
    pthread_mutex_unlock(&lock);

    return 0;
}

int JsonMethod_UnRegister(char *method)
{
    JsonMethod *m = find_method(method);
    if (m) {
        JsonMethod **mp, *m1;

        mp = &first_method;
        while (*mp) {
            m1 = *mp;
            if (m1 == m)
                *mp = m->next;
            else
                mp = &m1->next;
        }

        free(m);
    }

    return 0;
}

int JsonMethod_Call(char *method, json_object *params, json_object *result)
{
    JsonMethod *m = find_method(method);
    if (!m)
        return -2;

    return m->handler(params, result);
}
