#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <json-c/json.h>

#include "method.h"

#define BIG_HAND_HELD

static int wait_readable(int sockfd, int milliseconds)
{
    struct pollfd fds;

wait_again:
    memset(&fds, 0, sizeof(struct pollfd));

    fds.fd     = sockfd;
    fds.events = POLLIN;

    int ret = poll(&fds, 1, milliseconds);
    if (ret < 0 && (errno == EINTR))
        goto wait_again;	/* interrupted by signal, try again */

    return ret;
}

static int wait_writeable(int sockfd, int milliseconds)
{
    struct pollfd fds;

wait_again:
    memset(&fds, 0, sizeof(struct pollfd));

    fds.fd     = sockfd;
    fds.events = POLLOUT;

    int ret = poll(&fds, 1, milliseconds);
    if (ret < 0 && (errno == EINTR))
        goto wait_again;	/* interrupted by signal, try again */

    return ret;
}

static ssize_t read_timedwait(int fd, void *buf, size_t count, int milliseconds)
{
    int n;

read_again:
    if (wait_readable(fd, milliseconds) <= 0)
        return -1;

    n = read(fd, buf, count);
    if (n < 0) {
        if (n < 0 && (errno == EINTR || errno == EAGAIN))
            goto read_again;   /* Call read() again. */
        return -1;
    }

    return n;
}

static ssize_t write_timedwait(int fd, const void *buf, size_t count, int milliseconds)
{
    int n;

write_again:
    if (wait_writeable(fd, milliseconds) <= 0)
        return -1;

    n = write(fd, buf, count);
    if (n < 0) {
        if (n < 0 && (errno == EINTR || errno == EAGAIN))
            goto write_again;   /* Call write() again. */
        return -1;
    }

    return n;
}

static ssize_t readn_timedwait(int fd, void *buf, size_t count, int milliseconds)
{
    ssize_t n;
    size_t left = count;
    char *ptr = buf;

    while (left > 0) {
        n = read_timedwait(fd, ptr, left, milliseconds);
        if (n <= 0)
            return -1;

        left -= n;
        ptr  += n;
    }

    return count;
}

static ssize_t writen_timedwait(int fd, void *buf, size_t count, int milliseconds)
{
    ssize_t n;
    size_t left = count;
    const char *ptr = buf;

    while (left > 0) {
        n = write_timedwait(fd, ptr, left, milliseconds);
        if (n <= 0)
            return -1;

        left -= n;
        ptr  += n;
    }

    return count;
}

#define BACKLOG     (5)

static int tcp_server(char *ipaddr, int port)
{
    int ret;

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        return -1;

    in_addr_t s_addr;
    if (ipaddr && ipaddr[0] && inet_pton(AF_INET, ipaddr, &s_addr) != 1) {
        goto fail;
    } else {
        s_addr = htonl(INADDR_ANY);
    }

    /* Create socket for listening connections. */
    int reuseaddr = 1;
    ret = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(reuseaddr));
    if (ret < 0)
        goto fail;

    struct sockaddr_in localaddr;
    memset(&localaddr, 0, sizeof(localaddr));
    localaddr.sin_family = AF_INET;
    localaddr.sin_port   = htons(port);
    localaddr.sin_addr.s_addr = s_addr;

    ret = bind(sockfd, (struct sockaddr *)&localaddr, sizeof(localaddr));
    if (ret < 0)
        goto fail;

    ret = listen(sockfd, BACKLOG);
    if (ret < 0)
        goto fail;

    return sockfd;

fail:
    close(sockfd);
    return -1;
}

static int tcp_accept(int sockfd)
{
    struct sockaddr_in from;
    socklen_t fromlen = sizeof(struct sockaddr_in);
    return accept(sockfd, (struct sockaddr *)&from, &fromlen);
}

static int connect_timedwait(int sockfd, struct sockaddr *peeraddr, socklen_t len, int seconds)
{
	int flags = fcntl(sockfd, F_GETFL);
	if (flags < 0)
		return -1;

	/* nonblocking */
	if (fcntl(sockfd, F_SETFL, flags | O_NONBLOCK) < 0)
		return -1;

	if (connect(sockfd, peeraddr, len) < 0) {
		if (errno != EINPROGRESS)
			goto fail;

		if (wait_writeable(sockfd, seconds * 1000) < 0)
			goto fail;

		/* check if connect successfully */
		int optval;
		socklen_t optlen = sizeof(optval);

		if (getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &optval, &optlen) < 0) {
			perror("getsockopt");
			goto fail;
		}

		if (optval != 0) {
			/* connect failed */
			errno = optval;
			goto fail;
		}
	}

	/* restore socket status */
	if (fcntl(sockfd, F_SETFL, flags) < 0)
		return -1;

	return 0;

fail:
	/* restore socket status */
	if (fcntl(sockfd, F_SETFL, flags) < 0)
		return -1;

	return -1;
}

static int socket_set_nonblock(int socket, int enable)
{
	if (enable)
		return fcntl(socket, F_SETFL, fcntl(socket, F_GETFL) | O_NONBLOCK);
	else
		return fcntl(socket, F_SETFL, fcntl(socket, F_GETFL) & ~O_NONBLOCK);
}

/* resolve host with also IP address parsing */
static int tcp_connect(char *ipaddr, int port)
{
	int fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd < 0) {
		perror("socket");
		return -1;
	}

	struct sockaddr_in peeraddr;
	memset(&peeraddr, 0, sizeof(peeraddr));

	peeraddr.sin_family = AF_INET;
	peeraddr.sin_port = htons(port);
    peeraddr.sin_addr.s_addr = inet_addr(ipaddr);

	if (connect_timedwait(fd, (struct sockaddr *)&peeraddr, sizeof(peeraddr), 5) < 0) {
		close(fd);
		return -1;
	}

	return fd;
}

int testloop(void)
{
    json_object *object = json_object_new_object();
    if (!object)
        return -1;

    json_object_object_add(object, "method", json_object_new_string("GetVideoEncoderConfiguration"));

    json_object *params = json_object_new_object();
    json_object_object_add(params, "channel", json_object_new_int(0));
    json_object_object_add(params, "stream", json_object_new_int(0));
    json_object_object_add(object, "params", params);

#if 0
    json_object_object_add(object, "method", json_object_new_string("GetEvents"));
    json_object *params = json_object_new_object();
    json_object_object_add(params, "PullPoint", json_object_new_string("/EventPullPoint?Type=Camera&Token=SUB7F0098000D20"));
    json_object_object_add(object, "params", params);
#endif

#if 0
    json_object_object_add(object, "method", json_object_new_string("DestroyEventPullPoint"));
    json_object *params = json_object_new_object();
    json_object_object_add(params, "PullPoint", json_object_new_string("/EventPullPoint?Type=Camera&Token=SUB7F0098000D20"));
    json_object_object_add(object, "params", params);
#endif

    json_object_object_add(object, "id", json_object_new_int(0));

    const char *data = json_object_to_json_string_ext(object, JSON_C_TO_STRING_PRETTY);

    int data_len = strlen(data);

    int sockfd = tcp_connect("127.0.0.1", 12345);
    if (sockfd < 0) {
        perror("tcp_connect");
        return -1;
    }

    uint32_t size = htonl(data_len);
    int ret = writen_timedwait(sockfd, &size, sizeof(uint32_t), 5000);
    if (ret < 0) {
        json_object_put(object);
        close(sockfd);
        return -1;
    }

    ret = writen_timedwait(sockfd, (void *)data, data_len, 5000);
    if (ret < 0) {
        json_object_put(object);
        close(sockfd);
        return -1;
    }

    json_object_put(object);

    int len = readn_timedwait(sockfd, &size, sizeof(uint32_t), 5000);
    if (len <= 0) {
        close(sockfd);
        return -1;
    }

    size = ntohl(size);

    char *buf = malloc(size + 1);
    if (!buf) {
        close(sockfd);
        return -1;
    }
    buf[size] = '\0';

    len = readn_timedwait(sockfd, buf, size, 5000);
    if (len <= 0) {
        close(sockfd);
        free(buf);
        return -1;
    }

    printf("buf: %s\n", buf);

    free(buf);
    close(sockfd);

    return 0;
}

int main(void)
{
    while (1) {
        testloop();
        sleep(100);
    }
    return 0;
}
