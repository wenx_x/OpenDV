#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "json-c/json.h"

#include "method.h"
#include "netlink.h"
#include "httpsvc.h"

struct NetLink {
    char path[1024];
    HttpSvc *httpsvc;
};

typedef struct {
    char *buf;
    char *end;
    char *ptr;

    int done;
    char *response;

    pthread_mutex_t mutex;
    pthread_cond_t cond;
} CameraCtx;

#define INIT_BUFSIZE    (4 * 1024)

static long camera_open(HttpConn *conn, const char *url, void *priv_data)
{
    CameraCtx *ctx = malloc(sizeof(CameraCtx));
    if (!ctx)
        return 0;
    memset(ctx, 0, sizeof(CameraCtx));

    ctx->buf = malloc(INIT_BUFSIZE);
    if (!ctx->buf) {
        free(ctx);
        return 0;
    }
    ctx->end = ctx->buf + INIT_BUFSIZE;
    ctx->ptr = ctx->buf;

    pthread_cond_init(&ctx->cond, NULL);
    pthread_mutex_init(&ctx->mutex, NULL);

    return (long)ctx;
}

static int camera_close(long handle)
{
    CameraCtx *ctx = (CameraCtx *)handle;

    pthread_mutex_destroy(&ctx->mutex);
    pthread_cond_destroy(&ctx->cond);

    free(ctx->buf);
    free(ctx);
    return 0;
}

static ssize_t camera_read(long handle, uint8_t *buf, size_t size)
{
    CameraCtx *ctx = (CameraCtx *)handle;

    pthread_mutex_lock(&ctx->mutex);
    while (ctx->done == 0)
        pthread_cond_wait(&ctx->cond, &ctx->mutex);
    pthread_mutex_unlock(&ctx->mutex);

    if (ctx->response) {
        memcpy(buf, ctx->response, strlen(ctx->response) + 1);
        int n = strlen(ctx->response);
        free(ctx->response);
        ctx->response = NULL;
        return n;
    }

    return 0;
}

typedef struct {
    char method[128];
    json_object *params;
    int id;

    json_object *object;
} JsonRequest;

static JsonRequest *get_request(const char *data)
{
    JsonRequest *req = malloc(sizeof(JsonRequest));
    if (!req)
        return NULL;
    memset(req, 0, sizeof(JsonRequest));

    req->object = json_tokener_parse(data);
    if (!req->object) {
        free(req);
        return NULL;
    }

    json_object_object_foreach(req->object, key, val) {
        if (strcmp(key, "method") == 0)
            strncpy(req->method, json_object_get_string(val), sizeof(req->method) - 1);
        else if (strcmp(key, "params") == 0)
            req->params = val;
        else if (strcmp(key, "id") == 0)
            req->id = json_object_get_int(val);
    }

    return req;
}

static int put_request(JsonRequest *req)
{
    json_object_put(req->object);
    free(req);
    return 0;
}

static char *send_response(int status, json_object *result, int id)
{
    json_object *outobj = json_object_new_object();

    json_object_object_add(outobj, "status", json_object_new_int(status));

    if (result) {
        if (!json_object_object_length(result))
            json_object_put(result);
        else
            json_object_object_add(outobj, "result", result);
    }

    json_object_object_add(outobj, "id", json_object_new_int(id));

    const char *data = json_object_to_json_string_ext(outobj, JSON_C_TO_STRING_PRETTY);

    char *resp = strdup(data);

    json_object_put(outobj);

    return resp;
}

static char *handle_message(char *data, int len)
{
    JsonRequest *req = get_request(data);
    if (!req)
        return send_response(-32700, NULL, -1);

    json_object *result = json_object_new_object();
    if (!result)
        return NULL;

    int status = JsonMethod_Call(req->method, req->params, result);

    char *response = send_response(status, result, req->id);

    put_request(req);

    return response;
}

static ssize_t camera_write(long handle, uint8_t *buf, size_t size)
{
    CameraCtx *ctx = (CameraCtx *)handle;

    if (buf && size > 0) {
        int left = ctx->end - ctx->ptr;
        if (left < size) {
            int dat_size = ctx->ptr - ctx->buf;
            int new_size = dat_size + size;

            ctx->buf = realloc(ctx->buf, new_size);
            if (!ctx->buf)
                return -1;
            ctx->end = ctx->buf + new_size;
            ctx->ptr = ctx->buf + dat_size;
        }

        memcpy(ctx->ptr, buf, size);
        ctx->ptr += size;

        return size;
    }

    ctx->response = handle_message(ctx->buf, ctx->ptr - ctx->buf);
    if (!ctx->response)
        return -1;

    pthread_mutex_lock(&ctx->mutex);
    ctx->done = 1;
    pthread_cond_signal(&ctx->cond);
    pthread_mutex_unlock(&ctx->mutex);

    return 0;
}

static HttpOps camera_ops = {
    .open  = camera_open,
    .close = camera_close,
    .seek  = NULL,

    .read  = camera_read,
    .write = camera_write,
};

NetLink *NetLink_Create(HttpSvc *svc, const char *path)
{
    NetLink *link = malloc(sizeof(NetLink));
    if (!link)
        return NULL;
    memset(link, 0, sizeof(NetLink));

    link->httpsvc = svc;
    strncpy(link->path, path, sizeof(link->path) - 1);

    HttpSvc_AddRoute(link->httpsvc, HTTP_METHOD_POST, link->path, &camera_ops, NULL);

    return link;
}

int NetLink_Destroy(NetLink *link)
{
    HttpSvc_DelRoute(link->httpsvc, HTTP_METHOD_POST, link->path);
    free(link);
    return 0;
}

int NetLink_Register(char *method, int (*handler)(json_object *params, json_object *result))
{
    return JsonMethod_Register(method, handler);
}

int NetLink_UnRegister(char *method)
{
    return JsonMethod_UnRegister(method);
}
