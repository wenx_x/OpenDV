#ifndef __CAMLINK_METHOD_H__
#define __CAMLINK_METHOD_H__

#include <json-c/json.h>

int JsonMethod_Register(char *method, int (*handler)(json_object *params, json_object *result));
int JsonMethod_UnRegister(char *method);

int JsonMethod_Call(char *method, json_object *params, json_object *result);

#endif	/* __CAMLINK_METHOD_H__ */
