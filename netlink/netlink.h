#ifndef __NETLINK_H__
#define __NETLINK_H__

#include "httpsvc.h"
#include "json-c/json.h"

int NetLink_Init(void);
int NetLink_Quit(void);

typedef struct NetLink NetLink;

NetLink *NetLink_Create(HttpSvc *svc, const char *path);
int NetLink_Destroy(NetLink *link);

int NetLink_Register(char *method, int (*handler)(json_object *params, json_object *result));
int NetLink_UnRegister(char *method);

#endif
