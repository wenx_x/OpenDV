#ifndef __NETSVC_H__
#define __NETSVC_H__

#include "httpsvc.h"

int NetSvc_Init(HttpSvc *svc, const char *path);
int NetSvc_Quit(void);

#endif
