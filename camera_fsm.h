#ifndef __CAMERA_FSM_H__
#define __CAMERA_FSM_H__

#include <common.h>

int CamFsm_Init(void);
int CamFsm_Quit(void);

int CamFsm_SetMode(CameraMode mode);
int CamFsm_GetMode(CameraMode *mode);

int CamFsm_TakePhoto(void);
int CamFsm_AbortTakePhoto(void);

int CamFsm_StartRecordVideo(void);
int CamFsm_StopRecordVideo(void);

int CamFsm_IsRecordingVideo(void);
int CamFsm_IsTakingPhoto(void);

int CamFsm_SetVideoEncoderConfiguration(int stream_id, VideoEncoderConfiguration *conf);

int CamFsm_PlaybackStart(const char *filename);
int CamFsm_PlaybackStop(void);

int CamFsm_PlaybackPause(void);
int CamFsm_PlaybackResume(void);

int CamFsm_PlaybackSeek(int off_ms);
int CamFsm_PlaybackSetSpeed(float scale);

int CamFsm_GetVideoStreamHeadInfo(int stream_id, StreamInfoType type, uint8_t *head, uint32_t *len);

#endif /* __CAMERA_FSM_H__ */
