#ifndef __CAMAPI_H__
#define __CAMAPI_H__

#include "event.h"
#include "common.h"
#include "recmgr.h"
#include "camera_fsm.h"

int CamApi_Init(void);
int CamApi_Quit(void);

int CamApi_SystemShutdown(void);
int CamApi_SystemReboot(void);

int CamApi_SystemFactoryReset(int hard);

int CamApi_StartUpgrade(const char *filename);

int CamApi_SetSystemDateTime(char *datetime);
int CamApi_GetSystemDateTime(char *datetime, int len);

int CamApi_GetStorageStatus(StorageStatus *status);
int CamApi_FormatStorage(void);

int CamApi_GetRecordSettings(RecordSettings *settings);
int CamApi_SetRecordSettings(RecordSettings *settings);

int CamApi_GetPhotoTakingSettings(PhotoTakingSettings *settings);
int CamApi_SetPhotoTakingSettings(PhotoTakingSettings *settings);

int CamApi_GetImageSettings(ImageSettings *settings);
int CamApi_SetImageSettings(ImageSettings *settings);

int CamApi_GetOsdSettings(OsdSettings *settings);
int CamApi_SetOsdSettings(OsdSettings *settings);

int CamApi_GetWifiConfiguration(WifiConfiguration *conf);
int CamApi_SetWifiConfiguration(WifiConfiguration *conf);

int CamApi_GetVideoEncoderConfiguration(int stream, VideoEncoderConfiguration *conf);
int CamApi_SetVideoEncoderConfiguration(int stream, VideoEncoderConfiguration *conf);

int CamApi_GetAudioEncoderConfiguration(AudioEncoderConfiguration *conf);
int CamApi_SetAudioEncoderConfiguration(AudioEncoderConfiguration *conf);

int CamApi_GetCameraMode(CameraMode *mode);
int CamApi_SetCameraMode(CameraMode mode);

int CamApi_StartRecord(void);
int CamApi_StopRecord(void);

int CamApi_StartSnapshot(void);
int CamApi_StopSnapshot(void);

int CamApi_GetFilesNum(const char *suffix);
int CamApi_GetFilesList(int index, const char *suffix, FileItem *files, int nb_files);

int CamApi_DeleteFile(const char *filename);

int CamApi_CreateEventSubscriber(int *session_id);
int CamApi_DeleteEventSubscriber(int session_id);
int CamApi_GetEvents(int session_id, Event *events, int nmemb);

int CamApi_PlaybackStart(const char *filename);
int CamApi_PlaybackStop(void);

int CamApi_PlaybackPause(void);
int CamApi_PlaybackResume(void);

int CamApi_PlaybackSeek(int off_ms);
int CamApi_PlaybackSetSpeed(float scale);

#endif
