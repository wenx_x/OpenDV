#include <stdint.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/uio.h>

#include "stateMachine.h"

#include "params.h"
#include "config.h"
#include "logger.h"
#include "recmgr.h"
#include "common.h"
#include "storage.h"
#include "recorder.h"
#include "replayer.h"
#include "mediaapi.h"
#include "livestream.h"
#include "camera_fsm.h"

static const char *TAG = "CamFsm";

struct stateMachine fsm;

enum CAM_FSM_EVENT {
    CAM_EV_CHANGE_MODE_REC,
    CAM_EV_CHANGE_MODE_CAP,
    CAM_EV_CHANGE_MODE_SETTING,
    CAM_EV_CHANGE_MODE_PLAYBACK,
    CAM_EV_CHANGE_MODE_USERCONFIG,

    CAM_EV_MODE_CAP_PREVIEW,
    CAM_EV_MODE_CAP_START,
    CAM_EV_MODE_CAP_STOP,
    CAM_EV_MODE_CAP_SAVED,

    CAM_EV_FINISH_USERCONFIG,

    CAM_EV_MODE_REC_PREVIEW,
    CAM_EV_MODE_REC_START,
    CAM_EV_MODE_REC_STOP,

    CAM_EV_MODE_PLAYBACK_STARTED,
    CAM_EV_MODE_PLAYBACK_STOPPED,
};

typedef enum {
    USER_CONFIG_SET_VIDEO_ENCODER = 1,
} UserConfigType;

typedef struct {
    UserConfigType type;
    void *data1;
    void *data2;
    int retval;
} UserConfig;

typedef struct {
    const char *filename;
    int retval;
} PlaybackCtx;

static int handle_event(int event, void *data);

static void printEnterMsg(void *stateData, struct event *event)
{
    LogV(TAG, "Entering %s state", (char *)stateData);
}

static void printExitMsg(void *stateData, struct event *event)
{
    LogV(TAG, "Exiting %s state", (char *)stateData);
}

static void printErrMsg(void *stateData, struct event *event)
{
    LogV(TAG, "ENTERED ERROR STATE!");
}

static bool checkPreviousState(void *condition, struct event *event)
{
    struct state *previousState = (struct state *)condition;

    if (stateM_previousState(&fsm) == previousState)
        return true;

    return false;
}

static CameraMode currentMode = CAMERA_MODE_IDLE;

static struct state recPreviewState;
static struct state recStartedState;
static struct state capPreviewState;
static struct state capStartedState;
static struct state picInVideoState;
static struct state userConfigState;
static struct state recPlaybackState;
static struct state playbackStartedState;

static void enterIdleMode(void *stateData, struct event *event)
{
    printEnterMsg(stateData, event);
    currentMode = CAMERA_MODE_IDLE;
}

static struct state idleState = {
    .parentState = NULL,
    .entryState = NULL,
    .transitions = (struct transition[]){
        { CAM_EV_CHANGE_MODE_REC, NULL, NULL, NULL, &recPreviewState },
        { CAM_EV_CHANGE_MODE_CAP, NULL, NULL, NULL, &capPreviewState },
    },
    .numTransitions = 2,
    .data = "idle",
    .entryAction = enterIdleMode,
    .exitAction = printExitMsg,
};

static LiveStream *main_stream = NULL;
static LiveStream *sub_stream = NULL;

static int start_sub_stream(void)
{
    /* 2nd stream is always streaming */
    sub_stream = LiveStream_Open(1);
    if (!sub_stream)
        return -1;

    LiveStream_Start(sub_stream);

    return 0;
}

static int stop_sub_stream(void)
{
    if (sub_stream) {
        LiveStream_Stop(sub_stream);
        LiveStream_Close(sub_stream);
        sub_stream = NULL;
    }
    return 0;
}

static void enterUserConfigMode(void *stateData, struct event *event)
{
    printEnterMsg(stateData, event);
    currentMode = CAMERA_MODE_USERCONF;

    stop_sub_stream();

#if 0
    UserConfig *userCfg = event->data;
    if (!userCfg)
        return;

    if (userCfg->type == USER_CONFIG_SET_VIDEO_ENCODER) {
        int stream_id = (int)userCfg->data1;
        VideoEncoderConfiguration *vencCfg = userCfg->data2;
        Params_SetVideoEncoderConfiguration(stream_id, vencCfg);

        /* go back to previous state will reopen relative streams */
    }
#endif
}

static void exitUserConfigMode(void *stateData, struct event *event)
{
    printExitMsg(stateData, event);

    start_sub_stream();
}

static struct state userConfigState = {
    .parentState = NULL,
    .entryState = NULL,
    .transitions = (struct transition[]){
        { CAM_EV_FINISH_USERCONFIG, &capPreviewState,  checkPreviousState, NULL, &capPreviewState  },
        { CAM_EV_FINISH_USERCONFIG, &recPreviewState,  checkPreviousState, NULL, &recPreviewState  },
        { CAM_EV_FINISH_USERCONFIG, &recPlaybackState, checkPreviousState, NULL, &recPlaybackState },
    },
    .numTransitions = 3,
    .data = "user_config",
    .entryAction = enterUserConfigMode,
    .exitAction = exitUserConfigMode,
};

static VideoPreview *videoPreview = NULL;

static void enterRecPreviewMode(void *stateData, struct event *event)
{
    printEnterMsg(stateData, event);
    currentMode = CAMERA_MODE_RECORD;

    if (event->type == CAM_EV_CHANGE_MODE_REC) {
        videoPreview = MediaApi_OpenVideoPreview(1);
        if (!videoPreview) {
            LogV(TAG, "start record preview failed");
        }

        int *ret = event->data;
        main_stream = LiveStream_Open(0);
        if (!main_stream) {
            LogV(TAG, "open video stream 0 failed");
            *ret = -1;
            return;
        }

        if (LiveStream_Start(main_stream) < 0) {
            LiveStream_Close(main_stream);
            LogV(TAG, "start video stream 0 failed");
            *ret = -2;
            return;
        }
    }
}

static void exitRecPreviewMode(void *stateData, struct event *event)
{
    printExitMsg(stateData, event);

    if (event->type != CAM_EV_CHANGE_MODE_CAP && event->type != CAM_EV_CHANGE_MODE_PLAYBACK)
        return;

    if (main_stream) {
        LiveStream_Stop(main_stream);
        LiveStream_Close(main_stream);
        main_stream = NULL;
    }

    if (videoPreview) {
        MediaApi_CloseVideoPreview(videoPreview);
        videoPreview = NULL;
    }
}

static struct state recPreviewState = {
    .parentState = NULL,
    .entryState = NULL,
    .transitions = (struct transition[]){
        { CAM_EV_MODE_REC_START,         NULL, NULL, NULL, &recStartedState  },
        { CAM_EV_CHANGE_MODE_CAP,        NULL, NULL, NULL, &capPreviewState  },
        { CAM_EV_MODE_CAP_START,         NULL, NULL, NULL, &picInVideoState  },
        { CAM_EV_CHANGE_MODE_PLAYBACK,   NULL, NULL, NULL, &recPlaybackState },
        { CAM_EV_CHANGE_MODE_USERCONFIG, NULL, NULL, NULL, &userConfigState  },
    },
    .numTransitions = 5,
    .data = "rec_preview",
    .entryAction = enterRecPreviewMode,
    .exitAction = exitRecPreviewMode,
};

static PhotoStream *mainPhotoStream = NULL;

static int photo_cb(VideoFrame *frame, void *priv_data)
{
    /* save photo here */
    if (RecMgr_WriteJpg(frame->data, frame->size) < 0)
        LogV(TAG, "write jpg file failed");

    if (frame->last_frame)  /* end of capture */
        handle_event(CAM_EV_MODE_CAP_SAVED, NULL);

    return 0;
}

static void enterCapPreviewMode(void *stateData, struct event *event)
{
    printEnterMsg(stateData, event);
    currentMode = CAMERA_MODE_CAPTURE;

    if (event->type == CAM_EV_CHANGE_MODE_CAP) {
        videoPreview = MediaApi_OpenVideoPreview(0);
        if (!videoPreview) {
            LogV(TAG, "start capture preview failed");
        }

        int *ret = event->data;
        PhotoTakingSettings settings;
        Params_GetPhotoTakingSettings(&settings);

        PhotoEncodeConf photoCfg = {
            .width = settings.res_width,
            .height = settings.res_height,
            .qfactor = 90,
        };

        mainPhotoStream = MediaApi_OpenPhotoStream(0, &photoCfg, photo_cb, NULL);
        if (!mainPhotoStream) {
            LogV(TAG, "open photo stream 0 failed");
            *ret = -1;
            return;
        }

#if 0
        if (PhotoStream_SetPhotoAttr(mainPhotoStream) < 0) {
            LogV(TAG, "start photo stream 0 failed");
            *ret = -2;
            return;
        }
#endif
    }
}

static void exitCapPreviewMode(void *stateData, struct event *event)
{
    printExitMsg(stateData, event);

    if (event->type != CAM_EV_CHANGE_MODE_REC && event->type != CAM_EV_CHANGE_MODE_PLAYBACK)
        return;

    if (mainPhotoStream) {
        MediaApi_ClosePhotoStream(mainPhotoStream);
        mainPhotoStream = NULL;
    }

    if (videoPreview) {
        MediaApi_CloseVideoPreview(videoPreview);
        videoPreview = NULL;
    }
}

static struct state capPreviewState = {
    .parentState = NULL,
    .entryState = NULL,
    .transitions = (struct transition[]){
        { CAM_EV_MODE_CAP_START,         NULL, NULL, NULL, &capStartedState  },
        { CAM_EV_CHANGE_MODE_REC,        NULL, NULL, NULL, &recPreviewState  },
        { CAM_EV_CHANGE_MODE_PLAYBACK,   NULL, NULL, NULL, &recPlaybackState },
        { CAM_EV_CHANGE_MODE_USERCONFIG, NULL, NULL, NULL, &userConfigState  },
    },
    .numTransitions = 4,
    .data = "cap_preview",
    .entryAction = enterCapPreviewMode,
    .exitAction = exitCapPreviewMode,
};

static void startTakePhoto(void *stateData, struct event *event)
{
    printEnterMsg(stateData, event);
    if (mainPhotoStream)
        PhotoStream_Start(mainPhotoStream, 1);
}

static void stopTakePhoto(void *stateData, struct event *event)
{
    printExitMsg(stateData, event);
    if (mainPhotoStream)
        PhotoStream_Stop(mainPhotoStream);
}

static struct state capStartedState = {
    .parentState = NULL,
    .entryState = NULL,
    .transitions = (struct transition[]){
        { CAM_EV_MODE_CAP_SAVED, NULL, NULL, NULL, &capPreviewState },
        { CAM_EV_MODE_CAP_STOP,  NULL, NULL, NULL, &capPreviewState },
    },
    .numTransitions = 2,
    .data = "cap_started",
    .entryAction = startTakePhoto,
    .exitAction = stopTakePhoto,
};

static void *async_stop_record(void *arg)
{
    pthread_detach(pthread_self());
    handle_event(CAM_EV_MODE_REC_STOP, NULL);
    LogV(TAG, "record aborted, stop record asynchronously");
    return NULL;
}

static int stopRecord(void *priv_data)
{
    pthread_t tid;
    pthread_create(&tid, NULL, async_stop_record, NULL);
    return 0;
}

static void startRecordVideo(void *stateData, struct event *event)
{
    printEnterMsg(stateData, event);

    if (event->type == CAM_EV_MODE_CAP_SAVED)   /* return from PIV */
        return;

    int *ret = event->data;
    *ret = Recorder_StartRecord(stopRecord, NULL);
}

static void stopRecordVideo(void *stateData, struct event *event)
{
    printExitMsg(stateData, event);

    if (event->type == CAM_EV_MODE_CAP_START)   /* going to PIV */
        return;

    Recorder_StopRecord();
}

static struct state recStartedState = {
    .parentState = NULL,
    .entryState = NULL,
    .transitions = (struct transition[]){
        { CAM_EV_MODE_REC_STOP,  NULL, NULL, NULL, &recPreviewState },
        { CAM_EV_MODE_CAP_START, NULL, NULL, NULL, &picInVideoState },
    },
    .numTransitions = 2,
    .data = "rec_started",
    .entryAction = startRecordVideo,
    .exitAction = stopRecordVideo,
};

static void startTakePivPhoto(void *stateData, struct event *event)
{
    printEnterMsg(stateData, event);
}

static void stopTakePivPhoto(void *stateData, struct event *event)
{
    printExitMsg(stateData, event);
}

static struct state picInVideoState = {
    .parentState = NULL,
    .entryState = NULL,
    .transitions = (struct transition[]){
        { CAM_EV_MODE_CAP_SAVED, &recStartedState, checkPreviousState, NULL, &recStartedState },
        { CAM_EV_MODE_CAP_SAVED, &recPreviewState, checkPreviousState, NULL, &recPreviewState },
    },
    .numTransitions = 2,
    .data = "pic_in_video",
    .entryAction = startTakePivPhoto,
    .exitAction = stopTakePivPhoto,
};

static Replayer *replayer = NULL;

static void enterPlaybackStartedState(void *stateData, struct event *event)
{
    printEnterMsg(stateData, event);

    PlaybackCtx *playback = event->data;

    replayer = Replayer_Create(playback->filename);
    if (!replayer) {
        LogV(TAG, "Replayer_Create %s failed", playback->filename);
        playback->retval = -2;
        return;
    }

    if (Replayer_Start(replayer) < 0) {
        LogV(TAG, "Replayer_Start %s failed", playback->filename);
        playback->retval = -3;
        Replayer_Destroy(replayer);
        replayer = NULL;
        return;
    }
}

static void exitPlaybackStartedState(void *stateData, struct event *event)
{
    if (replayer) {
        Replayer_Stop(replayer);
        Replayer_Destroy(replayer);
        replayer = NULL;
    }

    printExitMsg(stateData, event);
}

static struct state playbackStartedState = {
    .parentState = NULL,
    .entryState = NULL,
    .transitions = (struct transition[]){
        { CAM_EV_MODE_PLAYBACK_STOPPED, NULL, NULL, NULL, &recPlaybackState },
    },
    .numTransitions = 1,
    .data = "playback_started",
    .entryAction = enterPlaybackStartedState,
    .exitAction = exitPlaybackStartedState,
};

static void enterRecPlaybackMode(void *stateData, struct event *event)
{
    printEnterMsg(stateData, event);
    currentMode = CAMERA_MODE_PLAYBACK;

    if (event->type == CAM_EV_CHANGE_MODE_CAP) {
        int *ret = event->data;
        /* TODO */
        *ret = 0;
    }
}

static void exitRecPlaybackMode(void *stateData, struct event *event)
{
    printExitMsg(stateData, event);

    if (event->type != CAM_EV_CHANGE_MODE_REC && event->type != CAM_EV_CHANGE_MODE_CAP)
        return;

    /* TODO */
}

static struct state recPlaybackState = {
    .parentState = NULL,
    .entryState = NULL,
    .transitions = (struct transition[]){
        { CAM_EV_CHANGE_MODE_CAP,        NULL, NULL, NULL, &capPreviewState      },
        { CAM_EV_CHANGE_MODE_REC,        NULL, NULL, NULL, &recPreviewState      },
        { CAM_EV_CHANGE_MODE_USERCONFIG, NULL, NULL, NULL, &userConfigState      },
        { CAM_EV_MODE_PLAYBACK_STARTED,  NULL, NULL, NULL, &playbackStartedState },
    },
    .numTransitions = 4,
    .data = "playback",
    .entryAction = enterRecPlaybackMode,
    .exitAction = exitRecPlaybackMode,
};

static struct state errorState = {
    .entryAction = printErrMsg
};

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

static int handle_event(int event, void *data)
{
    struct event ev = {
        .type = event,
        .data = data,
    };

    pthread_mutex_lock(&mutex);
    int ret = stateM_handleEvent(&fsm, &ev);
    pthread_mutex_unlock(&mutex);

    if (ret != stateM_stateChanged)
        return -1;

    return 0;
}

int CamFsm_Init(void)
{
    stateM_init(&fsm, &idleState, &errorState);

    CameraMode mode;
    Params_GetCameraMode(&mode);

    int ret = -1;

    int errcode = -1;

    if (mode == CAMERA_MODE_RECORD)
        ret = handle_event(CAM_EV_CHANGE_MODE_REC, &errcode);
    else if (mode == CAMERA_MODE_CAPTURE)
        ret = handle_event(CAM_EV_CHANGE_MODE_CAP, &errcode);

    start_sub_stream();

    return ret;
}

int CamFsm_Quit(void)
{
    return 0;
}

int CamFsm_SetMode(CameraMode mode)
{
    int ret = -1;
    int errcode = 0;

    if (mode == currentMode)
        return 0;

    switch (mode) {
    case CAMERA_MODE_RECORD:
        ret = handle_event(CAM_EV_CHANGE_MODE_REC, &errcode);
        break;
    case CAMERA_MODE_CAPTURE:
        ret = handle_event(CAM_EV_CHANGE_MODE_CAP, &errcode);
        break;
    case CAMERA_MODE_PLAYBACK:
        ret = handle_event(CAM_EV_CHANGE_MODE_PLAYBACK, &errcode);
        break;
    case CAMERA_MODE_USERCONF:
        ret = handle_event(CAM_EV_CHANGE_MODE_USERCONFIG, &errcode);
        break;
    default:
        LogV(TAG, "unknown camera mode %d", mode);
        return -1;
    }

    if (ret < 0)
        return ret;

    if (errcode < 0)
        return errcode;

    return 0;
}

int CamFsm_GetMode(CameraMode *mode)
{
    *mode = currentMode;
    return 0;
}

int CamFsm_TakePhoto(void)
{
    if (!Storage_HaveEnoughSpace()) {
        LogV(TAG, "disk error or have not enough space");
        return -1;
    }

    return handle_event(CAM_EV_MODE_CAP_START, NULL);
}

int CamFsm_AbortTakePhoto(void)
{
    return handle_event(CAM_EV_MODE_CAP_STOP, NULL);
}

int CamFsm_StartRecordVideo(void)
{
    int ret = 0;

    if (!Storage_HaveEnoughSpace()) {
        LogV(TAG, "disk error or have not enough space");
        return -1;
    }

    if (handle_event(CAM_EV_MODE_REC_START, &ret) < 0) {
        LogV(TAG, "start record video failed, not in right state");
        return -1;
    }

    if (ret < 0) {
        CamFsm_StopRecordVideo();
        LogV(TAG, "start record video failed");
        return ret;
    }

    LogV(TAG, "start record video success");

    return 0;
}

int CamFsm_StopRecordVideo(void)
{
    LogV(TAG, "stop record video");
    return handle_event(CAM_EV_MODE_REC_STOP, NULL);
}

int CamFsm_IsRecordingVideo(void)
{
    if (stateM_currentState(&fsm) == &recStartedState)
        return 1;
    return 0;
}

int CamFsm_IsTakingPhoto(void)
{
    if (stateM_currentState(&fsm) == &capStartedState)
        return 1;
    return 0;
}

int CamFsm_SetVideoEncoderConfiguration(int stream_id, VideoEncoderConfiguration *conf)
{
    if (currentMode != CAMERA_MODE_USERCONF)
        return -1;

    Params_GetVideoEncoderConfiguration(stream_id, conf);

    return 0;
}

static pthread_mutex_t lock_replayer = PTHREAD_MUTEX_INITIALIZER;

int CamFsm_PlaybackStart(const char *filename)
{
    PlaybackCtx playback = {
        .filename = filename,
        .retval = 0,
    };

    pthread_mutex_lock(&lock_replayer);
    int ret = handle_event(CAM_EV_MODE_PLAYBACK_STARTED, &playback);
    if (ret < 0) {
        pthread_mutex_unlock(&lock_replayer);
        LogV(TAG, "cannot start playback, not in right camera mode");
        return -1;
    }

    if (playback.retval != 0) {
        LogV(TAG, "start record playback failed, ret = %d", playback.retval);
        handle_event(CAM_EV_MODE_PLAYBACK_STOPPED, NULL);
        pthread_mutex_unlock(&lock_replayer);
        return playback.retval;
    }
    pthread_mutex_unlock(&lock_replayer);

    return 0;
}

int CamFsm_PlaybackStop(void)
{
    pthread_mutex_lock(&lock_replayer);
    handle_event(CAM_EV_MODE_PLAYBACK_STOPPED, NULL);
    pthread_mutex_unlock(&lock_replayer);
    return 0;
}

int CamFsm_PlaybackPause(void)
{
    int ret = -1;

    pthread_mutex_lock(&lock_replayer);
    if (replayer)
        ret = Replayer_Pause(replayer);
    pthread_mutex_unlock(&lock_replayer);

    return ret;
}

int CamFsm_PlaybackResume(void)
{
    int ret = -1;

    pthread_mutex_lock(&lock_replayer);
    if (replayer)
        ret = Replayer_Resume(replayer);
    pthread_mutex_unlock(&lock_replayer);

    return ret;
}

int CamFsm_PlaybackSeek(int off_ms)
{
    int ret = -1;

    pthread_mutex_lock(&lock_replayer);
    if (replayer)
        ret = Replayer_Seek(replayer, off_ms);
    pthread_mutex_unlock(&lock_replayer);

    return ret;
}

int CamFsm_PlaybackSetSpeed(float scale)
{
    int ret = -1;

    pthread_mutex_lock(&lock_replayer);
    if (replayer)
        ret = Replayer_SetSpeed(replayer, scale);
    pthread_mutex_unlock(&lock_replayer);

    return ret;
}

int CamFsm_GetVideoStreamHeadInfo(int stream_id, StreamInfoType type, uint8_t *head, uint32_t *len)
{
    LiveStream *st = NULL;

    if (stream_id == 0)
        st = main_stream;
    else if (stream_id == 1)
        st = sub_stream;

    if (!st)
        return -1;

    pthread_mutex_lock(&mutex);
    int ret = LiveStream_GetVideoStreamHeadInfo(st, type, head, len);
    pthread_mutex_unlock(&mutex);

    return ret;
}
