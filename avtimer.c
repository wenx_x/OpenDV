#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/time.h>

#include "avtimer.h"

static int64_t av_gettime_ms(void)
{
#if 0
	struct timespec tp;
	if (clock_gettime(CLOCK_MONOTONIC, &tp) < 0)
		return -1;
	return (int64_t)tp.tv_sec * 1000 + (int64_t)tp.tv_nsec / 1000000;
#else
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (int64_t)tv.tv_sec * 1000 + (int64_t)tv.tv_usec / 1000;
#endif
}

av_timer_t *av_timer_create(void)
{
	av_timer_t *timer = malloc(sizeof(av_timer_t));
	if (!timer)
		return NULL;
	memset(timer, 0, sizeof(av_timer_t));

	return timer;
}

int av_timer_destroy(av_timer_t *timer)
{
	free(timer);
	return 0;
}

void av_timer_start(av_timer_t *timer)
{
	timer->state = AV_TIMER_RUNNING;
	timer->orig = av_gettime_ms();
	timer->posix_timer_time = 0;
}

int av_timer_wait(av_timer_t *timer)
{
	int64_t diff, time;
	struct timeval tv;

	while (1) {
		time = av_gettime_ms() - timer->orig;
		diff = timer->posix_timer_time - time;
		if (diff < 0) {
//			printf("Must catchup %lli miliseconds.\n", diff);
			break;
		}

//		printf("Must wait %lli miliseconds.\n", diff);
		tv.tv_sec  = diff / 1000;
		tv.tv_usec = diff % 1000 * 1000;
		select(0, NULL, NULL, NULL, &tv);
	}

	timer->posix_timer_time += (timer->interval.tv_sec * 1000 +
			timer->interval.tv_usec / 1000);

	return 0;
}

void av_timer_reset(av_timer_t *timer)
{
	timer->state = AV_TIMER_STOPPED;
	timer->orig = av_gettime_ms();
	timer->posix_timer_time = 0;
}

void av_timer_set(av_timer_t *timer, struct timeval *interval)
{
	timer->interval = *interval;
}

//#define TEST

#ifdef TEST
int main(void)
{
	av_timer_t *timer;
	timer = av_timer_create();

	struct timeval tv;
	tv.tv_sec  = 0;
	tv.tv_usec = 400000;	/* 40 microseconds */
	av_timer_set(timer, &tv);

	av_timer_start(timer);
	av_timer_wait(timer);
	av_timer_destroy(timer);

	return 0;
}
#endif	/* TEST */
