#! /bin/sh

find . -name CMakeFiles -o -name CMakeCache.txt -o -name cmake_install.cmake | xargs rm -rf
