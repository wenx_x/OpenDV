file(GLOB HEADERS "*.h")
file(GLOB SOURCES "*.c")

add_library(bufchan ${SOURCES} ${HEADERS})
