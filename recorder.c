#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/vfs.h>
#include <sys/time.h>

#include "h264.h"
#include "flag.h"
#include "event.h"
#include "config.h"
#include "params.h"
#include "logger.h"
#include "common.h"
#include "recmgr.h"
#include "recorder.h"
#include "bufchan.h"
#include "mediaapi.h"
#include "camera_fsm.h"
#include "livestream.h"

const char *TAG = "Recorder";

typedef struct {
    int stream_id;
    LiveStreamReader *reader;
    int64_t start_ms;
    int64_t stop_ms;
    int got_keyframe;
} Stream;

typedef struct {
    Stream streams[2];
} RecorderCtx;

static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
static RecChan *rec_chan = NULL;

static int (*record_aborted_cb)(void *priv_data);
static void *record_aborted_priv_data = NULL;

#define MAX_READ_COUNT  (200)

static int64_t get_time(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (int64_t)tv.tv_sec * 1000 + (int64_t)tv.tv_usec / 1000;
}

static const uint8_t start_code[] = { 0x00, 0x00, 0x00, 0x01 };

static int get_extradata(Stream *st, uint8_t *extradata, int extrasize, int *width, int *height)
{
#ifdef USE_SIMULATE_STREAM
    int size = -1;

    int start_ms = st->start_ms > 0 ? st->start_ms : (get_time() - 2 * 1000);

    LiveStreamReader *reader = LiveStream_CreateReader(st->stream_id, start_ms);
    if (!reader)
        return -1;

    uint8_t *frame_buf = malloc(MAX_FRAME_SIZE);
    if (!frame_buf) {
        LiveStream_DestroyReader(reader);
        return -1;
    }

    int i;
    for (i = 0; i < MAX_READ_COUNT; i++) {
        StreamInfo info;

        struct iovec iov[2];
        iov[0].iov_base = &info;
        iov[0].iov_len = sizeof(StreamInfo);

        iov[1].iov_base = frame_buf;
        iov[1].iov_len = MAX_FRAME_SIZE;

        int len = LiveStream_ReadV(reader, iov, 2, 30);  /* only wait 5ms */
        if (len == 0)
            continue;

        if (info.frame_type != FRAME_TYPE_I)
            continue;

        uint8_t *ptr = extradata;
        uint8_t *end = extradata + extrasize;

        int sps_len = h264_get_sps(frame_buf, len - sizeof(StreamInfo), ptr, end - ptr);
        if (sps_len <= 0)
            continue;

        h264_sps_get_dimension(ptr, sps_len, width, height);

        LogV(TAG, "width: %d, height: %d", *width, *height);

        ptr += sps_len;

        int pps_len = h264_get_pps(frame_buf, len - sizeof(StreamInfo), ptr, end - ptr);
        if (pps_len <= 0)
            continue;
        ptr += pps_len;

        size = sps_len + pps_len;
        break;
    }

    LiveStream_DestroyReader(reader);
    free(frame_buf);

    return size;
#else
    VideoEncoderConfiguration vencCfg;
    Params_GetVideoEncoderConfiguration(st->stream_id, &vencCfg);

    uint8_t *ptr = extradata;
    uint8_t *end = extradata + extrasize;

    /* vps */
    if (vencCfg.codec_type == CODEC_TYPE_H265) {
        memcpy(ptr, start_code, sizeof(start_code));
        ptr += sizeof(start_code);

        uint32_t vps_len = end - ptr;
        if (CamFsm_GetVideoStreamHeadInfo(st->stream_id, STREAM_INFO_TYPE_VPS, ptr, &vps_len) < 0) {
            LogE(TAG, "get vps failed");
            return -1;
        }
        ptr += vps_len;
    }

    uint8_t *sps = ptr;

    /* sps */
    memcpy(ptr, start_code, sizeof(start_code));
    ptr += sizeof(start_code);

    uint32_t sps_len = end - ptr;
    if (CamFsm_GetVideoStreamHeadInfo(st->stream_id, STREAM_INFO_TYPE_SPS, ptr, &sps_len) < 0) {
        LogE(TAG, "get sps failed");
        return -1;
    }
    ptr += sps_len;

    h264_sps_get_dimension(sps, ptr - sps, width, height);

    /* pps */
    memcpy(ptr, start_code, sizeof(start_code));
    ptr += sizeof(start_code);

    uint32_t pps_len = end - ptr;
    if (CamFsm_GetVideoStreamHeadInfo(st->stream_id, STREAM_INFO_TYPE_PPS, ptr, &pps_len) < 0) {
        LogE(TAG, "get pps failed");
        return -1;
    }
    ptr += pps_len;

    return ptr - extradata;
#endif
}

static long rec_open(void *priv_data)
{
    RecorderCtx *ctx = malloc(sizeof(RecorderCtx));
    if (!ctx)
        return 0;
    memset(ctx, 0, sizeof(RecorderCtx));

    RecordSettings settings;
    Params_GetRecordSettings(&settings);

    int flags = AV_RDONLY;

    if (settings.pre_record_time > 0)
        flags |= AV_OLDEST;

    int64_t start_ms = get_time() - settings.pre_record_time * 1000;

    ctx->streams[0].stream_id = 0;
    ctx->streams[1].stream_id = 1;

    ctx->streams[0].start_ms = start_ms;
    ctx->streams[1].start_ms = start_ms;

    ctx->streams[0].reader = LiveStream_CreateReader(0, start_ms);
    ctx->streams[1].reader = LiveStream_CreateReader(1, start_ms);

    if (settings.duration > 0) {
        ctx->streams[0].stop_ms = get_time() + settings.duration * 1000;
        ctx->streams[1].stop_ms = get_time() + settings.duration * 1000;
    }

    LogV(TAG, "start recording success");

    return (long)ctx;
}

static int rec_close(long handle)
{
    RecorderCtx *ctx = (RecorderCtx *)handle;
    LiveStream_DestroyReader(ctx->streams[0].reader);
    LiveStream_DestroyReader(ctx->streams[1].reader);
    free(ctx);

    return 0;
}

static void *stop_record(void *arg)
{
    pthread_detach(pthread_self());

    pthread_mutex_lock(&lock);
    if (rec_chan) {
        RecMgr_CloseChan(rec_chan);
        if (record_aborted_cb)
            record_aborted_cb(record_aborted_priv_data);
        rec_chan = NULL;
    }
    pthread_mutex_unlock(&lock);

    return NULL;
}

static void asyn_close_channel(void)
{
    pthread_t tid;
    pthread_create(&tid, NULL, stop_record, NULL);
}

static int rec_read(long handle, int stream_id, RecFrame *frame)
{
    RecorderCtx *ctx = (RecorderCtx *)handle;

    Stream *st = (stream_id == 0) ? &ctx->streams[0] : &ctx->streams[1];

    StreamInfo info;

    struct iovec iov[2];
    iov[0].iov_base = &info;
    iov[0].iov_len = sizeof(StreamInfo);

    iov[1].iov_base = frame->data;
    iov[1].iov_len = sizeof(frame->data);

    int ret = LiveStream_ReadV(st->reader, iov, 2, 1000);
    if (ret <= 0)
        return ret;

    if (st->stop_ms > 0 && info.time_ms > st->stop_ms) {
        asyn_close_channel();
        return 0;
    }

    if (info.frame_type == FRAME_TYPE_I)
        st->got_keyframe = 1;

    /* some player needs key frame to be the first frame */
    if (st->got_keyframe == 0)
        return 0;

    if (info.media == MEDIA_TYPE_VIDEO) {
        frame->media = MEDIA_TYPE_VIDEO;
        frame->codec = CODEC_TYPE_H264;
        frame->pts = info.pts;
        frame->size = ret - sizeof(StreamInfo);
        frame->keyframe = (info.frame_type == FRAME_TYPE_I);
    } else if (info.media == MEDIA_TYPE_AUDIO) {
        frame->media = MEDIA_TYPE_AUDIO;
        frame->codec = CODEC_TYPE_AAC;
        frame->pts = info.pts;
        frame->size = ret - sizeof(StreamInfo);
    } else
        return 0;

    return 1;
}

#ifndef USE_SIMULATE_STREAM
typedef struct {
    uint8_t *buf;
    int buf_len;
    int picsize;

    JFlag *wait_flag;
} SnapCtx;

static int photo_cb(VideoFrame *frame, void *priv_data)
{
    SnapCtx *ctx = priv_data;

    if (ctx->buf_len < frame->size) {
        LogV(TAG, "too big jpeg file");
        return -1;
    }

    memcpy(ctx->buf, frame->data, frame->size);
    ctx->picsize = frame->size;

    j_flag_set(ctx->wait_flag, 0x01);

#if 0
    if (frame->last_frame)  /* end of capture */
        handle_event(CAM_EV_MODE_CAP_SAVED, NULL);
#endif

    return 0;
}

static int rec_snapshot(uint8_t *buf, int len, int *width, int *heigh)
{
    SnapCtx *ctx = malloc(sizeof(SnapCtx));
    if (!ctx)
        return -1;
    memset(ctx, 0, sizeof(SnapCtx));

    ctx->wait_flag = j_flag_alloc();

    ctx->buf = buf;
    ctx->buf_len = len;
    ctx->picsize = -1;

    VideoEncoderConfiguration vencCfg;
    Params_GetVideoEncoderConfiguration(1, &vencCfg);

    PhotoEncodeConf photoCfg = {
        .width = 1280,
        .height = 720,
        .qfactor = 90,
    };

    PhotoStream *st = MediaApi_OpenPhotoStream(1, &photoCfg, photo_cb, ctx);
    if (!st) {
        free(ctx);
        return -1;
    }

    PhotoStream_Start(st, 1);

    /* wait snap done */
    j_flag_timedwait(ctx->wait_flag, 0x01, 1000);

    PhotoStream_Stop(st);
    MediaApi_ClosePhotoStream(st);

    int size = ctx->picsize;

    j_flag_free(ctx->wait_flag);
    free(ctx);

    return size;
}
#endif

static int rec_video_info(long handle, int stream_id, VideoInfo *info)
{
    RecorderCtx *ctx = (RecorderCtx *)handle;

    VideoEncoderConfiguration video_cfg;
    if (Params_GetVideoEncoderConfiguration(stream_id, &video_cfg) < 0)
        return 0;

    info->codec = video_cfg.codec_type;
    info->time_base = 90000;

    info->extrasize = get_extradata(&ctx->streams[stream_id],
                                    info->extradata,
                                    sizeof(info->extradata),
                                    &info->width,
                                    &info->height);
    if (info->extrasize < 0) {
        LogV(TAG, "cannot get extradata from stream 0");
        info->extrasize = 0;
        return 0;
    }

    return 1;   /* have video stream */
}

static int rec_audio_info(long handle, int stream_id, AudioInfo *info)
{
    AudioEncoderConfiguration audio_cfg;
    if (Params_GetAudioEncoderConfiguration(&audio_cfg) < 0)
        return 0;

    if (audio_cfg.enable == 0)
        return 0;

    info->codec = audio_cfg.codec_type;
    info->nb_channels = audio_cfg.channels;
    info->time_base = audio_cfg.sample_rate;
    info->sample_rate = audio_cfg.sample_rate;
    info->frame_size = audio_cfg.samples_per_frame;  /* FIXME */

    return 1;
}

static RecOps rec_ops = {
    .get_video_info = rec_video_info,
    .get_audio_info = rec_audio_info,

    .open     = rec_open,
    .close    = rec_close,
    .read     = rec_read,
#ifndef USE_SIMULATE_STREAM
    .snapshot = rec_snapshot,
#else
    .snapshot = NULL,
#endif
};

static int rec_event(RecEvent *ev)
{
    LogV(TAG, "receive record event: %d", ev->event);

    Event event;
    memset(&event, 0, sizeof(Event));

    switch (ev->event) {
    case REC_EVENT_VOLUME_ADDED:
        event.event_type = EVENT_TYPE_STORAGE_ADDED;
        LogV(TAG, "volume %s added", ev->event_data.volume);
        break;
    case REC_EVENT_VOLUME_REMOVED:
        event.event_type = EVENT_TYPE_STORAGE_REMOVED;
        LogV(TAG, "volume %s removed", ev->event_data.volume);
        break;
    case REC_EVENT_RECORD_STARTED:
        event.event_type = EVENT_TYPE_VIDEO_RECORDING_STARTED;
        event.event_data.recording_stopped.state = RECORD_STATE_SUCCESS;

        strncpy(event.event_data.recording_started.snapshot,
                ev->event_data.record_event.snapshot,
                sizeof(event.event_data.recording_started.snapshot) - 1);
        break;
    case REC_EVENT_RECORD_STOPPED:
        event.event_type = EVENT_TYPE_VIDEO_RECORDING_STOPPED;
        event.event_data.recording_stopped.state = RECORD_STATE_SUCCESS;
        break;
    case REC_EVENT_RECORD_ABORTED:
        event.event_type = EVENT_TYPE_VIDEO_RECORDING_STOPPED;
        event.event_data.recording_stopped.state = RECORD_STATE_FAILED;
        asyn_close_channel();
        break;
    default:
        LogV(TAG, "unknown record event %d", ev->event);
        return 0;
    }

    Event_Dispatch(&event);

    return 0;
}

int Recorder_Init(void)
{
    RecordSettings settings;
    Params_GetRecordSettings(&settings);

    RecConf conf = {
        .recfmt = settings.recfmt,
        .max_rec_size = settings.max_record_size * 1024 * 1024LL,
    };

    return RecMgr_Init(&conf, rec_event);
}

int Recorder_Quit(void)
{
    return RecMgr_Quit();
}

int Recorder_StartRecord(int (*record_aborted)(void *priv_data), void *priv_data)
{
    pthread_mutex_lock(&lock);
    if (rec_chan) {
        LogV(TAG, "Record Channel Allready Opened");
        pthread_mutex_unlock(&lock);
        return -1;
    }

    record_aborted_cb = record_aborted;
    record_aborted_priv_data = priv_data;

    rec_chan = RecMgr_OpenChan(&rec_ops, NULL);
    if (!rec_chan) {
        LogV(TAG, "Open Record Channel Failed");
        pthread_mutex_unlock(&lock);
        return -1;
    }
    pthread_mutex_unlock(&lock);

    LogV(TAG, "Open Record Channel Success");

    return 0;
}

int Recorder_StopRecord(void)
{
    pthread_mutex_lock(&lock);
    if (rec_chan) {
        RecMgr_CloseChan(rec_chan);
        rec_chan = NULL;
    }
    pthread_mutex_unlock(&lock);

    return 0;
}

int Recorder_SetConf(RecConf *conf)
{
    pthread_mutex_lock(&lock);
    int ret = RecMgr_SetConf(conf);
    pthread_mutex_unlock(&lock);

    return ret;
}
