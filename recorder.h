#ifndef __RECORDER_H__
#define __RECORDER_H__

#include "recmgr.h"

int Recorder_Init(void);
int Recorder_Quit(void);

int Recorder_StartRecord(int (*record_aborted)(void *priv_data), void *priv_data);
int Recorder_StopRecord(void);

int Recorder_SetConf(RecConf *conf);

#endif
