#ifndef __STORAGE_H__
#define __STORAGE_H__

int Storage_Init(void);
int Storage_Quit(void);

int Storage_GetStatus(StorageStatus *status);

int Storage_Format(void);

int Storage_HaveEnoughSpace(void);

#endif
