/******************************************************************
 * Copyright(C), 2013,Santachi Video Technology (Shenzhen) Co.,Ltd.
 ******************************************************************
 * File Name    : hash.h
 * Description  : simple hash table implementation
 * Author       : liujingwen <jingwen.liu@santachi.com.cn>
 * Date         : 2013-08-30
 ******************************************************************/

#ifndef __HASH_H__
#define __HASH_H__

typedef struct JHashTable JHashTable;

JHashTable *j_hash_create(int nb_slots, int with_lock);
int j_hash_destroy(JHashTable *tbl);

int   j_hash_set(JHashTable *tbl, uint64_t key, void *value);
void *j_hash_del(JHashTable *tbl, uint64_t key);
void *j_hash_get(JHashTable *tbl, uint64_t key);

void j_hash_foreach(JHashTable *tbl, void (*func)(uint64_t key, void *data, void *ctx), void *ctx);

#endif
