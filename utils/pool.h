#ifndef __POOL_H__
#define __POOL_H__

#include <unistd.h>
#include <sys/uio.h>

#include "fifo.h"

typedef struct JPool {
	int size;
	JFifo *fifo;

	int auto_cover;

	pthread_cond_t cond;
	pthread_mutex_t lock;
} JPool;

JPool *j_pool_alloc(int size, int auto_cover);
void j_pool_free(JPool *pool);

ssize_t j_pool_read (JPool *pool, void *buf, size_t buf_size, int milliseconds);
ssize_t j_pool_write(JPool *pool, void *buf, size_t buf_size, int milliseconds);

ssize_t j_pool_readv (JPool *pool, struct iovec *iov, int iovcnt, int milliseconds);
ssize_t j_pool_writev(JPool *pool, struct iovec *iov, int iovcnt, int milliseconds);

int j_pool_reset(JPool *pool);

#endif /* __POOL_H__ */
