/******************************************************************
 * File Name    : list.h
 * Description  : link list implementation
 * Author       : William Liu <liujingwencn@gmail.com>
 * Date         : 2011-11-14
 ******************************************************************/

#ifndef __LIST_H__
#define __LIST_H__

#include "mempool.h"

typedef struct JList JList;

typedef struct JListNode {
	void             *data;
	struct JListNode *prev;
	struct JListNode *next;
} JListNode;

typedef int  (*JLFindFunc)(void *data, void *priv_data);
typedef void (*JLFreeFunc)(void *data, void *priv_data);
typedef void (*JLIterFunc)(void *data, void *priv_data);

JList *j_list_alloc(void);
void j_list_free(JList *list);

int j_list_length(JList *list);
int j_list_empty(JList *list);

int j_list_append(JList *list, void *data);
int j_list_prepend(JList *list, void *data);

JListNode *j_list_find(JList *list, void *data);
void *j_list_find_custom(JList *list, JLFindFunc func, void *priv_data);

int j_list_remove(JList *list, void *data);
int j_list_remove_full(JList *list, JLFreeFunc func, void *priv_data);
void *j_list_remove_last(JList *list);

int j_list_foreach(JList *list, JLIterFunc func, void *priv_data);

JListNode *j_list_first(JList *list);
JListNode *j_list_last(JList *list);
JListNode *j_list_next(JList *list, JListNode *prev);

#endif /* __LIST_H__ */
