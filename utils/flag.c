#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>

#include <sys/time.h>

#include "flag.h"

JFlag *j_flag_alloc(void)
{
	JFlag *f = malloc(sizeof(JFlag));
	if (!f)
		return NULL;
	memset(f, 0, sizeof(JFlag));

	pthread_mutex_init(&f->lock, NULL);
	pthread_cond_init(&f->cond, NULL);

	return f;
}

int j_flag_free(JFlag *f)
{
	pthread_mutex_destroy(&f->lock);
	pthread_cond_destroy(&f->cond);
	free(f);

	return 0;
}

int j_flag_set(JFlag *f, int bit)
{
	if (bit >= sizeof(f->mask) * 8)
		return -1;

	pthread_mutex_lock(&f->lock);
	f->mask |= (1 << bit);
	pthread_cond_broadcast(&f->cond);
	pthread_mutex_unlock(&f->lock);

	return 0;
}

int j_flag_clr(JFlag *f, int bit)
{
	if (bit >= sizeof(f->mask) * 8)
		return -1;

	pthread_mutex_lock(&f->lock);
	f->mask &= ~(1 << bit);
	pthread_cond_broadcast(&f->cond);
	pthread_mutex_unlock(&f->lock);

	return 0;
}

int j_flag_get(JFlag *f, int bit)
{
	if (bit >= sizeof(f->mask) * 8)
		return -1;

	pthread_mutex_lock(&f->lock);
	int val = !!(f->mask & (1 << bit));
	pthread_mutex_unlock(&f->lock);

	return val;
}

int j_flag_wait(JFlag *f, int mask)
{
	pthread_mutex_lock(&f->lock);
	while (f->mask != mask)
		pthread_cond_wait(&f->cond, &f->lock);
	pthread_mutex_unlock(&f->lock);

	return 0;
}

#define NSEC_PER_SEC    (1000 * 1000 * 1000)

static int time_after(struct timespec *tp, int milliseconds)
{
#if 0
    clock_gettime(CLOCK_REALTIME, tp);
#else
    struct timeval tv;
    gettimeofday(&tv, NULL);
    tp->tv_sec  = tv.tv_sec;
    tp->tv_nsec = tv.tv_usec * 1000;
#endif

    tp->tv_sec  += milliseconds / 1000;
    tp->tv_nsec += milliseconds % 1000 * 1000000;

    if (tp->tv_nsec >= NSEC_PER_SEC) {
        tp->tv_nsec -= NSEC_PER_SEC;
        tp->tv_sec  += 1;
    }

    return 0;
}

inline static int wait_signal(JFlag *f, struct timespec *tp)
{
    int ret;
again:
    ret = pthread_cond_timedwait(&f->cond, &f->lock, tp);
    if (ret == ETIMEDOUT)
        return 0;
    else if (ret == EINTR)
        goto again;

    return 1;
}

int j_flag_timedwait(JFlag *f, int mask, int milliseconds)
{
    struct timespec timeout;

    time_after(&timeout, milliseconds);

	pthread_mutex_lock(&f->lock);
	while (f->mask != mask) {
        if (wait_signal(f, &timeout) == 0) {
            pthread_mutex_unlock(&f->lock);
            return 0;
        }
    }
	pthread_mutex_unlock(&f->lock);

    return 1;
}

//#define TEST

#ifdef TEST
int main(void)
{
	return 0;
}
#endif
