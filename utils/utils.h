#ifndef __LIBUTILS_UTILS_H__
#define __LIBUTILS_UTILS_H__

#include <stdlib.h>
#include <sys/time.h>

//#include "config.h"

inline static struct timespec time_after(int milliseconds)
{
	struct timespec tp;
#ifdef HAVE_CLOCK_GETTIME
	clock_gettime(CLOCK_REALTIME, &tp);
#else
	struct timeval tv;
	gettimeofday(&tv, NULL);

	tp.tv_sec = tv.tv_sec;
	tp.tv_nsec = tv.tv_usec * 1000;
#endif

	int64_t nsec = tp.tv_nsec + (int64_t)milliseconds * 1000000;

	tp.tv_sec  += nsec / 1000000000;
	tp.tv_nsec  = nsec % 1000000000;

	return tp;
}

inline static int64_t gettime_ms(void)
{
#ifdef HAVE_CLOCK_GETTIME
	struct timespec tp;
	clock_gettime(CLOCK_MONOTONIC, &tp);
	return (int64_t)tp.tv_sec * 1000 + (int64_t)tp.tv_nsec / 1000000;
#else
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (int64_t)tv.tv_sec * 1000 + (int64_t)tv.tv_usec / 1000;
#endif
}
#endif
