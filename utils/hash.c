/******************************************************************
 * Copyright(C), 2013,Santachi Video Technology (Shenzhen) Co.,Ltd.
 ******************************************************************
 * File Name    : hash.c
 * Description  : simple hash table implementation
 * Author       : liujingwen <jingwen.liu@santachi.com.cn>
 * Date         : 2013-08-30
 ******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <pthread.h>

#include "mempool.h"
#include "hash.h"

typedef struct HashNode {
	uint64_t key;
	void *value;

	struct HashNode *next;
} HashNode;

typedef struct HashSlot {
	HashNode *first_node;
} HashSlot;

struct JHashTable {
	int num_slots;
	HashSlot *slots;

	JMemPool *mempool;

    int with_lock;
    pthread_mutex_t lock;
};

#define slot_index(tbl, key)	((key) % (tbl)->num_slots)
#define hslot(tbl, key)	(&(tbl)->slots[slot_index(tbl, key)])

inline static void hash_lock(JHashTable *tbl)
{
    if (tbl->with_lock)
        pthread_mutex_lock(&tbl->lock);
}

inline static void hash_unlock(JHashTable *tbl)
{
    if (tbl->with_lock)
        pthread_mutex_unlock(&tbl->lock);
}

static void slot_node_insert(HashSlot *slot, HashNode *node)
{
	node->next = slot->first_node;
	slot->first_node = node;
}

static HashNode *slot_node_lookup(HashSlot *slot, uint64_t key)
{
	HashNode *node;

	for (node = slot->first_node; node; node = node->next) {
		if (node->key == key)
			return node;
	}

	return NULL;
}

static HashNode *slot_node_remove(HashSlot *slot, uint64_t key)
{
	HashNode **np, *n1;

	np = &slot->first_node;
	while ((*np) != NULL) {
		n1 = *np;
		if (n1->key == key) {
			*np = n1->next;
			return n1;
		} else
			np = &n1->next;
	}

	return NULL;
}

JHashTable *j_hash_create(int nb_slots, int with_lock)
{
	JHashTable *tbl = malloc(sizeof(JHashTable));
	if (!tbl)
		return NULL;
	memset(tbl, 0, sizeof(JHashTable));

	tbl->slots = calloc(nb_slots, sizeof(HashSlot));
	if (!tbl->slots)
		goto fail;

	tbl->mempool = j_mempool_create(sizeof(HashNode), 128, 0);
	if (!tbl->mempool)
		goto fail;

	tbl->num_slots = nb_slots;

    tbl->with_lock = with_lock;

    pthread_mutex_init(&tbl->lock, NULL);

	return tbl;

fail:
	j_hash_destroy(tbl);
	return NULL;
}

int j_hash_destroy(JHashTable *tbl)
{
	if (!tbl)
		return -1;

	if (tbl->mempool)
		j_mempool_destroy(tbl->mempool);

	if (tbl->slots)
		free(tbl->slots);

    pthread_mutex_destroy(&tbl->lock);

	free(tbl);

	return 0;
}

int j_hash_set(JHashTable *tbl, uint64_t key, void *value)
{
	HashNode *node;
	HashSlot *slot;

	if (!tbl || !value)
		return -1;

    hash_lock(tbl);

	node = j_mempool_alloc(tbl->mempool);
	if (!node) {
        hash_unlock(tbl);
		return -1;
    }
	memset(node, 0, sizeof(HashNode));

	node->key = key;
	node->value = value;

	slot = hslot(tbl, key);

	slot_node_insert(slot, node);

    hash_unlock(tbl);

	return 0;
}

void *j_hash_del(JHashTable *tbl, uint64_t key)
{
	HashNode *node;
	HashSlot *slot;

	if (!tbl)
		return NULL;

    hash_lock(tbl);

	slot = hslot(tbl, key);

	node = slot_node_remove(slot, key);
	if (!node) {
        hash_unlock(tbl);
		printf("ERROR: key %llu not exist!\n", key);
		return NULL;
	}

	void *val = node->value;

	j_mempool_release(tbl->mempool, node);

    hash_unlock(tbl);

	return val;	/* cache it */
}

void *j_hash_get(JHashTable *tbl, uint64_t key)
{
	HashNode *node;
	HashSlot *slot;

	if (!tbl)
		return NULL;

    hash_lock(tbl);

	slot = hslot(tbl, key);

	node = slot_node_lookup(slot, key);
	if (node) {
        hash_unlock(tbl);
		return node->value;
    }

    hash_unlock(tbl);

	return NULL;
}

void j_hash_foreach(JHashTable *tbl, void (*func)(uint64_t key, void *data, void *ctx), void *ctx)
{
	int i;
	HashSlot *slot;
	HashNode *node, *node_next;

    hash_lock(tbl);

	for (i = 0; i < tbl->num_slots; i++) {
		slot = &tbl->slots[i];
		for (node = slot->first_node; node; node = node_next) {
			node_next = node->next;
			if (func)
				func(node->key, node->value, ctx);
		}
	}

    hash_unlock(tbl);
}

#ifdef TEST
typedef struct Sample {
	int i;
} Sample;

int main(void)
{
	Sample samples[1024];
	JHashTable *tbl = j_hash_create(1024);
	if (!tbl)
		return -1;

	int i;
	for (i = 0; i < 1024; i++) {
		samples[i].i = i;
		if (j_hash_set(tbl, i, &samples[i]) < 0)
			abort();
	}

	for (i = 0; i < 1026; i++) {
		Sample *sample = j_hash_get(tbl, i);
		if (!sample) {
			printf("node %d not found\n", i);
			continue;
		}
		printf("i = %d\n", sample->i);

		j_hash_del(tbl, i);
	}

	for (i = 0; i < 1024; i++) {
		Sample *sample = j_hash_get(tbl, i);
		if (!sample) {
			printf("node %d not found\n", i);
			continue;
		}
		printf("i = %d\n", sample->i);
	}

	j_hash_destroy(tbl);

	return 0;
}
#endif
