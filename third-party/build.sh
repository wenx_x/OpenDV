#! /bin/bash

BUILD=$PWD/build
TARGET=arm-hisiv600-linux
export CROSS_COMPILE=${TARGET}-
STAGING=$PWD/../staging

export THIRD_PARTY=$PWD

export CFLAGS="-Wall -I$STAGING/include"
export LDFLAGS=-L$STAGING/lib


echo $CFLAGS
echo $LDFLAGS

mkdir $BUILD 2> /dev/null
mkdir -p $STAGING/include 2> /dev/null
mkdir -p $STAGING/lib 2> /dev/null

function make_ffmpeg() {
    mkdir $BUILD/ffmpeg 2> /dev/null

    pushd $BUILD/ffmpeg
    $THIRD_PARTY/ffmpeg/configure  \
        --prefix=$STAGING                \
        --disable-decoders               \
        --disable-encoders               \
        --disable-hwaccels               \
        --disable-muxers                 \
        --disable-demuxers               \
        --disable-devices                \
        --disable-filters                \
        --disable-parsers                \
        --disable-protocols              \
        --disable-bsfs                   \
        --enable-gpl                     \
        --enable-cross-compile           \
        --cross-prefix=${CROSS_COMPILE}  \
        --arch=arm                       \
        --target-os=linux                \
        --enable-muxer=mp4               \
        --enable-muxer=mov               \
        --enable-protocol=file           \
        --disable-ffserver               \
        --disable-ffplay                 \
        --disable-ffprobe                \
        --disable-ffmpeg                 \
        --disable-swresample             \
        --disable-avdevice               \
        --disable-postproc               \
        --disable-swscale                \
        --enable-bsf=aac_adtstoasc       \
        --enable-bsf=hevc_mp4toannexb    \
        --enable-bsf=h264_mp4toannexb    \
        --disable-iconv                  \
        --disable-xlib                   \
        --enable-demuxer=mov

    make && make install
    popd
}

function make_jsonc() {
    mkdir $BUILD/json-c 2> /dev/null

    pushd $BUILD/json-c
    echo "ac_cv_func_malloc_0_nonnull=yes" > $HOST.cache
    echo "ac_cv_func_realloc_0_nonnull=yes" >> $HOST.cache

    $THIRD_PARTY/json-c/configure --prefix=$STAGING --enable-shared=no --cache-file=$HOST.cache --build=$MACHTYPE --host=$TARGET
    make && make install
    popd
}

function make_libmicrohttpd() {
    mkdir $BUILD/libmicrohttpd 2> /dev/null

    pushd $BUILD/libmicrohttpd
    $THIRD_PARTY/libmicrohttpd/configure --prefix=$STAGING --build=$MACHTYPE --host=$TARGET --enable-shared=no
    make && make install
    popd
}

function make_zlib() {
    cp -r $THIRD_PARTY/zlib $BUILD/zlib

    pushd $BUILD/zlib
    CHOST=$TARGET ./configure --prefix=$STAGING
    make && make install
    popd
}

function make_utils_linux() {
    mkdir $BUILD/util-linux 2> /dev/null

    pushd $BUILD/util-linux
    $THIRD_PARTY/util-linux/configure --prefix=$STAGING --build=$MACHTYPE --host=$TARGET --disable-all-programs --enable-libuuid --enable-shared=no
    make && make install
    popd
}

function make_parted() {
    mkdir $BUILD/parted 2> /dev/null

    pushd $BUILD/parted
    $THIRD_PARTY/parted/configure --prefix=$STAGING --build=$MACHTYPE --host=$TARGET --enable-shared=no --without-readline --disable-device-mapper
    make
    make install
    popd
}

function make_x264() {
    mkdir $BUILD/x264 2> /dev/null

    pushd $BUILD/x264
    $THIRD_PARTY/x264/configure --prefix=$STAGING --host=$TARGET --cross-prefix=$CROSS_COMPILE --enable-static --disable-asm --disable-cli --disable-opencl
    make && make install-lib-static
    popd
}

function make_freetype() {
    mkdir $BUILD/freetype 2> /dev/null

    pushd $BUILD/freetype
    $THIRD_PARTY/freetype/configure --prefix=$STAGING --build=$MACHTYPE --host=$TARGET --enable-shared=no
    make && make install
    popd
}

function make_exif() {
    mkdir $BUILD/libexif 2> /dev/null

    pushd $BUILD/libexif
    $THIRD_PARTY/libexif/configure --prefix=$STAGING --build=$MACHTYPE --host=$TARGET --enable-shared=no
    make && make install
    popd
}

function make_curl() {
    mkdir $BUILD/curl 2> /dev/null

    pushd $BUILD/curl
    $THIRD_PARTY/curl/configure --prefix=$STAGING --build=$MACHTYPE --host=$TARGET --enable-shared=no
    make && make install
    popd
}

make_x264
make_ffmpeg
make_jsonc
make_libmicrohttpd
make_utils_linux
make_parted
make_freetype
make_exif
make_curl
