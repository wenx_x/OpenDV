#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdint.h>

typedef enum {
    MEDIA_TYPE_VIDEO = 0,
    MEDIA_TYPE_AUDIO = 1,
    MEDIA_TYPE_IMAGE = 2,
} MediaType;

typedef enum {
    CODEC_TYPE_NONE = 0,
    CODEC_TYPE_H264 = 1,
    CODEC_TYPE_H265 = 2,
    CODEC_TYPE_AAC  = 3,
    CODEC_TYPE_PCMU = 4,
    CODEC_TYPE_PCMA = 5,

    CODEC_TYPE_JPEG = 6,
} CodecType;

typedef enum {
    FRAME_TYPE_UNKNOWN = 0,
    FRAME_TYPE_P = 1,
    FRAME_TYPE_I = 2,
    FRAME_TYPE_B = 3,
} FrameType;

typedef struct {
    MediaType media;
    CodecType codec;
    int64_t pts;
    int64_t time_ms;    /* use gettimeofday */
    FrameType frame_type;
} StreamInfo;

typedef enum {
    CODEC_PROFILE_BASELINE,
    CODEC_PROFILE_MAIN,
    CODEC_PROFILE_HIGH,
} CodecProfile;

typedef struct {
    CodecProfile profile;    /* 0 - baseline, 1 - main, 2 - high */
    CodecType codec_type;

    int width;
    int height;

    int gop;
    int framerate;
    int bitrate;
} VideoEncoderConfiguration;

typedef struct {
    int enable;
    CodecType codec_type;       /* AAC only */
    int bitrate;
    int sample_rate;
    int bit_width;              /* 8 - 8bit, 16 - 16bit, 24 - 24bit */
    int channels;               /* 1 - mono, 2 - stereo */
    int samples_per_frame;      /* 1 - mono, 2 - stereo */
} AudioEncoderConfiguration;

typedef struct {
    uint8_t enable;
    uint8_t font_size;
    uint8_t color;
    uint8_t alpha;
    uint16_t x, y;              /* in permillage, origin is the upper left corner */
    uint8_t time_format;        /* 0 - year-month-day hour:minute:second  */
} OsdTime;

typedef struct {
    uint8_t enable;
    uint8_t font_size;          /* pixel */
    uint8_t color;
    uint8_t alpha;
    uint16_t x, y;              /* in permillage, origin is the upper left corner */
    char text[1024];
} OsdText;

typedef struct {
    OsdTime time;
    OsdText text;
} OsdSettings;

typedef struct {
    int brightness;
    int contrast;
    int saturation;
    int hue;
    int sharpness;
} ImageSettings;

typedef struct {
    int  enable;
    char ssid[128];
    char password[64];
    int  band;   /* 0 - 2.4G, 1 - 5G */
} WifiConfiguration;

typedef enum {
    REC_FMT_MP4,
    REC_FMT_MOV,
    REC_FMT_JPG,
} RecFmt;

typedef struct {
    RecFmt recfmt;
    int pre_record_time;    /* in seconds */
    int max_record_size;    /* in MB      */
    int duration;           /* in seconds */
    int circulate_record;
} RecordSettings;

typedef struct {
    int res_width;
    int res_height;
} PhotoTakingSettings;

typedef enum {
    STORAGE_MEDIA_STATUS_READY,
    STORAGE_MEDIA_STATUS_ERROR,
    STORAGE_MEDIA_STATUS_LOWSPEED,
    STORAGE_MEDIA_STATUS_PROTECTED,
    STORAGE_MEDIA_STATUS_FULL,
    STORAGE_MEDIA_STATUS_NOTEXIST,
    STORAGE_MEDIA_STATUS_UNKNOWN_FILESYSTEM,
} StorageMediaStatus;

typedef struct {
    StorageMediaStatus status;
    int64_t total_size;
    int64_t free_size;
} StorageStatus;

typedef enum {
    CAMERA_MODE_IDLE,
    CAMERA_MODE_RECORD,
    CAMERA_MODE_CAPTURE,
    CAMERA_MODE_PLAYBACK,
    CAMERA_MODE_USERCONF,
} CameraMode;

typedef struct {
    CodecType codec;

    int time_base;  /* just like 90000 */

    int width;
    int height;

    int framerate;

    int64_t duration;   /* in millisecond */

    int extrasize;
    uint8_t extradata[4096];
} VideoInfo;

typedef struct {
    CodecType codec;

    int time_base;  /* just like 44100 */

    int bit_rate;
    int sample_rate;
    int nb_channels;
    int frame_size;
    int bit_width;

    int64_t duration;   /* in millisecond */

    int extrasize;
    uint8_t extradata[4096];
} AudioInfo;

typedef enum {
    PLAYBACK_CMD_NONE,
    PLAYBACK_CMD_START,
    PLAYBACK_CMD_STOP,
    PLAYBACK_CMD_PAUSE,
    PLAYBACK_CMD_RESUME,
    PLAYBACK_CMD_SPEED,
    PLAYBACK_CMD_SEEK,
} PlaybackCmd;

typedef enum {
    STREAM_INFO_TYPE_VPS,
    STREAM_INFO_TYPE_SPS,
    STREAM_INFO_TYPE_PPS,
} StreamInfoType;

typedef struct {
    char filename[128];
    int64_t filesize;
    char datetime[20];

    int index;
} FileItem;

typedef enum {
    EVENT_TYPE_PHOTO_TAKING_STARTED    = 0x00000001,
    EVENT_TYPE_PHOTO_TAKING_STOPPED    = 0x00000002,
    EVENT_TYPE_PHOTO_TAKEN             = 0x00000004,

    EVENT_TYPE_VIDEO_RECORDING_STARTED = 0x00000008,
    EVENT_TYPE_VIDEO_RECORDING_STOPPED = 0x00000010,

    EVENT_TYPE_UPGRADE_STARTED         = 0x00000020,
    EVENT_TYPE_UPGRADE_STOPPED         = 0x00000040,

    EVENT_TYPE_STORAGE_OUT_OF_SPACE    = 0x00000080,

    EVENT_TYPE_LOW_BATTERY_LEVEL       = 0x00000100,

    EVENT_TYPE_STORAGE_STATUS          = 0x00000200,
    EVENT_TYPE_STORAGE_ADDED           = 0x00000400,
    EVENT_TYPE_STORAGE_REMOVED         = 0x00000800,
} EventType;

typedef struct {
    int battery_level;      /* 0 - 100 */
} LowBatteryLevelEvent;

typedef struct {
    char filepath[1024];
    int filesize;
} PhotoTakenEvent;

typedef enum {
    RECORD_STATE_SUCCESS,
    RECORD_STATE_WRITE_ERROR,
    RECORD_STATE_NO_MORE_BUFFER,
    RECORD_STATE_NO_MORE_SPACE,
    RECORD_STATE_FAILED,
    RECORD_STATE_NO_SDCARD,
} RecordState;

typedef struct {
    RecordState state;
    char filepath[1024];
    char thumbnail[1024];
} VideoRecordingStoppedEvent;

typedef struct {
    char snapshot[1024];
} VideoRecordingStartedEvent;

typedef struct {
//    StorageMediaStatus status;
} StorageEvent;

typedef struct {
    EventType event_type;

    union {
        PhotoTakenEvent photo_taken;
        VideoRecordingStartedEvent recording_started;
        VideoRecordingStoppedEvent recording_stopped;
//        StorageEvent storage_event;
        LowBatteryLevelEvent low_battery_level;
    } event_data;
} Event;

#endif /* __COMMON_H__ */
