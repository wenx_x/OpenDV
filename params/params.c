#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "params.h"
#include "camera_fsm.h"
#include "json-c/json.h"

#define min(x, y)    ((x) < (y) ? (x) : (y))
#define NELEMS(x)   (sizeof(x) / sizeof((x)[0]))

typedef struct {
    WifiConfiguration wificonf;
    VideoEncoderConfiguration video_encoder[2];
    AudioEncoderConfiguration audio_encoder;
    ImageSettings image_settings;
    RecordSettings record_settings;
    PhotoTakingSettings phototaking_settings;
    OsdSettings osd_settings;
    CameraMode camera_mode;
} GlobalParams;

static GlobalParams gblparams = {
    .wificonf = {
        .enable = 1,
        .ssid = "mywifi",
        .password = "12345678",
        .band = 0,
    },

    .video_encoder[0] = {
        .profile = CODEC_PROFILE_HIGH,
        .codec_type = CODEC_TYPE_H264,
        .width = 3840,
        .height = 2160,
        .gop = 30,
        .framerate = 30,
        .bitrate = 16000000,
    },

    .video_encoder[1] = {
        .profile = CODEC_PROFILE_HIGH,
        .codec_type = CODEC_TYPE_H264,
        .width = 1280,
        .height = 720,
        .gop = 30,
        .framerate = 30,
        .bitrate = 2000000,
    },

    .audio_encoder = {
        .enable = 0,
        .codec_type = CODEC_TYPE_AAC,
        .bitrate = 48000,
        .sample_rate = 48000,
        .bit_width = 16,
        .channels = 2,
        .samples_per_frame = 1024,
    },

    .image_settings = {
        .brightness = 1,
        .contrast = 1,
        .saturation = 1,
        .hue = 1,
        .sharpness = 1,
    },

    .record_settings = {
        .recfmt = REC_FMT_MP4,
        .pre_record_time = 0,
        .max_record_size = 512,
        .circulate_record = 1,
        .duration = 0,
    },

    .phototaking_settings = {
        .res_width = 3840,
        .res_height = 2160,
    },

    .osd_settings = {
        .time = {
            .enable = 1,
            .font_size = 48,
            .color = 0x00,
            .alpha = 0xff,
            .x = 1,
            .y = 1,
            .time_format = 0,
        },
        .text = {
            .enable = 1,
            .font_size = 48,
            .color = 0x00,
            .alpha = 0xff,
            .x = 1000,
            .y = 1000,
            .text = "Hello, World!",
        },
    },
    .camera_mode = CAMERA_MODE_RECORD,
};

static char param_file[1024] = {0};

static json_object *wificonf_to_json(WifiConfiguration *conf)
{
    json_object *result = json_object_new_object();
    json_object_object_add(result, "enable",   json_object_new_int(conf->enable));
    json_object_object_add(result, "ssid",     json_object_new_string(conf->ssid));
    json_object_object_add(result, "password", json_object_new_string(conf->password));
    json_object_object_add(result, "band",     json_object_new_int(conf->band));

    return result;
}

static int json_to_wificonf(json_object *params, WifiConfiguration *conf)
{
    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "enable") == 0) {
            conf->enable = json_object_get_int(val);
        } else if (strcmp(key, "ssid") == 0) {
            strncpy(conf->ssid, json_object_get_string(val), sizeof(conf->ssid) - 1);
        } else if (strcmp(key, "password") == 0) {
            strncpy(conf->password, json_object_get_string(val), sizeof(conf->password) - 1);
        } else if (strcmp(key, "band") == 0) {
            conf->band = json_object_get_int(val);
        }
    }

    return 0;
}

static json_object *videoencoder_to_json(VideoEncoderConfiguration *conf)
{
    json_object *result = json_object_new_object();

    switch (conf->profile) {
    case CODEC_PROFILE_BASELINE:
        json_object_object_add(result, "profile", json_object_new_string("baseline"));
        break;
    case CODEC_PROFILE_MAIN:
        json_object_object_add(result, "profile", json_object_new_string("main"));
        break;
    case CODEC_PROFILE_HIGH:
        json_object_object_add(result, "profile", json_object_new_string("high"));
        break;
    default:
        printf("unknown profile %d\n", conf->profile);
        return NULL;
    }

    switch (conf->codec_type) {
    case CODEC_TYPE_H264:
        json_object_object_add(result, "codec_type", json_object_new_string("h264"));
        break;
    case CODEC_TYPE_H265:
        json_object_object_add(result, "codec_type", json_object_new_string("h265"));
        break;
    default:
        printf("unknown codec %d\n", conf->codec_type);
        return NULL;
    }

    json_object_object_add(result, "width",      json_object_new_int(conf->width));
    json_object_object_add(result, "height",     json_object_new_int(conf->height));
    json_object_object_add(result, "gop",        json_object_new_int(conf->gop));
    json_object_object_add(result, "framerate", json_object_new_int(conf->framerate));
    json_object_object_add(result, "bitrate",    json_object_new_int(conf->bitrate));

    return result;
}

static int get_profile_id(const char *profile)
{
    if (strcmp(profile, "baseline") == 0)
        return CODEC_PROFILE_BASELINE;
    else if (strcmp(profile, "main") == 0)
        return CODEC_PROFILE_MAIN;
    else if (strcmp(profile, "high") == 0)
        return CODEC_PROFILE_HIGH;

    return -1;
}

static int get_codec_type(const char *codec_type)
{
    if (strcmp(codec_type, "h264") == 0)
        return CODEC_TYPE_H264;
    else if (strcmp(codec_type, "h265") == 0)
        return CODEC_TYPE_H265;
    return -1;
}

static int json_to_videoencoder(json_object *params, VideoEncoderConfiguration *conf)
{
    if (!params)
        return -1;

    const char *profile = NULL;
    const char *codec_type = NULL;

    int width = -1;
    int height = -1;

    int gop = -1;
    int framerate = -1;
    int bitrate = -1;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "profile") == 0) {
            profile = json_object_get_string(val);
        } else if (strcmp(key, "codec_type") == 0) {
            codec_type = json_object_get_string(val);
        } else if (strcmp(key, "width") == 0) {
            width = json_object_get_int(val);
        } else if (strcmp(key, "height") == 0) {
            height = json_object_get_int(val);
        } else if (strcmp(key, "gop") == 0) {
            gop = json_object_get_int(val);
        } else if (strcmp(key, "framerate") == 0) {
            framerate = json_object_get_int(val);
        } else if (strcmp(key, "bitrate") == 0) {
            bitrate = json_object_get_int(val);
        }
    }

    if (profile)
        conf->profile = get_profile_id(profile);
    if (codec_type)
        conf->codec_type = get_codec_type(codec_type);
    if (width > 0)
        conf->width = width;
    if (height > 0)
        conf->height = height;
    if (gop > 0)
        conf->gop = gop;
    if (framerate > 0)
        conf->framerate = framerate;
    if (bitrate > 0)
        conf->bitrate = bitrate;

    return 0;
}

static json_object *recordsettings_to_json(RecordSettings *settings)
{
    json_object *result = json_object_new_object();

    switch (settings->recfmt) {
    case REC_FMT_MOV:
        json_object_object_add(result, "recfmt", json_object_new_string("MOV"));
        break;
    case REC_FMT_MP4:
        json_object_object_add(result, "recfmt", json_object_new_string("MP4"));
        break;
    default:
        printf("unknown recfmt %d\n", settings->recfmt);
        return NULL;
    }

    json_object_object_add(result, "pre_record_time", json_object_new_int(settings->pre_record_time));
    json_object_object_add(result, "max_record_size", json_object_new_int(settings->max_record_size));
    json_object_object_add(result, "circulate_record", json_object_new_int(settings->circulate_record));
    json_object_object_add(result, "duration", json_object_new_int(settings->duration));

    return result;
}

static int json_to_recordsettings(json_object *params, RecordSettings *settings)
{
    if (!params)
        return -1;

    const char *recfmt = NULL;
    int pre_record_time = -1;
    int max_record_size = -1;
    int circulate_record = -1;
    int duration = -1;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "recfmt") == 0) {
            recfmt = json_object_get_string(val);
        } else if (strcmp(key, "pre_record_time") == 0) {
            pre_record_time = json_object_get_int(val);
        } else if (strcmp(key, "max_record_size") == 0) {
            max_record_size = json_object_get_int(val);
        } else if (strcmp(key, "circulate_record") == 0) {
            circulate_record = json_object_get_int(val);
        } else if (strcmp(key, "duration") == 0) {
            duration = json_object_get_int(val);
        }
    }

    if (!recfmt && pre_record_time < 0 && max_record_size < 0 && circulate_record < 0 && duration < 0)
        return -1;

    if (recfmt) {
        if (strcmp(recfmt, "MOV") == 0)
            settings->recfmt = REC_FMT_MOV;
        else if (strcmp(recfmt, "MP4") == 0)
            settings->recfmt = REC_FMT_MP4;
        else
            return -1;
    }

    if (pre_record_time >= 0)
        settings->pre_record_time = pre_record_time;

    if (max_record_size >= 0)
        settings->max_record_size = max_record_size;

    if (circulate_record >= 0)
        settings->circulate_record = circulate_record;

    if (duration >= 0)
        settings->duration = duration;

    return 0;
}

static json_object *phototakingsettings_to_json(PhotoTakingSettings *settings)
{
    json_object *result = json_object_new_object();
    json_object_object_add(result, "res_width",  json_object_new_int(settings->res_width));
    json_object_object_add(result, "res_height", json_object_new_int(settings->res_height));
    return result;
}

static int json_to_phototakingsettings(json_object *params, PhotoTakingSettings *settings)
{
    if (!params)
        return -1;

    int res_width = -1;
    int res_height = -1;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "res_width") == 0) {
            res_width = json_object_get_int(val);
        } else if (strcmp(key, "res_height") == 0) {
            res_height = json_object_get_int(val);
        }
    }

    if (res_width < 0 || res_height < 0)
        return -1;

    settings->res_width = res_width;
    settings->res_height = res_height;

    return 0;
}

static json_object *audioencoder_to_json(AudioEncoderConfiguration *conf)
{
    json_object *result = json_object_new_object();

    json_object_object_add(result, "enable",  json_object_new_int(conf->enable));

    switch (conf->codec_type) {
    case CODEC_TYPE_AAC:
        json_object_object_add(result, "codec_type",  json_object_new_string("aac"));
        break;
    default:
        return NULL;
    }

    json_object_object_add(result, "bitrate",     json_object_new_int(conf->bitrate));
    json_object_object_add(result, "sample_rate", json_object_new_int(conf->sample_rate));
    json_object_object_add(result, "bit_width",   json_object_new_int(conf->bit_width));
    json_object_object_add(result, "channels",    json_object_new_int(conf->channels));
    json_object_object_add(result, "samples_per_frame",    json_object_new_int(conf->samples_per_frame));

    return result;
}

static int json_to_audioencoder(json_object *params, AudioEncoderConfiguration *conf)
{
    if (!params)
        return -1;

    const char *codec_type = NULL;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "enable") == 0) {
            conf->enable = json_object_get_int(val);
        } else if (strcmp(key, "codec_type") == 0) {
            codec_type = json_object_get_string(val);
        } else if (strcmp(key, "bitrate") == 0) {
            conf->bitrate = json_object_get_int(val);
        } else if (strcmp(key, "sample_rate") == 0) {
            conf->sample_rate = json_object_get_int(val);
        } else if (strcmp(key, "bit_width") == 0) {
            conf->bit_width = json_object_get_int(val);
        } else if (strcmp(key, "channels") == 0) {
            conf->channels = json_object_get_int(val);
        } else if (strcmp(key, "samples_per_frame") == 0) {
            conf->samples_per_frame = json_object_get_int(val);
        }
    }

    if (strcmp(codec_type, "aac") == 0)
        conf->codec_type = CODEC_TYPE_AAC;
    else
        return -1;

    return 0;
}

static json_object *imagesettings_to_json(ImageSettings *settings)
{
    json_object *result = json_object_new_object();
    json_object_object_add(result, "brightness", json_object_new_int(settings->brightness));
    json_object_object_add(result, "contrast",   json_object_new_int(settings->contrast));
    json_object_object_add(result, "saturation", json_object_new_int(settings->saturation));
    json_object_object_add(result, "hue",        json_object_new_int(settings->hue));
    json_object_object_add(result, "sharpness",  json_object_new_int(settings->sharpness));
    return result;
}

static int json_to_imagesettings(json_object *params, ImageSettings *settings)
{
    if (!params)
        return -1;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "brightness") == 0) {
            settings->brightness = json_object_get_int(val);
        } else if (strcmp(key, "contrast") == 0) {
            settings->contrast = json_object_get_int(val);
        } else if (strcmp(key, "saturation") == 0) {
            settings->saturation = json_object_get_int(val);
        } else if (strcmp(key, "hue") == 0) {
            settings->hue = json_object_get_int(val);
        } else if (strcmp(key, "sharpness") == 0) {
            settings->sharpness = json_object_get_int(val);
        }
    }

    return 0;
}

static int json_to_osdsettings(json_object *params, OsdSettings *settings)
{
    if (!params)
        return -1;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "time") == 0) {
            json_object_object_foreach(val, key1, val1) {
                if (strcmp(key1, "enable") == 0) {
                    settings->time.enable = json_object_get_int(val1);
                } else if (strcmp(key1, "font_size") == 0) {
                    settings->time.font_size = json_object_get_int(val1);
                } else if (strcmp(key1, "color") == 0) {
                    settings->time.color = json_object_get_int(val1);
                } else if (strcmp(key1, "alpha") == 0) {
                    settings->time.alpha = json_object_get_int(val1);
                } else if (strcmp(key1, "x") == 0) {
                    settings->time.x = json_object_get_int(val1);
                } else if (strcmp(key1, "y") == 0) {
                    settings->time.y = json_object_get_int(val1);
                } else if (strcmp(key1, "time_format") == 0) {
                    settings->time.time_format = json_object_get_int(val1);
                }
            }
        } else if (strcmp(key, "text") == 0) {
            json_object_object_foreach(val, key1, val1) {
                if (strcmp(key1, "enable") == 0) {
                    settings->text.enable = json_object_get_int(val1);
                } else if (strcmp(key1, "font_size") == 0) {
                    settings->text.font_size = json_object_get_int(val1);
                } else if (strcmp(key1, "color") == 0) {
                    settings->text.color = json_object_get_int(val1);
                } else if (strcmp(key1, "alpha") == 0) {
                    settings->text.alpha = json_object_get_int(val1);
                } else if (strcmp(key1, "x") == 0) {
                    settings->text.x = json_object_get_int(val1);
                } else if (strcmp(key1, "y") == 0) {
                    settings->text.y = json_object_get_int(val1);
                } else if (strcmp(key1, "text") == 0) {
                    strncpy(settings->text.text, json_object_get_string(val1), sizeof(settings->text.text) - 1);
                }
            }
        }
    }

    return 0;
}

static json_object *osdsettings_to_json(OsdSettings *settings)
{
    json_object *result = json_object_new_object();

    json_object *timeobj = json_object_new_object();

    json_object_object_add(timeobj, "enable",      json_object_new_int(settings->time.enable));
    json_object_object_add(timeobj, "font_size",   json_object_new_int(settings->time.font_size));
    json_object_object_add(timeobj, "color",       json_object_new_int(settings->time.color));
    json_object_object_add(timeobj, "alpha",       json_object_new_int(settings->time.alpha));
    json_object_object_add(timeobj, "x",           json_object_new_int(settings->time.x));
    json_object_object_add(timeobj, "y",           json_object_new_int(settings->time.y));
    json_object_object_add(timeobj, "time_format", json_object_new_int(settings->time.time_format));

    json_object *textobj = json_object_new_object();

    json_object_object_add(textobj, "enable",      json_object_new_int(settings->text.enable));
    json_object_object_add(textobj, "font_size",   json_object_new_int(settings->text.font_size));
    json_object_object_add(textobj, "color",       json_object_new_int(settings->text.color));
    json_object_object_add(textobj, "alpha",       json_object_new_int(settings->text.alpha));
    json_object_object_add(textobj, "x",           json_object_new_int(settings->text.x));
    json_object_object_add(textobj, "y",           json_object_new_int(settings->text.y));
    json_object_object_add(textobj, "text",        json_object_new_string(settings->text.text));

    json_object_object_add(result, "time", timeobj);
    json_object_object_add(result, "text", textobj);

    return result;
}


static json_object *params_to_json(GlobalParams *params)
{
    json_object *result = json_object_new_object();

    json_object_object_add(result, "WifiConfiguration", wificonf_to_json(&params->wificonf));

    json_object *encoder_array = json_object_new_array();
    int i;
    for (i = 0; i < NELEMS(params->video_encoder); i++) {
        json_object_array_add(encoder_array, videoencoder_to_json(&params->video_encoder[i]));
    }
    json_object_object_add(result, "VideoEncoderConfiguration", encoder_array);

    json_object_object_add(result, "AudioEncoderConfiguration",  audioencoder_to_json(&params->audio_encoder));
    json_object_object_add(result, "ImageSettings",  imagesettings_to_json(&params->image_settings));
    json_object_object_add(result, "RecordSettings", recordsettings_to_json(&params->record_settings));
    json_object_object_add(result, "PhotoTakingSettings", phototakingsettings_to_json(&params->phototaking_settings));
    json_object_object_add(result, "OsdSettings",    osdsettings_to_json(&params->osd_settings));

    if (params->camera_mode == CAMERA_MODE_CAPTURE)
        json_object_object_add(result, "CameraMode", json_object_new_string("capture"));
    else if (params->camera_mode == CAMERA_MODE_RECORD)
        json_object_object_add(result, "CameraMode", json_object_new_string("record"));

    return result;
}

static int json_to_params(json_object *objs, GlobalParams *params)
{
    json_object_object_foreach(objs, key, val) {
        if (strcmp(key, "WifiConfiguration") == 0) {
            json_to_wificonf(val, &params->wificonf);
        } else if (strcmp(key, "VideoEncoderConfiguration") == 0) {
            int length = min(json_object_array_length(val), NELEMS(params->video_encoder));
            int i;
            for (i = 0; i < length; i++) {
                json_object *file = json_object_array_get_idx(val, i);
                json_to_videoencoder(file, &params->video_encoder[i]);
            }
        } else if (strcmp(key, "AudioEncoder") == 0) {
            json_to_audioencoder(val, &params->audio_encoder);
        } else if (strcmp(key, "ImageSettings") == 0) {
            json_to_imagesettings(val, &params->image_settings);
        } else if (strcmp(key, "RecordSettings") == 0) {
            json_to_recordsettings(val, &params->record_settings);
        } else if (strcmp(key, "PhotoTakingSettings") == 0) {
            json_to_phototakingsettings(val, &params->phototaking_settings);
        } else if (strcmp(key, "OsdSettings") == 0) {
            json_to_osdsettings(val, &params->osd_settings);
        } else if (strcmp(key, "CameraMode") == 0) {
            const char *mode = json_object_get_string(val);
            if (strcmp(mode, "record") == 0)
                params->camera_mode = CAMERA_MODE_RECORD;
            else if (strcmp(mode, "capture") == 0)
                params->camera_mode = CAMERA_MODE_CAPTURE;
        }
    }

    return 0;
}

int Params_Init(const char *filename)
{
    int fd = -1;

    strncpy(param_file, filename, sizeof(param_file) - 1);

    if (access(filename, F_OK | R_OK | W_OK) < 0) {
        fd = open(filename, O_CREAT | O_TRUNC | O_RDWR, 0644);
        if (fd < 0)
            return -1;

        json_object *params = params_to_json(&gblparams);

        const char *data = json_object_to_json_string_ext(params, JSON_C_TO_STRING_PRETTY);
        if (!data) {
            json_object_put(params);
            close(fd);
            return -1;
        }

        if (write(fd, data, strlen(data)) < 0) {
            json_object_put(params);
            close(fd);
            return -1;
        }

        json_object_put(params);
        close(fd);
    } else {
        fd = open(filename, O_RDWR, 0644);
        if (fd < 0)
            return -1;

        struct stat st;
        if (fstat(fd, &st) < 0) {
            close(fd);
            return -1;
        }

        char *buf = malloc(st.st_size + 1);
        if (!buf) {
            close(fd);
            return -1;
        }
        memset(buf, 0, st.st_size + 1);

        if (read(fd, buf, st.st_size) != st.st_size) {
            free(buf);
            close(fd);
            return -1;
        }

        json_object *obj = json_tokener_parse(buf);

        json_to_params(obj, &gblparams);

        json_object_put(obj);
        free(buf);
        close(fd);
    }

    return 0;
}

static int Params_Flush(void)
{
    char new_file[1024];
    snprintf(new_file, sizeof(new_file), "%s.tmp", param_file);

    int fd = open(new_file, O_CREAT | O_TRUNC | O_RDWR, 0644);
    if (fd < 0)
        return -1;

    json_object *params = params_to_json(&gblparams);

    const char *data = json_object_to_json_string_ext(params, JSON_C_TO_STRING_PRETTY);
    if (!data) {
        json_object_put(params);
        close(fd);
        return -1;
    }

    if (write(fd, data, strlen(data) + 1) < 0) {
        json_object_put(params);
        close(fd);
        return -1;
    }

    json_object_put(params);
    close(fd);

    /* atomic save */
    return rename(new_file, param_file);
}

int Params_GetWifiConfiguration(WifiConfiguration *wifi)
{
    *wifi = gblparams.wificonf;
    return 0;
}

int Params_SetWifiConfiguration(WifiConfiguration *wifi)
{
    gblparams.wificonf = *wifi;
    Params_Flush();
    return 0;
}

int Params_GetCameraMode(CameraMode *mode)
{
    *mode = gblparams.camera_mode;
    return 0;
}

int Params_SetCameraMode(CameraMode mode)
{
    gblparams.camera_mode = mode;
    Params_Flush();
    return 0;
}

int Params_GetVideoEncoderConfiguration(int stream, VideoEncoderConfiguration *conf)
{
    if (stream < 0 || stream >= NELEMS(gblparams.video_encoder))
        return -1;
    *conf = gblparams.video_encoder[stream];
    return 0;
}

int Params_SetVideoEncoderConfiguration(int stream, VideoEncoderConfiguration *conf)
{
    if (stream < 0 || stream >= NELEMS(gblparams.video_encoder))
        return -1;

    gblparams.video_encoder[stream] = *conf;
    Params_Flush();
    return 0;
}

int Params_GetAudioEncoderConfiguration(AudioEncoderConfiguration *conf)
{
    *conf = gblparams.audio_encoder;
    return 0;
}

int Params_SetAudioEncoderConfiguration(AudioEncoderConfiguration *conf)
{
    gblparams.audio_encoder = *conf;
    Params_Flush();
    return 0;
}

int Params_GetImageSettings(ImageSettings *settings)
{
    *settings = gblparams.image_settings;
    return 0;
}

int Params_SetImageSettings(ImageSettings *settings)
{
    gblparams.image_settings = *settings;
    Params_Flush();
    return 0;
}

int Params_GetRecordSettings(RecordSettings *settings)
{
    *settings = gblparams.record_settings;
    return 0;
}

int Params_SetRecordSettings(RecordSettings *settings)
{
    gblparams.record_settings = *settings;
    Params_Flush();
    return 0;
}

int Params_GetPhotoTakingSettings(PhotoTakingSettings *settings)
{
    *settings = gblparams.phototaking_settings;
    return 0;
}

int Params_SetPhotoTakingSettings(PhotoTakingSettings *settings)
{
    gblparams.phototaking_settings = *settings;
    Params_Flush();
    return 0;
}

int Params_GetOsdSettings(OsdSettings *settings)
{
    *settings = gblparams.osd_settings;
    return 0;
}

int Params_SetOsdSettings(OsdSettings *settings)
{
    if (settings->time.x > 1000 || settings->time.y > 1000)
        return -1;

    if (settings->text.x > 1000 || settings->text.y > 1000)
        return -1;

    gblparams.osd_settings = *settings;
    Params_Flush();

    return 0;
}

#if 0
int main(void)
{
#if 0
#if 0
    const char *data = json_object_to_json_string_ext(params_to_json(&gblparams), JSON_C_TO_STRING_PRETTY);
    printf("%s\n", data);
#else
    char buf[32 * 1024] = {0};

    int fd = open("params.txt", O_RDONLY);
    if (fd < 0)
        return -1;

    read(fd, buf, sizeof(buf));
    printf("buf: %s\n", buf);

    json_object *obj = json_tokener_parse(buf);

    GlobalParams params;
    json_to_params(obj, &params);
    printf("params.wificonf.enabele = %d\n", params.wificonf.enable);

    const char *data = json_object_to_json_string_ext(params_to_json(&params), JSON_C_TO_STRING_PRETTY);
    printf("%s\n", data);
#endif
#endif

    Params_Init("default_params.json");

    WifiConfiguration conf;
    Params_GetWifiConfiguration(&conf);

    printf("conf.enable = %d\n", conf.enable);
    printf("conf.ssid = %s\n", conf.ssid);
    printf("conf.password = %s\n", conf.password);
    printf("conf.band = %d\n", conf.band);

    conf.enable = 0;

    Params_SetWifiConfiguration(&conf);

    return 0;
}
#endif
