#ifndef __PARAMS_H__
#define __PARAMS_H__

#include "common.h"

int Params_Init(const char *filename);

int Params_GetCameraMode(CameraMode *mode);
int Params_SetCameraMode(CameraMode mode);

int Params_GetWifiConfiguration(WifiConfiguration *wifi);
int Params_SetWifiConfiguration(WifiConfiguration *wifi);

int Params_GetVideoEncoderConfiguration(int stream, VideoEncoderConfiguration *conf);
int Params_SetVideoEncoderConfiguration(int stream, VideoEncoderConfiguration *conf);

int Params_GetAudioEncoderConfiguration(AudioEncoderConfiguration *conf);
int Params_SetAudioEncoderConfiguration(AudioEncoderConfiguration *conf);

int Params_GetImageSettings(ImageSettings *settings);
int Params_SetImageSettings(ImageSettings *settings);

int Params_GetRecordSettings(RecordSettings *settings);
int Params_SetRecordSettings(RecordSettings *settings);

int Params_GetPhotoTakingSettings(PhotoTakingSettings *settings);
int Params_SetPhotoTakingSettings(PhotoTakingSettings *settings);

int Params_GetOsdSettings(OsdSettings *settings);
int Params_SetOsdSettings(OsdSettings *settings);

#endif
