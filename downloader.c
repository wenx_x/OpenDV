#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include "exif.h"
#include "logger.h"
#include "httpsvc.h"
#include "recmgr.h"

static const char *TAG = "DownLoader";

static char urlpath[1024];

typedef enum {
    FILE_TYPE_ORIGINAL  = 0,
    FILE_TYPE_THUMBNAIL = 1,
} FileType;

typedef struct {
    FileType filetype;   /* 0 - original, 1 - thumbnail */
    char filename[128];
    RecRawFile *rec;

    ExifReader *exif;
} DownloadCtx;

static long download_open(HttpConn *conn, const char *url, void *priv_data)
{
    DownloadCtx *ctx = malloc(sizeof(DownloadCtx));
    if (!ctx)
        return 0;
    memset(ctx, 0, sizeof(DownloadCtx));

    const char *filetype = HttpConn_LookupConnectionValue(conn, HTTP_VALUE_KIND_GET_ARGUMENT, "filetype");
    if (filetype)
        ctx->filetype = FILE_TYPE_THUMBNAIL;

    strncpy(ctx->filename, url + strlen(urlpath), sizeof(ctx->filename) - 1);

    if (ctx->filetype == FILE_TYPE_THUMBNAIL) {
        char fullname[1024];
        if (RecMgr_GetRecFullpath(ctx->filename, fullname, sizeof(fullname)) < 0) {
            LogV(TAG, "RecMgr_GetRecFullpath failed");
            free(ctx);
            return 0;
        }

        ctx->exif = ExifReader_Open(fullname);
        if (!ctx->exif) {
            LogV(TAG, "ExifReader_Open %s failed", fullname);
            free(ctx);
            return 0;
        }
    } else {
        ctx->rec = RecMgr_OpenRaw(ctx->filename);
        if (!ctx->rec) {
            LogV(TAG, "RecMgr_OpenRaw %s failed", ctx->filename);
            free(ctx);
            return 0;
        }
    }

    LogV(TAG, "open file %s type %d success", ctx->filename, ctx->filetype);

    return (long)ctx;
}

static int download_close(long handle)
{
    DownloadCtx *ctx = (DownloadCtx *)handle;

    if (ctx->exif)
        ExifReader_Close(ctx->exif);

    if (ctx->rec)
        RecMgr_CloseRaw(ctx->rec);

    free(ctx);

    return 0;
}

static int download_seek(long handle, uint64_t pos)
{
    int ret = 0;
    DownloadCtx *ctx = (DownloadCtx *)handle;

    if (ctx->filetype == FILE_TYPE_ORIGINAL)
        ret = RecMgr_SeekRaw(ctx->rec, pos);

    return ret;
}

static ssize_t download_read(long handle, uint8_t *buf, size_t size)
{
    DownloadCtx *ctx = (DownloadCtx *)handle;

    if (ctx->filetype == FILE_TYPE_ORIGINAL)
        return RecMgr_ReadRaw(ctx->rec, buf, size);

    return ExifReader_Read(ctx->exif, buf, size);
}

static HttpOps download_ops = {
    .open  = download_open,
    .close = download_close,
    .seek  = download_seek,

    .read  = download_read,
    .write = NULL,
};

int DownLoader_Init(HttpSvc *httpsvc, char *path)
{
    strncpy(urlpath, path, sizeof(urlpath) - 1);

   /* url must end up with '/' */
    if (path[strlen(path) - 1] != '/')
        strcat(urlpath, "/");

    return HttpSvc_AddRoute(httpsvc, HTTP_METHOD_GET, urlpath, &download_ops, NULL);
}
