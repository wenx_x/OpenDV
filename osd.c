#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>

#include "logger.h"
#include "params.h"
#include "avtimer.h"
#include "fontface.h"
#include "mediaapi.h"
#include "font_noto_mono.h"

#include "osd.h"

static const char *TAG = "OSD";

static int get_time_str(char *str, int str_len)
{
    time_t now = time(NULL);
    struct tm tm;
    localtime_r(&now, &tm);

    snprintf(str, str_len, "%04d-%02d-%02d %02d:%02d:%02d",
            tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
            tm.tm_hour, tm.tm_min, tm.tm_sec);

    return 0;
}

static void *osd_monitor(void *arg)
{
    av_timer_t *timer = av_timer_create();
    if (!timer)
        return NULL;

    struct timeval itv = {
        .tv_sec  = 1,
        .tv_usec = 0,
    };

    av_timer_set(timer, &itv);
    av_timer_start(timer);

    OsdTime prev_osdtime = {0};
    OsdText prev_osdtext = {0};

    FontFace *time_ff = NULL;
    FontFace *text_ff = NULL;

    VideoOSD *time_osd = MediaApi_OpenVideoOSD(0);
    if (!time_osd)
        return NULL;

    VideoOSD *text_osd = MediaApi_OpenVideoOSD(1);
    if (!text_osd)
        return NULL;

    while (1) {
        OsdSettings osd;
        Params_GetOsdSettings(&osd);

        OsdTime *time = &osd.time;
        OsdText *text = &osd.text;

        if (memcmp(time, &prev_osdtime, sizeof(OsdTime)) != 0) {
            if (time->enable == 0) {
                if (prev_osdtime.enable) {
                    MediaApi_StopOSD(time_osd);
                    /* TODO: disable osd here */
                }

                if (time_ff) {
                    FontFace_Destroy(time_ff);
                    time_ff = NULL;
                }
            }
        }

        if (time->enable) {
            if (time->font_size != prev_osdtime.font_size) {    /* font size has been changed */
                if (time_ff) {
                    FontFace_Destroy(time_ff);
                    time_ff = NULL;
                }
            }

            if (!time_ff) {
                time_ff = FontFace_CreateByMemory(font_noto_mono, sizeof(font_noto_mono), 0, time->font_size);
                if (!time_ff) {
                    LogV(TAG, "Create Time FontFace failed");
                    break;
                }
            }

            char time_str[64];
            get_time_str(time_str, sizeof(time_str));

            uint32_t *result;
            int width, height;

            int size = FontFace_Render(time_ff, time_str, &result, &width, &height);
            if (size < 0) {
                LogV(TAG, "Draw Time Bitmap failed");
                break;
            }
#if 0
            printf("%s.%d: #### size = %d, width = %d, height = %d\n", __func__, __LINE__, size, width, height);

            int fd = open(time_str, O_CREAT | O_TRUNC | O_WRONLY, 0644);
            write(fd, result, size);
            close(fd);
#endif

            MediaApi_SetOSDAttr(time_osd, result, size, time->color, time->alpha, time->x, time->y, width, height);
            MediaApi_StartOSD(time_osd);

            /* TODO: render datetime string to LCD */

            free(result);
        }

        if (memcmp(text, &prev_osdtext, sizeof(OsdText)) != 0) {
            if (text->enable && strlen(text->text) > 0) {
                text_ff = FontFace_CreateByMemory(font_noto_mono, sizeof(font_noto_mono), 0, text->font_size);
                if (!text_ff) {
                    LogV(TAG, "Create Text FontFace failed");
                    break;
                }

                uint32_t *result;
                int width, height;

                int size = FontFace_Render(text_ff, text->text, &result, &width, &height);
                if (size < 0) {
                    LogV(TAG, "Draw Text Bitmap failed");
                    break;
                }

#if 0
                /* TODO: render datetext string to LCD */
                char filename[1024];
                snprintf(filename, sizeof(filename), "%s_%dx%d.raw", text->text, width, height);
                int fd = open(filename, O_CREAT | O_TRUNC | O_WRONLY, 0644);
                write(fd, result, size);
                close(fd);
#else
                MediaApi_SetOSDAttr(text_osd, result, size, text->color, text->alpha, text->x, text->y, width, height);
                MediaApi_StartOSD(text_osd);
#endif

                free(result);

                FontFace_Destroy(text_ff);
                text_ff = NULL;
            } else {
                if (prev_osdtext.enable) {
                    /* TODO: disable osd here */
                    MediaApi_StopOSD(text_osd);
                }
            }
        }

        prev_osdtime = osd.time;
        prev_osdtext = osd.text;

        av_timer_wait(timer);
    }

    LogV(TAG, "OSD Monitor aborted");

    if (time_ff)
        FontFace_Destroy(time_ff);

    return NULL;
}

int Osd_Init(void)
{
    pthread_t tid;
    pthread_create(&tid, NULL, osd_monitor, NULL);

    return 0;
}

int Osd_Quit(void)
{
    return 0;
}
