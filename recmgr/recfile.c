#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "h264.h"
#include "recfile.h"
#include "recmgr.h"
#include "put_bits.h"

#include <libavutil/timestamp.h>
#include <libavformat/avformat.h>

typedef struct {
    char filename[1024];
    AVFormatContext *ofmt_ctx;

    AVStream *video_st;
    AVStream *audio_st;

    AVBSFContext *audio_bsf;

    int header_written;

    int64_t video_pts_base;
    int64_t audio_pts_base;
} RecFileWriter;

typedef struct {
    char filename[1024];
    AVFormatContext *ifmt_ctx;

    AVPacket pkt;

    AVBSFContext *video_bsf;

    int video_idx;
    int audio_idx;

    AVStream *video_st;
    AVStream *audio_st;
} RecFileReader;

struct RecFile {
    char filepath[1024];
    RecFlag flag;

    union {
        RecFileWriter *writer;
        RecFileReader *reader;
    } handler;
};

int RecFile_Init(void)
{
    av_register_all();
    return 0;
}

int RecFile_Quit(void)
{
    return 0;
}

RecFileWriter *RecFile_OpenForWrite(char *filename)
{
    RecFileWriter *writer = malloc(sizeof(RecFileWriter));
    if (!writer)
        return NULL;
    memset(writer, 0, sizeof(RecFileWriter));

    writer->video_pts_base = -1;
    writer->audio_pts_base = -1;

    writer->header_written = 0;
    strncpy(writer->filename, filename, sizeof(writer->filename) - 1);

    if (avformat_alloc_output_context2(&writer->ofmt_ctx, NULL, NULL, filename) < 0)
        goto fail;

    if (!writer->ofmt_ctx)
        goto fail;

    if (!(writer->ofmt_ctx->flags & AVFMT_NOFILE)) {
        int ret = avio_open(&writer->ofmt_ctx->pb, writer->filename, AVIO_FLAG_WRITE);
        if (ret < 0) {
            fprintf(stderr, "Could not open output file '%s'", writer->filename);
            goto fail;
        }
    }

    return writer;

fail:
    if (writer->ofmt_ctx)
        avformat_free_context(writer->ofmt_ctx);
    free(writer);
    return NULL;
}

int RecFile_CloseWriter(RecFileWriter *writer)
{
    if (!writer)
        return -1;

    if (writer->header_written)
        av_write_trailer(writer->ofmt_ctx);

    if (!(writer->ofmt_ctx->flags & AVFMT_NOFILE)) {
        /* Close the output file. */
        avio_closep(&writer->ofmt_ctx->pb);
    }

    if (writer->audio_bsf)
        av_bsf_free(&writer->audio_bsf);

    avformat_free_context(writer->ofmt_ctx);
    free(writer);

    return 0;
}

int RecFileWriter_AddVideoStream(RecFileWriter *writer, CodecType codec, int time_base, int width, int height, uint8_t *extradata, int extrasize)
{
    AVFormatContext *oc = writer->ofmt_ctx;
    AVCodecParameters *codecpar = NULL;

    if (codec != CODEC_TYPE_H264 && codec != CODEC_TYPE_H265)
        return -1;

    writer->video_st = avformat_new_stream(oc, NULL);
    writer->video_st->id = oc->nb_streams - 1;
    writer->video_st->time_base = (AVRational){ 1, time_base };

    codecpar = writer->video_st->codecpar;
    codecpar->codec_type = AVMEDIA_TYPE_VIDEO;

    if (codec == CODEC_TYPE_H264)
        codecpar->codec_id = AV_CODEC_ID_H264;
    else
        codecpar->codec_id = AV_CODEC_ID_H265;

    codecpar->width  = width;
    codecpar->height = height;
    codecpar->format = AV_PIX_FMT_YUV420P;

    if (extrasize > 0) {
        codecpar->extradata = av_mallocz(extrasize);
        memcpy(codecpar->extradata, extradata, extrasize);
        codecpar->extradata_size = extrasize;
    }

//    av_dump_format(oc, 0, writer->filename, 1);

    return 0;
}

int RecFileWriter_AddAudioStream(RecFileWriter *writer, CodecType codec, int time_base, int nb_channels, int frame_size, int bit_rate, int sample_rate, uint8_t *extradata, int extrasize)
{
    AVFormatContext *oc = writer->ofmt_ctx;
    AVCodecParameters *codecpar = NULL;

    if (codec != CODEC_TYPE_AAC)
        return -1;

    writer->audio_st = avformat_new_stream(oc, NULL);
    writer->audio_st->id = oc->nb_streams - 1;
    writer->audio_st->time_base = (AVRational){ 1, time_base };

    codecpar = writer->audio_st->codecpar;
    codecpar->codec_type = AVMEDIA_TYPE_AUDIO;
    codecpar->codec_id = AV_CODEC_ID_AAC;
    codecpar->bit_rate = bit_rate;
    codecpar->sample_rate = sample_rate;
    codecpar->channels = nb_channels;
    codecpar->frame_size = frame_size;

    if (extrasize > 0) {
        codecpar->extradata = av_mallocz(extrasize);
        memcpy(codecpar->extradata, extradata, extrasize);
        codecpar->extradata_size = extrasize;
    }

    /* aac_adtstoasc bit stream filter for removing ADTS header */
    const AVBitStreamFilter *filter = NULL;

    filter = av_bsf_get_by_name("aac_adtstoasc");
    if (!filter) {
        printf("bit stream filter aac_adtstoasc not found\n");
        return -1;
    }

    if (av_bsf_alloc(filter, &writer->audio_bsf) < 0)
        return -1;

    if (avcodec_parameters_copy(writer->audio_bsf->par_in, writer->audio_st->codecpar) < 0) {
        av_bsf_free(&writer->audio_bsf);
        writer->audio_bsf = NULL;
        return -1;
    }

    if (av_bsf_init(writer->audio_bsf) < 0) {
        av_bsf_free(&writer->audio_bsf);
        writer->audio_bsf = NULL;
        return -1;
    }

//    av_dump_format(oc, 0, writer->filename, 1);

    return 0;
}

static int RecFile_WriteFrame(RecFileWriter *writer, RecFrame *frame)
{
    if (!writer->header_written) {
        int ret = avformat_write_header(writer->ofmt_ctx, NULL);
        if (ret < 0) {
            fprintf(stderr, "Error occurred when opening output file: %s\n",
                    av_err2str(ret));
            return -1;
        }
        writer->header_written = 1;
    }

    AVPacket pkt;
    av_init_packet(&pkt);

    switch (frame->media) {
    case MEDIA_TYPE_VIDEO:
        if (!writer->video_st)
            return 0;

        pkt.stream_index = writer->video_st->index;

        if (writer->video_pts_base == -1)
            writer->video_pts_base = frame->pts;

        /* pts must start from 0 in MOV/MP4 format */
        pkt.pts = frame->pts - writer->video_pts_base;
        break;
    case MEDIA_TYPE_AUDIO:
        if (!writer->audio_st)
            return 0;

        pkt.stream_index = writer->audio_st->index;

        if (writer->audio_pts_base == -1)
            writer->audio_pts_base = frame->pts;

        /* pts must start from 0 in MOV/MP4 format */
        pkt.pts = frame->pts - writer->audio_pts_base;
        break;
    default:
        return 0;
    }

    pkt.flags = frame->keyframe ? AV_PKT_FLAG_KEY : 0;
    pkt.data  = frame->data;
    pkt.size  = frame->size;
    pkt.dts   = pkt.pts;

    if (frame->media == MEDIA_TYPE_VIDEO) {
        if (av_interleaved_write_frame(writer->ofmt_ctx, &pkt) < 0) {
            printf("write video frame failed\n");
            return -1;
        }
    } else {
        if (av_bsf_send_packet(writer->audio_bsf, &pkt) < 0)
            return -1;

        AVPacket pkt1 = {
            .data = NULL,
            .size = 0,
        };

        if (av_bsf_receive_packet(writer->audio_bsf, &pkt1) < 0)
            return -1;

        if (av_interleaved_write_frame(writer->ofmt_ctx, &pkt1) < 0) {
            printf("write audio frame failed\n");
            av_packet_unref(&pkt1);
            return -1;
        }

        av_packet_unref(&pkt1);
    }

    return 0;
}

static RecFileReader *RecFile_OpenForRead(char *filename)
{
    RecFileReader *reader = malloc(sizeof(RecFileReader));
    if (!reader)
        return NULL;
    memset(reader, 0, sizeof(RecFileReader));

    strncpy(reader->filename, filename, sizeof(reader->filename) - 1);

    if (avformat_open_input(&reader->ifmt_ctx, filename, NULL, NULL) < 0) {
        fprintf(stderr, "Could not open input file '%s'\n", filename);
        goto end;
    }

    if (avformat_find_stream_info(reader->ifmt_ctx, NULL) < 0) {
        fprintf(stderr, "Failed to retrieve input stream information\n");
        goto end;
    }

    reader->video_idx = av_find_best_stream(reader->ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    reader->audio_idx = av_find_best_stream(reader->ifmt_ctx, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);

    if (reader->video_idx < 0 && reader->audio_idx < 0)
        goto end;

    if (reader->video_idx >= 0) {
        reader->video_st = reader->ifmt_ctx->streams[reader->video_idx];

        const AVBitStreamFilter *filter = NULL;

        if (reader->video_st->codecpar->codec_id == AV_CODEC_ID_H264)
            filter = av_bsf_get_by_name("h264_mp4toannexb");
        else if (reader->video_st->codecpar->codec_id == AV_CODEC_ID_H265)
            filter = av_bsf_get_by_name("hevc_mp4toannexb");
        else
            goto end;

        if (av_bsf_alloc(filter, &reader->video_bsf) < 0)
            goto end;

        if (avcodec_parameters_copy(reader->video_bsf->par_in, reader->video_st->codecpar) < 0)
            goto end;

        if (av_bsf_init(reader->video_bsf) < 0)
            goto end;
    }

    if (reader->audio_idx >= 0) {
        reader->audio_st = reader->ifmt_ctx->streams[reader->audio_idx];
    }

    av_init_packet(&reader->pkt);
    reader->pkt.data = NULL;
    reader->pkt.size = 0;

    return reader;

end:
    if (reader->ifmt_ctx)
        avformat_close_input(&reader->ifmt_ctx);
    if (reader->video_bsf)
        av_bsf_free(&reader->video_bsf);
    free(reader);
    return NULL;
}

static int write_annexb_extradata(const uint8_t *in, uint8_t **buf, int *size)
{
    uint16_t sps_size, pps_size;
    uint8_t *out;
    int out_size;

    *buf = NULL;
    if (*size >= 4 && (AV_RB32(in) == 0x00000001 || AV_RB24(in) == 0x000001))
        return 0;
    if (*size < 11 || in[0] != 1)
        return AVERROR_INVALIDDATA;

    sps_size = AV_RB16(&in[6]);
    if (11 + sps_size > *size)
        return AVERROR_INVALIDDATA;
    pps_size = AV_RB16(&in[9 + sps_size]);
    if (11 + sps_size + pps_size > *size)
        return AVERROR_INVALIDDATA;
    out_size = 8 + sps_size + pps_size;
    out = av_mallocz(out_size + AV_INPUT_BUFFER_PADDING_SIZE);
    if (!out)
        return AVERROR(ENOMEM);
    AV_WB32(&out[0], 0x00000001);
    memcpy(out + 4, &in[8], sps_size);
    AV_WB32(&out[4 + sps_size], 0x00000001);
    memcpy(out + 8 + sps_size, &in[11 + sps_size], pps_size);
    *buf = out;
    *size = out_size;
    return 0;
}

static int get_video_extradata(AVCodecParameters *codecpar, uint8_t *buf, int buf_len)
{
    int size = 0;

    if (codecpar->extradata_size <= 0)
        return 0;

    if (codecpar->extradata[0] == 1) {
        uint8_t *extradata = NULL;
        int extradata_size = buf_len;
        if (write_annexb_extradata(codecpar->extradata, &extradata, &extradata_size) < 0)
            return -1;

        memcpy(buf, extradata, extradata_size);
        size = extradata_size;

        av_freep(&extradata);
    } else {
        if (buf_len < codecpar->extradata_size)
            return -1;
        memcpy(buf, codecpar->extradata, codecpar->extradata_size);
        size = codecpar->extradata_size;
    }

    return size;
}

static int get_duration(AVStream *st, int64_t time_base)
{
    int duration;

    if (st->start_time < st->duration)
        duration = (st->duration - st->start_time) * 1000 / time_base;
    else
        duration = st->duration * 1000 / time_base;

    return duration;
}

int RecFile_GetRecInfo(RecFile *f, RecInfo *recinfo)
{
    if (!f || !recinfo)
        return -1;

    memset(recinfo, 0, sizeof(RecInfo));

    if (f->flag == REC_FLAGS_WRONLY)
        return -1;  /* FIXME: not impletement yet */

    RecFileReader *reader = f->handler.reader;
    if (!reader)
        return -1;

    if (reader->video_st) {
        VideoInfo *info = &recinfo->video_info;
        AVCodecParameters *codecpar = reader->video_st->codecpar;

        info->width  = codecpar->width;
        info->height = codecpar->height;

        if (codecpar->codec_id == AV_CODEC_ID_H264)
            info->codec = CODEC_TYPE_H264;
        else if (codecpar->codec_id == AV_CODEC_ID_H265)
            info->codec = CODEC_TYPE_H265;
        else
            info->codec = CODEC_TYPE_NONE;

        AVStream *st = reader->video_st;

        if (st->avg_frame_rate.den > 0)
            info->framerate = st->avg_frame_rate.num / (float)st->avg_frame_rate.den;

        info->time_base = st->time_base.den / st->time_base.num;
        info->duration = get_duration(st, info->time_base);
        info->extrasize = get_video_extradata(codecpar, info->extradata, sizeof(info->extradata));

        recinfo->has_video = 1;
    }

    if (reader->audio_st) {
        AudioInfo *info = &recinfo->audio_info;
        AVCodecParameters *codecpar = reader->audio_st->codecpar;

        if (codecpar->codec_id == AV_CODEC_ID_AAC)
            info->codec = CODEC_TYPE_AAC;
        else
            info->codec = CODEC_TYPE_NONE;

        AVStream *st = reader->audio_st;

        info->sample_rate = codecpar->sample_rate;
        info->nb_channels = codecpar->channels;
        info->time_base = st->time_base.den / st->time_base.num;
        info->duration = get_duration(st, info->time_base);

        recinfo->has_audio = 1;
    }

    return 0;
}

int RecFile_Seek(RecFile *f, int off_ms)
{
    if (f->flag == REC_FLAGS_WRONLY)
        return -1;  /* FIXME: not impletement yet */

    RecFileReader *reader = f->handler.reader;
    if (!reader)
        return -1;

#if 1
    if (avformat_seek_file(reader->ifmt_ctx, -1, INT64_MIN, off_ms * 1000, INT64_MAX, 0) < 0)
        printf("avformat_seek_file failed\n");
#else
    RecInfo info;
    if (RecFile_GetRecInfo(f, &info) < 0)
        return -1;

    if (info.framerate <= 0)
        return -1;

    int frames = off_ms / (1000.0 / info.framerate);

    if (av_seek_frame(reader->ifmt_ctx, reader->video_idx, frames, AVSEEK_FLAG_FRAME) < 0) {
        printf("av_seek_frame failed\n");
        return -1;
    }

    printf("av_seek_frame success\n");
#endif

    return 0;
}

static int RecFile_CloseReader(RecFileReader *reader)
{
    if (!reader)
        return -1;

    if (reader->ifmt_ctx)
        avformat_close_input(&reader->ifmt_ctx);
    if (reader->video_bsf)
        av_bsf_free(&reader->video_bsf);
    free(reader);

    return 0;
}

#define ADTS_HEADER_SIZE    (7)

static const int mpeg4audio_sample_rates[16] = {
    96000, 88200, 64000, 48000, 44100, 32000,
    24000, 22050, 16000, 12000, 11025, 8000, 7350
};

static int adts_write_frame_header(int objecttype, int sample_rate,
                                   int channel_conf, uint8_t *buf, int size)
{
    PutBitContext pb;

    unsigned full_frame_size = (unsigned)ADTS_HEADER_SIZE + size;

    int rate_index;
    for (rate_index = 0; rate_index < 16; rate_index++)
        if (mpeg4audio_sample_rates[rate_index] == sample_rate)
            break;
    if (rate_index == 16)
        return -1;

    init_put_bits(&pb, buf, ADTS_HEADER_SIZE);

    /* adts_fixed_header */
    put_bits(&pb, 12, 0xfff);   /* syncword */
    put_bits(&pb, 1, 0);        /* ID */
    put_bits(&pb, 2, 0);        /* layer */
    put_bits(&pb, 1, 1);        /* protection_absent */
    put_bits(&pb, 2, objecttype); /* profile_objecttype */
    put_bits(&pb, 4, rate_index);
    put_bits(&pb, 1, 0);        /* private_bit */
    put_bits(&pb, 3, channel_conf); /* channel_configuration */
    put_bits(&pb, 1, 0);        /* original_copy */
    put_bits(&pb, 1, 0);        /* home */

    /* adts_variable_header */
    put_bits(&pb, 1, 0);        /* copyright_identification_bit */
    put_bits(&pb, 1, 0);        /* copyright_identification_start */
    put_bits(&pb, 13, full_frame_size); /* aac_frame_length */
    put_bits(&pb, 11, 0x7ff);   /* adts_buffer_fullness */
    put_bits(&pb, 2, 0);        /* number_of_raw_data_blocks_in_frame */

    flush_put_bits(&pb);

    return 0;
}

static int RecFile_ReadFrame(RecFileReader *reader, RecFrame *frame)
{
    AVPacket *pkt = &reader->pkt;

    int ret = av_read_frame(reader->ifmt_ctx, pkt);
    if (ret < 0)
        return ret;

    if (pkt->stream_index == reader->video_idx) {
        if (av_bsf_send_packet(reader->video_bsf, pkt) < 0) {
            av_packet_unref(pkt);
            return -1;
        }

        AVPacket pkt1 = {
            .data = NULL,
            .size = 0,
        };

        if (av_bsf_receive_packet(reader->video_bsf, &pkt1) < 0) {
            av_packet_unref(pkt);
            return -1;
        }

        if (sizeof(frame->data) < pkt1.size) {
            av_packet_unref(&pkt1);
            av_packet_unref(pkt);
            return -1;
        }

        frame->media = MEDIA_TYPE_VIDEO;

        switch (reader->video_st->codecpar->codec_id) {
        case AV_CODEC_ID_H264:
            frame->codec = CODEC_TYPE_H264;
            break;
        case AV_CODEC_ID_H265:
            frame->codec = CODEC_TYPE_H265;
            break;
        default:
            printf("unknown video codec type\n");
            av_packet_unref(&pkt1);
            av_packet_unref(pkt);
            return -1;
        }

        frame->pts = pkt1.pts;
        frame->keyframe = !!(pkt1.flags & AV_PKT_FLAG_KEY);
        frame->size = pkt1.size;
        memcpy(frame->data, pkt1.data, pkt1.size);

        av_packet_unref(&pkt1);
        av_packet_unref(pkt);

        return 1;
    } else if (pkt->stream_index == reader->audio_idx) {
        frame->media = MEDIA_TYPE_AUDIO;

        switch (reader->audio_st->codecpar->codec_id) {
        case AV_CODEC_ID_AAC:
            frame->codec = CODEC_TYPE_AAC;
            break;
        default:
            printf("unknown audio codec type\n");
            av_packet_unref(pkt);
            return -1;
        }

        AVCodecParameters *codecpar = reader->audio_st->codecpar;

        /* append ADTS header */
        uint8_t adts_header[ADTS_HEADER_SIZE];
        adts_write_frame_header(1, codecpar->sample_rate, codecpar->channels, adts_header, pkt->size);
        memcpy(frame->data, adts_header, ADTS_HEADER_SIZE);

        memcpy(frame->data + ADTS_HEADER_SIZE, pkt->data, pkt->size);

        frame->size = pkt->size + ADTS_HEADER_SIZE;
        frame->pts = pkt->pts;

        av_packet_unref(pkt);

        return 1;
    }

    av_packet_unref(pkt);

    return 0;
}

RecFile *RecFile_Open(char *filepath, RecFlag flag)
{
    RecFile *f = malloc(sizeof(RecFile));
    if (!f)
        return NULL;
    memset(f, 0, sizeof(RecFile));

    f->flag = flag;

    if (f->flag == REC_FLAGS_WRONLY) {
        f->handler.writer = RecFile_OpenForWrite(filepath);
        if (!f->handler.writer) {
            free(f);
            return NULL;
        }
    } else {
        f->handler.reader = RecFile_OpenForRead(filepath);
        if (!f->handler.reader) {
            free(f);
            return NULL;
        }
    }

    return f;
}

int RecFile_Close(RecFile *f)
{
    int ret = -1;

    if (f->flag == REC_FLAGS_WRONLY) {
        ret = RecFile_CloseWriter(f->handler.writer);
    } else {
        ret = RecFile_CloseReader(f->handler.reader);
    }

    return ret;
}

void RecFile_DumpInfo(RecFile *f)
{
    if (f->flag == REC_FLAGS_WRONLY) {
        if (f->handler.writer->ofmt_ctx)
            av_dump_format(f->handler.writer->ofmt_ctx, 0, f->handler.writer->filename, 1);
    } else {
        if (f->handler.reader->ifmt_ctx)
            av_dump_format(f->handler.reader->ifmt_ctx, 0, f->handler.reader->filename, 0);
    }
}

int RecFile_AddVideoStream(RecFile *f, CodecType codec, int time_base, int width, int height, uint8_t *extradata, int extrasize)
{
    if (f->flag != REC_FLAGS_WRONLY)
        return -1;

    return RecFileWriter_AddVideoStream(f->handler.writer, codec, time_base, width, height, extradata, extrasize);
}

int RecFile_AddAudioStream(RecFile *f, CodecType codec, int time_base, int nb_channels, int frame_size, int bit_rate, int sample_rate, uint8_t *extradata, int extrasize)
{
    if (f->flag != REC_FLAGS_WRONLY)
        return -1;

    return RecFileWriter_AddAudioStream(f->handler.writer, codec, time_base, nb_channels, frame_size, bit_rate, sample_rate, extradata, extrasize);
}

int RecFile_Read(RecFile *f, RecFrame *frame)
{
    if (f->flag !=  REC_FLAGS_RDONLY)
        return -1;

    return RecFile_ReadFrame(f->handler.reader, frame);
}

int RecFile_Write(RecFile *f, RecFrame *frame)
{
    if (f->flag !=  REC_FLAGS_WRONLY)
        return -1;

    return RecFile_WriteFrame(f->handler.writer, frame);
}
