#ifndef __RECFILE_H__
#define __RECFILE_H__

#include <stdint.h>

#include "recmgr.h"

typedef enum {
    REC_FLAGS_WRONLY,
    REC_FLAGS_RDONLY,
} RecFlag;

typedef struct RecFile RecFile;

int RecFile_Init(void);
int RecFile_Quit(void);

RecFile *RecFile_Open(char *filepath, RecFlag flag);
int RecFile_Close(RecFile *f);
int RecFile_Seek(RecFile *f, int off_ms);

void RecFile_DumpInfo(RecFile *f);

int RecFile_AddVideoStream(RecFile *f, CodecType codec, int time_base, int width, int height, uint8_t *extradata, int extrasize);
int RecFile_AddAudioStream(RecFile *f, CodecType codec, int time_base, int nb_channels, int frame_size, int bit_rate, int sample_rate, uint8_t *extradata, int extrasize);

int RecFile_Read(RecFile *f, RecFrame *frame);
int RecFile_Write(RecFile *f, RecFrame *frame);

int RecFile_GetRecInfo(RecFile *f, RecInfo *info);

#endif
