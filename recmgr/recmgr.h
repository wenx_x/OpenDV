#ifndef __RECMGR_H__
#define __RECMGR_H__

#include <stdint.h>
#include <pthread.h>

#include "common.h"

typedef enum {
    REC_EVENT_VOLUME_ADDED,
    REC_EVENT_VOLUME_REMOVED,

    REC_EVENT_RECORD_STARTED,
    REC_EVENT_RECORD_STOPPED,
    REC_EVENT_RECORD_ABORTED,
} RecEventType;

typedef struct {
    RecEventType event;

    union {
        struct {
            char snapshot[128];
        } record_event;

        char volume[1024];
    } event_data;
} RecEvent;

#define MAX_FRAME_SIZE  (1024 * 1024)

typedef struct {
    MediaType media;
    CodecType codec;

    int64_t pts;

    int width;
    int height;

    int keyframe;

    int size;
    uint8_t data[MAX_FRAME_SIZE];
} RecFrame;

typedef struct {
    /* stream_id: 0 - normal record, 1 - thumbnail */
    int (*get_video_info)(long handle, int stream_id, VideoInfo *video_info);
    int (*get_audio_info)(long handle, int stream_id, AudioInfo *audio_info);

    long (*open)(void *priv_data);
    int (*close)(long handle);

    int (*read)(long handle, int stream_id, RecFrame *frame);

    int (*snapshot)(uint8_t *buf, int len, int *width, int *heigh);
} RecOps;

typedef struct RecChan RecChan;

typedef struct {
    RecFmt recfmt;
    int64_t max_rec_size;  /* in byte */
} RecConf;

int RecMgr_Init(RecConf *conf, int (*event_cb)(RecEvent *ev));
int RecMgr_Quit(void);

int RecMgr_SetConf(RecConf *conf);

int RecMgr_AddVolume(const char *path);
int RecMgr_DeleteVolume(const char *path);

RecChan *RecMgr_OpenChan(RecOps *ops, void *priv_data);
int RecMgr_CloseChan(RecChan *chan);

/* sort by time, order = 0, newest first, order = 1, oldest first */
int RecMgr_GetFilesList(int start_index, const char *suffix, FileItem *files, int nb_files, int order);

int RecMgr_GetFilesNum(const char *suffix);
int RecMgr_DeleteFile(const char *filename);

typedef struct Record Record;

Record *RecMgr_Open(const char *filename);
int RecMgr_Close(Record *rec);
int RecMgr_Read(Record *rec, RecFrame *frame);
int RecMgr_Seek(Record *rec, int off_ms);

void RecMgr_DumpRecInfo(Record *rec);

typedef struct RecRawFile RecRawFile;

RecRawFile *RecMgr_OpenRaw(char *filename);
int RecMgr_ReadRaw(RecRawFile *rawfile, uint8_t *buf, int len);
int RecMgr_CloseRaw(RecRawFile *rawfile);
int RecMgr_SeekRaw(RecRawFile *rawfile, uint64_t pos);

typedef struct {
    int has_video;
    int has_audio;

    VideoInfo video_info;
    AudioInfo audio_info;
} RecInfo;

int RecMgr_GetRecInfo(const char *filename, RecInfo *info);

int RecMgr_WriteJpg(uint8_t *jpgdata, int jpgsize);
int RecMgr_WriteJpgByName(const char *filepath, uint8_t *jpgdata, int jpgsize);

int RecMgr_GetRecFullpath(char *filename, char *filepath, int len);

#endif
