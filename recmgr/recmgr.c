#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "list.h"
#include "recfile.h"
#include "recmgr.h"
#include "logger.h"

static const char *TAG = "recmgr";

#define MAX_PIC_SIZE    (1024 * 1024)

#define NELEMS(x)   (sizeof(x) / sizeof((x)[0]))

#define MAX_STREAM_NUM  (2)

typedef struct {
    int stream_id;

    char filename[1024];

    RecChan *chan;
//    RecStreamDesc *desc;
    RecFile *recfile;

    int has_video;
    int has_audio;

    pthread_t record_tid;
    int stop_record;
} RecTask;

struct RecChan {
    RecOps *ops;
    void *priv_data;

    long rec_hndl;

    int stop_all_record;
    int stop_monitor;

    int started;
    int aborted;

    char snapshot[1024];

    pthread_t monitor_tid;
};

struct Record {
    RecFile *file;
};

typedef struct {
    char path[1024];
} RecVolume;

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

static RecConf recconf;

static RecVolume *currentVol = NULL;

static int initialized = 0;

static int (*gbl_event_cb)(RecEvent *ev);

int RecMgr_Init(RecConf *conf, int (*event_cb)(RecEvent *ev))
{
    pthread_mutex_lock(&mutex);
    if (initialized == 0) {
        recconf = *conf;
        RecFile_Init();
        gbl_event_cb = event_cb;
        initialized = 1;
    }
    pthread_mutex_unlock(&mutex);

    return 0;
}

int RecMgr_Quit(void)
{
    pthread_mutex_lock(&mutex);
    if (initialized > 0) {
        RecFile_Quit();
        initialized = 0;
    }
    pthread_mutex_unlock(&mutex);

    return 0;
}

int RecMgr_SetConf(RecConf *conf)
{
    pthread_mutex_lock(&mutex);
    recconf = *conf;
    pthread_mutex_unlock(&mutex);
    return 0;
}

static int get_filename(RecFmt format, char *normal, int normal_len, char *thumbnail, int thumbnail_len, char *snapshot, int snapshot_len)
{
    char *suffix = NULL;
    switch (format) {
    case REC_FMT_MP4:
        suffix = "MP4";
        break;
    case REC_FMT_MOV:
        suffix = "MOV";
        break;
    case REC_FMT_JPG:
        suffix = "JPG";
        break;
    default:
        return -1;
    }

    char datetime[64];

    struct tm t;
    time_t now = time(NULL);
    localtime_r(&now, &t);

    snprintf(datetime, sizeof(datetime),
            "%04d%02d%02dT%02d%02d%02d",
            t.tm_year + 1900, t.tm_mon, t.tm_mday,
            t.tm_hour, t.tm_min, t.tm_sec);

    snprintf(normal, normal_len, "%s/MAX_%s.%s", currentVol->path, datetime, suffix);

    if (thumbnail_len > 0) {
        snprintf(thumbnail, thumbnail_len, "%s/THM_%s.%s", currentVol->path, datetime, suffix);
    }

    if (snapshot_len > 0) {
        snprintf(snapshot, snapshot_len, "%s/SCN_%s.JPG", currentVol->path, datetime);
    }

    return 0;
}

static int get_filename_safe(RecFmt format, char *normal, int normal_len, char *thumbnail, int thumbnail_len, char *snapshot, int snapshot_len)
{
    pthread_mutex_lock(&mutex);
    int ret = get_filename(format, normal, normal_len, thumbnail, thumbnail_len, snapshot, snapshot_len);
    pthread_mutex_unlock(&mutex);
    return ret;
}

#if 0
static char *remove_leading_slash(char *filepath)
{
    char *p = filepath;

    while (*p == '/')
        p++;

    return p;
}
#endif

static int dispatch_event(RecChan *chan, RecEventType event_type)
{
    RecEvent ev;
    memset(&ev, 0, sizeof(RecEvent));

#if 0
    if (event_type != REC_EVENT_RECORD_ABORTED) {
        char *snapshot  = remove_leading_slash(chan->snapshot + strlen(currentVol->path));

        strncpy(ev.event_data.record_event.filepath,  filename,  sizeof(ev.event_data.record_event.filepath));
        strncpy(ev.event_data.record_event.thumbnail, thumbnail, sizeof(ev.event_data.record_event.thumbnail));
        strncpy(ev.event_data.record_event.snapshot,  snapshot,  sizeof(ev.event_data.record_event.snapshot));
    }
#endif

    ev.event = event_type;

    if (gbl_event_cb)
        gbl_event_cb(&ev);

    return 0;
}

int RecMgr_AddVolume(const char *path)
{
    if (currentVol != NULL) {
        fprintf(stderr, "volume already exist is %s\n", currentVol->path);
        return -1;
    }

    if (access(path, F_OK | R_OK | W_OK | X_OK) < 0) {
        fprintf(stderr, "RecMgr_AddVolume: add volume %s failed, %s\n", path, strerror(errno));
        return -1;
    }

    RecVolume *volume = malloc(sizeof(RecVolume));
    if (!volume)
        return -1;
    memset(volume, 0, sizeof(RecVolume));

    strncpy(volume->path, path, sizeof(volume->path) - 1);

    pthread_mutex_lock(&mutex);
    currentVol = volume;

    if (gbl_event_cb) {
        RecEvent ev = { .event = REC_EVENT_VOLUME_ADDED };
        strncpy(ev.event_data.volume, path, sizeof(ev.event_data.volume) - 1);
        gbl_event_cb(&ev);
    }

    pthread_mutex_unlock(&mutex);

    return 0;
}

int RecMgr_DeleteVolume(const char *path)
{
    if (!currentVol)
        return -1;

    if (strncmp(currentVol->path, path, sizeof(currentVol->path)) != 0)
        return -1;

    free(currentVol);
    currentVol = NULL;

    if (gbl_event_cb) {
        RecEvent ev = { .event = REC_EVENT_VOLUME_REMOVED };
        strncpy(ev.event_data.volume, path, sizeof(ev.event_data.volume) - 1);
        gbl_event_cb(&ev);
    }

    return 0;
}

static int reach_size_limit(int64_t size)
{
    pthread_mutex_lock(&mutex);
    if (recconf.max_rec_size > 0 && size > recconf.max_rec_size) {
        pthread_mutex_unlock(&mutex);
        return 1;
    }
    pthread_mutex_unlock(&mutex);

    return 0;
}

static void *record_thread(void *ctx)
{
    RecFrame *frame = NULL;

    RecTask *task = (RecTask *)ctx;
    RecChan *chan = task->chan;

    frame = malloc(sizeof(RecFrame));
    if (!frame)
        goto fail;

    int64_t total_size = 0;

    while (!chan->stop_all_record && !task->stop_record) {
        int got_frame = chan->ops->read(chan->rec_hndl, task->stream_id, frame);
        if (got_frame == 0)
            continue;
        else if (got_frame < 0)
            goto fail;

        if (frame->size > sizeof(frame->data)) {
            printf("invalid frame size\n");
            goto fail;
        }

        if (currentVol == NULL)
            goto fail;

        if (RecFile_Write(task->recfile, frame) < 0)
            goto fail;

        total_size += frame->size;
        if (reach_size_limit(total_size)) {
            LogV(TAG, "%s reach max record size", task->filename);
            break;
        }
    }

    free(frame);

    chan->stop_all_record = 1;

    LogV(TAG, "record stream %d stopped", task->stream_id);

    return NULL;

fail:
    if (frame)
        free(frame);

    chan->stop_all_record = 1;
    chan->aborted = 1;

    LogV(TAG, "record stream %d aborted", task->stream_id);

    return NULL;
}

static RecTask *RecTask_Create(RecChan *chan, int stream_id, char *filename, VideoInfo *videoInfo, AudioInfo *audioInfo)
{
    RecTask *task = malloc(sizeof(RecTask));
    if (!task)
        return NULL;
    memset(task, 0, sizeof(RecTask));

    task->chan = chan;
    task->stream_id = stream_id;

    strncpy(task->filename, filename, sizeof(task->filename) - 1);

    task->recfile = RecFile_Open(filename, REC_FLAGS_WRONLY);
    if (!task->recfile) {
        LogV(TAG, "RecFile_Open %s failed", filename);
        goto fail;
    }

    if (videoInfo) {
        if (RecFile_AddVideoStream(task->recfile,
                                   videoInfo->codec,     videoInfo->time_base,
                                   videoInfo->width,     videoInfo->height,
                                   videoInfo->extradata, videoInfo->extrasize) < 0) {
            LogV(TAG, "RecFile_AddVideoStream(%s) failed", filename);
            goto fail;
        }
        task->has_video = 1;
    }

    if (audioInfo) {
        if (RecFile_AddAudioStream(task->recfile,
                                   audioInfo->codec,       audioInfo->time_base,
                                   audioInfo->nb_channels, audioInfo->frame_size,
                                   audioInfo->bit_rate,    audioInfo->sample_rate,
                                   audioInfo->extradata,   audioInfo->extrasize) < 0) {
            LogV(TAG, "RecFile_AddAudioStream(%s) failed", filename);
            goto fail;
        }
        task->has_audio = 1;
    }

    RecFile_DumpInfo(task->recfile);

    return task;

fail:
    if (task->recfile)
        RecFile_Close(task->recfile);
    free(task);

    return NULL;
}

static int RecTask_Start(RecTask *task)
{
    if (pthread_create(&task->record_tid, NULL, record_thread, task) != 0)
        return -1;
    return 0;
}

static int RecTask_Wait(RecTask *task)
{
    if (task->record_tid) {
        pthread_join(task->record_tid, NULL);
        task->record_tid = 0;
    }
    return 0;
}

static int RecTask_Destroy(RecTask *task)
{
    task->stop_record = 1;

    if (task->record_tid) {
        pthread_join(task->record_tid, NULL);
        task->record_tid = 0;
    }

    if (task->recfile)
        RecFile_Close(task->recfile);
    free(task);

    return 0;
}

static void *record_monitor(void *arg)
{
    RecChan *chan = arg;

    int i;
    char filename[128];
    char thumbnail[128];
    char snapshot[128];

    VideoInfo videoInfo;
    AudioInfo audioInfo;

    RecTask *task[MAX_STREAM_NUM] = { NULL };

    chan->rec_hndl = chan->ops->open(chan->priv_data);
    if (!chan->rec_hndl)
        goto fail;

    while (!chan->stop_monitor) {
        int nb_streams = 0;
        chan->aborted = 0;
        chan->stop_all_record = 0;

        if (get_filename_safe(recconf.recfmt,
                              filename,  sizeof(filename),
                              thumbnail, sizeof(thumbnail),
                              snapshot,  sizeof(snapshot)) < 0)
            goto fail;

        /* create record tasks */
        for (i = 0; i < MAX_STREAM_NUM; i++) {
            int has_video = chan->ops->get_video_info(chan->rec_hndl, i, &videoInfo);
            int has_audio = chan->ops->get_audio_info(chan->rec_hndl, i, &audioInfo);

            if (has_video <= 0 && has_audio <= 0)
                continue;

            VideoInfo *vinfo = has_video > 0 ? &videoInfo : NULL;
            AudioInfo *ainfo = has_audio > 0 ? &audioInfo : NULL;
            char *recname = i == 0 ? filename : thumbnail;

            task[i] = RecTask_Create(chan, i, recname, vinfo, ainfo);
            if (!task[i])
                goto fail;

            nb_streams++;
        }

        if (nb_streams == 0)
            goto fail;

        /* start recording */
        for (i = 0; i < MAX_STREAM_NUM; i++) {
            if (task[i])
                RecTask_Start(task[i]);
        }

        /* take video snapshot */
        if (chan->ops->snapshot) {
            uint8_t *buf = malloc(MAX_PIC_SIZE);
            if (buf) {
                int width, height;
                int len = chan->ops->snapshot(buf, MAX_PIC_SIZE, &width, &height);
                if (len > 0) {
                    if (RecMgr_WriteJpgByName(snapshot, buf, len) == 0) {
                        strncpy(chan->snapshot, snapshot, sizeof(chan->snapshot) - 1);
                    }
                }
                free(buf);
            }
        }

        /* send record started event */
        if (!chan->started) {
            dispatch_event(chan, REC_EVENT_RECORD_STARTED);
            chan->started = 1;
        }

        /* wait recording finish */
        for (i = 0; i < MAX_STREAM_NUM; i++) {
            if (task[i]) {
                RecTask_Wait(task[i]);
                RecTask_Destroy(task[i]);
                task[i] = NULL;
            }
        }

        if (chan->aborted)
            goto fail;
    }

    chan->ops->close(chan->rec_hndl);
    chan->rec_hndl = 0;

    dispatch_event(chan, REC_EVENT_RECORD_STOPPED);

    return 0;

fail:
    chan->stop_all_record = 1;

    for (i = 0; i < MAX_STREAM_NUM; i++) {
        if (task[i])
            RecTask_Destroy(task[i]);
    }

    if (chan->rec_hndl) {
        chan->ops->close(chan->rec_hndl);
        chan->rec_hndl = 0;
    }

    dispatch_event(chan, REC_EVENT_RECORD_ABORTED);

    return NULL;
}

RecChan *RecMgr_OpenChan(RecOps *ops, void *priv_data)
{
    if (!ops || !ops->get_video_info || !ops->get_audio_info || !ops->open || !ops->close || !ops->read) {
        printf("RecMgr_OpenChan: invalid ops\n");
        return NULL;
    }

    RecChan *chan = malloc(sizeof(RecChan));
    if (!chan)
        return NULL;
    memset(chan, 0, sizeof(RecChan));

    chan->ops = ops;
    chan->priv_data = priv_data;

    pthread_mutex_lock(&mutex);
    if (currentVol == NULL) {
        pthread_mutex_unlock(&mutex);
        free(chan);
        return NULL;
    }

    int ret = pthread_create(&chan->monitor_tid, NULL, record_monitor, chan);
    if (ret != 0) {
        pthread_mutex_unlock(&mutex);
        free(chan);
        return NULL;
    }

    pthread_mutex_unlock(&mutex);

    return chan;
}

int RecMgr_CloseChan(RecChan *chan)
{
    chan->stop_monitor = 1;
    chan->stop_all_record = 1;

    if (chan->monitor_tid) {
        pthread_join(chan->monitor_tid, NULL);
        chan->monitor_tid = 0;
    }

    free(chan);

    return 0;
}

static int get_file_list(char *path, int index, const char *suffix, FileItem *items, int nb_items, int order)
{
    char name_filter[128] = {0};

    if (suffix && strlen(suffix) > 0) {
        char *token, *string, *tofree;

        tofree = string = strdup(suffix);

        int nsuffix = 0;
        while ((token = strsep(&string, ",")) != NULL) {
            if (nsuffix != 0)
                strcat(name_filter, " -o ");
            strcat(name_filter, "-name '*");
            strcat(name_filter, token);
            strcat(name_filter, "'");
            nsuffix++;
        }

        free(tofree);
    }

    char cmd[1024];

    if (order == 0) {
        snprintf(cmd, sizeof(cmd), "ls -t $(find %s -type f %s)", path, name_filter);
    } else {
        snprintf(cmd, sizeof(cmd), "ls -rt $(find %s -type f %s)", path, name_filter);
    }

//    printf("cmd: %s\n", cmd);

    FILE *fp = popen(cmd, "r");
    if (!fp) {
        LogV(TAG, "popen %s failed: %s", cmd, strerror(errno));
        return -1;
    }

    int count = 0;
    int pathlen = strlen(path);
    int in_path = 0;

    char filepath[1024];

    int curidx = 0;

    while (fgets(filepath, sizeof(filepath), fp) != NULL) {
        curidx++;
        if (curidx <= index)
            continue;

        char *CR = strchr(filepath, '\r');
        if (CR)
            *CR = '\0';

        char *LF = strchr(filepath, '\n');
        if (LF)
            *LF = '\0';

        struct stat st;
        if (stat(filepath, &st) != 0)
            continue;

        if (!S_ISREG(st.st_mode))
            continue;

        if (!in_path) {
            if (strncmp(path, filepath, pathlen) != 0) {
                count = 0;
                break;
            }
            in_path = 1;
        }

        FileItem *item = &items[count];
        memset(item, 0, sizeof(FileItem));

        char *ptr = filepath + strlen(path);    /* remove mount point */
        if (*ptr == '/')
            ptr++;

        if (*ptr == '/')
            ptr++;

        strncpy(item->filename, ptr, sizeof(item->filename) - 1);
        item->filesize = st.st_size;
        item->index    = curidx - 1;

        struct tm t;
        gmtime_r(&st.st_mtime, &t);

        snprintf(item->datetime, sizeof(item->datetime), "%04d-%02d-%02dT%02d:%02d:%02d",
                t.tm_year + 1900, t.tm_mon + 1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec);

        count++;
        if (count >= nb_items)
            break;
    }

    pclose(fp);
    return count;
}

int RecMgr_GetFilesList(int start_index, const char *suffix, FileItem *files, int nb_files, int order)
{
    if (currentVol == NULL)
        return -1;

    return get_file_list(currentVol->path, start_index, suffix, files, nb_files, order);
}

int RecMgr_DeleteFile(const char *filename)
{
    pthread_mutex_lock(&mutex);
    if (currentVol == NULL) {
        pthread_mutex_unlock(&mutex);
        return -1;
    }

    char filepath[1024];
    snprintf(filepath, sizeof(filepath), "%s/%s", currentVol->path, filename);
    pthread_mutex_unlock(&mutex);

    return unlink(filepath);
}

static int get_total_filenum(char *path, const char *suffix)
{
    char name_filter[128] = {0};

    if (suffix && strlen(suffix) > 0) {
        char *token, *string, *tofree;

        tofree = string = strdup(suffix);

        int nsuffix = 0;
        while ((token = strsep(&string, ",")) != NULL) {
            if (nsuffix != 0)
                strcat(name_filter, " -o ");
            strcat(name_filter, "-name '*");
            strcat(name_filter, token);
            strcat(name_filter, "'");
            nsuffix++;
        }
        printf("name_filter: %s\n", name_filter);

        free(tofree);
    }

    char cmd[1024];
    snprintf(cmd, sizeof(cmd), "find %s -type f %s | wc -l", path, name_filter);

//    printf("cmd: %s\n", cmd);

    FILE *fp = popen(cmd, "r");
    if (!fp) {
        LogV(TAG, "popen %s failed: %s", cmd, strerror(errno));
        return -1;
    }

    char result[128];
    if (fgets(result, sizeof(result), fp) == NULL) {
        pclose(fp);
        return 0;
    }

    return atoi(result);
}

int RecMgr_GetFilesNum(const char *suffix)
{
    if (currentVol == NULL)
        return -1;

    return get_total_filenum(currentVol->path, suffix);
}

Record *RecMgr_Open(const char *filename)
{
    if (currentVol == NULL)
        return NULL;

    Record *rec = malloc(sizeof(Record));
    if (!rec)
        return NULL;
    memset(rec, 0, sizeof(Record));

    char filepath[1024];
    snprintf(filepath, sizeof(filepath), "%s/%s", currentVol->path, filename);

    rec->file = RecFile_Open(filepath, REC_FLAGS_RDONLY);
    if (!rec->file) {
        free(rec);
        return NULL;
    }

    return rec;
}

int RecMgr_Close(Record *rec)
{
    if (!rec)
        return -1;

    RecFile_Close(rec->file);
    free(rec);

    return 0;
}

void RecMgr_DumpRecInfo(Record *rec)
{
    if (rec->file)
        RecFile_DumpInfo(rec->file);
}

int RecMgr_Read(Record *rec, RecFrame *frame)
{
    if (!rec)
        return -1;

    return RecFile_Read(rec->file, frame);
}

int RecMgr_Seek(Record *rec, int off_ms)
{
    return RecFile_Seek(rec->file, off_ms);
}

struct RecRawFile {
    int fd;
    char filepath[1024];
};

RecRawFile *RecMgr_OpenRaw(char *filename)
{
    RecRawFile *rawfile = malloc(sizeof(RecRawFile));
    if (!rawfile)
        return NULL;
    memset(rawfile, 0, sizeof(RecRawFile));

    pthread_mutex_lock(&mutex);
    if (currentVol == NULL) {
        free(rawfile);
        pthread_mutex_unlock(&mutex);
        return NULL;
    }

    snprintf(rawfile->filepath, sizeof(rawfile->filepath), "%s/%s", currentVol->path, filename);
    pthread_mutex_unlock(&mutex);

    rawfile->fd = open(rawfile->filepath, O_RDONLY);
    if (rawfile->fd < 0) {
        free(rawfile);
        return NULL;
    }

    return rawfile;
}

int RecMgr_ReadRaw(RecRawFile *rawfile, uint8_t *buf, int len)
{
    if (!rawfile || rawfile->fd < 0)
        return -1;
    return read(rawfile->fd, buf, len);
}

int RecMgr_SeekRaw(RecRawFile *rawfile, uint64_t pos)
{
    if (!rawfile || rawfile->fd < 0)
        return -1;
    return lseek(rawfile->fd, pos, SEEK_SET);
}

int RecMgr_CloseRaw(RecRawFile *rawfile)
{
    if (!rawfile || rawfile->fd < 0)
        return -1;
    close(rawfile->fd);
    free(rawfile);
    return 0;
}

int RecMgr_GetRecInfo(const char *filename, RecInfo *info)
{
    Record *rec = RecMgr_Open(filename);
    if (!rec)
        return -1;

    if (RecFile_GetRecInfo(rec->file, info) < 0) {
        RecMgr_Close(rec);
        return -1;
    }

    RecMgr_Close(rec);

    return 0;
}

static int write_file(const char *filename, void *buf, int len)
{
    int fd = open(filename, O_CREAT | O_TRUNC | O_WRONLY, 0644);
    if (fd < 0) {
        LogV(TAG, "open %s failed: %s", filename, strerror(errno));
        return -1;
    }

    if (write(fd, buf, len) != len) {
        LogV(TAG, "write %s failed: %s", filename, strerror(errno));
        close(fd);
        return -1;
    }

    close(fd);

    return 0;
}

int RecMgr_WriteJpg(uint8_t *jpgdata, int jpgsize)
{
    char filepath[256] = {0};

    if (get_filename_safe(REC_FMT_JPG, filepath, sizeof(filepath), NULL, 0, NULL, 0) < 0)
        return -1;

    if (write_file(filepath, jpgdata, jpgsize) < 0)
        return -1;

    LogV(TAG, "%s saved", filepath);

    return 0;
}

int RecMgr_WriteJpgByName(const char *filepath, uint8_t *jpgdata, int jpgsize)
{
    if (write_file(filepath, jpgdata, jpgsize) < 0)
        return -1;

    LogV(TAG, "%s saved", filepath);

    return 0;
}

int RecMgr_GetRecFullpath(char *filename, char *filepath, int len)
{
    pthread_mutex_lock(&mutex);
    if (currentVol == NULL) {
        pthread_mutex_unlock(&mutex);
        return -1;
    }

    snprintf(filepath, len, "%s/%s", currentVol->path, filename);
    pthread_mutex_unlock(&mutex);

    return 0;
}
