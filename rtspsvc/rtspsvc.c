#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#include <netdb.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#ifdef HAVE_LINUX_IP_H
#include <linux/ip.h>
#endif

#include "url.h"
#include "list.h"
#include "rtcp.h"
#include "base64.h"
#include "netio.h"
#include "queue.h"
#include "rtspsvc.h"

#define min(x, y)       ((x) < (y) ? (x) : (y))
#define max(x, y)       ((x) > (y) ? (x) : (y))
#define NELEMS(a)       (sizeof(a) / sizeof((a)[0]))

#define MAX_UDP_PAYLOAD_SIZE    (1472)
#define MAX_RTP_PAYLOAD_SIZE    (MAX_UDP_PAYLOAD_SIZE - 12) /* minus sizeof(rtp_hdr_t) */

#define RTCP_SDES_CNAME "OpenDV"
#define RTSP_SERVER     "OpenDV RTSP Server"

typedef enum {
    RtspState_Init       = 0,
    RtspState_Ready      = 1,
    RtspState_Playing    = 2,
    RtspState_Recording  = 3,
} RtspState;

typedef struct {
    char path[1024];
    RtspHandler handler;
} RtspRoute;

struct RtspSvc {
    int server_fd;
    int port;

    pthread_t service_tid;
    int abort;

    pthread_mutex_t r_lock;
    JList *allroutes;

    pthread_mutex_t s_lock;
    JList *allsessions;
};

typedef struct rtp_hdr_s {
#ifdef BIGENDIAN
    uint8_t v:2;            /* protocol version */
    uint8_t p:1;            /* padding flag */
    uint8_t x:1;            /* header extension flag */
    uint8_t cc:4;           /* CSRC count */
    uint8_t m:1;            /* marker bit */
    uint8_t pt:7;           /* payload type */
#else
    uint8_t cc:4;
    uint8_t x:1;
    uint8_t p:1;
    uint8_t v:2;
    uint8_t pt:7;
    uint8_t m:1;
#endif
    uint16_t seq;           /* sequence number */
    uint32_t ts;            /* timestamp */
    uint32_t ssrc;          /* synchronization source */
//    uint32_t csrc[2];     /* optional CSRC list */
} rtp_hdr_t;

typedef struct RtpStream {
    int enabled;

    int rtp_fd;
    int rtcp_fd;

    int rtp_port;
    int rtcp_port;

    int client_rtp_port;
    int client_rtcp_port;

    MediaDesc desc;

    rtp_hdr_t rtp_hdr;
    rtp_stat_t stat;
} RtpStream;

typedef struct {
    RtspSvc *rtspsvc;

    int rtsp_fd;

    pthread_t session_tid;
    pthread_t streame_tid;

    int abort_session;

    RtspRoute *route;
    UrlData *url;

    int has_video;
    int has_audio;
    RtpStream video_st;
    RtpStream audio_st;

    char session_id[64];

    RtspState state;
    int max_payload_size;

    struct sockaddr_in peeraddr;

    /* handle rtsp request */
    char  buf[8192];
    char *ptr;
    char *end;

    JQueue *msgq;
} RtspSession;

#define MAX_SESSION_TIMEOUT     (30)

#define SPACE_CHARS " \t\r\n"

static void skip_spaces(char **pp)
{
    char *p;
    p = *pp;
    while (*p == ' ' || *p == '\t')
        p++;
    *pp = p;
}

static void get_word(char *buf, int buf_size, char **pp)
{
    char *p;
    char *q;

    p = *pp;
    skip_spaces(&p);
    q = buf;
    while (!isspace(*p) && *p != '\0') {
        if ((q - buf) < buf_size - 1)
            *q++ = *p;
        p++;
    }
    if (buf_size > 0)
        *q = '\0';
    *pp = p;
}

static void get_word_until_chars(char *buf, int buf_size,
        char *sep, char **pp)
{
    char *p;
    char *q;

    p = *pp;
    p += strspn(p, SPACE_CHARS);
    q = buf;
    while (!strchr(sep, *p) && *p != '\0') {
        if ((q - buf) < buf_size - 1)
            *q++ = *p;
        p++;
    }
    if (buf_size > 0)
        *q = '\0';
    *pp = p;
}

static void get_word_sep(char *buf, int buf_size, char *sep,
        char **pp)
{
    if (**pp == '/') (*pp)++;
    get_word_until_chars(buf, buf_size, sep, pp);
}

/* get one line which terminated by '\n', "\r\n" or NULL */
static int get_line(char *buf, int buf_size, char **pp)
{
    int len;
    char *p = *pp;
    char *p1, *p2;

    if (!buf || (buf_size <= 0) || !pp)
        return -EINVAL;

    if (buf_size == 1) {
        buf[0] = '\0';
        return 0;
    }

    p1 = strchr(p, '\n');
    if (p1 == NULL) {
        /* line terminated by NULL */
        len = buf_size - 1;     /* lefe one byte for '\0' */
        strncpy(buf, p, len);
        buf[len] = '\0';
        *pp = p + strlen(buf);
        return 0;
    }

    switch (p1 - p) {
    case 0: /* empty line terminated by '\n' */
        buf[0] = '\0';
        *pp += 1;
        break;
    case 1:
        if (*(p1-1) == '\r') { /* empty line terminated by "\r\n" */
            buf[0] = '\0';
        } else {
            buf[0] = p[0];
            buf[1] = '\0';
        }

        *pp += 2;
        break;
    default:
        if (*(p1-1) == '\r')
            p2 = p1 - 1;
        else
            p2 = p1;

        len = min(p2 - p, buf_size - 1);    /* lefe one byte for '\0' */
        memcpy(buf, p, len);
        buf[len] = '\0';

        *pp = p1 + 1;   /* skip to next line */
    }

    return 0;
}

static int stristart(char *str, const char *pfx, char **ptr)
{
    while (*pfx && toupper((unsigned)*pfx) == toupper((unsigned)*str)) {
        pfx++;
        str++;
    }
    if (!*pfx && ptr)
        *ptr = str;
    return !*pfx;
}

typedef struct {
    char method[64];
    char url[1024];
    char protocol[64];

    char session[512];

    int cseq;

    char user_agent[128];
    char accept[128];
    char transport[1024];
    char range[128];
    char scale[32];

    char content_base[1024];
    char content_type[128];
    int  content_length;
} RTSPMessageHeader;

static char supported_method[][64] = {
    "OPTIONS", "DESCRIBE", "SETUP", "PLAY", "PAUSE", "TEARDOWN", "GET_PARAMETER"
};

static int valid_method(char *method)
{
    int i;
    for (i = 0; i < NELEMS(supported_method); i++) {
        if (strcmp(method, supported_method[i]) == 0)
            return 1;
    }

    return 0;
}

static int rtsp_parse_line(char *line, RTSPMessageHeader *h)
{
    char *p = line;

    skip_spaces(&p);

    if (stristart(p, "Cseq:", &p)) {
        h->cseq = strtol(p, NULL, 10);
    } else if (stristart(p, "User-Agent:", &p)) {
        skip_spaces(&p);
        strncpy(h->user_agent, p, sizeof(h->user_agent) - 1);
    } else if (stristart(p, "Accept:", &p)) {
        skip_spaces(&p);
        strncpy(h->accept, p, sizeof(h->accept) - 1);
    } else if (stristart(p, "Transport:", &p)) {
        skip_spaces(&p);
        strncpy(h->transport, p, sizeof(h->transport) - 1);
    } else if (stristart(p, "Range:", &p)) {
        skip_spaces(&p);
        strncpy(h->range, p, sizeof(h->range) - 1);
    } else if (stristart(p, "Scale:", &p)) {
        skip_spaces(&p);
        strncpy(h->scale, p, sizeof(h->scale) - 1);
    } else if (stristart(p, "Content-Base:", &p)) {
        skip_spaces(&p);
        strncpy(h->content_base, p, sizeof(h->content_base) - 1);
    } else if (stristart(p, "Content-Type:", &p)) {
        skip_spaces(&p);
        strncpy(h->content_type, p, sizeof(h->content_type) - 1);
    } else if (stristart(p, "Content-Length:", &p)) {
        skip_spaces(&p);
        h->content_length = atoi(p);
    } else if (stristart(p, "Session:", &p)) {
        skip_spaces(&p);
        strncpy(h->session, p, sizeof(h->session) - 1);
    }

    return 0;
}

static int rtsp_parse_request(char *buf, RTSPMessageHeader *h)
{
    memset(h, 0, sizeof(RTSPMessageHeader));

    char *p = buf;

    get_word(h->method, sizeof(h->method), &p);
    get_word(h->url, sizeof(h->url), &p);
    get_word(h->protocol, sizeof(h->protocol), &p);

    /* check version name */
    if (strcmp(h->protocol, "RTSP/1.0") != 0)
        return -1;

    if (!valid_method(h->method))
        return -1;

    char line[1024];

    while (*p != '\0') {
        if (get_line(line, sizeof(line), &p) < 0)
            return -1;

        if (line[0] == '\0')
            continue;   /* skip empty line */

        rtsp_parse_line(line, h);
    }

    return 0;
}

static int rtsp_cmd_options(RtspSession *s, RTSPMessageHeader *h)
{
    char response[4096] = {0};

    if (strlen(h->session) > 0) {
        snprintf(response, sizeof(response),
                "RTSP/1.0 200 OK\r\n"
                "CSeq: %d\r\n"
                "Public: OPTIONS, DESCRIBE, SETUP, TEARDOWN, PLAY, GET_PARAMETER\r\n"
                "Session: %s\r\n"
                "\r\n",
                h->cseq, s->session_id);
    } else {
        snprintf(response, sizeof(response),
                "RTSP/1.0 200 OK\r\n"
                "CSeq: %d\r\n"
                "Public: OPTIONS, DESCRIBE, SETUP, TEARDOWN, PLAY, GET_PARAMETER\r\n"
                "\r\n",
                h->cseq);
    }

    if (j_net_writen_timedwait(s->rtsp_fd, response, strlen(response)) < 0)
        return -1;

    return 0;
}

static int socket_address(int fd, char *ipaddr, int len)
{
    struct sockaddr_in localaddr;
    socklen_t addrlen = sizeof(struct sockaddr_in);

    if (getsockname(fd, (struct sockaddr *)&localaddr, &addrlen) < 0)
        return -1;

    if (inet_ntop(AF_INET, &localaddr.sin_addr, ipaddr, len) == NULL)
        return -1;

    return 0;
}

typedef struct PayloadType {
    MediaType media;
    CodecType codec;
    char mine[32];
    int payload_type;
    int track_id;
} PayloadType;

#define VIDEO_TRACK_ID  (0)
#define AUDIO_TRACK_ID  (1)

static PayloadType payload_maps[] = {
    { MEDIA_TYPE_VIDEO, CODEC_TYPE_H264, "H264",          96, VIDEO_TRACK_ID },
    { MEDIA_TYPE_AUDIO, CODEC_TYPE_PCMU, "PCMU",          0,  AUDIO_TRACK_ID },
    { MEDIA_TYPE_AUDIO, CODEC_TYPE_PCMA, "PCMA",          8,  AUDIO_TRACK_ID },
    { MEDIA_TYPE_AUDIO, CODEC_TYPE_AAC,  "MPEG4-GENERIC", 97, AUDIO_TRACK_ID },
};

static PayloadType *find_payload(CodecType codec)
{
    int i;
    for (i = 0; i < NELEMS(payload_maps); i++) {
        PayloadType *payload = &payload_maps[i];
        if (payload->codec == codec)
            return payload;
    }
    return NULL;
}

static const int avpriv_mpeg4audio_sample_rates[16] = {
    96000, 88200, 64000, 48000, 44100, 32000,
    24000, 22050, 16000, 12000, 11025, 8000, 7350
};

static int latm_context2config(MediaDesc *desc, int profile, char *aac_config, int len)
{
    uint8_t config[2] = {0};
    uint8_t type = profile + 1;

    int rate_index;
    for (rate_index = 0; rate_index < 16; rate_index++)
        if (avpriv_mpeg4audio_sample_rates[rate_index] == desc->sample_rate)
            break;
    if (rate_index == 16)
        return -1;

    config[0] = (type << 3) | (rate_index >> 1);
    config[1] = (rate_index << 7) | (desc->audio_channels << 3);
    snprintf(aac_config, len, "%02x%02x", config[0], config[1]);

    return 0;
}

static int rtsp_make_sdp(RtspSession *s, RTSPMessageHeader *h, char *sdp, int sdp_len)
{
    RtspRoute *route = s->route;
    if (!route)
        return -1;

    RtspHandler *handler = &route->handler;
    if (!handler->describe)
        return -1;

    MediaDesc descs[8];
    int ndescs = handler->describe(s->url->path, descs, NELEMS(descs));
    if (ndescs <= 0)
        return -1;

    char *p = sdp;
    char *e = sdp + sdp_len;

    snprintf(p, e - p, "v=0\r\n");
    p += strlen(p);

    char ipaddr[64] = {0};
    socket_address(s->rtsp_fd, ipaddr, sizeof(ipaddr));

    snprintf(p, e - p, "o=- %ld %d IN IP4 %s\r\n", time(NULL), 1, ipaddr);
    p += strlen(p);

    snprintf(p, e - p, "s=%s\r\n", RTSP_SERVER);
    p += strlen(p);

    snprintf(p, e - p, "t=0 0\r\n");
    p += strlen(p);

    int i;
    for (i = 0; i < ndescs; i++) {
        MediaDesc *desc = &descs[i];

        PayloadType *payload = find_payload(desc->codec);
        if (!payload)
            continue;

        char *media_type = NULL;

        switch (payload->media) {
        case MEDIA_TYPE_VIDEO:
            if (s->has_video)
                continue;   /* allready has video stream */
            s->has_video = 1;
            media_type = "video";
            memcpy(&s->video_st.desc, desc, sizeof(MediaDesc));
            break;
        case MEDIA_TYPE_AUDIO:
            if (s->has_audio)
                continue;   /* allready has audio stream */
            s->has_audio = 1;
            media_type = "audio";
            memcpy(&s->audio_st.desc, desc, sizeof(MediaDesc));
            break;
        default:
            continue;
        }

        snprintf(p, e - p, "m=%s 0 RTP/AVP %d\r\n", media_type, payload->payload_type);
        p += strlen(p);

        if (desc->duration > 0) {
            snprintf(p, e - p, "a=range:npt=0-%d\r\n", desc->duration / 1000);
            p += strlen(p);
        }

        if (payload->media == MEDIA_TYPE_AUDIO && desc->audio_channels > 0) {
            snprintf(p, e - p, "a=rtpmap:%d %s/%d/%d\r\n", payload->payload_type, payload->mine, desc->sample_rate, desc->audio_channels);
            p += strlen(p);
        } else {
            snprintf(p, e - p, "a=rtpmap:%d %s/%d\r\n", payload->payload_type, payload->mine, desc->sample_rate);
            p += strlen(p);
        }

        if (desc->codec == CODEC_TYPE_H264) {
            if (desc->sps_len > 0 && desc->pps_len > 0) {
                char b64_sps[512];
                char b64_pps[128];
                snprintf(p, e - p,
                        "a=fmtp:%d packetization-mode=1;"
                        "profile-level-id=%02X%02X%02X;"
                        "sprop-parameter-sets=%s,%s\r\n",
                        payload->payload_type, desc->sps[4 + 1], desc->sps[4 + 2], desc->sps[4 + 3],
                        base64_encode(b64_sps, sizeof(b64_sps), desc->sps + 4, desc->sps_len - 4),
                        base64_encode(b64_pps, sizeof(b64_pps), desc->pps + 4, desc->pps_len - 4));
                p += strlen(p);
            }
        } else if (desc->codec == CODEC_TYPE_AAC) {
            int profile = 1;
            char config[64] = {0};
            if (latm_context2config(desc, profile, config, sizeof(config)) < 0)
                return -1;
            snprintf(p, e - p, "a=fmtp:%d profile-level-id=%d;mode=AAC-hbr;"
                    "sizelength=13;indexlength=3;indexdeltalength=3;config=%s\r\n",
                    payload->payload_type, profile, config);
            p += strlen(p);
        }

        snprintf(p, e - p, "a=control:track%d\r\n", payload->track_id);
        p += strlen(p);
    }

    return 0;
}

static int rtsp_cmd_describe(RtspSession *s, RTSPMessageHeader *h)
{
    if (strlen(h->accept) > 0 && strcasecmp(h->accept, "application/sdp") != 0)
        return -1;

    char sdp[4096] = {0};
    if (rtsp_make_sdp(s, h, sdp, sizeof(sdp)) < 0)
        return -1;

    char response[4096] = {0};
    snprintf(response, sizeof(response),
            "RTSP/1.0 200 OK\r\n"
            "CSeq: %d\r\n"
            "Content-Length: %d\r\n"
            "Content-Type: application/sdp\r\n"
            "\r\n"
            "%s",
            h->cseq, strlen(sdp), sdp);

    if (j_net_writen_timedwait(s->rtsp_fd, response, strlen(response)) < 0)
        return -1;

    return 0;
}

static int read_random(uint32_t *dst, const char *file)
{
    int fd = open(file, O_RDONLY);
    int err = -1;

    if (fd == -1)
        return -1;
    err = read(fd, dst, sizeof(*dst));
    close(fd);

    return err;
}

static int get_random_num(void)
{
    uint32_t num;
    if (read_random(&num, "/dev/urandom") == sizeof(num))
        return num;

    if (read_random(&num, "/dev/random") == sizeof(num))
        return num;

    struct timeval tv;
    gettimeofday(&tv, NULL);
    srandom(tv.tv_usec);
    return random();
}

#define MAX_TRY (100)
static int get_server_port(int *rtp_fd, int *rtp_port, int *rtcp_fd, int *rtcp_port)
{
    struct sockaddr_in addr;

    int i;
    for (i = 0; i < MAX_TRY; i++) {
        *rtp_port = get_random_num() & 0xfffe;
        *rtcp_port = *rtp_port + 1;

        *rtp_fd = socket(AF_INET, SOCK_DGRAM, 0);
        if (*rtp_fd < 0)
            continue;

        addr.sin_family = AF_INET;
        addr.sin_port = htons(*rtp_port);
        addr.sin_addr.s_addr = htonl(INADDR_ANY);

        if (bind(*rtp_fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
            close(*rtp_fd);
            continue;
        }

#ifdef HAVE_LINUX_IP_H
        uint8_t service_type = IPTOS_PREC_CRITIC_ECP | IPTOS_LOWDELAY;
        if (setsockopt(*rtp_fd, SOL_IP, IP_TOS, (void *)&service_type, sizeof(service_type)) < 0) {
            close(*rtp_fd);
            continue;
        }
#endif

        *rtcp_fd = socket(AF_INET, SOCK_DGRAM, 0);
        if (*rtcp_fd < 0) {
            close(*rtp_fd);
            continue;
        }

        addr.sin_family = AF_INET;
        addr.sin_port = htons(*rtcp_port);
        addr.sin_addr.s_addr = htonl(INADDR_ANY);

        if (bind(*rtcp_fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
            close(*rtcp_fd);
            close(*rtp_fd);
            continue;
        }

        break;
    }

    if (i == MAX_TRY)
        return -1;

    return 0;
}

enum RTSPTransport {
    RTSP_TRANSPORT_RTP, /**< Standards-compliant RTP */
    RTSP_TRANSPORT_RDT, /**< Realmedia Data Transport */
    RTSP_TRANSPORT_RAW, /**< Raw data (over UDP) */
    RTSP_TRANSPORT_NB
};

/**
 * Network layer over which RTP/etc packet data will be transported.
 */
enum RTSPLowerTransport {
    RTSP_LOWER_TRANSPORT_UDP = 0,           /**< UDP/unicast */
    RTSP_LOWER_TRANSPORT_TCP = 1,           /**< TCP; interleaved in RTSP */
    RTSP_LOWER_TRANSPORT_UDP_MULTICAST = 2, /**< UDP/multicast */
    RTSP_LOWER_TRANSPORT_NB,
    RTSP_LOWER_TRANSPORT_HTTP = 8,          /**< HTTP tunneled - not a proper
                                              transport mode as such,
                                              only for use via AVOptions */
    RTSP_LOWER_TRANSPORT_CUSTOM = 16,       /**< Custom IO - not a public
                                              option for lower_transport_mask,
                                              but set in the SDP demuxer based
                                              on a flag. */
};

typedef struct RTSPTransportField {
    /** interleave ids, if TCP transport; each TCP/RTSP data packet starts
     * with a '$', stream length and stream ID. If the stream ID is within
     * the range of this interleaved_min-max, then the packet belongs to
     * this stream. */
    int interleaved_min, interleaved_max;

    /** UDP multicast port range; the ports to which we should connect to
     * receive multicast UDP data. */
    int port_min, port_max;

    /** UDP client ports; these should be the local ports of the UDP RTP
     * (and RTCP) sockets over which we receive RTP/RTCP data. */
    int client_port_min, client_port_max;

    /** UDP unicast server port range; the ports to which we should connect
     * to receive unicast UDP RTP/RTCP data. */
    int server_port_min, server_port_max;

    /** time-to-live value (required for multicast); the amount of HOPs that
     * packets will be allowed to make before being discarded. */
    int ttl;

    /** transport set to record data */
    int mode_record;

    struct sockaddr_storage destination; /**< destination IP address */
    char source[INET6_ADDRSTRLEN + 1]; /**< source IP address */

    /** data/packet transport protocol; e.g. RTP or RDT */
    enum RTSPTransport transport;

    /** network layer transport protocol; e.g. TCP or UDP uni-/multicast */
    enum RTSPLowerTransport lower_transport;
} RTSPTransportField;

static void rtsp_parse_range(int *min_ptr, int *max_ptr, char **pp)
{
    char *q;
    char *p;
    int v;

    q = *pp;
    q += strspn(q, SPACE_CHARS);
    v = strtol(q, &p, 10);
    if (*p == '-') {
        p++;
        *min_ptr = v;
        v = strtol(p, &p, 10);
        *max_ptr = v;
    } else {
        *min_ptr = v;
        *max_ptr = v;
    }
    *pp = p;
}

static int get_sockaddr(const char *buf, struct sockaddr_storage *sock)
{
    struct addrinfo hints = { 0 }, *ai = NULL;
    hints.ai_flags = AI_NUMERICHOST;
    if (getaddrinfo(buf, NULL, &hints, &ai))
        return -1;
    memcpy(sock, ai->ai_addr, min(sizeof(*sock), ai->ai_addrlen));
    freeaddrinfo(ai);
    return 0;
}

static void rtsp_parse_transport(RTSPTransportField *th, char *p)
{
    char transport_protocol[16];
    char profile[16];
    char lower_transport[16];
    char parameter[16];
    char buf[256];

    memset(th, 0, sizeof(RTSPTransportField));

    p += strspn(p, SPACE_CHARS);
    if (*p == '\0')
        return;

    get_word_sep(transport_protocol, sizeof(transport_protocol),
            "/", &p);
    if (!strcasecmp (transport_protocol, "rtp")) {
        get_word_sep(profile, sizeof(profile), "/;,", &p);
        lower_transport[0] = '\0';
        /* rtp/avp/<protocol> */
        if (*p == '/') {
            get_word_sep(lower_transport, sizeof(lower_transport),
                    ";,", &p);
        }
        th->transport = RTSP_TRANSPORT_RTP;
    } else if (!strcasecmp (transport_protocol, "x-pn-tng") ||
            !strcasecmp (transport_protocol, "x-real-rdt")) {
        /* x-pn-tng/<protocol> */
        get_word_sep(lower_transport, sizeof(lower_transport), "/;,", &p);
        profile[0] = '\0';
        th->transport = RTSP_TRANSPORT_RDT;
    } else if (!strcasecmp(transport_protocol, "raw")) {
        get_word_sep(profile, sizeof(profile), "/;,", &p);
        lower_transport[0] = '\0';
        /* raw/raw/<protocol> */
        if (*p == '/') {
            get_word_sep(lower_transport, sizeof(lower_transport),
                    ";,", &p);
        }
        th->transport = RTSP_TRANSPORT_RAW;
    }
    if (!strcasecmp(lower_transport, "TCP"))
        th->lower_transport = RTSP_LOWER_TRANSPORT_TCP;
    else
        th->lower_transport = RTSP_LOWER_TRANSPORT_UDP;

    if (*p == ';')
        p++;
    /* get each parameter */
    while (*p != '\0' && *p != ',') {
        get_word_sep(parameter, sizeof(parameter), "=;,", &p);
        if (!strcmp(parameter, "port")) {
            if (*p == '=') {
                p++;
                rtsp_parse_range(&th->port_min, &th->port_max, &p);
            }
        } else if (!strcmp(parameter, "client_port")) {
            if (*p == '=') {
                p++;
                rtsp_parse_range(&th->client_port_min,
                        &th->client_port_max, &p);
            }
        } else if (!strcmp(parameter, "server_port")) {
            if (*p == '=') {
                p++;
                rtsp_parse_range(&th->server_port_min,
                        &th->server_port_max, &p);
            }
        } else if (!strcmp(parameter, "interleaved")) {
            if (*p == '=') {
                p++;
                rtsp_parse_range(&th->interleaved_min,
                        &th->interleaved_max, &p);
            }
        } else if (!strcmp(parameter, "multicast")) {
            if (th->lower_transport == RTSP_LOWER_TRANSPORT_UDP)
                th->lower_transport = RTSP_LOWER_TRANSPORT_UDP_MULTICAST;
        } else if (!strcmp(parameter, "ttl")) {
            if (*p == '=') {
                char *end;
                p++;
                th->ttl = strtol(p, &end, 10);
                p = end;
            }
        } else if (!strcmp(parameter, "destination")) {
            if (*p == '=') {
                p++;
                get_word_sep(buf, sizeof(buf), ";,", &p);
                get_sockaddr(buf, &th->destination);
            }
        } else if (!strcmp(parameter, "source")) {
            if (*p == '=') {
                p++;
                get_word_sep(buf, sizeof(buf), ";,", &p);
                strncpy(th->source, buf, sizeof(th->source) - 1);
            }
        } else if (!strcmp(parameter, "mode")) {
            if (*p == '=') {
                p++;
                get_word_sep(buf, sizeof(buf), ";, ", &p);
                if (!strcmp(buf, "record") ||
                        !strcmp(buf, "receive"))
                    th->mode_record = 1;
            }
        }

        while (*p != ';' && *p != '\0' && *p != ',')
            p++;
        if (*p == ';')
            p++;
    }
    if (*p == ',')
        p++;
}

#define MAX_FRAME_SIZE  (1024 * 1024)

typedef struct rtp_pkt_s {
    rtp_hdr_t rtp_hdr;
    uint8_t data[2048];
} rtp_pkt_t;

static int init_rtp_head(rtp_hdr_t *h, CodecType codec)
{
    if (!h)
        return -1;

    memset(h, 0, sizeof(rtp_hdr_t));

    h->v  = 2;
    h->p  = 0;
    h->x  = 0;
    h->cc = 0;
    h->m  = 0;

    PayloadType *payload = find_payload(codec);
    if (!payload)
        return -1;

    h->pt   = payload->payload_type;
    h->seq  = get_random_num() & 0xffff;
    h->ts   = 0;
    h->ssrc = get_random_num();

    return 0;
}

static int rtsp_cmd_setup(RtspSession *s, RTSPMessageHeader *h)
{
    if (strlen(h->transport) == 0)
        return -1;

    char *p = h->transport;

    RTSPTransportField transport;
    rtsp_parse_transport(&transport, p);

    if (transport.lower_transport != RTSP_LOWER_TRANSPORT_UDP) {
        printf("unsupport lower transport type %d\n", transport.lower_transport);
        return -1;
    }

    if (!transport.client_port_min || !transport.client_port_max)
        return -1;

    char *ptr = strrchr(h->url, '/');
    if (!ptr)
        return -1;
    ptr++;  /* skip '/' */

    int track_id = 0;
    if (sscanf(ptr, "track%d", &track_id) != 1)
        return -1;

    RtpStream *st = NULL;

    switch (track_id) {
    case VIDEO_TRACK_ID:
        if (!s->has_video)
            return -1;
        st = &s->video_st;
        break;
    case AUDIO_TRACK_ID:
        if (!s->has_audio)
            return -1;
        st = &s->audio_st;
        break;
    default:
        printf("unknown track id %d\n", track_id);
        return -1;
    }

    st->client_rtp_port  = transport.client_port_min;
    st->client_rtcp_port = transport.client_port_max;

    if (get_server_port(&st->rtp_fd, &st->rtp_port, &st->rtcp_fd, &st->rtcp_port) < 0)
        return -1;

    st->enabled = 1;

    if (init_rtp_head(&st->rtp_hdr, st->desc.codec) < 0)
        return -1;

    snprintf(s->session_id, sizeof(s->session_id), "%08X", (uint32_t)s);

    char response[4096] = {0};
    snprintf(response, sizeof(response),
            "RTSP/1.0 200 OK\r\n"
            "CSeq: %d\r\n"
            "Transport: RTP/AVP;unicast;client_port=%d-%d;server_port=%d-%d\r\n"
            "Session: %s\r\n"
            "\r\n",
            h->cseq, st->client_rtp_port, st->client_rtcp_port,
            st->rtp_port, st->rtcp_port, s->session_id);

    if (j_net_writen_timedwait(s->rtsp_fd, response, strlen(response)) < 0)
        return -1;

    s->state = RtspState_Ready;

    return 0;
}

static void increase_seq_number(uint16_t *seqp)
{
    *seqp = htons(ntohs(*seqp) + 1);
    return;
}

static int rtsp_send_rtp(RtspSession *s, RtpStream *st, uint8_t *buf, int size)
{
    struct sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_addr.s_addr = s->peeraddr.sin_addr.s_addr,
        .sin_port = htons(st->client_rtp_port),
    };

    int len = sendto(st->rtp_fd, buf, size, 0, (struct sockaddr *)&addr, sizeof(struct sockaddr_in));
    if (len < 0)
        return -1;

    st->stat.octet_count  += len;
    st->stat.packet_count += 1;

    return 0;
}

static int nal_send(RtspSession *s, const uint8_t *buf, int size, int last, uint32_t pts)
{
    RtpStream *st = &s->video_st;

    rtp_pkt_t pkt1, *pkt = &pkt1;
    st->rtp_hdr.ts = htonl(pts);
    pkt->rtp_hdr = st->rtp_hdr;

    if (size <= s->max_payload_size) {
        pkt->rtp_hdr.m = last;
        memcpy(pkt->data, buf, size);
        if (rtsp_send_rtp(s, st, (uint8_t *)pkt, sizeof(rtp_hdr_t) + size) < 0)
            return -1;
    } else {
        uint8_t type = buf[0] & 0x1F;
        uint8_t nri = buf[0] & 0x60;

        pkt->data[0] = 28;        /* FU Indicator; Type = 28 ---> FU-A */
        pkt->data[0] |= nri;

        pkt->data[1] = type;
        pkt->data[1] |= 1 << 7;

        buf  += 1;
        size -= 1;

        while (size + 2 > s->max_payload_size) {
            pkt->rtp_hdr.m = 0;
            memcpy(&pkt->data[2], buf, s->max_payload_size - 2);

            int buf_size = sizeof(rtp_hdr_t) + s->max_payload_size;
            if (rtsp_send_rtp(s, st, (uint8_t *)pkt, buf_size) < 0)
                return -1;

            buf  += s->max_payload_size - 2;
            size -= s->max_payload_size - 2;

            pkt->data[1] &= ~(1 << 7);

            increase_seq_number(&pkt->rtp_hdr.seq);
        }

        pkt->data[1] |= 1 << 6;
        pkt->rtp_hdr.m = last;
        memcpy(&pkt->data[2], buf, size);

        int buf_size = sizeof(rtp_hdr_t) + size + 2;
        if (rtsp_send_rtp(s, st, (uint8_t *)pkt, buf_size) < 0)
            return -1;
    }

    /* update sequence number */
    increase_seq_number(&pkt->rtp_hdr.seq);
    st->rtp_hdr.seq = pkt->rtp_hdr.seq;

    return 0;
}

static uint8_t *h264_find_startcode_internal(uint8_t *p, uint8_t *end)
{
    uint8_t *a = p + 4 - ((intptr_t)p & 3);

    for (end -= 3; p < a && p < end; p++) {
        if (p[0] == 0 && p[1] == 0 && p[2] == 1)
            return p;
    }

    for (end -= 3; p < end; p += 4) {
        uint32_t x = *(uint32_t *)p;
//      if ((x - 0x01000100) & (~x) & 0x80008000) // little endian
//      if ((x - 0x00010001) & (~x) & 0x00800080) // big endian
        if ((x - 0x01010101) & (~x) & 0x80808080) { // generic
            if (p[1] == 0) {
                if (p[0] == 0 && p[2] == 1)
                    return p;
                if (p[2] == 0 && p[3] == 1)
                    return p+1;
            }
            if (p[3] == 0) {
                if (p[2] == 0 && p[4] == 1)
                    return p+2;
                if (p[4] == 0 && p[5] == 1)
                    return p+3;
            }
        }
    }

    for (end += 3; p < end; p++) {
        if (p[0] == 0 && p[1] == 0 && p[2] == 1)
            return p;
    }

    return end + 3;
}

static uint8_t *h264_find_startcode(uint8_t *p, uint8_t *end)
{
    uint8_t *out = h264_find_startcode_internal(p, end);
    if (p < out && out < end && !out[-1]) out--;
    return out;
}

static int rtp_send_h264(RtspSession *s, uint8_t *buf, int size, int last_slice, int64_t pts)
{
    uint8_t *p = buf;
    uint8_t *end = buf + size;

    uint8_t *r, *r1;

    r = h264_find_startcode(p, end);
    while (r < end) {
        while (!*(r++));
        r1 = h264_find_startcode(r, end);

        int last = (r1 == end && last_slice) ? 1 : 0;
        if (nal_send(s, r, r1 - r, last, pts) < 0)
            return -1;
        r = r1;
    }

    return 0;
}

#define ADTS_HEADER_SIZE    (7)

static int rtp_send_aac(RtspSession *s, uint8_t *buf, int size, int last_slice, int64_t pts)
{
    if (size > s->max_payload_size) {
        printf("audio frame size[%d] is too large\n", size);
        return -1;
    }

    RtpStream *st = &s->audio_st;

    rtp_pkt_t pkt1, *pkt = &pkt1;
    st->rtp_hdr.ts = htonl(pts);

    pkt->rtp_hdr   = st->rtp_hdr;
    pkt->rtp_hdr.m = last_slice;

    uint8_t *pbuf = buf;

    if ((buf[0] == 0xff) && ((buf[1] & 0xF0) == 0xF0)) {
        /* trim ADTS header */
        pbuf += ADTS_HEADER_SIZE;
        size -= ADTS_HEADER_SIZE;
    }

    uint8_t *ptr = pkt->data;

    *(ptr++) = 0x00;
    *(ptr++) = 0x10;
    *(ptr++) = (size & 0x1fe0) >> 5;
    *(ptr++) = (size & 0x1f)   << 3;

    memcpy(ptr, pbuf, size);
    ptr += size;

    if (rtsp_send_rtp(s, st, (uint8_t *)pkt, sizeof(rtp_hdr_t) + (ptr - pkt->data)) < 0)
        return -1;

    increase_seq_number(&pkt->rtp_hdr.seq);
    st->rtp_hdr.seq = pkt->rtp_hdr.seq;

    return 0;
}

static int rtp_send_g711(RtspSession *s, uint8_t *buf, int size, int last_slice, int64_t pts)
{
    if (size > s->max_payload_size) {
        printf("audio frame size[%d] is too large\n", size);
        return -1;
    }

    RtpStream *st = &s->audio_st;

    rtp_pkt_t pkt1, *pkt = &pkt1;
    st->rtp_hdr.ts = htonl(pts);
    pkt->rtp_hdr   = st->rtp_hdr;
    pkt->rtp_hdr.m = last_slice;

    memcpy(pkt->data, buf, size);
    if (rtsp_send_rtp(s, st, (uint8_t *)pkt, sizeof(rtp_hdr_t) + size) < 0)
        return -1;

    return 0;
}

#define NTP_OFFSET 2208988800ULL
#define NTP_OFFSET_US (NTP_OFFSET * 1000000ULL)

static int64_t av_gettime(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (int64_t)tv.tv_sec * 1000000 + tv.tv_usec;
}

static uint64_t get_ntp_time(void)
{
    return (av_gettime() / 1000) * 1000 + NTP_OFFSET_US;
}

static int rtsp_send_rtcp(RtspSession *s, RtpStream *st, uint8_t *buf, int size)
{
#if 0
    struct sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_addr.s_addr = s->peeraddr.sin_addr.s_addr,
        .sin_port = htons(st->client_rtcp_port),
    };

    if (sendto(st->rtcp_fd, buf, size, 0, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) < 0)
        return -1;
#endif

    return 0;
}

#define RTP_VERSION (2)

static int rtcp_send_sr(RtspSession *s, RtpStream *st, rtp_stat_t *stat, int64_t ntp_time)
{
    uint8_t buf[512] = {0};

    uint8_t *ptr = buf;

    rtp_hdr_t *rtp_hdrp = &st->rtp_hdr;

    /*
     * buidl rtcp sender report
     */
    rtcp_sr_t *sr = (rtcp_sr_t *)ptr;
    memset(sr, 0, sizeof(rtcp_sr_t));
    sr->ch.version = RTP_VERSION;
    sr->ch.rc = 0;
    sr->ch.packet_type = RTCP_SR;
    sr->ch.length = htons((sizeof(rtcp_sr_t) + 3) / 4 - 1); /* length in words - 1 */
    sr->ssrc = rtp_hdrp->ssrc;
    sr->si.ntp_timestamp_msw = htonl(ntp_time / 1000000);
    sr->si.ntp_timestamp_lsw = htonl(((ntp_time % 1000000) << 32) / 1000000 + 1);
    sr->si.rtp_timestamp = rtp_hdrp->ts;
    sr->si.senders_packet_count = htonl(stat->packet_count);
    sr->si.senders_octet_count = htonl(stat->octet_count);

    ptr += sizeof(rtcp_sr_t);

    /*
     * build rtcp source description
     */
    rtcp_sdes_t *ds = (rtcp_sdes_t *)ptr;
    memset(ds, 0, sizeof(rtcp_sdes_t));
    ds->ch.version = RTP_VERSION;
    ds->ch.rc = 1;
    ds->ch.packet_type = RTCP_SDES;
    ds->chunk.ssrc = rtp_hdrp->ssrc;

    ptr += sizeof(rtcp_sdes_t);

    sdes_item_t *sdes = (sdes_item_t *)ptr;
    sdes->item_type = 1;
    sdes->len = strlen(RTCP_SDES_CNAME);
    memcpy(sdes->content, RTCP_SDES_CNAME, sdes->len);

    ptr += sizeof(sdes_item_t) + strlen(RTCP_SDES_CNAME);

    /* length in words - 1 */
    ds->ch.length = htons((ptr - (uint8_t *)ds + 3) / 4 - 1);

    int total_len = (((ptr - buf) + 3) / 4) * 4;
    if (rtsp_send_rtcp(s, st, buf, total_len) < 0)
        return -1;

    return 0;
}

typedef enum {
    PLAY_CMD_PLAY,
    PLAY_CMD_SEEK,
    PLAY_CMD_SCALE,
    PLAY_CMD_PAUSE,
} PlayCmd;

typedef struct {
    int cmd;    /* 0 - play, 1 - seek, 2 - scale */
    union {
        int   off_ms;    /* millisecond */
        float scale;
    } data;
} PlayMsg;

static void *rtp_streamer(void *arg)
{
    RtspSession *s = (RtspSession *)arg;
    if (!s || !s->route)
        return NULL;

    RtspSvc *svc = s->rtspsvc;
    if (!svc)
        return NULL;

    RtspHandler *handler = &s->route->handler;

    long handle = handler->open(s->url->path);
    if (!handle) {
        s->abort_session = 1;
        return NULL;
    }

    MediaInfo minfo;
    uint8_t *buf = malloc(MAX_FRAME_SIZE);
    if (!buf)
        goto fail;

    while (!svc->abort && !s->abort_session) {
        PlayMsg *msg = j_queue_get(s->msgq, 0);
        if (msg) {
            switch (msg->cmd) {
            case PLAY_CMD_PLAY:
                break;
            case PLAY_CMD_SEEK:
                if (handler->seek)
                    handler->seek(handle, msg->data.off_ms);
                break;
            case PLAY_CMD_SCALE:
                if (handler->scale)
                    handler->scale(handle, msg->data.scale);
                break;
            case PLAY_CMD_PAUSE:
                break;
            }
            free(msg);
        }

        memset(&minfo, 0, sizeof(MediaInfo));

        int len = handler->read(handle, &minfo, buf, MAX_FRAME_SIZE);
        if (len == 0)
            continue;
        else if (len < 0)
            break;

        RtpStream *stream = NULL;

        switch (minfo.codec) {
        case CODEC_TYPE_H264:
            stream = &s->video_st;
            if (!stream->enabled)
                continue;
            rtp_send_h264(s, buf, len, minfo.last_slice, minfo.pts);
            break;
        case CODEC_TYPE_AAC:
            stream = &s->audio_st;
            if (!stream->enabled)
                continue;
            rtp_send_aac(s, buf, len, minfo.last_slice, minfo.pts);
            break;
        case CODEC_TYPE_PCMU:
        case CODEC_TYPE_PCMA:
            stream = &s->audio_st;
            if (!stream->enabled)
                continue;
            rtp_send_g711(s, buf, len, minfo.last_slice, minfo.pts);
            break;
        default:
            printf("codec type[%d] is unknown\n", minfo.codec);
            goto fail;
        }

        int64_t ntp_time = get_ntp_time();
        rtp_stat_t *stat = &stream->stat;
        if (llabs(ntp_time - stat->last_rtcp_ntp_time) > 5 * 1000000) {
            if (rtcp_send_sr(s, stream, stat, ntp_time) < 0)
                break;
            stat->last_rtcp_ntp_time = ntp_time;
        }
    }

fail:
    if (handler->close)
        handler->close(handle);

    if (buf)
        free(buf);
    s->abort_session = 1;

    return NULL;
}

static int rtsp_cmd_play(RtspSession *s, RTSPMessageHeader *h)
{
    if (s->state != RtspState_Ready && s->state != RtspState_Playing)
        return -1;  /* method not allowed */

    if (s->state != RtspState_Playing) {
        if (pthread_create(&s->streame_tid, NULL, rtp_streamer, s) != 0)
            return -1;
    }

    float start_sec = 0;
    float end_sec = 0;

    char range[64] = {0};
    if (strlen(h->range) != 0) {
        sscanf(h->range, "npt=%f-%f", &start_sec, &end_sec);

        PlayMsg *msg = malloc(sizeof(PlayMsg));
        if (!msg)
            return -1;
        memset(msg, 0, sizeof(PlayMsg));

        msg->cmd = PLAY_CMD_SEEK;
        msg->data.off_ms = start_sec * 1000;

        j_queue_put(s->msgq, msg);

        snprintf(range, sizeof(range), "Range: npt=%.3f-%.3f\r\n", start_sec, s->video_st.desc.duration / 1000.0);
    } else {
        snprintf(range, sizeof(range), "Range: npt=0.000-\r\n");
    }

    if (strlen(h->scale) != 0) {
        PlayMsg *msg = malloc(sizeof(PlayMsg));
        if (!msg)
            return -1;
        memset(msg, 0, sizeof(PlayMsg));

        msg->cmd = PLAY_CMD_SCALE;
        msg->data.scale = atof(h->scale);

        j_queue_put(s->msgq, msg);
    }

    char rtpinfo[1024] = {0};
    if (s->video_st.desc.duration > 0) {
        if (s->url->port) {
            snprintf(rtpinfo, sizeof(rtpinfo), "RTP-Info: url=rtsp://%s:%s%s/track0;seq=%d;rtptime=%d\r\n",
                    s->url->host, s->url->port, s->url->path, s->video_st.rtp_hdr.seq, ntohl(s->video_st.rtp_hdr.ts));
        } else {
            snprintf(rtpinfo, sizeof(rtpinfo), "RTP-Info: url=rtsp://%s%s/track0;seq=%d;rtptime=%d\r\n",
                    s->url->host, s->url->path, s->video_st.rtp_hdr.seq, ntohl(s->video_st.rtp_hdr.ts));
        }
    }

    char response[4096] = {0};
    snprintf(response, sizeof(response),
            "RTSP/1.0 200 OK\r\n"
            "CSeq: %d\r\n"
            "%s"
            "%s"
            "Session: %s\r\n"
            "\r\n",
            h->cseq,
            range,
            rtpinfo,
            s->session_id);

    if (j_net_writen_timedwait(s->rtsp_fd, response, strlen(response)) < 0)
        return -1;

    s->state = RtspState_Playing;

    return 0;
}

static int rtsp_cmd_pause(RtspSession *s, RTSPMessageHeader *h)
{
    char response[4096] = {0};

    snprintf(response, sizeof(response),
            "RTSP/1.0 200 OK\r\n"
            "CSeq: %d\r\n"
            "Session: %s\r\n"
            "\r\n",
            h->cseq, s->session_id);

    if (j_net_writen_timedwait(s->rtsp_fd, response, strlen(response)) < 0)
        return -1;

    PlayMsg *msg = malloc(sizeof(PlayMsg));
    if (!msg)
        return -1;
    memset(msg, 0, sizeof(PlayMsg));

    msg->cmd = PLAY_CMD_PAUSE;

    j_queue_put(s->msgq, msg);

    return 0;
}

static int rtsp_cmd_teardown(RtspSession *s, RTSPMessageHeader *h)
{
    char response[4096] = {0};

    snprintf(response, sizeof(response),
            "RTSP/1.0 200 OK\r\n"
            "CSeq: %d\r\n"
            "Session: %s\r\n"
            "\r\n",
            h->cseq, s->session_id);

    if (j_net_writen_timedwait(s->rtsp_fd, response, strlen(response)) < 0)
        return -1;

    s->abort_session = 1;

    return 0;
}

static int rtsp_cmd_get_parameter(RtspSession *s, RTSPMessageHeader *h)
{
    char response[4096] = {0};

    snprintf(response, sizeof(response),
            "RTSP/1.0 200 OK\r\n"
            "CSeq: %d\r\n"
            "Session: %s\r\n"
            "\r\n",
            h->cseq, s->session_id);

    if (j_net_writen_timedwait(s->rtsp_fd, response, strlen(response)) < 0)
        return -1;

    return 0;
}

typedef struct {
    char method[64];
    int (*func)(RtspSession *s, RTSPMessageHeader *h);
} RtspMethod;

static RtspMethod methods[] = {
    { "OPTIONS",       rtsp_cmd_options       },
    { "DESCRIBE",      rtsp_cmd_describe      },
    { "SETUP",         rtsp_cmd_setup         },
    { "PLAY",          rtsp_cmd_play          },
    { "TEARDOWN",      rtsp_cmd_teardown      },
    { "PAUSE",         rtsp_cmd_pause         },
    { "GET_PARAMETER", rtsp_cmd_get_parameter },
};

static RtspMethod *find_method(char *method)
{
    int i;
    for (i = 0; i < NELEMS(methods); i++) {
        RtspMethod *m = &methods[i];
        if (strcmp(method, m->method) == 0)
            return m;
    }

    return NULL;
}

static RtspRoute *find_route(RtspSvc *svc, char *path)
{
    pthread_mutex_lock(&svc->r_lock);

    JListNode *n, *n_next;
    for (n = j_list_first(svc->allroutes); n; n = n_next) {
        n_next = j_list_next(svc->allroutes, n);

        RtspRoute *route = n->data;
        if (strncmp(route->path, path, strlen(route->path)) == 0) {
            pthread_mutex_unlock(&svc->r_lock);
            return route;
        }
    }

    pthread_mutex_unlock(&svc->r_lock);

    return NULL;
}

static int handle_rtsp_request(RtspSession *s)
{
    int len = j_net_read_timedwait(s->rtsp_fd, s->ptr, s->end - s->ptr, 1000);
    if (len <= 0)
        return len;

    s->ptr += len;
    if (s->ptr == s->end) {
        printf("message too long\n");
        return -1;
    }

    char *ending = strstr(s->buf, "\r\n\r\n");
    if (!ending)
        return 0;
    *ending = '\0';

    char *content = ending + 4;

    RTSPMessageHeader header;
    if (rtsp_parse_request(s->buf, &header) < 0)
        return -1;

    if (strlen(s->session_id) > 0) {
        if (strcmp(s->session_id, header.session) != 0)
            return -1;
    }

    if (!s->url) {
        s->url = UrlParse(header.url);
        if (!s->url)
            return -1;

        if (!s->url->path || !strlen(s->url->path))
            return -1;
    }

    if (!s->route) {
        s->route = find_route(s->rtspsvc, s->url->path);
        if (!s->route)
            return -1;
    }

    if (header.content_length > 0) {
        int nbyte = s->ptr - content;
        int left = header.content_length - nbyte;
        if (left > s->end - s->ptr)
            return -1;  /* not enought buffer */

        len = j_net_readn_timedwait(s->rtsp_fd, s->ptr, left);
        if (len <= 0)
            return -1;
        s->ptr += len;
    }

    RtspMethod *method = find_method(header.method);
    if (!method)
        return -1;

    if (method->func(s, &header) < 0)
        return -1;

    s->ptr = s->buf;  /* reset buffer */

    return 0;
}

static int close_stream(RtpStream *st)
{
    if (!st->enabled)
        return 0;

    close(st->rtp_fd);
    close(st->rtcp_fd);

    return 0;
}

static int close_session(RtspSession *s)
{
    s->abort_session = 1;

    if (s->url)
        UrlFree(s->url);

    void *msg;
    while ((msg = j_queue_get(s->msgq, 0)) != NULL)
        free(msg);

    j_queue_destroy(s->msgq);
    close_stream(&s->video_st);
    close_stream(&s->audio_st);
    close(s->rtsp_fd);
    free(s);

    return 0;
}

static int handle_rtcp_packet(RtspSession *s, RtpStream *st)
{
    uint8_t buf[4096];
    if (recvfrom(st->rtcp_fd, buf, sizeof(buf), 0, NULL, NULL) <= 0)
        return -1;
    return 0;
}

static void *session_thread(void *arg)
{
    RtspSession *s = (RtspSession *)arg;
    if (!s)
        return NULL;

    RtspSvc *svc = s->rtspsvc;

    time_t timeout = time(NULL) + MAX_SESSION_TIMEOUT;

    RtpStream *video_st = &s->video_st;
    RtpStream *audio_st = &s->audio_st;

    fd_set rfds;

    while (!svc->abort && !s->abort_session) {
        time_t now = time(NULL);
        if (now > timeout)
            break;

        int maxfd = s->rtsp_fd;

        FD_ZERO(&rfds);
        FD_SET(s->rtsp_fd, &rfds);

        if (video_st->rtcp_fd >= 0) {
            FD_SET(video_st->rtcp_fd, &rfds);
            maxfd = max(maxfd, video_st->rtcp_fd);
        }

        if (audio_st->rtcp_fd >= 0) {
            FD_SET(audio_st->rtcp_fd, &rfds);
            maxfd = max(maxfd, audio_st->rtcp_fd);
        }

        struct timeval tv = {
            .tv_sec  = 1,
            .tv_usec = 0,
        };

        int ret = select(maxfd + 1, &rfds, NULL, NULL, &tv);
        if (ret < 0)
            break;
        else if (ret == 0)
            continue;

        if (FD_ISSET(s->rtsp_fd, &rfds)) {
            ret = handle_rtsp_request(s);
        } else if (video_st->rtcp_fd >= 0 && FD_ISSET(video_st->rtcp_fd, &rfds)) {
            ret = handle_rtcp_packet(s, video_st);
        } else if (audio_st->rtcp_fd >= 0 && FD_ISSET(audio_st->rtcp_fd, &rfds)) {
            ret = handle_rtcp_packet(s, audio_st);
        }

        if (ret < 0)
            break;

        timeout = time(NULL) + MAX_SESSION_TIMEOUT;
    }

    s->abort_session = 1;

    if (s->streame_tid)
        pthread_join(s->streame_tid, NULL);

    return NULL;
}

static int do_cleanup(RtspSvc *svc)
{
    JListNode *n, *n_next;

    pthread_mutex_lock(&svc->s_lock);
    for (n = j_list_first(svc->allsessions); n; n = n_next) {
        n_next = j_list_next(svc->allsessions, n);

        RtspSession *s = n->data;
        if (!s->abort_session)
            continue;

        if (pthread_join(s->session_tid, NULL) == 0) {
            /* remove from session list */
            j_list_remove(svc->allsessions, s);
            close_session(s);
        }
    }
    pthread_mutex_unlock(&svc->s_lock);

    return 0;
}

static void *rtsp_service(void *arg)
{
    RtspSvc *svc = (RtspSvc *)arg;
    if (!svc)
        return NULL;

    while (!svc->abort) {
        do_cleanup(svc);

        int ret = j_net_wait_readable(svc->server_fd, 1000);
        if (ret < 0)
            break;
        else if (ret == 0)
            continue;

        int connfd = j_net_tcp_accept(svc->server_fd);
        if (connfd < 0)
            continue;

        RtspSession *s = malloc(sizeof(RtspSession));
        if (!s) {
            close(connfd);
            continue;
        }
        memset(s, 0, sizeof(RtspSession));

        s->rtspsvc = svc;
        s->rtsp_fd = connfd;
        s->state   = RtspState_Init;
        s->max_payload_size = MAX_RTP_PAYLOAD_SIZE;

        s->ptr = s->buf;
        s->end = s->ptr + sizeof(s->buf);

        s->video_st.rtp_fd  = -1;
        s->video_st.rtcp_fd = -1;

        s->audio_st.rtp_fd  = -1;
        s->audio_st.rtcp_fd = -1;

        s->msgq = j_queue_new();

        socklen_t addrlen = sizeof(struct sockaddr_in);
        if (getpeername(s->rtsp_fd, (struct sockaddr *)&s->peeraddr, &addrlen) < 0) {
            j_queue_destroy(s->msgq);
            close(connfd);
            free(s);
            continue;
        }

        if (pthread_create(&s->session_tid, NULL, session_thread, s) != 0) {
            j_queue_destroy(s->msgq);
            close(connfd);
            free(s);
            continue;
        }

        pthread_mutex_lock(&svc->s_lock);
        j_list_append(svc->allsessions, s);
        pthread_mutex_unlock(&svc->s_lock);
    }

    return NULL;
}

RtspSvc *RtspSvc_Create(int port)
{
    RtspSvc *svc = malloc(sizeof(RtspSvc));
    if (!svc)
        return NULL;
    memset(svc, 0, sizeof(RtspSvc));

    svc->port = port;
    pthread_mutex_init(&svc->r_lock, NULL);
    pthread_mutex_init(&svc->s_lock, NULL);

    svc->allroutes = j_list_alloc();
    if (!svc->allroutes)
        goto fail;

    svc->allsessions = j_list_alloc();
    if (!svc->allsessions)
        goto fail;

    svc->server_fd = j_net_tcp_server(NULL, port);
    if (svc->server_fd < 0)
        goto fail;

    if (pthread_create(&svc->service_tid, NULL, rtsp_service, svc) != 0)
        goto fail;

    return svc;

fail:
    RtspSvc_Destroy(svc);

    return NULL;
}

int RtspSvc_Destroy(RtspSvc *svc)
{
    if (!svc)
        return -1;

    svc->abort = 1;

    JListNode *n, *n_next;

    pthread_mutex_lock(&svc->s_lock);
    for (n = j_list_first(svc->allsessions); n; n = n_next) {
        n_next = j_list_next(svc->allsessions, n);
        RtspSession *s = n->data;
        s->abort_session = 1;
    }
    pthread_mutex_unlock(&svc->s_lock);

    if (svc->service_tid != 0)
        pthread_join(svc->service_tid, NULL);

    do_cleanup(svc);

    /* remove all routes */
    for (n = j_list_first(svc->allroutes); n; n = n_next) {
        n_next = j_list_next(svc->allroutes, n);
        RtspRoute *route = n->data;
        j_list_remove(svc->allroutes, route);
        free(route);
    }

    if (svc->server_fd >= 0)
        close(svc->server_fd);

    if (svc->allsessions)
        j_list_free(svc->allsessions);

    if (svc->allroutes)
        j_list_free(svc->allroutes);

    pthread_mutex_destroy(&svc->r_lock);
    pthread_mutex_destroy(&svc->s_lock);

    free(svc);

    return 0;
}

int RtspSvc_AddRoute(RtspSvc *svc, const char *path, RtspHandler *handler)
{
    if (!svc || !path || !strlen(path))
        return -1;

    if (!handler || !handler->open || !handler->describe || !handler->read)
        return -1;

    RtspRoute *route = malloc(sizeof(RtspRoute));
    if (!route)
        return -1;
    memset(route, 0, sizeof(RtspRoute));

    strncpy(route->path, path, sizeof(route->path) - 1);
    memcpy(&route->handler, handler, sizeof(RtspHandler));

    pthread_mutex_lock(&svc->r_lock);
    j_list_append(svc->allroutes, route);
    pthread_mutex_unlock(&svc->r_lock);

    return 0;
}
