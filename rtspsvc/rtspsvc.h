#ifndef __RTSPSVC_H__
#define __RTSPSVC_H__

#include "common.h"

typedef struct RtspSvc RtspSvc;

RtspSvc *RtspSvc_Create(int port);
int RtspSvc_Destroy(RtspSvc *svc);

typedef struct MediaDesc {
    CodecType codec;
    int sample_rate;
    int audio_channels;

    int duration;

    uint8_t sps[512];
    int sps_len;

    uint8_t pps[128];
    int pps_len;
} MediaDesc;

typedef struct MediaInfo {
    CodecType codec;

    int last_slice;

    int64_t pts;
} MediaInfo;

typedef struct RtspHandler {
    int  (*describe)(char *path, MediaDesc *descs, int nmemb);

    long (*open)(char *path);
    int  (*close)(long handle);

    /* AAC data should not contain ADTS header */
    int  (*read)(long handle, MediaInfo *info, uint8_t *data, size_t size);

    int (*seek)(long handle, int off_ms);
    int (*scale)(long handle, float scale);
} RtspHandler;

int RtspSvc_AddRoute(RtspSvc *svc, const char *path, RtspHandler *handler);

#endif /* __RTSPSVC_H__ */
