#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "rtspsvc.h"

static long rtsp_open(char *path)
{
    return 0;
}

static int rtsp_close(long handle)
{
    return 0;
}

static int rtsp_describe(char *path, MediaDesc *descs, int nmemb)
{
    MediaDesc *desc = &descs[0];
    memset(desc, 0, sizeof(MediaDesc));

    desc->codec = CodecType_H264;
    desc->sample_rate = 90000;

    return 1;
}

static int rtsp_read(long handle, MediaInfo *info, uint8_t *data, size_t size)
{
    return 0;
}

int main(void)
{
    RtspSvc *svc = RtspSvc_Create(8554);
    if (!svc)
        return -1;

    RtspHandler handler = {
        .open     = rtsp_open,
        .close    = rtsp_close,
        .describe = rtsp_describe,
        .read     = rtsp_read,
    };

    if (RtspSvc_AddRoute(svc, "/stream", &handler) < 0) {
        RtspSvc_Destroy(svc);
        return -1;
    }

    while (1)
        sleep(10);

    RtspSvc_Destroy(svc);

    return 0;
}
