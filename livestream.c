#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "params.h"
#include "config.h"
#include "logger.h"
#include "bufchan.h"
#include "mediaapi.h"

#include "livestream.h"

static const char *TAG = "LiveStream";

struct LiveStream {
    long w_handle;
    char *stream_name;
    VideoStream *video_st;
    AudioStream *audio_st;
};

int LiveStream_Init(void)
{
    BufChan_MkFifo(LIVESTREAM_LARGE, 8 * 1024 * 1024);
    BufChan_MkFifo(LIVESTREAM_SMALL, 4 * 1024 * 1024);
    return 0;
}

int LiveStream_Quit(void)
{
    BufChan_UnLink(LIVESTREAM_LARGE);
    BufChan_UnLink(LIVESTREAM_SMALL);
    return 0;
}

static int video_cb(VideoFrame *frame, void *priv_data)
{
    LiveStream *live = priv_data;

    struct iovec iov[2];
    iov[0].iov_base = &frame->sinfo;
    iov[0].iov_len = sizeof(StreamInfo);

    iov[1].iov_base = frame->data;
    iov[1].iov_len = frame->size;

    BufChan_WriteV(live->w_handle, iov, 2);

    return 0;
}

static int audio_cb(AudioFrame *frame, void *priv_data)
{
    LiveStream *live = priv_data;

    struct iovec iov[2];
    iov[0].iov_base = &frame->sinfo;
    iov[0].iov_len = sizeof(StreamInfo);

    iov[1].iov_base = frame->data;
    iov[1].iov_len = frame->size;

    BufChan_WriteV(live->w_handle, iov, 2);

    return 0;
}

LiveStream *LiveStream_Open(int stream_id)
{
    LiveStream *live = malloc(sizeof(LiveStream));
    if (!live)
        return NULL;
    memset(live, 0, sizeof(LiveStream));

    if (stream_id == 0)
        live->stream_name = LIVESTREAM_LARGE;
    else if (stream_id == 1)
        live->stream_name = LIVESTREAM_SMALL;
    else {
        LogE(TAG, "invalid stream id %d", stream_id);
        free(live);
        return NULL;
    }

    live->w_handle = BufChan_Open(live->stream_name, AV_WRONLY);
    if (!live->w_handle) {
        free(live);
        return NULL;
    }

    VideoEncoderConfiguration video_cfg;
    Params_GetVideoEncoderConfiguration(stream_id, &video_cfg);

    VideoEncodeConf videoCfg = {
        .codec = video_cfg.codec_type,
        .width = video_cfg.width,
        .height = video_cfg.height,
        .framerate = video_cfg.framerate,
        .bitrate = video_cfg.bitrate,
        .gop = video_cfg.gop,
    };

    live->video_st = MediaApi_OpenVideoStream(stream_id, &videoCfg, video_cb, live);
    if (!live->video_st) {
        BufChan_Close(live->w_handle);
        free(live);
        return NULL;
    }

    AudioEncoderConfiguration audio_cfg;
    Params_GetAudioEncoderConfiguration(&audio_cfg);

    if (audio_cfg.enable) {
        AudioEncodeConf audioCfg = {
            .codec = audio_cfg.codec_type,
            .bitrate = audio_cfg.bitrate,
            .sample_rate = audio_cfg.sample_rate,
            .bit_width = audio_cfg.bit_width,
            .channels = audio_cfg.channels,
            .samples_per_frame = audio_cfg.samples_per_frame,
        };

        live->audio_st = MediaApi_OpenAudioStream(&audioCfg, audio_cb, live);
        if (!live->audio_st) {
            MediaApi_CloseVideoStream(live->video_st);
            BufChan_Close(live->w_handle);
            free(live);
            return NULL;
        }
    }

    return live;
}

int LiveStream_Close(LiveStream *live)
{
    if (live->video_st)
        MediaApi_CloseVideoStream(live->video_st);

    if (live->audio_st)
        MediaApi_CloseAudioStream(live->audio_st);

    BufChan_Close(live->w_handle);
    BufChan_Erase(live->stream_name);  /* erase buffered stream data */

    free(live);

    return 0;
}

int LiveStream_GetVideoStreamHeadInfo(LiveStream *live, StreamInfoType type, uint8_t *head, uint32_t *len)
{
    return VideoStream_GetStreamHeadInfo(live->video_st, type, head, len);
}

int LiveStream_Start(LiveStream *live)
{
    if (live->video_st) {
        if (VideoStream_Start(live->video_st, -1) < 0)
            return -1;
    }

    if (live->audio_st) {
        if (AudioStream_Start(live->audio_st) < 0)
            return -1;
    }

    return 0;
}

int LiveStream_Stop(LiveStream *live)
{
    if (live->video_st)
        VideoStream_Stop(live->video_st);

    if (live->audio_st)
        AudioStream_Stop(live->audio_st);

    return 0;
}

struct LiveStreamReader {
    long r_handle;
    int64_t start_ms;
};

static int filter_frame(struct iovec *iovs, int iovcnt, void *priv_data)
{
    StreamInfo info;
    uint8_t *ptr = (uint8_t *)&info;

    LiveStreamReader *reader = (LiveStreamReader *)priv_data;

    if (iovs[0].iov_len >= sizeof(StreamInfo))
        memcpy(ptr, iovs[0].iov_base, sizeof(StreamInfo));
    else {
        memcpy(ptr, iovs[0].iov_base, iovs[0].iov_len);
        ptr += iovs[0].iov_len;
        memcpy(ptr, iovs[1].iov_base, sizeof(StreamInfo) - iovs[0].iov_len);
    }

    if (info.time_ms > reader->start_ms)
        return 1;   /* stop iteration */

    return 0;
}

LiveStreamReader *LiveStream_CreateReader(int stream_id, int64_t start_ms)
{
    LiveStreamReader *reader = malloc(sizeof(LiveStreamReader));
    if (!reader)
        return NULL;
    memset(reader, 0, sizeof(LiveStreamReader));

    char *stream_name = NULL;

    if (stream_id == 0)
        stream_name = LIVESTREAM_LARGE;
    else
        stream_name = LIVESTREAM_SMALL;

    reader->start_ms = start_ms;

    int flags = AV_RDONLY;

    if (reader->start_ms > 0)
        flags |= AV_OLDEST;

    reader->r_handle = BufChan_Open(stream_name, flags);
    if (!reader->r_handle) {
        free(reader);
        return NULL;
    }

    if (reader->start_ms > 0)
        BufChan_ForEach(reader->r_handle, filter_frame, reader);

    return reader;
}

int LiveStream_DestroyReader(LiveStreamReader *reader)
{
    BufChan_Close(reader->r_handle);
    free(reader);
    return 0;
}

int LiveStream_ReadV(LiveStreamReader *reader, struct iovec *iov, int iovcnt, int milliseconds)
{
    return BufChan_ReadV(reader->r_handle, iov, iovcnt, milliseconds);
}
