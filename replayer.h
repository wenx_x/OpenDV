#ifndef __REPLAYER_H__
#define __REPLAYER_H__

typedef struct Replayer Replayer;

Replayer *Replayer_Create(const char *filename);
int Replayer_Destroy(Replayer *r);

int Replayer_Start(Replayer *r);
int Replayer_Stop(Replayer *r);

int Replayer_Seek(Replayer *r, int off_ms);
int Replayer_SetSpeed(Replayer *r, float scale);

int Replayer_Pause(Replayer *r);
int Replayer_Resume(Replayer *r);

#endif /* __REPLAYER_H__ */
