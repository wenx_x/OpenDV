#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "libexif/exif-loader.h"
#include "libexif/exif-data.h"

#include "exif.h"

#define min(x, y)    ((x) < (y) ? (x) : (y))

ExifReader *ExifReader_Open(char *filename)
{
    ExifReader *r = malloc(sizeof(ExifReader));
    if (!r)
        return NULL;
    memset(r, 0, sizeof(ExifReader));

    r->loader = exif_loader_new();
    if (!r->loader) {
        free(r);
        return NULL;
    }

    exif_loader_write_file(r->loader, filename);

    r->data = exif_loader_get_data(r->loader);
    if (!r->data) {
        exif_loader_unref(r->loader);
        free(r);
        return NULL;
    }

    if (!r->data->data || !r->data->size) {
        ExifReader_Close(r);
        return NULL;
    }

    r->ptr = r->data->data;
    r->end = r->data->data + r->data->size;

    return r;
}

int ExifReader_Close(ExifReader *r)
{
    if (r->data)
        exif_data_unref(r->data);

    if (r->loader)
        exif_loader_unref(r->loader);

    free(r);

    return 0;
}

int ExifReader_Read(ExifReader *r, uint8_t *buf, int len)
{
    int left = r->end - r->ptr;
    if (left <= 0)
        return 0;

    int size = min(left, len);

    memcpy(buf, r->ptr, size);
    r->ptr += size;

    return size;
}
