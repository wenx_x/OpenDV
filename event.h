#ifndef __EVENT_H__
#define __EVENT_H__

#include <stdint.h>

#include "common.h"

int Event_Init(void);
int Event_Quit(void);

int Event_Dispatch(Event *ev);

typedef struct Subscriber Subscriber;

Subscriber *Event_NewSubscriber(uint32_t events, int (*event_cb)(Event *ev, void *priv_data), void *priv_data);
int Event_DelSubscriber(Subscriber *sub);

#endif
