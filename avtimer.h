#ifndef __AVTIMER_H_
#define __AVTIMER_H_

#define AV_TIMER_STOPPED 0
#define AV_TIMER_RUNNING 1

typedef struct av_timer_s {
	int state;
	int64_t orig;				/* in microseconds */
	int64_t posix_timer_time;	/* in microseconds */
	struct timeval interval;
} av_timer_t;

av_timer_t *av_timer_create(void);
int av_timer_destroy(av_timer_t *timer);
void av_timer_start(av_timer_t *timer);
int av_timer_wait(av_timer_t *timer);
void av_timer_reset(av_timer_t *timer);
void av_timer_set(av_timer_t *timer, struct timeval *interval);

#endif /* __AVTIMER_H_ */
