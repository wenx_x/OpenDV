#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/vfs.h>

#include "vod.h"
#include "event.h"
#include "logger.h"
#include "config.h"
#include "params.h"
#include "config.h"
#include "storage.h"
#include "diskmgr.h"
#include "globaldata.h"
#include "camera_fsm.h"

#define NELEMS(x)   (sizeof(x) / sizeof((x)[0]))

typedef struct {
    int online;
    DiskHndl *mmc;

    int available;

    int nparts;

    PartHndl *part;
} SdcardCtx;

static const char *TAG = "Storage";

static SdcardCtx sdcard;

static int hotplug_cb(const char *url, int action, void *priv_data)
{
    SdcardCtx *ctx = (SdcardCtx *)priv_data;
    if (!ctx)
        return -1;

    ctx->online = action;

    if (action == DISK_ONLINE) {
        LogV(TAG, "SD card %s is online", url);

        ctx->available = 0;

        ctx->mmc = DiskMgr_OpenDisk(url);
        if (!ctx->mmc) {
            LogV(TAG, "open %s failed: %s", url, strerror(errno));
            return -1;
        }

        ctx->nparts = DiskMgr_GetPartNum(ctx->mmc);
        if (ctx->nparts == 0) {
            LogV(TAG, "%s need create partition first", url);
            ctx->available = 0;
            return 0;
        }

        LogV(TAG, "there's %d parts in %s", ctx->nparts, SDCARD_MOUNT_POINT);

        int i;
        for (i = 0; i < ctx->nparts; i++) {
            PartHndl *p = DiskMgr_GetPartById(ctx->mmc, i);
            if (p) {
                /* check mount point */
                if (access(SDCARD_MOUNT_POINT, F_OK | W_OK | R_OK | X_OK) != 0) {
                    if (mkdir(SDCARD_MOUNT_POINT, 0777) < 0) {
                        LogV(TAG, "mkdir %s failed: %s", SDCARD_MOUNT_POINT, strerror(errno));
                        return -1;
                    }
                }

                PartInfo info;
                if (DiskMgr_GetPartInfo(p, &info) < 0) {
                    LogV(TAG, "%s: get part info failed", SDCARD_MOUNT_POINT);
                    continue;
                }

                if (DiskMgr_Mount(p, SDCARD_MOUNT_POINT) < 0) {
                    LogV(TAG, "mount %s failed: %s", SDCARD_MOUNT_POINT, strerror(errno));
                    continue;
                }

                LogV(TAG, "mount %s success", SDCARD_MOUNT_POINT);

                RecMgr_AddVolume(SDCARD_MOUNT_POINT);

                ctx->part = p;
                ctx->available = 1;
                break;
            }
        }

        if (ctx->available == 0) {
            LogV(TAG, "SD card %s is not available", url);
        }
    } else if (action == DISK_OFFLINE) {
        LogV(TAG, "SD card %s is offline", url);

        if (ctx->available) {
            if (ctx->part) {
                RecMgr_DeleteVolume(SDCARD_MOUNT_POINT);
                DiskMgr_Umount(ctx->part);
            }

            DiskMgr_CloseDisk(ctx->mmc);

            ctx->available = 0;
            ctx->part = NULL;
            ctx->mmc = NULL;
        }
    }

    return 0;
}

static int abort_request = 0;
static pthread_t disk_mon_tid;

static void *disk_mon(void *arg)
{
    int low_space = 0;

    while (!abort_request) {
        RecordSettings settings;
        Params_GetRecordSettings(&settings);

        if (Storage_HaveEnoughSpace() == 0) {
            if (settings.circulate_record) {
                /* Delete oldest media files in circulate record mode */
                FileItem files[2];
                int ret = RecMgr_GetFilesList(0, FILE_DELETE_IN_LOW_SPACE, files, NELEMS(files), 1);
                if (ret > 0) {
                    int i;
                    for (i = 0; i < ret; i++) {
                        RecMgr_DeleteFile(files[i].filename);
                        LogV(TAG, "low storage space, %s was deleted", files[i].filename);
                    }
                }
            } else {
                if (low_space == 0) {
                    Event event = { .event_type = EVENT_TYPE_STORAGE_OUT_OF_SPACE };
                    Event_Dispatch(&event);

                    if (CamFsm_IsRecordingVideo())
                        CamFsm_StopRecordVideo();
                    else if (CamFsm_IsTakingPhoto())
                        CamFsm_AbortTakePhoto();
                }
            }
            low_space = 1;
        } else {
            low_space = 0;
        }

        sleep(5);
    }

    return NULL;
}

int Storage_Init(void)
{
    DiskMgr_Init();
    DiskMgr_AddWatcher(SDCARD_DEVICE_NAME, hotplug_cb, &sdcard);

    abort_request = 0;
    pthread_create(&disk_mon_tid, NULL, disk_mon, NULL);

    return 0;
}

int Storage_Quit(void)
{
    abort_request = 1;
    pthread_join(disk_mon_tid, NULL);

    DiskMgr_Quit();
    return 0;
}

int Storage_GetStatus(StorageStatus *status)
{
    memset(status, 0, sizeof(StorageStatus));

    if (sdcard.online == 0)
        status->status = STORAGE_MEDIA_STATUS_NOTEXIST; /* no card */
    else if (sdcard.online && sdcard.available)
        status->status = STORAGE_MEDIA_STATUS_READY; /* ready */
    else
        status->status = STORAGE_MEDIA_STATUS_ERROR; /* error */

    if (status->status == STORAGE_MEDIA_STATUS_READY) {
        PartInfo info;

        if (sdcard.part) {
            if (DiskMgr_GetPartInfo(sdcard.part, &info) < 0) {
                status->status = STORAGE_MEDIA_STATUS_ERROR;
                return 0;
            }

//            status->total_size = info.totalsize;

            if (access(SDCARD_MOUNT_POINT, F_OK) == 0) {
                struct statfs sfs;
                if (statfs(SDCARD_MOUNT_POINT, &sfs) < 0) {
                    status->status = STORAGE_MEDIA_STATUS_ERROR;
                    return -1;
                }

                if (access(SDCARD_MOUNT_POINT, W_OK) != 0) {
                    status->status = STORAGE_MEDIA_STATUS_PROTECTED;
                }

                status->free_size = (int64_t)sfs.f_bsize * (int64_t)sfs.f_bavail;
                status->total_size = (int64_t)sfs.f_bsize * (int64_t)sfs.f_blocks;
            }
        }
    }

    return 0;
}

int Storage_Format(void)
{
    if (sdcard.online == 0)
        return -1;

    if (sdcard.available) {
        if (sdcard.part) {
            if (DiskMgr_Umount(sdcard.part) < 0) {
                LogV(TAG, "DiskMgr_Umount failed");
                sdcard.available = 0;
                return -1;
            }
            sdcard.part = NULL;
            sdcard.available = 0;
        }
    }

    if (DiskMgr_MakeParts(sdcard.mmc, LABEL_TYPE_MSDOS, LABEL_TYPE_MSDOS, 1) < 0) {
        LogV(TAG, "DiskMgr_MakeParts failed");
        return -1;
    }

    sdcard.nparts = DiskMgr_GetPartNum(sdcard.mmc);
    if (sdcard.nparts != 1) {
        LogV(TAG, "DiskMgr_GetPartNum failed");
        return -1;
    }

    sdcard.part = DiskMgr_GetPartById(sdcard.mmc, 0);
    if (!sdcard.part) {
        LogV(TAG, "DiskMgr_GetPartById(0) failed");
        return -1;
    }

    if (DiskMgr_MkFs(sdcard.part, FS_TYPE_FAT32) < 0) {
        LogV(TAG, "DiskMgr_MkFs failed");
        return -1;
    }

    if (DiskMgr_Mount(sdcard.part, SDCARD_MOUNT_POINT) < 0) {
        LogV(TAG, "DiskMgr_Mount failed");
        return -1;
    }

    sdcard.available = 1;

    LogV(TAG, "NetApi_FormatStorage success");

    return 0;
}

int Storage_HaveEnoughSpace(void)
{
    StorageStatus status;
    int ret = Storage_GetStatus(&status);
    if (ret < 0)
        return 0;

    if (status.status == STORAGE_MEDIA_STATUS_READY) {
        int64_t freesize = status.free_size;
        if (freesize > STORAGE_LOW_WARTER_MARK)
            return 1;
    }

    return 0;
}
