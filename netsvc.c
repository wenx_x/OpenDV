#include <stdio.h>
#include <string.h>

#include "camapi.h"
#include "logger.h"
#include "recmgr.h"
#include "netlink.h"
#include "httpsvc.h"
#include "camera_fsm.h"

static const char *TAG = "NetSvc";

#define NELEMS(x)   (sizeof(x) / sizeof((x)[0]))

static int __Hello(json_object *params, json_object *result)
{
    json_object_object_add(result, "Name", json_object_new_string("OpenDV"));
    return 0;
}

static int __StartUpgrade(json_object *params, json_object *result)
{
    const char *filename = NULL;
    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "filename") == 0) {
            filename = json_object_get_string(val);
        }
    }

    return CamApi_StartUpgrade(filename);
}

static int __SystemShutdown(json_object *params, json_object *result)
{
    return CamApi_SystemShutdown();
}

static int __SystemReboot(json_object *params, json_object *result)
{
    return CamApi_SystemReboot();
}

static int __SystemFactoryReset(json_object *params, json_object *result)
{
    int hard = 0;
    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "hard") == 0) {
            hard = json_object_get_int(val);
        }
    }

    if (CamApi_SystemFactoryReset(hard) < 0)
        return -1;

    return 0;
}

static int __GetSystemDateTime(json_object *params, json_object *result)
{
    char datetime[64] = {0};

    int ret = CamApi_GetSystemDateTime(datetime, sizeof(datetime));
    if (ret < 0)
        return -1;

    json_object_object_add(result, "datetime", json_object_new_string(datetime));

    return 0;
}

static int __SetSystemDateTime(json_object *params, json_object *result)
{
    if (!params)
        return -1;

    char datetime[64] = {0};

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "datetime") == 0) {
            strncpy(datetime, json_object_get_string(val), sizeof(datetime) - 1);
        }
    }

    if (CamApi_SetSystemDateTime(datetime) < 0)
        return -1;

    return 0;
}

static int __GetStorageStatus(json_object *params, json_object *result)
{
    StorageStatus status;
    int ret = CamApi_GetStorageStatus(&status);
    if (ret < 0)
        return -1;

    json_object_object_add(result, "status",    json_object_new_int(status.status));
    json_object_object_add(result, "total_size", json_object_new_int64(status.total_size));
    json_object_object_add(result, "free_size",  json_object_new_int64(status.free_size));

    return 0;
}

static int __FormatStorage(json_object *params, json_object *result)
{
    int ret = CamApi_FormatStorage();
    if (ret < 0)
        return -1;
    return 0;
}

static int __GetWifiConfiguration(json_object *params, json_object *result)
{
    WifiConfiguration conf;
    int ret = CamApi_GetWifiConfiguration(&conf);
    if (ret < 0)
        return -1;

    json_object_object_add(result, "enable",   json_object_new_int(conf.enable));
    json_object_object_add(result, "ssid",     json_object_new_string(conf.ssid));
    json_object_object_add(result, "password", json_object_new_string(conf.password));
    json_object_object_add(result, "band",     json_object_new_int(conf.band));

    return 0;
}

static int __SetWifiConfiguration(json_object *params, json_object *result)
{
    if (!params)
        return -1;

    WifiConfiguration conf;
    if (CamApi_GetWifiConfiguration(&conf) < 0)
        return -1;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "enable") == 0) {
            conf.enable = json_object_get_int(val);
        } else if (strcmp(key, "ssid") == 0) {
            strncpy(conf.ssid, json_object_get_string(val), sizeof(conf.ssid) - 1);
        } else if (strcmp(key, "password") == 0) {
            strncpy(conf.password, json_object_get_string(val), sizeof(conf.password) - 1);
        } else if (strcmp(key, "band") == 0) {
            conf.band = json_object_get_int(val);
        }
    }

    if (CamApi_SetWifiConfiguration(&conf) < 0)
        return -1;

    return 0;
}

static int __GetVideoEncoderConfiguration(json_object *params, json_object *result)
{
    if (!params)
        return -1;

    int stream = -1;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "stream") == 0) {
            stream = json_object_get_int(val);
        }
    }

    if (stream < 0)
        return -1;

    VideoEncoderConfiguration conf;
    int ret = CamApi_GetVideoEncoderConfiguration(stream, &conf);
    if (ret < 0)
        return -1;

    switch (conf.profile) {
    case CODEC_PROFILE_BASELINE:
        json_object_object_add(result, "profile", json_object_new_string("baseline"));
        break;
    case CODEC_PROFILE_MAIN:
        json_object_object_add(result, "profile", json_object_new_string("main"));
        break;
    case CODEC_PROFILE_HIGH:
        json_object_object_add(result, "profile", json_object_new_string("high"));
        break;
    default:
        LogE(TAG, "unknown profile %d\n", conf.profile);
        return -1;
    }

    switch (conf.codec_type) {
    case CODEC_TYPE_H264:
        json_object_object_add(result, "codec_type", json_object_new_string("h264"));
        break;
    case CODEC_TYPE_H265:
        json_object_object_add(result, "codec_type", json_object_new_string("h265"));
        break;
    default:
        LogE(TAG, "unknown codec %d\n", conf.codec_type);
        return -1;
    }

    json_object_object_add(result, "width", json_object_new_int(conf.width));
    json_object_object_add(result, "height", json_object_new_int(conf.height));
    json_object_object_add(result, "gop", json_object_new_int(conf.gop));
    json_object_object_add(result, "framerate", json_object_new_int(conf.framerate));
    json_object_object_add(result, "bitrate", json_object_new_int(conf.bitrate));

    return 0;
}

static int get_profile_id(const char *profile)
{
    if (strcmp(profile, "baseline") == 0)
        return CODEC_PROFILE_BASELINE;
    else if (strcmp(profile, "main") == 0)
        return CODEC_PROFILE_MAIN;
    else if (strcmp(profile, "high") == 0)
        return CODEC_PROFILE_HIGH;

    return -1;
}

static int get_codec_type(const char *codec_type)
{
    if (strcmp(codec_type, "h264") == 0)
        return CODEC_TYPE_H264;
    else if (strcmp(codec_type, "h265") == 0)
        return CODEC_TYPE_H265;
    else if (strcmp(codec_type, "aac") == 0)
        return CODEC_TYPE_AAC;
    return -1;
}

static int __SetVideoEncoderConfiguration(json_object *params, json_object *result)
{
    if (!params)
        return -1;

    int stream = -1;

    const char *profile = NULL;
    const char *codec_type = NULL;

    int width = -1;
    int height = -1;

    int gop = -1;
    int framerate = -1;
    int bitrate = -1;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "stream") == 0) {
            stream = json_object_get_int(val);
        } else if (strcmp(key, "profile") == 0) {
            profile = json_object_get_string(val);
        } else if (strcmp(key, "codec_type") == 0) {
            codec_type = json_object_get_string(val);
        } else if (strcmp(key, "width") == 0) {
            width = json_object_get_int(val);
        } else if (strcmp(key, "height") == 0) {
            height = json_object_get_int(val);
        } else if (strcmp(key, "gop") == 0) {
            gop = json_object_get_int(val);
        } else if (strcmp(key, "framerate") == 0) {
            framerate = json_object_get_int(val);
        } else if (strcmp(key, "bitrate") == 0) {
            bitrate = json_object_get_int(val);
        }
    }

    if (stream < 0)
        return -1;

    VideoEncoderConfiguration conf;
    if (CamApi_GetVideoEncoderConfiguration(stream, &conf) < 0)
        return -1;

    if (profile)
        conf.profile = get_profile_id(profile);
    if (codec_type)
        conf.codec_type = get_codec_type(codec_type);
    if (width > 0)
        conf.width = width;
    if (height > 0)
        conf.height = height;
    if (gop > 0)
        conf.gop = gop;
    if (framerate > 0)
        conf.framerate = framerate;
    if (bitrate > 0)
        conf.bitrate = bitrate;

    if (CamApi_SetVideoEncoderConfiguration(stream, &conf) < 0)
        return -1;

    return 0;
}

static int __GetAudioEncoderConfiguration(json_object *params, json_object *result)
{
    AudioEncoderConfiguration conf;
    if (CamApi_GetAudioEncoderConfiguration(&conf) < 0)
        return -1;

    json_object_object_add(result, "enable",  json_object_new_int(conf.enable));

    switch (conf.codec_type) {
    case CODEC_TYPE_AAC:
        json_object_object_add(result, "codec_type",  json_object_new_string("aac"));
        break;
    default:
        return -1;
    }

    json_object_object_add(result, "bitrate",     json_object_new_int(conf.bitrate));
    json_object_object_add(result, "sample_rate", json_object_new_int(conf.sample_rate));
    json_object_object_add(result, "bit_width",   json_object_new_int(conf.bit_width));
    json_object_object_add(result, "channels",    json_object_new_int(conf.channels));
    json_object_object_add(result, "samples_per_frame", json_object_new_int(conf.samples_per_frame));

    return 0;
}

static int __SetAudioEncoderConfiguration(json_object *params, json_object *result)
{
    if (!params)
        return -1;

    AudioEncoderConfiguration conf;
    if (CamApi_GetAudioEncoderConfiguration(&conf) < 0)
        return -1;

    const char *codec_type = NULL;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "enable") == 0) {
            conf.enable = json_object_get_int(val);
        } else if (strcmp(key, "codec_type") == 0) {
            codec_type = json_object_get_string(val);
        } else if (strcmp(key, "bitrate") == 0) {
            conf.bitrate = json_object_get_int(val);
        } else if (strcmp(key, "sample_rate") == 0) {
            conf.sample_rate = json_object_get_int(val);
        } else if (strcmp(key, "bit_width") == 0) {
            conf.bit_width = json_object_get_int(val);
        } else if (strcmp(key, "channels") == 0) {
            conf.channels = json_object_get_int(val);
        } else if (strcmp(key, "samples_per_frame") == 0) {
            conf.samples_per_frame = json_object_get_int(val);
        }
    }

    if (codec_type) {
        conf.codec_type = get_codec_type(codec_type);
        if (conf.codec_type < 0)
            return -1;
    }

    if (CamApi_SetAudioEncoderConfiguration(&conf) < 0)
        return -1;

    return 0;
}

int __GetRecordSettings(json_object *params, json_object *result)
{
    RecordSettings settings;
    int ret = CamApi_GetRecordSettings(&settings);
    if (ret < 0)
        return -1;

    switch (settings.recfmt) {
    case REC_FMT_MOV:
        json_object_object_add(result, "recfmt", json_object_new_string("MOV"));
        break;
    case REC_FMT_MP4:
        json_object_object_add(result, "recfmt", json_object_new_string("MP4"));
        break;
    default:
        LogE(TAG, "unknown recfmt %d\n", settings.recfmt);
        return -1;
    }

    json_object_object_add(result, "pre_record_time", json_object_new_int(settings.pre_record_time));
    json_object_object_add(result, "max_record_size", json_object_new_int(settings.max_record_size));
    json_object_object_add(result, "circulate_record", json_object_new_int(settings.circulate_record));
    json_object_object_add(result, "duration", json_object_new_int(settings.duration));

    return 0;
}

int __SetRecordSettings(json_object *params, json_object *result)
{
    if (!params)
        return -1;

    RecordSettings settings;
    int ret = CamApi_GetRecordSettings(&settings);
    if (ret < 0)
        return -1;

    const char *recfmt = NULL;
    int pre_record_time = -1;
    int max_record_size = -1;
    int circulate_record = -1;
    int duration = -1;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "recfmt") == 0) {
            recfmt = json_object_get_string(val);
        } else if (strcmp(key, "pre_record_time") == 0) {
            pre_record_time = json_object_get_int(val);
        } else if (strcmp(key, "max_record_size") == 0) {
            max_record_size = json_object_get_int(val);
        } else if (strcmp(key, "circulate_record") == 0) {
            circulate_record = json_object_get_int(val);
        } else if (strcmp(key, "duration") == 0) {
            duration = json_object_get_int(val);
        }
    }

    if (!recfmt && pre_record_time < 0 && max_record_size < 0 && circulate_record < 0 && duration < 0)
        return -1;

    if (recfmt) {
        if (strcmp(recfmt, "MOV") == 0)
            settings.recfmt = REC_FMT_MOV;
        else if (strcmp(recfmt, "MP4") == 0)
            settings.recfmt = REC_FMT_MP4;
        else
            return -1;
    }

    if (pre_record_time >= 0)
        settings.pre_record_time = pre_record_time;

    if (max_record_size >= 0)
        settings.max_record_size = max_record_size;

    if (circulate_record >= 0)
        settings.circulate_record = circulate_record;

    if (duration >= 0)
        settings.duration = duration;

    return CamApi_SetRecordSettings(&settings);
}

int __GetPhotoTakingSettings(json_object *params, json_object *result)
{
    PhotoTakingSettings settings;
    int ret = CamApi_GetPhotoTakingSettings(&settings);
    if (ret < 0)
        return -1;

    json_object_object_add(result, "res_width",  json_object_new_int(settings.res_width));
    json_object_object_add(result, "res_height", json_object_new_int(settings.res_height));

    return 0;
}

int __SetPhotoTakingSettings(json_object *params, json_object *result)
{
    if (!params)
        return -1;

    PhotoTakingSettings settings;
    if (CamApi_GetPhotoTakingSettings(&settings) < 0)
        return -1;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "res_width") == 0) {
            settings.res_width = json_object_get_int(val);
        } else if (strcmp(key, "res_height") == 0) {
            settings.res_height = json_object_get_int(val);
        }
    }

    if (CamApi_SetPhotoTakingSettings(&settings) < 0)
        return -1;

    return 0;
}
int __GetImageSettings(json_object *params, json_object *result)
{
    ImageSettings settings;
    int ret = CamApi_GetImageSettings(&settings);
    if (ret < 0)
        return -1;

    json_object_object_add(result, "brightness", json_object_new_int(settings.brightness));
    json_object_object_add(result, "contrast",   json_object_new_int(settings.contrast));
    json_object_object_add(result, "saturation", json_object_new_int(settings.saturation));
    json_object_object_add(result, "hue",        json_object_new_int(settings.hue));
    json_object_object_add(result, "sharpness",  json_object_new_int(settings.sharpness));

    return 0;
}

int __SetImageSettings(json_object *params, json_object *result)
{
    if (!params)
        return -1;

    ImageSettings settings;
    if (CamApi_GetImageSettings(&settings) < 0)
        return -1;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "brightness") == 0) {
            settings.brightness = json_object_get_int(val);
        } else if (strcmp(key, "contrast") == 0) {
            settings.contrast = json_object_get_int(val);
        } else if (strcmp(key, "saturation") == 0) {
            settings.saturation = json_object_get_int(val);
        } else if (strcmp(key, "hue") == 0) {
            settings.hue = json_object_get_int(val);
        } else if (strcmp(key, "sharpness") == 0) {
            settings.sharpness = json_object_get_int(val);
        }
    }

    if (CamApi_SetImageSettings(&settings) < 0)
        return -1;

    return 0;
}

int __GetOsdSettings(json_object *params, json_object *result)
{
    OsdSettings settings;

    if (CamApi_GetOsdSettings(&settings) < 0)
        return -1;

    json_object *timeobj = json_object_new_object();

    json_object_object_add(timeobj, "enable",      json_object_new_int(settings.time.enable));
    json_object_object_add(timeobj, "font_size",   json_object_new_int(settings.time.font_size));
    json_object_object_add(timeobj, "color",       json_object_new_int(settings.time.color));
    json_object_object_add(timeobj, "alpha",       json_object_new_int(settings.time.alpha));
    json_object_object_add(timeobj, "x",           json_object_new_int(settings.time.x));
    json_object_object_add(timeobj, "y",           json_object_new_int(settings.time.y));
    json_object_object_add(timeobj, "time_format", json_object_new_int(settings.time.time_format));

    json_object *textobj = json_object_new_object();

    json_object_object_add(textobj, "enable",      json_object_new_int(settings.text.enable));
    json_object_object_add(textobj, "font_size",   json_object_new_int(settings.text.font_size));
    json_object_object_add(textobj, "color",       json_object_new_int(settings.text.color));
    json_object_object_add(textobj, "alpha",       json_object_new_int(settings.text.alpha));
    json_object_object_add(textobj, "x",           json_object_new_int(settings.text.x));
    json_object_object_add(textobj, "y",           json_object_new_int(settings.text.y));
    json_object_object_add(textobj, "text",        json_object_new_string(settings.text.text));

    json_object_object_add(result, "time", timeobj);
    json_object_object_add(result, "text", textobj);

    return 0;
}

int __SetOsdSettings(json_object *params, json_object *result)
{
    if (!params)
        return -1;

    OsdSettings settings;

    if (CamApi_GetOsdSettings(&settings) < 0)
        return -1;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "time") == 0) {
            json_object_object_foreach(val, key1, val1) {
                if (strcmp(key1, "enable") == 0) {
                    settings.time.enable = json_object_get_int(val1);
                } else if (strcmp(key1, "font_size") == 0) {
                    settings.time.font_size = json_object_get_int(val1);
                } else if (strcmp(key1, "color") == 0) {
                    settings.time.color = json_object_get_int(val1);
                } else if (strcmp(key1, "alpha") == 0) {
                    settings.time.alpha = json_object_get_int(val1);
                } else if (strcmp(key1, "x") == 0) {
                    settings.time.x = json_object_get_int(val1);
                } else if (strcmp(key1, "y") == 0) {
                    settings.time.y = json_object_get_int(val1);
                } else if (strcmp(key1, "time_format") == 0) {
                    settings.time.time_format = json_object_get_int(val1);
                }
            }
        } else if (strcmp(key, "text") == 0) {
            json_object_object_foreach(val, key1, val1) {
                if (strcmp(key1, "enable") == 0) {
                    settings.text.enable = json_object_get_int(val1);
                } else if (strcmp(key1, "font_size") == 0) {
                    settings.text.font_size = json_object_get_int(val1);
                } else if (strcmp(key1, "color") == 0) {
                    settings.text.color = json_object_get_int(val1);
                } else if (strcmp(key1, "alpha") == 0) {
                    settings.text.alpha = json_object_get_int(val1);
                } else if (strcmp(key1, "x") == 0) {
                    settings.text.x = json_object_get_int(val1);
                } else if (strcmp(key1, "y") == 0) {
                    settings.text.y = json_object_get_int(val1);
                } else if (strcmp(key1, "text") == 0) {
                    strncpy(settings.text.text, json_object_get_string(val1), sizeof(settings.text.text) - 1);
                }
            }
        }
    }

    if (CamApi_SetOsdSettings(&settings) < 0)
        return -1;

    return 0;
}

static int __SetCameraMode(json_object *params, json_object *result)
{
    CameraMode mode;
    const char *mode_str = NULL;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "mode") == 0) {
            mode_str = json_object_get_string(val);
        }
    }

    if (!mode_str)
        return -1;

    if (strcmp(mode_str, "record") == 0)
        mode = CAMERA_MODE_RECORD;
    else if (strcmp(mode_str, "capture") == 0)
        mode = CAMERA_MODE_CAPTURE;
    else if (strcmp(mode_str, "playback") == 0)
        mode = CAMERA_MODE_PLAYBACK;
    else
        return -1;

    return CamApi_SetCameraMode(mode);
}

static int __GetCameraMode(json_object *params, json_object *result)
{
    CameraMode mode;
    if (CamApi_GetCameraMode(&mode) < 0)
        return -1;

    if (mode == CAMERA_MODE_RECORD)
        json_object_object_add(result, "mode", json_object_new_string("record"));
    else if (mode == CAMERA_MODE_CAPTURE)
        json_object_object_add(result, "mode", json_object_new_string("capture"));
    else if (mode == CAMERA_MODE_IDLE)
        json_object_object_add(result, "mode", json_object_new_string("idle"));
    else if (mode == CAMERA_MODE_PLAYBACK)
        json_object_object_add(result, "mode", json_object_new_string("playback"));
    else
        return -1;

    return 0;
}

static int __StartRecord(json_object *params, json_object *result)
{
    return CamApi_StartRecord();
}

static int __StopRecord(json_object *params, json_object *result)
{
    return CamApi_StopRecord();
}

static int __StartSnapshot(json_object *params, json_object *result)
{
    return CamApi_StartSnapshot();
}

static int __StopSnapshot(json_object *params, json_object *result)
{
    return CamApi_StopSnapshot();
}

static int __StartPlayback(json_object *params, json_object *result)
{
    const char *filename = NULL;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "filename") == 0) {
            filename = json_object_get_string(val);
        }
    }

    if (filename == NULL)
        return -1;

    return CamApi_PlaybackStart(filename);
}

static int __StopPlayback(json_object *params, json_object *result)
{
    return CamApi_PlaybackStop();
}

PlaybackCmd get_playback_cmd(const char *cmd_str)
{
    PlaybackCmd cmd;

    if (strcmp(cmd_str, "pause") == 0)
        cmd = PLAYBACK_CMD_PAUSE;
    else if (strcmp(cmd_str, "resume") == 0)
        cmd = PLAYBACK_CMD_RESUME;
    else if (strcmp(cmd_str, "speed") == 0)
        cmd = PLAYBACK_CMD_SPEED;
    else if (strcmp(cmd_str, "seek") == 0)
        cmd = PLAYBACK_CMD_SEEK;
    else
        cmd = PLAYBACK_CMD_NONE;

    return cmd;
}

static int __PlaybackControl(json_object *params, json_object *result)
{
    int off_ms = -1;
    float scale = 0;
    PlaybackCmd cmd = PLAYBACK_CMD_NONE;

    const char *cmd_str = NULL;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "command") == 0) {
            cmd_str = json_object_get_string(val);
            cmd = get_playback_cmd(cmd_str);
        } else if (strcmp(key, "scale") == 0) {
            scale = json_object_get_double(val);
        } else if (strcmp(key, "off_ms") == 0) {
            off_ms = json_object_get_int(val);
        }
    }

    if (cmd == PLAYBACK_CMD_NONE)
        return -1;

    if (cmd == PLAYBACK_CMD_SPEED && scale <= 0)
        return -1;

    if (cmd == PLAYBACK_CMD_SEEK &&  off_ms < 0)
        return -1;

    int ret;

    switch (cmd) {
    case PLAYBACK_CMD_PAUSE:
        ret = CamApi_PlaybackPause();
        break;
    case PLAYBACK_CMD_RESUME:
        ret = CamApi_PlaybackResume();
        break;
    case PLAYBACK_CMD_SEEK:
        ret = CamApi_PlaybackSeek(off_ms);
        break;
    case PLAYBACK_CMD_SPEED:
        ret = CamApi_PlaybackSetSpeed(scale);
        break;
    default:
        LogV(TAG, "Unknown playback control command: %d", cmd);
        ret = -1;
    }

    return ret;
}

static int __GetFilesNum(json_object *params, json_object *result)
{
    const char *suffix = NULL;
    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "suffix") == 0) {
            suffix = json_object_get_string(val);
        }
    }

    int total = CamApi_GetFilesNum(suffix);
    if (total < 0)
        return -1;

    json_object_object_add(result, "total", json_object_new_int(total));

    return 0;
}

static int __GetFilesList(json_object *params, json_object *result)
{
    int index = -1;
    int numfiles = -1;
    const char *suffix = NULL;
    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "index") == 0) {
            index = json_object_get_int(val);
        } else if (strcmp(key, "numfiles") == 0) {
            numfiles = json_object_get_int(val);
        } else if (strcmp(key, "suffix") == 0) {
            suffix = json_object_get_string(val);
        }
    }

    if (index < 0 || numfiles < 0)
        return -1;

    if (numfiles > 1024)
        numfiles = 1024;

    FileItem *files = calloc(numfiles, sizeof(FileItem));
    if (!files)
        return -1;

    int n = CamApi_GetFilesList(index, suffix, files, numfiles);

    json_object *files_array = json_object_new_array();

    int i;
    for (i = 0; i < n; i++) {
        json_object *file_obj = json_object_new_object();

        FileItem *file = &files[i];
        json_object_object_add(file_obj, "index", json_object_new_int(file->index));
        json_object_object_add(file_obj, "name",  json_object_new_string(file->filename));
        json_object_object_add(file_obj, "size",  json_object_new_int64(file->filesize));
        json_object_object_add(file_obj, "time",  json_object_new_string(file->datetime));
        json_object_array_add(files_array, file_obj);
    }

    json_object_object_add(result, "filelist", files_array);

    return 0;
}

static int __DeleteFiles(json_object *params, json_object *result)
{
    const char *filename = NULL;
    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "filelist") == 0) {
            int length = json_object_array_length(val);

            int i;
            for (i = 0; i < length; i++) {
                json_object *file = json_object_array_get_idx(val, i);
                filename = json_object_get_string(file);
                CamApi_DeleteFile(filename);
            }
        }
    }

    return 0;
}

static int __CreateEventSubscriber(json_object *params, json_object *result)
{
    int session_id;

    if (CamApi_CreateEventSubscriber(&session_id) < 0)
        return -1;

    json_object_object_add(result, "session_id", json_object_new_int(session_id));

    return 0;
}

static int __DeleteEventSubscriber(json_object *params, json_object *result)
{
    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "session_id") == 0) {
            int session_id = json_object_get_int(val);
            if (CamApi_DeleteEventSubscriber(session_id) == 0)
                return 0;
        }
    }

    return -1;
}

static char *get_event_type_str(EventType event_type)
{
    char *ev = NULL;

    switch (event_type) {
    case EVENT_TYPE_PHOTO_TAKING_STARTED:
        ev = "photo_taking_started";
        break;
    case EVENT_TYPE_PHOTO_TAKING_STOPPED:
        ev = "photo_taking_stopped";
        break;
    case EVENT_TYPE_PHOTO_TAKEN:
        ev = "photo_taken";
        break;
    case EVENT_TYPE_VIDEO_RECORDING_STARTED:
        ev = "video_recording_started";
        break;
    case EVENT_TYPE_VIDEO_RECORDING_STOPPED:
        ev = "video_recording_stopped";
        break;
    case EVENT_TYPE_UPGRADE_STARTED:
        ev = "upgrade_started";
        break;
    case EVENT_TYPE_UPGRADE_STOPPED:
        ev = "upgrade_stopped";
        break;
    case EVENT_TYPE_STORAGE_OUT_OF_SPACE:
        ev = "storage_out_of_space";
        break;
    case EVENT_TYPE_LOW_BATTERY_LEVEL:
        ev = "low_battery_level";
        break;
    case EVENT_TYPE_STORAGE_STATUS:
        ev = "storage_status";
        break;
    case EVENT_TYPE_STORAGE_ADDED:
        ev = "storage_added";
        break;
    case EVENT_TYPE_STORAGE_REMOVED:
        ev = "storage_removed";
        break;
    }

    return ev;
}

static int __GetEvents(json_object *params, json_object *result)
{
    int session_id = -1;

    json_object_object_foreach(params, key, val) {
        if (strcmp(key, "session_id") == 0) {
            session_id = json_object_get_int(val);
        }
    }

    if (session_id < 0)
        return -1;

    Event events[16];
    int nmemb = CamApi_GetEvents(session_id, events, NELEMS(events));
    if (nmemb < 0)
        return 0;

    json_object *event_array = json_object_new_array();

    int i;
    for (i = 0; i < nmemb; i++) {
        json_object *event_obj = json_object_new_object();

        Event *ev = &events[i];

        char *ev_str = get_event_type_str(ev->event_type);
        if (!ev_str) {
            json_object_put(event_obj);
            continue;
        }

        json_object_object_add(event_obj, "event_type", json_object_new_string(ev_str));

        switch (ev->event_type) {
        case EVENT_TYPE_VIDEO_RECORDING_STARTED:
            json_object_object_add(event_obj, "snapshot", json_object_new_string(ev->event_data.recording_started.snapshot));
            break;
        case EVENT_TYPE_VIDEO_RECORDING_STOPPED:
            json_object_object_add(event_obj, "filename", json_object_new_string(ev->event_data.recording_stopped.filepath));
            json_object_object_add(event_obj, "thumbnail", json_object_new_string(ev->event_data.recording_stopped.thumbnail));
            break;
        default:
            break;
        }

        json_object_array_add(event_array, event_obj);
    }

    json_object_object_add(result, "events", event_array);

    return 0;
}

int NetSvc_Init(HttpSvc *svc, const char *path)
{
    NetLink *link = NetLink_Create(svc, path);
    if (!link)
        return -1;

    CamApi_Init();

    NetLink_Register("Hello", __Hello);

    NetLink_Register("StartUpgrade", __StartUpgrade);

    NetLink_Register("SystemShutdown",     __SystemShutdown);
    NetLink_Register("SystemReboot",       __SystemReboot);
    NetLink_Register("SystemFactoryReset", __SystemFactoryReset);

    NetLink_Register("GetSystemDateTime", __GetSystemDateTime);
    NetLink_Register("SetSystemDateTime", __SetSystemDateTime);

    NetLink_Register("GetStorageStatus", __GetStorageStatus);
    NetLink_Register("FormatStorage",    __FormatStorage);

    NetLink_Register("GetWifiConfiguration", __GetWifiConfiguration);
    NetLink_Register("SetWifiConfiguration", __SetWifiConfiguration);

    NetLink_Register("GetVideoEncoderConfiguration", __GetVideoEncoderConfiguration);
    NetLink_Register("SetVideoEncoderConfiguration", __SetVideoEncoderConfiguration);

    NetLink_Register("GetAudioEncoderConfiguration", __GetAudioEncoderConfiguration);
    NetLink_Register("SetAudioEncoderConfiguration", __SetAudioEncoderConfiguration);

    NetLink_Register("SetCameraMode", __SetCameraMode);
    NetLink_Register("GetCameraMode", __GetCameraMode);

    NetLink_Register("StartRecord", __StartRecord);
    NetLink_Register("StopRecord",  __StopRecord);

    NetLink_Register("StartSnapshot", __StartSnapshot);
    NetLink_Register("StopSnapshot",  __StopSnapshot);

    NetLink_Register("StartPlayback", __StartPlayback);
    NetLink_Register("StopPlayback",  __StopPlayback);
    NetLink_Register("PlaybackControl", __PlaybackControl);

    NetLink_Register("GetRecordSettings", __GetRecordSettings);
    NetLink_Register("SetRecordSettings", __SetRecordSettings);

    NetLink_Register("GetPhotoTakingSettings", __GetPhotoTakingSettings);
    NetLink_Register("SetPhotoTakingSettings", __SetPhotoTakingSettings);

    NetLink_Register("GetImageSettings", __GetImageSettings);
    NetLink_Register("SetImageSettings", __SetImageSettings);

    NetLink_Register("GetOsdSettings", __GetOsdSettings);
    NetLink_Register("SetOsdSettings", __SetOsdSettings);

    NetLink_Register("GetFilesNum",  __GetFilesNum);
    NetLink_Register("GetFilesList", __GetFilesList);
    NetLink_Register("DeleteFiles",  __DeleteFiles);

    NetLink_Register("CreateEventSubscriber", __CreateEventSubscriber);
    NetLink_Register("DeleteEventSubscriber", __DeleteEventSubscriber);
    NetLink_Register("GetEvents", __GetEvents);

    return 0;
}

int NetSvc_Quit(void)
{
    CamApi_Quit();
    return 0;
}
