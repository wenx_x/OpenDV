#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>

#include "vod.h"
#include "logger.h"
#include "replayer.h"
#include "mediaapi.h"

static const char *TAG = "replayer";

#define MAX_FRAME_SIZE  (1024 * 1024)

struct Replayer {
    VodFile *vodfile;
    VideoPlayer *player;

    pthread_t tid;
    int abort;

    int pause;
};

Replayer *Replayer_Create(const char *filename)
{
    Replayer *r = malloc(sizeof(Replayer));
    if (!r)
        return NULL;
    memset(r, 0, sizeof(Replayer));

    r->vodfile = VodFile_Open(filename);
    if (!r->vodfile) {
        LogV(TAG, "VodFile_Open %s failed!", filename);
        free(r);
        return NULL;
    }

    RecInfo recInfo;
    if (RecMgr_GetRecInfo(filename, &recInfo) < 0) {
        LogV(TAG, "get %s detail info failed!", filename);
        VodFile_Close(r->vodfile);
        free(r);
        return NULL;
    }

    if (!recInfo.has_video) {
        LogV(TAG, "unknown video codec");
        VodFile_Close(r->vodfile);
        free(r);
        return NULL;
    }

    VideoInfo *vinfo = NULL;

    if (recInfo.has_video)
        vinfo = &recInfo.video_info;

    AudioInfo *ainfo = NULL;
    if (recInfo.has_audio)
        ainfo = &recInfo.audio_info;

    r->player = MediaApi_CreatePlayer(vinfo, ainfo);
    if (!r->player) {
        LogV(TAG, "MediaApi_CreatePlayer failed");
        VodFile_Close(r->vodfile);
        free(r);
        return NULL;
    }

    return r;
}

int Replayer_Destroy(Replayer *r)
{
    MediaApi_DestroyPlayer(r->player);
    VodFile_Close(r->vodfile);
    free(r);
    return 0;
}

static void *replay_thread(void *arg)
{
    Replayer *r = (Replayer *)arg;

    uint8_t *buf = malloc(MAX_FRAME_SIZE);

    while (!r->abort) {
        if (r->pause) {
            usleep(30 * 1000);
            continue;
        }

        VodFrameInfo finfo;
        int size = VodFile_ReadFrame(r->vodfile, &finfo, buf, MAX_FRAME_SIZE);
        if (size <= 0)
            break;

        VideoPlayer_SendStream(r->player, finfo.media, buf, size, finfo.pts);
    }

    return NULL;
}

int Replayer_Start(Replayer *r)
{
    pthread_create(&r->tid, NULL, replay_thread, r);
    return 0;
}

int Replayer_Stop(Replayer *r)
{
    if (r->tid) {
        r->abort = 1;
        pthread_join(r->tid, NULL);
    }
    return 0;
}

int Replayer_Seek(Replayer *r, int off_ms)
{
    return VodFile_SeekByTime(r->vodfile, off_ms);
}

int Replayer_SetSpeed(Replayer *r, float scale)
{
    return VodFile_SetScaleFactor(r->vodfile, scale);
}

int Replayer_Pause(Replayer *r)
{
    if (!r)
        return -1;

    r->pause = 1;

    return 0;
}

int Replayer_Resume(Replayer *r)
{
    if (!r)
        return -1;

    VodFile_Resume(r->vodfile);
    r->pause = 0;

    return 0;
}
