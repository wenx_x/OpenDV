#ifndef __VOD_H__
#define __VOD_H__

#include <stdint.h>

#include "rtspsvc.h"
#include "recmgr.h"

typedef struct VodFile VodFile;

int VodFile_GetMediaDesc(char *filename, MediaDesc *descs, int nmemb);

VodFile *VodFile_Open(const char *filename);
int VodFile_Close(VodFile *vf);

typedef struct {
    MediaType media;
    CodecType codec;

    int64_t pts;

    int width;
    int height;

    int keyframe;
} VodFrameInfo;

int VodFile_ReadFrame(VodFile *vf, VodFrameInfo *info, uint8_t *data, size_t size);
int VodFile_SeekByTime(VodFile *vf, int off_ms);
int VodFile_SetScaleFactor(VodFile *vf, float scale);

int VodFile_Resume(VodFile *vf);

#endif
