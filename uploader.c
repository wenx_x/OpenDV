#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>

#include "exif.h"
#include "logger.h"
#include "config.h"
#include "recmgr.h"
#include "httpsvc.h"

static const char *TAG = "UpLoader";

static char urlpath[1024];

typedef enum {
    FILE_TYPE_ORIGINAL  = 0,
    FILE_TYPE_THUMBNAIL = 1,
} FileType;

typedef struct {
    int fd;
    char *url;

    uint32_t totalsize;
    char filepath[1024];
} UploadCtx;

static long upload_open(HttpConn *conn, const char *url, void *priv_data)
{
    UploadCtx *ctx = malloc(sizeof(UploadCtx));
    if (!ctx)
        return 0;
    memset(ctx, 0, sizeof(UploadCtx));

    ctx->url = strdup(url);
    if (!ctx->url) {
        free(ctx);
        return 0;
    }

    char *filename = basename(ctx->url);
    if (strlen(filename) == 0) {
        LogV(TAG, "cannot get filename");
        free(ctx->url);
        free(ctx);
        return 0;
    }

    snprintf(ctx->filepath, sizeof(ctx->filepath), "%s/%s", UPLOAD_PATH, filename);

    ctx->fd = open(ctx->filepath, O_CREAT | O_TRUNC | O_WRONLY, 0644);
    if (ctx->fd < 0) {
        LogV(TAG, "create %s failed: %s", ctx->filepath, strerror(errno));
        free(ctx);
        return 0;
    }

    LogV(TAG, "create %s success", ctx->filepath);

    return (long)ctx;
}

static int upload_close(long handle)
{
    UploadCtx *ctx = (UploadCtx *)handle;

    LogV(TAG, "upload %s done, total size is %u byte", ctx->filepath, ctx->totalsize);

    close(ctx->fd);
    free(ctx->url);
    free(ctx);

    return 0;
}

static int upload_seek(long handle, uint64_t pos)
{
    UploadCtx *ctx = (UploadCtx *)handle;
    if (lseek(ctx->fd, pos, SEEK_SET) < 0) {
        LogV(TAG, "lseek file offset %llu failed: %s", pos, strerror(errno));
        return -1;
    }
    return 0;
}

static ssize_t upload_write(long handle, uint8_t *buf, size_t size)
{
    UploadCtx *ctx = (UploadCtx *)handle;

    if (!buf || !size)
        return 0;

    int ret = write(ctx->fd, buf, size);
    if (ret != size) {
        LogV(TAG, "write %s failed: %s", ctx->filepath, strerror(errno));
        return -1;
    }

    ctx->totalsize += size;

    return size;
}

static HttpOps uploader_ops = {
    .open  = upload_open,
    .close = upload_close,
    .seek  = upload_seek,

    .read  = NULL,
    .write = upload_write,
};

int UpLoader_Init(HttpSvc *httpsvc, char *path)
{
    strncpy(urlpath, path, sizeof(urlpath) - 1);

   /* url must end up with '/' */
    if (path[strlen(path) - 1] != '/')
        strcat(urlpath, "/");

    return HttpSvc_AddRoute(httpsvc, HTTP_METHOD_POST, urlpath, &uploader_ops, NULL);
}
