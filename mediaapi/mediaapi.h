#ifndef __HI_MODULES_H__
#define __HI_MODULES_H__

#include "common.h"

int MediaApi_Init(void);
int MediaApi_Quit(void);

typedef enum {
    VIDEO_FMT_ID_3840x2160P30,
    VIDEO_FMT_ID_1080P30,
    VIDEO_FMT_ID_720P30,
    VIDEO_FMT_ID_720P25,
} FmtId;

int MediaApi_StartVideoInput(FmtId fmt_id);
int MediaApi_StopVideoInput(void);

int MediaApi_StartAudioInput(void);
int MediaApi_StopAudioInput(void);

typedef struct {
    StreamInfo sinfo;

    int last_frame; /* last captured photo */

    uint8_t *data;
    int size;
} VideoFrame;

typedef struct {
    StreamInfo sinfo;

    int size;
    void *data;
} AudioFrame;

/*
 * Video Stream
 */
typedef struct VideoStream VideoStream;

typedef struct {
    CodecType codec;
    int width;
    int height;
    int framerate;
    int bitrate;
    int gop;
} VideoEncodeConf;

/* type: 0 - MainVideo, 1 - SubVideo */
VideoStream *MediaApi_OpenVideoStream(int type, VideoEncodeConf *conf, int (*frame_cb)(VideoFrame *frame, void *priv_data), void *priv_data);
int MediaApi_CloseVideoStream(VideoStream *st);

int VideoStream_Start(VideoStream *st, int nb_frames);
int VideoStream_Stop(VideoStream *st);
int VideoStream_GetStreamHeadInfo(VideoStream *st, StreamInfoType type, uint8_t *head, uint32_t *len);

/*
 * Photo Stream
 */
typedef struct PhotoStream PhotoStream;

typedef struct {
    int width;
    int height;
    int qfactor;
} PhotoEncodeConf;

/* type: 0 - MainSnap, 1 - SubSnap */
PhotoStream *MediaApi_OpenPhotoStream(int type, PhotoEncodeConf *conf, int (*frame_cb)(VideoFrame *frame, void *priv_data), void *priv_data);
int MediaApi_ClosePhotoStream(PhotoStream *st);

int PhotoStream_Start(PhotoStream *st, int nb_frames);
int PhotoStream_Stop(PhotoStream *st);

/*
 * Audio Stream
 */
typedef struct AudioStream AudioStream;

typedef struct {
    CodecType codec;
    int bitrate;
    int sample_rate;
    int bit_width;
    int channels;
    int samples_per_frame;
} AudioEncodeConf;

AudioStream *MediaApi_OpenAudioStream(AudioEncodeConf *conf, int (*frame_cb)(AudioFrame *frame, void *priv_data), void *priv_data);
int MediaApi_CloseAudioStream(AudioStream *st);

int AudioStream_Start(AudioStream *st);
int AudioStream_Stop(AudioStream *st);

/*
 * OSD
 */

typedef struct VideoOSD VideoOSD;

VideoOSD *MediaApi_OpenVideoOSD(int vosd_id);
int MediaApi_CloseVideoOSD(VideoOSD *vosd);

int MediaApi_SetOSDAttr(VideoOSD *vosd, uint32_t *rgba, int len, int color, int alpha, uint16_t x, uint16_t y, uint16_t width, uint16_t height);

int MediaApi_StartOSD(VideoOSD *vosd);
int MediaApi_StopOSD(VideoOSD *vosd);

/*
 * Exif
 */
typedef struct {
    char description[32];
    char manufacturer[32];
    char model_number[32];
    char firmware_ver[32];

    int metering_mode;
    int light_source;
    int focal_length;
    int focal_35mm;
} ExifInfo;

int MediaApi_SetExifInfo(ExifInfo *exif);

typedef struct VideoPreview VideoPreview;

/* type: 0 - capture preview, 1 - record preview */
VideoPreview *MediaApi_OpenVideoPreview(int type);
int MediaApi_CloseVideoPreview(VideoPreview *vprev);

typedef struct VideoPlayer VideoPlayer;

VideoPlayer *MediaApi_CreatePlayer(VideoInfo *vinfo, AudioInfo *ainfo);
int MediaApi_DestroyPlayer(VideoPlayer *player);

int VideoPlayer_SendStream(VideoPlayer *player, MediaType media, void *data, int size, int64_t pts);

#endif /* __HI_MODULES_H__ */
