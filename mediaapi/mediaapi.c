#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>

#include "hi_mapi_vcap_define.h"
#include "hi_mapi_venc_define.h"
#include "hi_mapi_vproc_define.h"
#include "hi_mapi_audio_component.h"

#include "hi_mapi_sys.h"
#include "hi_mapi_acap.h"
#include "hi_mapi_aenc.h"
#include "hi_mapi_disp.h"
#include "hi_mapi_vcap.h"
#include "hi_mapi_venc.h"
#include "hi_mapi_vproc.h"

#include "hi_mapi_audio_component_define.h"

#include "hi_mw_avplayer.h"

#include "logger.h"
#include "mediaapi.h"

static const char *TAG = "MediaApi";

#define NELEMS(x)   (sizeof(x) / sizeof((x)[0]))

#define MAX_VIDEO_CAPTURE    (HI_VCAP_MAX_NUM)
#define MAX_VIDEO_PROCESSOR  (2)
#define MAX_VPORT_PER_VPROC  (4)
#define MAX_VIDEO_ENCODER    (4)

typedef enum {
    ENCODE_STREAM_TYPE_MAIN_VIDEO  = 0x01,
    ENCODE_STREAM_TYPE_SUB_VIDEO   = 0x02,

    ENCODE_STREAM_TYPE_MAIN_SNAP   = 0x04,
    ENCODE_STREAM_TYPE_SUB_SNAP    = 0x08,

    ENCODE_STREAM_TYPE_REC_PREVIEW = 0x10,
    ENCODE_STREAM_TYPE_CAP_PREVIEW = 0x20,
} EncodeStreamType;

typedef enum {
    CAPTURE_TYPE_VIDEO,
    CAPTURE_TYPE_SNAP,
} CaptureType;

typedef struct {
    int vcap_id;
    CaptureType type;   /* 0 - video, 1 - snap */
    int res_width;
    int res_height;
    int framerate;
} VideoCaptureConfig;

typedef struct {
    int vport_id;
    EncodeStreamType type;
    int framerate;
    int res_width;
    int res_height;
    int pixel_fmt;  /* 0 - YUV420, 1 - YUV422 */
} VideoPortConfig;

typedef struct {
    int vproc_id;
    int capability;
    CaptureType type;   /* 0 - video, 1 - snap */
    int max_width;
    int max_height;

    int num_vports;
    VideoPortConfig vports[MAX_VPORT_PER_VPROC];
} VideoProcessorConfig;

typedef struct {
    int vcap_id;
    int vproc_id;
    int vport_id;
} VideoPort;

typedef struct {
    int enable;

    int venc_id;

    VideoPort *vport;

    HI_MPP_PAYLOAD_TYPE_E codec_id;
//    int type;   /* 0:MainVideo,1:SubVideo,2:MainSnap,3:SubSnap */
    int bufsize;

    int res_width;
    int res_height;

    /* h264/h265 specified */
    int framerate;
    int gop;
    int bitrate;

    /* jpeg specified */
    int qfactor;

    int (*frame_cb)(VideoFrame *frame, void *priv_data);
    void *priv_data;

    HI_VENC_CALLBACK_S stVencCB;

    uint8_t *buffer;
    uint8_t *bufend;
    uint8_t *bufptr;
} VideoEncoder;

static VideoEncoder videoEncoders[MAX_VIDEO_ENCODER];

typedef struct {
    AAC_TYPE_E          enAACType;
    AAC_BPS_E           enBitRate;
    HI_MPP_AUDIO_SAMPLE_RATE_E enSmpRate;
    HI_MPP_AUDIO_BITWIDTH_E enBitWidth;
    HI_MPP_AUDIO_SOUND_TYPE_E enSoundType;
    AAC_TRANS_TYPE_E    enTransType;

    HI_S16              s16BandWidth;
    int                 u32PtNumPerFrm;
} AudioEncoderConfig;

static AudioEncoderConfig audioCfg = {
    .enAACType    = AAC_TYPE_AACLC,
    .enBitRate    = AAC_BPS_48K,
    .enBitWidth   = HI_MPP_AUDIO_BITWIDTH_16,
    .enSmpRate    = HI_MPP_AUDIO_SAMPLE_RATE_48,
    .enSoundType  = HI_MPP_AUDIO_SOUND_TYPE_MONO,
    .enTransType  = AAC_TRANS_TYPE_ADTS,
    .s16BandWidth = 0,
    .u32PtNumPerFrm = 1024,
};

typedef struct {
    VideoCaptureConfig vcapCfg;
    VideoProcessorConfig vprocCfg;
} VideoPipe;

typedef struct {
    FmtId fmt_id;
    char *fmt_name;
    int nb_video_pipes;
    VideoPipe videoPipes[2];
} VideoConfig;

static VideoConfig videoCfgs[] = {
    {
        .fmt_id = VIDEO_FMT_ID_3840x2160P30,
        .fmt_name = "3840x2160p30",
        .nb_video_pipes = 2,
        .videoPipes[0] = {
            .vcapCfg = {
                .vcap_id = 0,
                .type = CAPTURE_TYPE_SNAP,   /* 0 - video, 1 - snap */
                .res_width = 3840,
                .res_height = 2160,
                .framerate = 30,
            },
            .vprocCfg = {
                .vproc_id = 0,
                .capability = ENCODE_STREAM_TYPE_MAIN_SNAP | ENCODE_STREAM_TYPE_CAP_PREVIEW,
                .type = CAPTURE_TYPE_SNAP,  /* SNAP */
                .max_width = 3840,
                .max_height = 2160,
                .num_vports = 2,
                .vports = {
                    {
                        .vport_id = 0,
                        .type = ENCODE_STREAM_TYPE_MAIN_SNAP,
                        .framerate = 30,
                        .res_width = 3840,
                        .res_height = 2160,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 1,
                        .type = ENCODE_STREAM_TYPE_CAP_PREVIEW,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    }
                }
            },
        },
        .videoPipes[1] = {
            .vcapCfg = {
                .vcap_id = 1,
                .type = CAPTURE_TYPE_VIDEO,   /* 0 - video, 1 - snap */
                .res_width = 3840,
                .res_height = 2160,
                .framerate = 30,
            },
            .vprocCfg = {
                .vproc_id = 1,
                .capability = ENCODE_STREAM_TYPE_MAIN_VIDEO | ENCODE_STREAM_TYPE_SUB_VIDEO |
                              ENCODE_STREAM_TYPE_SUB_SNAP   | ENCODE_STREAM_TYPE_REC_PREVIEW,
                .type = CAPTURE_TYPE_VIDEO,  /* video */
                .max_width = 3840,
                .max_height = 2160,
                .num_vports = 4,
                .vports = {
                    {
                        .vport_id = 0,
                        .type = ENCODE_STREAM_TYPE_MAIN_VIDEO,
                        .framerate = 30,
                        .res_width = 3840,
                        .res_height = 2160,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 1,
                        .type = ENCODE_STREAM_TYPE_SUB_VIDEO,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 2,
                        .type = ENCODE_STREAM_TYPE_SUB_SNAP,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 3,
                        .type = ENCODE_STREAM_TYPE_REC_PREVIEW,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    }
                }
            }
        }
    },
    {
        .fmt_id = VIDEO_FMT_ID_1080P30,
        .fmt_name = "1080P30",
        .nb_video_pipes = 2,
        .videoPipes[0] = {
            .vcapCfg = {
                .vcap_id = 0,
                .type = CAPTURE_TYPE_SNAP,   /* 0 - video, 1 - snap */
                .res_width = 1920,
                .res_height = 1080,
                .framerate = 30,
            },
            .vprocCfg = {
                .vproc_id = 0,
                .capability = ENCODE_STREAM_TYPE_MAIN_SNAP | ENCODE_STREAM_TYPE_CAP_PREVIEW,
                .type = CAPTURE_TYPE_SNAP,  /* SNAP */
                .max_width = 1920,
                .max_height = 1080,
                .num_vports = 2,
                .vports = {
                    {
                        .vport_id = 0,
                        .type = ENCODE_STREAM_TYPE_MAIN_SNAP,
                        .framerate = 30,
                        .res_width = 1920,
                        .res_height = 1080,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 1,
                        .type = ENCODE_STREAM_TYPE_CAP_PREVIEW,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    }
                }
            },
        },
        .videoPipes[1] = {
            .vcapCfg = {
                .vcap_id = 1,
                .type = CAPTURE_TYPE_VIDEO,   /* 0 - video, 1 - snap */
                .res_width = 1920,
                .res_height = 1080,
                .framerate = 30,
            },
            .vprocCfg = {
                .vproc_id = 1,
                .capability = ENCODE_STREAM_TYPE_MAIN_VIDEO | ENCODE_STREAM_TYPE_SUB_VIDEO |
                              ENCODE_STREAM_TYPE_SUB_SNAP   | ENCODE_STREAM_TYPE_REC_PREVIEW,
                .type = CAPTURE_TYPE_VIDEO,  /* video */
                .max_width = 1920,
                .max_height = 1080,
                .num_vports = 4,
                .vports = {
                    {
                        .vport_id = 0,
                        .type = ENCODE_STREAM_TYPE_MAIN_VIDEO,
                        .framerate = 30,
                        .res_width = 1920,
                        .res_height = 1080,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 1,
                        .type = ENCODE_STREAM_TYPE_SUB_VIDEO,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 2,
                        .type = ENCODE_STREAM_TYPE_SUB_SNAP,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 3,
                        .type = ENCODE_STREAM_TYPE_REC_PREVIEW,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    }
                }
            }
        }
    },
    {
        .fmt_id = VIDEO_FMT_ID_720P30,
        .fmt_name = "720P30",
        .nb_video_pipes = 2,
        .videoPipes[0] = {
            .vcapCfg = {
                .vcap_id = 0,
                .type = CAPTURE_TYPE_SNAP,   /* 0 - video, 1 - snap */
                .res_width = 1280,
                .res_height = 720,
                .framerate = 30,
            },
            .vprocCfg = {
                .vproc_id = 0,
                .capability = ENCODE_STREAM_TYPE_MAIN_SNAP | ENCODE_STREAM_TYPE_CAP_PREVIEW,
                .type = CAPTURE_TYPE_SNAP,  /* SNAP */
                .max_width = 1280,
                .max_height = 720,
                .num_vports = 2,
                .vports = {
                    {
                        .vport_id = 0,
                        .type = ENCODE_STREAM_TYPE_MAIN_SNAP,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 1,
                        .type = ENCODE_STREAM_TYPE_CAP_PREVIEW,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    }
                }
            },
        },
        .videoPipes[1] = {
            .vcapCfg = {
                .vcap_id = 1,
                .type = CAPTURE_TYPE_VIDEO,   /* 0 - video, 1 - snap */
                .res_width = 1280,
                .res_height = 720,
                .framerate = 30,
            },
            .vprocCfg = {
                .vproc_id = 1,
                .capability = ENCODE_STREAM_TYPE_MAIN_VIDEO | ENCODE_STREAM_TYPE_SUB_VIDEO |
                              ENCODE_STREAM_TYPE_SUB_SNAP   | ENCODE_STREAM_TYPE_REC_PREVIEW,
                .type = CAPTURE_TYPE_VIDEO,  /* video */
                .max_width = 1280,
                .max_height = 720,
                .num_vports = 4,
                .vports = {
                    {
                        .vport_id = 0,
                        .type = ENCODE_STREAM_TYPE_MAIN_VIDEO,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 1,
                        .type = ENCODE_STREAM_TYPE_SUB_VIDEO,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 2,
                        .type = ENCODE_STREAM_TYPE_SUB_SNAP,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 3,
                        .type = ENCODE_STREAM_TYPE_REC_PREVIEW,
                        .framerate = 30,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    }
                }
            }
        }
    },
    {
        .fmt_id = VIDEO_FMT_ID_720P25,
        .fmt_name = "720P25",
        .nb_video_pipes = 2,
        .videoPipes[0] = {
            .vcapCfg = {
                .vcap_id = 0,
                .type = CAPTURE_TYPE_SNAP,   /* 0 - video, 1 - snap */
                .res_width = 1280,
                .res_height = 720,
                .framerate = 25,
            },
            .vprocCfg = {
                .vproc_id = 0,
                .capability = ENCODE_STREAM_TYPE_MAIN_SNAP | ENCODE_STREAM_TYPE_CAP_PREVIEW,
                .type = CAPTURE_TYPE_SNAP,  /* SNAP */
                .max_width = 1280,
                .max_height = 720,
                .num_vports = 2,
                .vports = {
                    {
                        .vport_id = 0,
                        .type = ENCODE_STREAM_TYPE_MAIN_SNAP,
                        .framerate = 25,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 1,
                        .type = ENCODE_STREAM_TYPE_CAP_PREVIEW,
                        .framerate = 25,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    }
                }
            },
        },
        .videoPipes[1] = {
            .vcapCfg = {
                .vcap_id = 1,
                .type = CAPTURE_TYPE_VIDEO,   /* 0 - video, 1 - snap */
                .res_width = 1280,
                .res_height = 720,
                .framerate = 25,
            },
            .vprocCfg = {
                .vproc_id = 1,
                .capability = ENCODE_STREAM_TYPE_MAIN_VIDEO | ENCODE_STREAM_TYPE_SUB_VIDEO |
                              ENCODE_STREAM_TYPE_SUB_SNAP   | ENCODE_STREAM_TYPE_REC_PREVIEW,
                .type = CAPTURE_TYPE_VIDEO,  /* video */
                .max_width = 1280,
                .max_height = 720,
                .num_vports = 4,
                .vports = {
                    {
                        .vport_id = 0,
                        .type = ENCODE_STREAM_TYPE_MAIN_VIDEO,
                        .framerate = 25,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 1,
                        .type = ENCODE_STREAM_TYPE_SUB_VIDEO,
                        .framerate = 25,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 2,
                        .type = ENCODE_STREAM_TYPE_SUB_SNAP,
                        .framerate = 25,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    },
                    {
                        .vport_id = 3,
                        .type = ENCODE_STREAM_TYPE_REC_PREVIEW,
                        .framerate = 25,
                        .res_width = 1280,
                        .res_height = 720,
                        .pixel_fmt = 0,
                    }
                }
            }
        }
    }
};

static VideoConfig *currentVideoCfg = NULL;

static VideoCaptureConfig *get_video_capture_config(int vcap_id)
{
    if (currentVideoCfg == NULL)
        return NULL;

    int i;
    for (i = 0; i < currentVideoCfg->nb_video_pipes; i++) {
        if (currentVideoCfg->videoPipes[i].vcapCfg.vcap_id == vcap_id)
            return &currentVideoCfg->videoPipes[i].vcapCfg;
    }

    return NULL;
}

static int MediaApi_SetupSensor(HI_MPP_SENSOR_ATTR_S *pstSensorMode)
{
    HI_HANDLE SensorHdl = 0;

    pstSensorMode->enWdrMode = HI_MPP_WDR_MODE_NONE;

    HI_S32 s32Ret = HI_MAPI_Sensor_SetAttr(SensorHdl, pstSensorMode);
    if (s32Ret != HI_SUCCESS)
        return -1;

    return 0;
}

#define CHECK_RET(ret)  do { if (ret != HI_SUCCESS)  return -1; } while (0)
#define CHECK_RET_NULL(ret)  do { if (ret != HI_SUCCESS)  return NULL; } while (0)

static int MediaApi_StartVideoCapture(int vcap_id, VideoCaptureConfig *vcapCfg)
{
    HI_MPP_VCAP_ATTR_S stVCapAttr;
    HI_HANDLE VCapHdl = vcap_id;

    stVCapAttr.stResolution.u32Width  = vcapCfg->res_width;
    stVCapAttr.stResolution.u32Height = vcapCfg->res_height;

    stVCapAttr.s32FrameRate   = vcapCfg->framerate;
    stVCapAttr.enWdrMode      = HI_MPP_WDR_MODE_NONE;
    stVCapAttr.enPixelFormat  = HI_MPP_PIXEL_FORMAT_420;
    stVCapAttr.enCompressMode = HI_COMPRESS_MODE_SEG;

    if (vcapCfg->type == CAPTURE_TYPE_VIDEO)
        stVCapAttr.enVCapType = HI_MPP_VCAP_TYPE_VIDEO;
    else if (vcapCfg->type == CAPTURE_TYPE_SNAP)
        stVCapAttr.enVCapType = HI_MPP_VCAP_TYPE_SNAP;
    else {
        LogV(TAG, "unknown capture type: %d", vcapCfg->type);
        return -1;
    }

    CHECK_RET(HI_MAPI_VCap_SetAttr(VCapHdl, &stVCapAttr));

    CHECK_RET(HI_MAPI_VCap_Start(VCapHdl));

    CHECK_RET(HI_MAPI_VCap_Isp_Start(VCapHdl));

    return 0;
}

static int MediaApi_StopVideoCapture(int vcap_id)
{
    HI_HANDLE VCapHdl = vcap_id;

    HI_MAPI_VCap_Isp_Stop(VCapHdl);
    HI_MAPI_VCap_Stop(VCapHdl);

    return 0;
}

static int MediaApi_StartVideoProcessor(int vproc_id, int vcap_id, VideoProcessorConfig *vprocCfg)
{
    if (vproc_id < 0 || vproc_id >= MAX_VIDEO_PROCESSOR)
        return -1;

    HI_HANDLE VCapHdl = vcap_id;
    HI_HANDLE VProcHdl = vproc_id;

    HI_VPROC_ATTR_S stVprocAttr;

    if (vprocCfg->type == CAPTURE_TYPE_VIDEO)
        stVprocAttr.enVProcType = VPROC_TYPE_VIDEO;
    else if (vprocCfg->type == CAPTURE_TYPE_SNAP)
        stVprocAttr.enVProcType = VPROC_TYPE_SNAP;
    else {
        LogV(TAG, "unknown capture type: %d", vprocCfg->type);
        return -1;
    }

    stVprocAttr.u32MaxW = vprocCfg->max_width;
    stVprocAttr.u32MaxH = vprocCfg->max_height;

    stVprocAttr.bStitchEnable = HI_FALSE;

    if (HI_MAPI_VProc_Init(VProcHdl, &stVprocAttr) != HI_SUCCESS)
        return -1;

    if (HI_MAPI_VProc_Start(VProcHdl) != HI_SUCCESS) {
        HI_MAPI_VProc_DeInit(VProcHdl);
        return -1;
    }

    if (HI_MAPI_VProc_Bind_VCap(VCapHdl, VProcHdl) != HI_SUCCESS) {
        HI_MAPI_VProc_Stop(VProcHdl);
        HI_MAPI_VProc_DeInit(VProcHdl);
        return -1;
    }

    return 0;
}

static int MediaApi_StopVideoProcessor(int vproc_id, int vcap_id)
{
    HI_HANDLE VCapHdl = vcap_id;
    HI_HANDLE VProcHdl = vproc_id;

    HI_MAPI_VProc_UnBind_VCap(VCapHdl, VProcHdl);

    HI_MAPI_VProc_Stop(VProcHdl);

    HI_MAPI_VProc_DeInit(VProcHdl);

    return 0;
}

static HI_HANDLE SensorHdl = 0;
static HI_MPP_SENSOR_ATTR_S *pstSensorModes = NULL;
static HI_S32 s32ModesCnt;

int MediaApi_Init(void)
{
    HI_S32 s32Ret = HI_SUCCESS;

    s32Ret = HI_MAPI_Sys_Init();
    if (s32Ret != HI_SUCCESS)
        return -1;

    s32Ret = HI_MAPI_Media_Init();
    if (s32Ret != HI_SUCCESS)
        return -1;

    s32Ret = HI_MAPI_Sensor_GetAllModes(SensorHdl, &pstSensorModes, &s32ModesCnt);
    if (s32Ret != HI_SUCCESS)
        return -1;

#if 1
    int i;
    for (i = 0; i < s32ModesCnt; i++) {
        HI_MPP_SENSOR_ATTR_S *pstSensorMode = &pstSensorModes[i];

        printf("mode %d: %dx%dp%d\n", i,
                pstSensorMode->stResolution.u32Width,
                pstSensorMode->stResolution.u32Height,
                pstSensorMode->s32FrameRate);
    }
#endif

    return 0;
}

int MediaApi_Quit(void)
{
    if (pstSensorModes) {
        free(pstSensorModes);
        pstSensorModes = NULL;
    }

    HI_MAPI_Media_DeInit();
    HI_MAPI_Sys_DeInit();

    return 0;
}

static HI_MPP_SENSOR_ATTR_S *find_best_sensor_mode(int width, int height, int framerate)
{
    HI_MPP_SENSOR_ATTR_S *seletedMode = NULL;

    int i;
    for (i = 0; i < s32ModesCnt; i++) {
        HI_MPP_SENSOR_ATTR_S *pstSensorMode = &pstSensorModes[i];
        if (width <= pstSensorMode->stResolution.u32Width &&
            height <= pstSensorMode->stResolution.u32Height &&
            framerate <= pstSensorMode->s32FrameRate) {
            if (seletedMode == NULL)
                seletedMode = pstSensorMode;
            else {
                if (seletedMode->stResolution.u32Width  >= pstSensorMode->stResolution.u32Width &&
                    seletedMode->stResolution.u32Height >= pstSensorMode->stResolution.u32Height &&
                    seletedMode->s32FrameRate           >= pstSensorMode->s32FrameRate)
                    seletedMode = pstSensorMode;
            }
        }
    }

    return seletedMode;
}

typedef struct {
    FmtId fmt_id;
    int width;
    int height;
    int framerate;
} FmtEntry;

static FmtEntry fmtTables[] = {
    { VIDEO_FMT_ID_3840x2160P30, 3840, 2160, 30 },
    { VIDEO_FMT_ID_1080P30,      1920, 1080, 30 },
    { VIDEO_FMT_ID_720P30,       1280, 720,  30 },
    { VIDEO_FMT_ID_720P25,       1280, 720,  25 },
};

static FmtEntry *lookup_fmt_entry(FmtId fmt_id)
{
    int i;
    for (i = 0; i < NELEMS(fmtTables); i++) {
        FmtEntry *entry = &fmtTables[i];
        if (entry->fmt_id == fmt_id)
            return entry;
    }

    return NULL;
}

static int get_fmt_width(FmtId fmt_id)
{
    FmtEntry *entry = lookup_fmt_entry(fmt_id);
    if (!entry)
        return -1;

    return entry->width;
}

static int get_fmt_height(FmtId fmt_id)
{
    FmtEntry *entry = lookup_fmt_entry(fmt_id);
    if (!entry)
        return -1;

    return entry->height;
}

static int get_fmt_framerate(FmtId fmt_id)
{
    FmtEntry *entry = lookup_fmt_entry(fmt_id);
    if (!entry)
        return -1;

    return entry->framerate;
}

static VideoConfig *find_video_config_by_fmt_id(FmtId fmt_id)
{
    int i;
    for (i = 0; i < NELEMS(videoCfgs); i++) {
        VideoConfig *videoCfg = &videoCfgs[i];
        if (videoCfg->fmt_id == fmt_id)
            return videoCfg;
    }

    return NULL;
}

int MediaApi_StartVideoPipe(VideoPipe *videoPipe)
{
    VideoCaptureConfig *vcapCfg = &videoPipe->vcapCfg;
    if (MediaApi_StartVideoCapture(vcapCfg->vcap_id, vcapCfg) < 0)
        return -1;

    VideoProcessorConfig *vprocCfg = &videoPipe->vprocCfg;

    if (MediaApi_StartVideoProcessor(vprocCfg->vproc_id, vcapCfg->vcap_id, vprocCfg) < 0)
        return -1;

    return 0;
}

int MediaApi_StopVideoPipe(VideoPipe *videoPipe)
{
    VideoCaptureConfig *vcapCfg = &videoPipe->vcapCfg;
    VideoProcessorConfig *vprocCfg = &videoPipe->vprocCfg;

    MediaApi_StopVideoProcessor(vprocCfg->vproc_id, vcapCfg->vcap_id);
    MediaApi_StopVideoCapture(vcapCfg->vcap_id);

    return 0;
}

int MediaApi_StartVideoInput(FmtId fmt_id)
{
    int width = get_fmt_width(fmt_id);
    int height = get_fmt_height(fmt_id);
    int framerate = get_fmt_framerate(fmt_id);

    if (width < 0 || height < 0 || framerate < 0) {
        LogV(TAG, "unknown resolution %dx%dP%d", width, height, framerate);
        return -1;
    }

    HI_MPP_SENSOR_ATTR_S *pstSensorMode = find_best_sensor_mode(width, height, framerate);
    if (!pstSensorMode) {
        LogV(TAG, "invalid sensor mode %dx%dP%d", width, height, framerate);
        return -1;
    }

    if (MediaApi_SetupSensor(pstSensorMode) < 0) {
        LogV(TAG, "MediaApi_SetupSensor failed, mode %dx%dP%d", width, height, framerate);
        return -1;
    }

    VideoConfig *videoCfg = find_video_config_by_fmt_id(fmt_id);
    if (!videoCfg)
        return -1;

    int i;
    for (i = 0; i < NELEMS(videoCfg->videoPipes); i++) {
        if (MediaApi_StartVideoPipe(&videoCfg->videoPipes[i]) < 0)
            return -1;
    }

    currentVideoCfg = videoCfg;

    return 0;
}

int MediaApi_StopVideoInput(void)
{
    VideoConfig *videoCfg = currentVideoCfg;

    int i;
    for (i = 0; i < NELEMS(videoCfg->videoPipes); i++) {
        MediaApi_StopVideoPipe(&videoCfg->videoPipes[i]);
    }

    return 0;
}

static VideoProcessorConfig *find_best_video_processor_config(EncodeStreamType type)
{
    if (!currentVideoCfg)
        return NULL;

    int i;
    for (i = 0; i < NELEMS(currentVideoCfg->videoPipes); i++) {
        VideoProcessorConfig *vprocCfg = &currentVideoCfg->videoPipes[i].vprocCfg;
        if (vprocCfg->capability & type)
            return vprocCfg;
    }

    return NULL;
}

static VideoPortConfig *find_best_video_port_config(VideoProcessorConfig *vprocCfg, EncodeStreamType type)
{
    if (!currentVideoCfg)
        return NULL;

    int vport_id;
    for (vport_id = 0; vport_id < NELEMS(vprocCfg->vports); vport_id++) {
        VideoPortConfig *vport = &vprocCfg->vports[vport_id];
        if (vport->type == type)
            return vport;
    }

    return NULL;
}

static VideoCaptureConfig *find_related_video_capture_config(VideoProcessorConfig *vprocCfg)
{
    if (!currentVideoCfg)
        return NULL;

    int i;
    for (i = 0; i < NELEMS(currentVideoCfg->videoPipes); i++) {
        if (vprocCfg == &currentVideoCfg->videoPipes[i].vprocCfg)
            return &currentVideoCfg->videoPipes[i].vcapCfg;
    }

    return NULL;
}

VideoPort *MediaApi_GetVideoPort(EncodeStreamType type)
{
    VideoProcessorConfig *vprocCfg = find_best_video_processor_config(type);
    if (!vprocCfg)
        return NULL;

    VideoPortConfig *vportCfg = find_best_video_port_config(vprocCfg, type);
    if (!vportCfg)
        return NULL;

    VideoCaptureConfig *vcapCfg = find_related_video_capture_config(vprocCfg);
    if (!vcapCfg)
        return NULL;

    VideoPort *vport = malloc(sizeof(VideoPort));
    if (!vport)
        return NULL;
    memset(vport, 0, sizeof(VideoPort));

    vport->vcap_id = vcapCfg->vcap_id;
    vport->vproc_id = vprocCfg->vproc_id;
    vport->vport_id = vportCfg->vport_id;

    HI_VPORT_ATTR_S stVPortAttr;
    stVPortAttr.s32FrameRate = vportCfg->framerate;
    stVPortAttr.stResolution.u32Width = vportCfg->res_width;
    stVPortAttr.stResolution.u32Height = vportCfg->res_height;
    stVPortAttr.enPixFormat = HI_MPP_PIXEL_FORMAT_420;

    if (type == ENCODE_STREAM_TYPE_MAIN_SNAP)
        stVPortAttr.enCompressMode = HI_COMPRESS_MODE_NONE;
    else
        stVPortAttr.enCompressMode = HI_COMPRESS_MODE_SEG;

    if (HI_MAPI_VProc_Port_SetAttr(vport->vproc_id, vport->vport_id, &stVPortAttr) != HI_SUCCESS) {
        LogV(TAG, "HI_MAPI_VProc_Port_SetAttr(%d-%d) failed", vport->vproc_id, vport->vport_id);
        free(vport);
        return NULL;
    }

    if (HI_MAPI_VProc_Port_Start(vport->vproc_id, vport->vport_id) != HI_SUCCESS) {
        LogV(TAG, "HI_MAPI_VProc_Port_Start(%d-%d) failed", vport->vproc_id, vport->vport_id);
        free(vport);
        return NULL;
    }

    return vport;
}

int MediaApi_PutVideoPort(VideoPort *vport)
{
    HI_MAPI_VProc_Port_Stop(vport->vproc_id, vport->vport_id);
    free(vport);

    return 0;
}

static int get_frame_size(HI_VENC_DATA_S *pVStreamData)
{
    int size = 0;

    int i = 0;
    for (i = 0; i < pVStreamData->u32PackCount; i++) {
        int len = pVStreamData->astPack[i].au32Len[0];
        int off = pVStreamData->astPack[i].u32Offset;

        size += len - off;
    }

    return size;
}

static int64_t get_time(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (int64_t)tv.tv_sec * 1000 + (int64_t)tv.tv_usec / 1000;
}

static HI_S32 VIDEO_DataProc(HI_HANDLE VencHdl, HI_VENC_DATA_S *pVStreamData, HI_VOID *pPrivateData)
{
    HI_U8 *pPackVirtAddr;
    HI_U32 u32PackLength;
    HI_U32 u32PackOffset;

    VideoEncoder *venc = (VideoEncoder *)pPrivateData;

    int64_t pts;
    uint8_t *data = NULL;
    int size = 0;

    CodecType codec;
    MediaType media;

    FrameType frame_type = FRAME_TYPE_UNKNOWN;

    HI_MPP_PAYLOAD_TYPE_E enPayloadType = pVStreamData->astPack[0].stDataType.enPayloadType;
    if (enPayloadType == HI_MPP_PAYLOAD_TYPE_H264) {
        media = MEDIA_TYPE_VIDEO;
        codec = CODEC_TYPE_H264;
        HI_VENC_DATA_H264E_NALU_TYPE_E enH264EType = pVStreamData->astPack[0].stDataType.enH264EType;

        switch (enH264EType) {
        case HI_ENC_H264E_NALU_PSLICE:
            frame_type = FRAME_TYPE_P;
            break;
        case HI_ENC_H264E_NALU_ISLICE:
        case HI_ENC_H264E_NALU_IDRSLICE:
            frame_type = FRAME_TYPE_I;
            break;
        case HI_ENC_H264E_NALU_BSLICE:
            frame_type = FRAME_TYPE_B;
            break;
        default:
            LogV(TAG, "unknown h264 payload type %d", enPayloadType);
            return HI_FALSE;
        }
    } else if (enPayloadType == HI_MPP_PAYLOAD_TYPE_H265) {
        media = MEDIA_TYPE_VIDEO;
        codec = CODEC_TYPE_H265;

        HI_VENC_DATA_H265E_NALU_TYPE_E enH265EType = pVStreamData->astPack[0].stDataType.enH265EType;
        switch (enH265EType) {
        case HI_ENC_H265E_NALU_PSLICE:
            frame_type = FRAME_TYPE_P;
            break;
        case HI_ENC_H265E_NALU_ISLICE:
        case HI_ENC_H265E_NALU_IDRSLICE:
            frame_type = FRAME_TYPE_I;
            break;
        case HI_ENC_H265E_NALU_BSLICE:
            frame_type = FRAME_TYPE_B;
            break;
        default:
            LogV(TAG, "unknown h265 payload type %d", enPayloadType);
            return HI_FALSE;
        }
    } else if (enPayloadType == HI_MPP_PAYLOAD_TYPE_JPEG) {
        media = MEDIA_TYPE_IMAGE;
        codec = CODEC_TYPE_JPEG;
    } else {
        LogV(TAG, "unknown codec type %d", enPayloadType);
        return -1;
    }

    /* fullfill videoframe */
    if (pVStreamData->u32PackCount == 1) {
        pPackVirtAddr = pVStreamData->astPack[0].pu8Addr[0];
        u32PackLength = pVStreamData->astPack[0].au32Len[0];
        u32PackOffset = pVStreamData->astPack[0].u32Offset;

        pts  = pVStreamData->astPack[0].u64PTS * 90000 / 1000000;
        data = pPackVirtAddr + u32PackOffset;
        size = u32PackLength - u32PackOffset;
    } else {
        int frame_size = get_frame_size(pVStreamData);
        int bufsize = venc->bufend - venc->buffer;

        if (frame_size > bufsize) {
            if (!venc->buffer) {
                venc->buffer = malloc(frame_size);
                venc->bufend = venc->buffer + frame_size;
            } else {
                venc->buffer = realloc(venc->buffer, frame_size);
                venc->bufend = venc->buffer + frame_size;
            }
        }

        uint8_t *ptr = venc->buffer;

        int i = 0;
        for (i = 0; i < pVStreamData->u32PackCount; i++) {
            pPackVirtAddr = pVStreamData->astPack[i].pu8Addr[0];
            u32PackLength = pVStreamData->astPack[i].au32Len[0];
            u32PackOffset = pVStreamData->astPack[i].u32Offset;

            memcpy(ptr, pPackVirtAddr + u32PackOffset, u32PackLength - u32PackOffset);
            ptr += u32PackLength - u32PackOffset;
        }

        pts  = pVStreamData->astPack[0].u64PTS * 90000 / 1000000;
        data = venc->buffer;
        size = ptr - venc->buffer;
    }

    VideoFrame f = {
        .sinfo = {
            .media = media,
            .codec = codec,
            .pts = pts,
            .time_ms = get_time(),
            .frame_type = frame_type,
        },
        .last_frame = pVStreamData->bEndOfStream,
        .data = data,
        .size = size,
    };

    if (venc->frame_cb)
        venc->frame_cb(&f, venc->priv_data);

    return 0;
}

static pthread_mutex_t venc_lock = PTHREAD_MUTEX_INITIALIZER;

static VideoEncoder *find_and_enable_video_encoder(void)
{
    pthread_mutex_lock(&venc_lock);
    int i;
    for (i = 0; i < NELEMS(videoEncoders); i++) {
        VideoEncoder *venc = &videoEncoders[i];
        if (venc->enable == 0) {
            memset(venc, 0, sizeof(VideoEncoder));
            venc->enable = 1;
            venc->venc_id = i;

            pthread_mutex_unlock(&venc_lock);
            return venc;
        }
    }
    pthread_mutex_unlock(&venc_lock);

    return NULL;
}

VideoEncoder *MediaApi_GetVideoEncoder(CodecType codec_id, int res_width, int res_height, int framerate, int bitrate, int gop)
{
    VideoEncoder *venc = find_and_enable_video_encoder();
    if (!venc)
        return NULL;

    switch (codec_id) {
    case CODEC_TYPE_H264:
        venc->codec_id = HI_MPP_PAYLOAD_TYPE_H264;
        break;
    case CODEC_TYPE_H265:
        venc->codec_id = HI_MPP_PAYLOAD_TYPE_H265;
        break;
    default:
        LogE(TAG, "unknown video codec id %d\n", codec_id);
        return NULL;
    }

    venc->res_width = res_width;
    venc->res_height = res_height;

    venc->framerate = framerate;
    venc->bitrate = bitrate;
    venc->gop = gop;
    venc->bufsize = 2 * res_width * res_height;

    HI_HANDLE hVencHdl = venc->venc_id;

    HI_MPP_VENC_ATTR_S stVencAttr;
    stVencAttr.stVencPloadTypeAttr.enType = venc->codec_id;
    stVencAttr.stVencPloadTypeAttr.stResolution.u32Width = venc->res_width;
    stVencAttr.stVencPloadTypeAttr.stResolution.u32Height = venc->res_height;
    stVencAttr.stVencPloadTypeAttr.u32BufSize = venc->bufsize;

    if (venc->codec_id == HI_MPP_PAYLOAD_TYPE_H265) {
        stVencAttr.stVencPloadTypeAttr.stAttrH265e.u32Profile = 0;
        stVencAttr.stVencPloadTypeAttr.stAttrH265e.stParamRef.u32Base = 1;
        stVencAttr.stVencPloadTypeAttr.stAttrH265e.stParamRef.u32Enhance = 0;
        stVencAttr.stVencPloadTypeAttr.stAttrH265e.stParamRef.bEnablePred = HI_TRUE;
    } else if (venc->codec_id == HI_MPP_PAYLOAD_TYPE_H264) {
        stVencAttr.stVencPloadTypeAttr.stAttrH264e.u32Profile = 0;
        stVencAttr.stVencPloadTypeAttr.stAttrH264e.stParamRef.u32Base = 1;
        stVencAttr.stVencPloadTypeAttr.stAttrH264e.stParamRef.u32Enhance = 0;
        stVencAttr.stVencPloadTypeAttr.stAttrH264e.stParamRef.bEnablePred = HI_TRUE;
    }

    stVencAttr.stRcAttr.enRcMode = HI_MPP_VENC_RC_MODE_CBR;
    stVencAttr.stRcAttr.stAttrCbr.u32Gop = venc->gop;
    stVencAttr.stRcAttr.stAttrCbr.u32FrameRate = venc->framerate;
    stVencAttr.stRcAttr.stAttrCbr.u32BitRate = venc->bitrate / 1000;

    if (HI_MAPI_VEnc_Init(hVencHdl, &stVencAttr) != HI_SUCCESS) {
        venc->enable = 0;
        return NULL;
    }

    return venc;
}

/* qfactor: [1, 99] */
VideoEncoder *MediaApi_GetJpegEncoder(int res_width, int res_height, int qfactor)
{
    if (qfactor < 0 || qfactor > 99)
        return NULL;

    VideoEncoder *venc = find_and_enable_video_encoder();
    if (!venc) {
        LogV(TAG, "no more avaialble video encoder");
        return NULL;
    }

    venc->res_width = res_width;
    venc->res_height = res_height;
    venc->qfactor = qfactor;
    venc->bufsize = 2 * res_width * res_height;

    HI_HANDLE hVencHdl = venc->venc_id;
    HI_MPP_VENC_ATTR_S stVencAttr;
    stVencAttr.stVencPloadTypeAttr.enType = HI_MPP_PAYLOAD_TYPE_JPEG;
    stVencAttr.stVencPloadTypeAttr.stResolution.u32Width = res_width;
    stVencAttr.stVencPloadTypeAttr.stResolution.u32Height = res_height;
    stVencAttr.stVencPloadTypeAttr.u32BufSize = venc->bufsize;
    stVencAttr.stVencPloadTypeAttr.stAttrJpege.bEnableDCF = HI_TRUE;
    stVencAttr.stVencPloadTypeAttr.stAttrJpege.u32Qfactor = qfactor;

    if (HI_MAPI_VEnc_Init(hVencHdl, &stVencAttr) != HI_SUCCESS) {
        venc->enable = 0;
        return NULL;
    }

    return venc;
}

int MediaApi_PutVideoEncoder(VideoEncoder *venc)
{
    HI_MAPI_VEnc_DeInit(venc->venc_id);
    if (venc->buffer != NULL)
        free(venc->buffer);
    memset(venc, 0, sizeof(VideoEncoder));
    return 0;
}

int VideoEncoder_BindVideoPort(VideoEncoder *venc, VideoPort *vport)
{
    venc->vport = vport;

    HI_S32 s32Ret = HI_MAPI_VEnc_Bind_VProc(vport->vproc_id, vport->vport_id, venc->venc_id);
    if (s32Ret != HI_SUCCESS) {
        LogV(TAG, "HI_MAPI_VEnc_Bind_VProc failed, vproc_id = %d, vport_id = %d, venc_id = %d, s32Ret: %x",
                vport->vproc_id, vport->vport_id, venc->venc_id, s32Ret);
        return -1;
    }

    return 0;
}

int VideoEncoder_UnBindVideoPort(VideoEncoder *venc, VideoPort *vport)
{
    CHECK_RET(HI_MAPI_VEnc_UnBind_VProc(vport->vproc_id, vport->vport_id, venc->venc_id));
    return 0;
}

int VideoEncoder_StartEncode(VideoEncoder *venc, int framecnt)
{
    HI_S32 s32Ret = HI_MAPI_VEnc_Start(venc->venc_id, framecnt);
    if (s32Ret != HI_SUCCESS) {
        LogV(TAG, "HI_MAPI_VEnc_Start failed, s32Ret: %08x", s32Ret);
        return -1;
    }

    return 0;
}

int VideoEncoder_StopEncode(VideoEncoder *venc)
{
    HI_S32 s32Ret = HI_MAPI_VEnc_Stop(venc->venc_id);
    if (s32Ret != HI_SUCCESS) {
        LogV(TAG, "HI_MAPI_VEnc_Stop failed, s32Ret: %08x", s32Ret);
        return -1;
    }
    return 0;
}

int VideoEncoder_SetPhotoAttr(VideoEncoder *jpgenc)
{
    HI_S32 s32Ret;
    //trigger snap
    HI_MPP_SNAP_MODE_E enSnapMode = HI_MPP_SNAP_MODE_PROTUNE;

    HI_MPP_SNAP_ATTR_S stSnapAttr;
    stSnapAttr.bSnapInfoCCM = HI_FALSE;
    stSnapAttr.u32FrameCnt = 5;
    stSnapAttr.stProAttr.enProOpType = HI_MPP_VCAP_PROTUNE_OP_TYPE_NONE;

    s32Ret = HI_MAPI_VCap_SetSnapAttr(jpgenc->vport->vcap_id, enSnapMode, &stSnapAttr);
    if (s32Ret != HI_SUCCESS) {
        LogV(TAG, "HI_MAPI_VCap_SetSnapAttr failed, s32Ret: %08x", s32Ret);
        return -1;
    }

    HI_PHOTO_PROC_TYPE_E enPhotoType = PHOTO_PROC_TYPE_LL;
    HI_PHOTO_PROC_ATTR_S stPhotoAttr;
    stPhotoAttr.stLLAttr.s32NrLuma = 4;
    stPhotoAttr.stLLAttr.s32NrChroma = 4;
    stPhotoAttr.stLLAttr.s32Sharpen = 4;
    stPhotoAttr.stLLAttr.s32Saturation = 2;
    stPhotoAttr.stLLAttr.s32Iso = 6400;
    s32Ret = HI_MAPI_VProc_SetPhotoAttr(jpgenc->vport->vproc_id, jpgenc->vport->vport_id, enPhotoType, &stPhotoAttr);
    if (s32Ret != HI_SUCCESS) {
        LogV(TAG, "HI_MAPI_VProc_SetPhotoAttr failed, s32Ret: %08x", s32Ret);
        return -1;
    }

    return 0;
}

int VideoEncoder_TriggerTakePhoto(VideoEncoder *jpgenc)
{
    HI_S32 s32Ret;

    s32Ret = HI_MAPI_VCap_Trigger(jpgenc->vport->vcap_id);
    if (s32Ret != HI_SUCCESS) {
        LogV(TAG, "HI_MAPI_VCap_Trigger failed, s32Ret: %08x", s32Ret);
        return -1;
    }

    s32Ret = HI_MAPI_VProc_PhotoProcess(jpgenc->vport->vproc_id, jpgenc->vport->vport_id);
    if (s32Ret != HI_SUCCESS) {
        LogV(TAG, "HI_MAPI_VProc_PhotoProcess failed, s32Ret: %08x", s32Ret);
        return -1;
    }

    return 0;
}

struct VideoStream {
    VideoPort *vport;
    VideoEncoder *venc;

    EncodeStreamType stream_type;
};

static int VideoEncoder_RegisterCallBack(VideoEncoder *venc,
                                         int (*frame_cb)(VideoFrame *frame, void *priv_data),
                                         void *priv_data)
{
    venc->frame_cb = frame_cb;
    venc->priv_data = priv_data;

    venc->stVencCB.pfnDataCB = VIDEO_DataProc;
    venc->stVencCB.pPrivateData = venc;

    HI_S32 s32Ret = HI_MAPI_VEnc_RegisterCallback(venc->venc_id, &venc->stVencCB);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_VEnc_RegisterCallback fail s32Ret: %x\n", s32Ret);
        return -1;
    }

    return 0;
}

static int VideoEncoder_UnRegisterCallBack(VideoEncoder *venc)
{
    if (HI_MAPI_VEnc_UnRegisterCallback(venc->venc_id, &venc->stVencCB) != HI_SUCCESS)
        return -1;
    return 0;
}

/* type: 0 - MainVideo, 1 - SubVideo */
VideoStream *MediaApi_OpenVideoStream(int type,
                                      VideoEncodeConf *conf,
                                      int (*frame_cb)(VideoFrame *frame, void *priv_data),
                                      void *priv_data)
{
    VideoStream *st = malloc(sizeof(VideoStream));
    if (!st)
        return NULL;
    memset(st, 0, sizeof(VideoStream));

    if (type == 0)
        st->stream_type = ENCODE_STREAM_TYPE_MAIN_VIDEO;
    else if (type == 1)
        st->stream_type = ENCODE_STREAM_TYPE_SUB_VIDEO;
    else {
        free(st);
        return NULL;
    }

    st->vport = MediaApi_GetVideoPort(st->stream_type);
    if (!st->vport) {
        free(st);
        return NULL;
    }

    st->venc = MediaApi_GetVideoEncoder(conf->codec, conf->width, conf->height, conf->framerate, conf->bitrate, conf->gop);
    if (!st->venc) {
        MediaApi_PutVideoPort(st->vport);
        free(st);
        return NULL;
    }

    if (VideoEncoder_RegisterCallBack(st->venc, frame_cb, priv_data) < 0) {
        MediaApi_PutVideoEncoder(st->venc);
        MediaApi_PutVideoPort(st->vport);
        free(st);
        return NULL;
    }

    if (VideoEncoder_BindVideoPort(st->venc, st->vport) < 0) {
        MediaApi_PutVideoEncoder(st->venc);
        MediaApi_PutVideoPort(st->vport);
        free(st);
        return NULL;
    }

    return st;
}

int MediaApi_CloseVideoStream(VideoStream *st)
{
    VideoEncoder_UnBindVideoPort(st->venc, st->vport);
    VideoEncoder_UnRegisterCallBack(st->venc);

    MediaApi_PutVideoEncoder(st->venc);
    MediaApi_PutVideoPort(st->vport);

    free(st);

    return 0;
}

int VideoStream_Start(VideoStream *st, int nb_frames)
{
    return VideoEncoder_StartEncode(st->venc, nb_frames);
}

int VideoStream_Stop(VideoStream *st)
{
    return VideoEncoder_StopEncode(st->venc);
}

int VideoStream_GetStreamHeadInfo(VideoStream *st, StreamInfoType type, uint8_t *head, uint32_t *len)
{
    HI_VENC_HEAD_INFO_TYPE_E enType;

    switch (type) {
    case STREAM_INFO_TYPE_VPS:
        enType = HI_VENC_HEAD_INFO_TYPE_VPS;
        break;
    case STREAM_INFO_TYPE_SPS:
        enType = HI_VENC_HEAD_INFO_TYPE_SPS;
        break;
    case STREAM_INFO_TYPE_PPS:
        enType = HI_VENC_HEAD_INFO_TYPE_PPS;
        break;
    }

    HI_S32 s32Ret = HI_MAPI_VEnc_GetStreamHeadInfo(st->venc->venc_id, enType, (HI_CHAR *)head, len);
    if (s32Ret != HI_SUCCESS)
        return -1;

    return 0;
}

struct PhotoStream {
    VideoPort *vport;
    VideoEncoder *venc;

    EncodeStreamType stream_type;
};

/* type: 0 - MainSnap, 1 - SubSnap */
PhotoStream *MediaApi_OpenPhotoStream(int type,
                                      PhotoEncodeConf *conf,
                                      int (*frame_cb)(VideoFrame *frame, void *priv_data),
                                      void *priv_data)
{
    PhotoStream *st = malloc(sizeof(PhotoStream));
    if (!st)
        return NULL;
    memset(st, 0, sizeof(PhotoStream));

    if (type == 0)
        st->stream_type = ENCODE_STREAM_TYPE_MAIN_SNAP;
    else if (type == 1)
        st->stream_type = ENCODE_STREAM_TYPE_SUB_SNAP;
    else {
        free(st);
        return NULL;
    }

    st->vport = MediaApi_GetVideoPort(st->stream_type);
    if (!st->vport) {
        free(st);
        return NULL;
    }

    st->venc = MediaApi_GetJpegEncoder(conf->width, conf->height, conf->qfactor);
    if (!st->venc) {
        MediaApi_PutVideoPort(st->vport);
        free(st);
        return NULL;
    }

    if (VideoEncoder_RegisterCallBack(st->venc, frame_cb, priv_data) < 0) {
        MediaApi_PutVideoEncoder(st->venc);
        MediaApi_PutVideoPort(st->vport);
        free(st);
        return NULL;
    }

    if (VideoEncoder_BindVideoPort(st->venc, st->vport) < 0) {
        MediaApi_PutVideoEncoder(st->venc);
        MediaApi_PutVideoPort(st->vport);
        free(st);
        return NULL;
    }

    return st;
}

int MediaApi_ClosePhotoStream(PhotoStream *st)
{
    VideoEncoder_UnBindVideoPort(st->venc, st->vport);
    VideoEncoder_UnRegisterCallBack(st->venc);

    MediaApi_PutVideoEncoder(st->venc);
    MediaApi_PutVideoPort(st->vport);

    free(st);

    return 0;
}

int PhotoStream_Start(PhotoStream *st, int nb_frames)
{
    if (VideoEncoder_StartEncode(st->venc, nb_frames) < 0)
        return -1;

    if (st->stream_type == ENCODE_STREAM_TYPE_MAIN_SNAP) {
        if (VideoEncoder_SetPhotoAttr(st->venc) < 0) {
            VideoEncoder_StopEncode(st->venc);
            return -1;
        }

        if (VideoEncoder_TriggerTakePhoto(st->venc) < 0) {
            VideoEncoder_StopEncode(st->venc);
            return -1;
        }
    }

    return 0;
}

int PhotoStream_Stop(PhotoStream *st)
{
    return VideoEncoder_StopEncode(st->venc);
}

struct VideoOSD {
    int vosd_id;

    HI_U32 u32PhyAddr;
    HI_VOID *pVitAddr;
    HI_U32 u32BufLen;
};

VideoOSD *MediaApi_OpenVideoOSD(int vosd_id)
{
    VideoOSD *vosd = malloc(sizeof(VideoOSD));
    if (!vosd)
        return NULL;
    memset(vosd, 0, sizeof(VideoOSD));

    vosd->vosd_id = vosd_id;

    return vosd;
}

int MediaApi_CloseVideoOSD(VideoOSD *vosd)
{
    if (vosd->pVitAddr) {
        HI_MAPI_FreeBuffer(vosd->u32PhyAddr, vosd->pVitAddr);
        vosd->pVitAddr = NULL;
        vosd->u32PhyAddr = 0;
    }

    free(vosd);
    return 0;
}

int MediaApi_SetOSDAttr(VideoOSD *vosd, uint32_t *rgba, int len, int color, int alpha, uint16_t x, uint16_t y, uint16_t width, uint16_t height)
{
    if (vosd->pVitAddr && vosd->u32BufLen < len) {
        HI_MAPI_FreeBuffer(vosd->u32PhyAddr, vosd->pVitAddr);
        vosd->pVitAddr = NULL;
        vosd->u32PhyAddr = 0;
        vosd->u32BufLen = 0;
    }

    if (!vosd->pVitAddr) {
        char osdname[32];
        snprintf(osdname, sizeof(osdname), "osd%d", vosd->vosd_id);
        if (HI_MAPI_AllocBuffer(&vosd->u32PhyAddr, &vosd->pVitAddr, len, osdname) != HI_SUCCESS)
            return -1;
        vosd->u32BufLen = len;
    }

    memcpy(vosd->pVitAddr, rgba, len);

    int i;
    for (i = 0; i < MAX_VIDEO_CAPTURE; i++) {
        VideoCaptureConfig *vcapCfg = get_video_capture_config(i);
        if (!vcapCfg)
            continue;

        uint32_t x_pixel = vcapCfg->res_width  * x / 1000;
        uint32_t y_pixel = vcapCfg->res_height * y / 1000;

        uint32_t new_x, new_y;

        if (x_pixel + width > vcapCfg->res_width)
            new_x = vcapCfg->res_width - width;
        else
            new_x = x_pixel;

        if (y_pixel + height > vcapCfg->res_height)
            new_y = vcapCfg->res_height - height;
        else
            new_y = y_pixel;

        /* must be 2 alignd */
        new_x = (new_x >> 1) << 1;
        new_y = (new_y >> 1) << 1;

        HI_MPP_OSD_ATTR_S osdAttr = {
            .stBitmapAttr = {
                .enPixelFormat = HI_MPP_PIXEL_FORMAT_RGB_8888,
                .u32Width = width,
                .u32Height = height,
                .pData = (void *)vosd->u32PhyAddr,
            },
            .stOsdDisplayAttr = {
                .bShow = HI_TRUE,
                .u32Color = color,
                .u32Alpha = alpha,
                .s32RegionX = new_x,
                .s32RegionY = new_y,
            },
        };

        HI_S32 s32Ret = HI_MAPI_VCap_OSD_SetAttr(i, vosd->vosd_id, &osdAttr);
        if (s32Ret != HI_SUCCESS) {
            LogV(TAG, "HI_MAPI_VCap_OSD_SetAttr(%d) failed, s32Ret: %x", i, s32Ret);
            continue;
        }
    }

    return 0;
}

int MediaApi_StartOSD(VideoOSD *vosd)
{
    int i;
    for (i = 0; i < MAX_VIDEO_CAPTURE; i++) {
        if (HI_MAPI_VCap_OSD_Start(i, vosd->vosd_id) != HI_SUCCESS)
            return -1;
    }
    return 0;
}

int MediaApi_StopOSD(VideoOSD *vosd)
{
    int i;
    for (i = 0; i < MAX_VIDEO_CAPTURE; i++) {
        if (HI_MAPI_VCap_OSD_Stop(i, vosd->vosd_id) != HI_SUCCESS)
            return -1;
    }
    return 0;
}

int MediaApi_SetExifInfo(ExifInfo *exif)
{
    HI_MPP_SNAP_EXIF_INFO_S stExifInfo;
    memset(&stExifInfo, 0, sizeof(HI_MPP_SNAP_EXIF_INFO_S ));

    strncpy((char *)stExifInfo.au8ImageDescription, exif->description,  sizeof(stExifInfo.au8ImageDescription) - 1);
    strncpy((char *)stExifInfo.au8Make,             exif->manufacturer, sizeof(stExifInfo.au8Make) - 1);
    strncpy((char *)stExifInfo.au8Model,            exif->model_number, sizeof(stExifInfo.au8Model) - 1);
    strncpy((char *)stExifInfo.au8Software,         exif->firmware_ver, sizeof(stExifInfo.au8Software) - 1);

    stExifInfo.u32MeteringMode = exif->metering_mode;
    stExifInfo.u32LightSource = exif->light_source;
    stExifInfo.u32FocalLength = exif->focal_length;
    stExifInfo.u8FocalLengthIn35mmFilm = exif->focal_35mm;

    int i;
    for (i = 0; i < MAX_VIDEO_CAPTURE; i++) {
        HI_MAPI_VCap_SetExifInfo(i, &stExifInfo);
    }

    return 0;
}

int MediaApi_StartAudioInput(void)
{
    HI_S32 s32Ret;
    HI_HANDLE ACapHdl = 0;

    HI_MPP_ACAP_ATTR_S stAcapAttr;
    stAcapAttr.enAudioMode = HI_MPP_AUDIO_MODE_I2S_MASTER;
    stAcapAttr.enBitwidth = HI_MPP_AUDIO_BITWIDTH_16;
    stAcapAttr.enSampleRate = HI_MPP_AUDIO_SAMPLE_RATE_48;
    stAcapAttr.enSoundMode = HI_MPP_AUDIO_SOUND_MODE_LEFT;
    stAcapAttr.u32PtNumPerFrm = audioCfg.u32PtNumPerFrm;

    s32Ret = HI_MAPI_ACap_Init(ACapHdl, &stAcapAttr);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_ACap_Init fail s32Ret: %d\n", s32Ret);
        return -1;
    }

    s32Ret = HI_MAPI_ACap_Start(ACapHdl);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_ACap_Start fail s32Ret: %d\n", s32Ret);
        return -1;
    }

    HI_MPP_AUDIO_GAIN_S stAudioGain;
    stAudioGain.s32Gain = 20;
    s32Ret = HI_MAPI_ACap_SetVolume(ACapHdl, &stAudioGain);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_ACap_SetVolume fail s32Ret: %d\n", s32Ret);
        return -1;
    }

#if 0
    s32Ret = HI_MAPI_Register_AudioEncoder(HI_MPP_AUDIO_FORMAT_AAC);
    if (s32Ret != HI_SUCCESS ) {
        LogE(TAG, "HI_MAPI_Register_AudioEncoder fail\n");
        return HI_FAILURE;
    }
#endif

    return 0;
}

int MediaApi_StopAudioInput(void)
{
    HI_S32 s32Ret;
    HI_HANDLE ACapHdl = 0;

#if 0
    s32Ret = HI_MAPI_UnRegister_AudioEncoder(HI_MPP_AUDIO_FORMAT_AAC);
    if (s32Ret != HI_SUCCESS)
        return -1;
#endif

    s32Ret = HI_MAPI_ACap_Stop(ACapHdl);
    if (s32Ret != HI_SUCCESS)
        return -1;

    s32Ret = HI_MAPI_ACap_DeInit(ACapHdl);
    if (s32Ret != HI_SUCCESS)
        return -1;

    return 0;
}

typedef struct {
    int enable;

    HI_HANDLE ACapHdl;
    HI_HANDLE AEncHdl;

    HI_AENC_CALLBACK_S stAencCB;

    int (*frame_cb)(AudioFrame *frame, void *priv_data);
    void *priv_data;
} AudioEncoder;

struct AudioStream {
    HI_HANDLE ACapHdl;
    HI_HANDLE AEncHdl;

    AudioEncoder *aenc;
};

static AudioEncoder audioEncoders[2];

static HI_S32 AUDIO_DataProc(HI_HANDLE AencHdl, HI_MPP_AENC_STREAM_S *pAStreamData, HI_VOID *pPrivateData)
{
    AudioEncoder *aenc = pPrivateData;

    AudioFrame f = {
        .sinfo = {
            .media = MEDIA_TYPE_AUDIO,
            .codec = CODEC_TYPE_AAC,
            .pts = pAStreamData->u64TimeStamp,
            .time_ms = get_time(),
        },
        .data = pAStreamData->pu8Addr,
        .size = pAStreamData->u32Len,
    };

    if (aenc->frame_cb)
        aenc->frame_cb(&f, aenc->priv_data);

    return HI_SUCCESS;
}

static int find_avaialble_audio_encoder(void)
{
    int i;
    for (i = 0; i < NELEMS(audioEncoders); i++) {
        if (audioEncoders[i].enable == 0)
            return i;
    }

    return -1;
}

static int AudioEncoder_RegisterCallBack(AudioEncoder *aenc,
                                         int (*frame_cb)(AudioFrame *frame, void *priv_data),
                                         void *priv_data)
{
    aenc->frame_cb = frame_cb;
    aenc->priv_data = priv_data;

    aenc->stAencCB.pfnDataCB = AUDIO_DataProc;
    aenc->stAencCB.pPrivateData = aenc;

    HI_S32 s32Ret = HI_MAPI_AEnc_RegisterCallback(aenc->AEncHdl, &aenc->stAencCB);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_AEnc_RegisterCallback fail s32Ret: %x\n", s32Ret);
        return -1;
    }

    return 0;
}

static int AudioEncoder_UnRegisterCallBack(AudioEncoder *aenc)
{
    HI_MAPI_AEnc_UnRegisterCallback(aenc->AEncHdl, &aenc->stAencCB);
    return 0;
}

static int AudioEncoder_BindAudioCapture(AudioEncoder *aenc, HI_HANDLE ACapHdl)
{
    HI_S32 s32Ret = HI_MAPI_AEnc_BindACap(ACapHdl, aenc->AEncHdl);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_AEnc_BindACap fail s32Ret: %x\n", s32Ret);
        return -1;
    }

    aenc->ACapHdl = ACapHdl;

    return 0;
}

static int AudioEncoder_UnBindAudioCapture(AudioEncoder *aenc)
{
    HI_MAPI_AEnc_UnBindACap(aenc->ACapHdl, aenc->AEncHdl);
    return 0;
}

AudioEncoder *MediaApi_GetAudioEncoder(AudioEncoderConfig *audioCfg)
{
    int aenc_id = find_avaialble_audio_encoder();
    if (aenc_id < 0) {
        LogV(TAG, "there is no avaialble audio encoder");
        return NULL;
    }

    AudioEncoder *aenc = &audioEncoders[aenc_id];
    memset(aenc, 0, sizeof(AudioEncoder));

    aenc->ACapHdl = 0;
    aenc->AEncHdl = aenc_id;

    HI_S32 s32Ret;

    AENC_ATTR_AAC_S stAacAencAttr;
    stAacAencAttr.enAACType = audioCfg->enAACType;
    stAacAencAttr.enBitRate = audioCfg->enBitRate;
    stAacAencAttr.enBitWidth = audioCfg->enBitWidth;
    stAacAencAttr.enSmpRate = audioCfg->enSmpRate;
    stAacAencAttr.enSoundType = audioCfg->enSoundType;
    stAacAencAttr.enTransType = audioCfg->enTransType;
    stAacAencAttr.s16BandWidth = audioCfg->s16BandWidth;

    HI_MPP_AENC_ATTR_S stAencAttr;
    stAencAttr.enAencFormat = HI_MPP_AUDIO_FORMAT_AAC;
    stAencAttr.u32PtNumPerFrm = audioCfg->u32PtNumPerFrm;
    stAencAttr.pValue = &stAacAencAttr;
    stAencAttr.u32Len = sizeof(stAacAencAttr);

    s32Ret = HI_MAPI_AEnc_Init(aenc->AEncHdl, &stAencAttr);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_AEnc_Init fail s32Ret: %x\n", s32Ret);
        return NULL;
    }

    return aenc;
}

int MediaApi_PutAudioEncoder(AudioEncoder *aenc)
{
    HI_S32 s32Ret;

    s32Ret = HI_MAPI_AEnc_UnBindACap(aenc->ACapHdl, aenc->AEncHdl);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_AEnc_UnBindACap fail s32Ret: %x\n", s32Ret);
        return -1;
    }

    s32Ret = HI_MAPI_AEnc_Stop(aenc->AEncHdl);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_AEnc_Stop fail s32Ret: %d\n", s32Ret);
        return -1;
    }

    s32Ret = HI_MAPI_AEnc_UnRegisterCallback(aenc->AEncHdl, &aenc->stAencCB);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_AEnc_UnRegisterCallback fail s32Ret: %d\n", s32Ret);
        return -1;
    }

    s32Ret = HI_MAPI_AEnc_DeInit(aenc->AEncHdl);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_AEnc_DeInit fail s32Ret: %x\n", s32Ret);
        return -1;
    }

    return 0;
}

static int MediaApi_GetAudioEncoderConfig(AudioEncodeConf *conf, AudioEncoderConfig *audioCfg)
{
    if (conf->codec != CODEC_TYPE_AAC) {
        LogE(TAG, "invalid audio codec: %d\n", conf->codec);
        return -1;  /* AAC only */
    }

    switch (conf->bitrate) {
    case AAC_BPS_16K:
    case AAC_BPS_22K:
    case AAC_BPS_24K:
    case AAC_BPS_32K:
    case AAC_BPS_48K:
    case AAC_BPS_64K:
    case AAC_BPS_96K:
    case AAC_BPS_128K:
    case AAC_BPS_256K:
    case AAC_BPS_320K:
        audioCfg->enBitRate = conf->bitrate;
        break;
    default:
        LogE(TAG, "invalid bitrate: %d\n", conf->bitrate);
        return -1;
    }

    switch (conf->sample_rate) {
    case HI_MPP_AUDIO_SAMPLE_RATE_8:
    case HI_MPP_AUDIO_SAMPLE_RATE_11025:
    case HI_MPP_AUDIO_SAMPLE_RATE_16:
    case HI_MPP_AUDIO_SAMPLE_RATE_22050:
    case HI_MPP_AUDIO_SAMPLE_RATE_24:
    case HI_MPP_AUDIO_SAMPLE_RATE_32:
    case HI_MPP_AUDIO_SAMPLE_RATE_441:
    case HI_MPP_AUDIO_SAMPLE_RATE_48:
        audioCfg->enSmpRate = conf->sample_rate;
        break;
    default:
        LogE(TAG, "invalid sample rate: %d\n", conf->sample_rate);
        return -1;
    }

    switch (conf->bit_width) {
    case 8:
        audioCfg->enBitWidth = HI_MPP_AUDIO_BITWIDTH_8;
        break;
    case 16:
        audioCfg->enBitWidth = HI_MPP_AUDIO_BITWIDTH_16;
        break;
    case 24:
        audioCfg->enBitWidth = HI_MPP_AUDIO_BITWIDTH_24;
        break;
    default:
        LogE(TAG, "invalid bit width: %d\n", conf->bit_width);
        return -1;
    }

    if (conf->channels == 1)
        audioCfg->enSoundType = HI_MPP_AUDIO_SOUND_TYPE_MONO;
    else if (conf->channels == 2)
        audioCfg->enSoundType = HI_MPP_AUDIO_SOUND_TYPE_STEREO;
    else {
        LogE(TAG, "invalid sound channels: %d\n", conf->channels);
        return -1;
    }

    audioCfg->u32PtNumPerFrm = conf->samples_per_frame;

    return 0;
}

AudioStream *MediaApi_OpenAudioStream(AudioEncodeConf *conf, int (*frame_cb)(AudioFrame *frame, void *priv_data), void *priv_data)
{
    AudioStream *st = malloc(sizeof(AudioStream));
    if (!st)
        return NULL;
    memset(st, 0, sizeof(AudioStream));

    AudioEncoderConfig audioCfg;
    if (MediaApi_GetAudioEncoderConfig(conf, &audioCfg) < 0) {
        free(st);
        return NULL;
    }

    st->aenc = MediaApi_GetAudioEncoder(&audioCfg);
    if (!st->aenc) {
        free(st);
        return NULL;
    }

    if (AudioEncoder_RegisterCallBack(st->aenc, frame_cb, priv_data) < 0) {
        MediaApi_PutAudioEncoder(st->aenc);
        return NULL;
    }

    if (AudioEncoder_BindAudioCapture(st->aenc, 0) < 0) {
        AudioEncoder_UnRegisterCallBack(st->aenc);
        MediaApi_PutAudioEncoder(st->aenc);
        return NULL;
    }

    return st;
}

int MediaApi_CloseAudioStream(AudioStream *st)
{
    AudioEncoder_UnBindAudioCapture(st->aenc);
    AudioEncoder_UnRegisterCallBack(st->aenc);
    MediaApi_PutAudioEncoder(st->aenc);
    free(st);
    return 0;
}

int AudioStream_Start(AudioStream *st)
{
    HI_S32 s32Ret = HI_MAPI_AEnc_Start(st->aenc->AEncHdl);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_AEnc_Start fail s32Ret: %d\n", s32Ret);
        return -1;
    }

    return 0;
}

int AudioStream_Stop(AudioStream *st)
{
    HI_S32 s32Ret = HI_MAPI_AEnc_Stop(st->aenc->AEncHdl);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_AEnc_Stop fail s32Ret: %x\n", s32Ret);
        return -1;
    }

    return 0;
}

struct VideoPreview {
    VideoPort *vport;
    int disp_id;
    int wnd_id;

    EncodeStreamType stream_type;
};

/* type: 0 - capture preview, 1 - record preview */
VideoPreview *MediaApi_OpenVideoPreview(int type)
{
    HI_S32 s32Ret;

    VideoPreview *vprev = malloc(sizeof(VideoPreview));
    if (!vprev)
        return NULL;
    memset(vprev, 0, sizeof(VideoPreview));

    if (type == 0) {
        vprev->stream_type = ENCODE_STREAM_TYPE_CAP_PREVIEW;
    } else {
        vprev->stream_type = ENCODE_STREAM_TYPE_REC_PREVIEW;
    }

    vprev->vport = MediaApi_GetVideoPort(vprev->stream_type);
    if (!vprev->vport) {
        LogV(TAG, "MediaApi_GetVideoPort(%d) failed", vprev->stream_type);
        free(vprev);
        return NULL;
    }

    HI_HANDLE DispHdl = 0;
    HI_MPP_DISP_ATTR_S stDispAttr;
    stDispAttr.u32BgColor = 0xFF;
    stDispAttr.enIntfType = DISP_INTF_BT1120;
    stDispAttr.enIntfSync = DISP_SYNC_1080P30;
    memset(&stDispAttr.stSyncInfo, 0, sizeof(HI_MPP_DISP_SYNC_INFO_S));

    s32Ret = HI_MAPI_Disp_Init(DispHdl, &stDispAttr);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_Disp_Init failed, s32Ret = %08x\n", s32Ret);
        MediaApi_PutVideoPort(vprev->vport);
        free(vprev);
        return NULL;
    }

    s32Ret = HI_MAPI_Disp_Start(DispHdl);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_Disp_Start failed, s32Ret = %08x\n", s32Ret);
        HI_MAPI_Disp_DeInit(DispHdl);
        MediaApi_PutVideoPort(vprev->vport);
        free(vprev);
        return NULL;
    }

    HI_HANDLE WndHdl = 0;
    HI_MPP_DISP_WINDOW_ATTR_S stWndAttr;
    stWndAttr.stRect.u32X = 0;
    stWndAttr.stRect.u32Y = 0;
    stWndAttr.stRect.u32Width = 1280;
    stWndAttr.stRect.u32Height = 720;
    stWndAttr.u32Priority = 0;

    s32Ret = HI_MAPI_Disp_Window_SetAttr(DispHdl, WndHdl, &stWndAttr);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_Disp_Window_SetAttr failed, s32Ret = %08x\n", s32Ret);
        HI_MAPI_Disp_Stop(DispHdl);
        HI_MAPI_Disp_DeInit(DispHdl);
        MediaApi_PutVideoPort(vprev->vport);
        free(vprev);
        return NULL;
    }

    s32Ret = HI_MAPI_Disp_Window_Start(DispHdl, WndHdl);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_Disp_Window_Start failed, s32Ret = %08x\n", s32Ret);
        HI_MAPI_Disp_Stop(DispHdl);
        HI_MAPI_Disp_DeInit(DispHdl);
        MediaApi_PutVideoPort(vprev->vport);
        free(vprev);
        return NULL;
    }

    s32Ret = HI_MAPI_Disp_Bind_VProc(vprev->vport->vproc_id, vprev->vport->vport_id, DispHdl, WndHdl);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MAPI_Disp_Bind_VProc failed, s32Ret = %08x\n", s32Ret);
        HI_MAPI_Disp_Window_Stop(DispHdl, WndHdl);
        HI_MAPI_Disp_Stop(DispHdl);
        HI_MAPI_Disp_DeInit(DispHdl);
        MediaApi_PutVideoPort(vprev->vport);
        free(vprev);
        return NULL;
    }

    vprev->disp_id = DispHdl;
    vprev->wnd_id = WndHdl;

    return vprev;
}

int MediaApi_CloseVideoPreview(VideoPreview *vprev)
{
    HI_HANDLE DispHdl = vprev->disp_id;
    HI_HANDLE WndHdl = vprev->wnd_id;

    HI_MAPI_Disp_UnBind_VProc(vprev->vport->vproc_id, vprev->vport->vport_id, DispHdl, WndHdl);

    HI_MAPI_Disp_Window_Stop(DispHdl, WndHdl);

    HI_MAPI_Disp_Stop(DispHdl);

    HI_MAPI_Disp_DeInit(DispHdl);

    MediaApi_PutVideoPort(vprev->vport);

    free(vprev);

    return 0;
}

struct VideoPlayer {
    HI_HANDLE hAVplayer;
    HI_HANDLE hVOHandle;
    HI_HANDLE hAOHandle;

    int video_time_base;
    int audio_time_base;
};

VideoPlayer *MediaApi_CreatePlayer(VideoInfo *vinfo, AudioInfo *ainfo)
{
#if 0
    HI_S32 s32Ret;

    if (!vinfo)
        return NULL;

    VideoPlayer *player = malloc(sizeof(VideoPlayer));
    if (!player)
        return NULL;
    memset(player, 0, sizeof(VideoPlayer));

    player->hVOHandle = 0;
    player->hAOHandle = 0;

    player->video_time_base = vinfo ? vinfo->time_base : 0;
    player->audio_time_base = ainfo ? ainfo->time_base : 0;

    s32Ret = HI_MW_AVPLAYER_Init();
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MW_AVPLAYER_Init failed, s32Ret = %x", s32Ret);
        free(player);
        return NULL;
    }

    HI_AVPLAYER_MEDIA_TYPE_E eMediaType;

    if (ainfo) {
        eMediaType = E_PLAYER_TYPE_VIDEO_AUDIO;
    } else {
        eMediaType = E_PLAYER_TYPE_VIDEO;
    }

    HI_AVPLAYER_VIDEO_TYPE_E eVideoType;
    if (vinfo->codec == CODEC_TYPE_H264)
        eVideoType = E_PLAYER_VID_TYPE_H264;
    else if (vinfo->codec == CODEC_TYPE_H265)
        eVideoType = E_PLAYER_VID_TYPE_H265;
    else {
        LogE(TAG, "Unknown video codec type %d", vinfo->codec);
        HI_MW_AVPLAYER_Deinit();
        free(player);
        return NULL;
    }

    HI_AVPLAYER_MEDIA_INFO_S stMediaInfo;
    stMediaInfo.eMediaType = eMediaType;

    stMediaInfo.stVideoInfo.s32Width = vinfo->width;
    stMediaInfo.stVideoInfo.s32Height = vinfo->height;
    stMediaInfo.stVideoInfo.s32Fps = vinfo->framerate;
    stMediaInfo.stVideoInfo.eType = eVideoType;

    if (ainfo) {
        if (ainfo->codec != CODEC_TYPE_AAC) {
            LogE(TAG, "Unknown audio codec type %d", ainfo->codec);
            HI_MW_AVPLAYER_Deinit();
            free(player);
            return NULL;
        }

        stMediaInfo.stAudioInfo.s32SampleRate = ainfo->sample_rate;
        stMediaInfo.stAudioInfo.s32Channel = ainfo->nb_channels;
        stMediaInfo.stAudioInfo.s32BitWidth = ainfo->bit_width;
        stMediaInfo.stAudioInfo.eType = E_PLAYER_AUD_TYPE_AAC;
    }

    s32Ret = HI_MW_AVPLAYER_Create(&player->hAVplayer, &stMediaInfo);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MW_AVPLAYER_Create failed, s32Ret = %x", s32Ret);
        HI_MW_AVPLAYER_Deinit();
        free(player);
        return NULL;
    }

    s32Ret = HI_MW_AVPLAYER_AttachVO(player->hAVplayer, player->hVOHandle);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MW_AVPLAYER_AttachVO failed, s32Ret = %x", s32Ret);
        HI_MW_AVPLAYER_Destroy(player->hAVplayer);
        HI_MW_AVPLAYER_Deinit();
        free(player);
        return NULL;
    }

    if (ainfo) {
        s32Ret = HI_MW_AVPLAYER_AttachAO(player->hAVplayer, player->hAOHandle);
        if (s32Ret != HI_SUCCESS) {
            LogE(TAG, "HI_MW_AVPLAYER_AttachAO failed, s32Ret = %x", s32Ret);
            HI_MW_AVPLAYER_Destroy(player->hAVplayer);
            HI_MW_AVPLAYER_Deinit();
            free(player);
            return NULL;
        }
    }

    s32Ret = HI_MW_AVPLAYER_Start(player->hAVplayer);
    if (s32Ret != HI_SUCCESS) {
        LogE(TAG, "HI_MW_AVPLAYER_Start failed, s32Ret = %x", s32Ret);
        HI_MW_AVPLAYER_Destroy(player->hAVplayer);
        HI_MW_AVPLAYER_Deinit();
        free(player);
        return NULL;
    }

    return player;
#else
    return NULL;
#endif
}

int MediaApi_DestroyPlayer(VideoPlayer *player)
{
#if 0
    HI_MW_AVPLAYER_Destroy(player->hAVplayer);
    HI_MW_AVPLAYER_Deinit();
#endif
    return 0;
}

int VideoPlayer_SendStream(VideoPlayer *player, MediaType media, void *data, int size, int64_t pts)
{
#if 0
    HI_AVPLAYER_BUFFER_TYPE_E eType;

    if (media == MEDIA_TYPE_VIDEO)
        eType = E_PLAYER_BUFFER_VIDEO;
    else
        eType = E_PLAYER_BUFFER_AUDIO;

    int64_t pts1;

    if (media == MEDIA_TYPE_VIDEO)
        pts1 = pts * 1000000 / player->video_time_base;
    else
        pts1 = pts;


    HI_AVPLAYER_STREAM_INFO_S stStreamInfo = {
        .pData = data,
        .u32DataLen = size,
        .u32BufLen = size,
        .u32Pts = pts1,
        .u32Dts = pts1,
    };

    HI_S32 s32Ret = HI_MW_AVPLAYER_PutStreamBuffer(player->hAVplayer, eType, stStreamInfo);
    if (s32Ret != HI_SUCCESS)
        return -1;
#endif
    return 0;
}
