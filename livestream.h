#ifndef __LIVESTREAM_H__
#define __LIVESTREAM_H__

int LiveStream_Init(void);
int LiveStream_Quit(void);

typedef struct LiveStream LiveStream;

LiveStream *LiveStream_Open(int stream_id);
int LiveStream_Close(LiveStream *live);

int LiveStream_GetVideoStreamHeadInfo(LiveStream *live, StreamInfoType type, uint8_t *head, uint32_t *len);

int LiveStream_Start(LiveStream *live);
int LiveStream_Stop(LiveStream *live);

typedef struct LiveStreamReader LiveStreamReader;

LiveStreamReader *LiveStream_CreateReader(int stream_id, int64_t start_ms);
int LiveStream_DestroyReader(LiveStreamReader *reader);
int LiveStream_ReadV(LiveStreamReader *reader, struct iovec *iov, int iovcnt, int milliseconds);

#endif  /* __LIVESTREAM_H__ */
