#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/vfs.h>
#include <sys/time.h>

#include "h264.h"
#include "list.h"
#include "event.h"
#include "config.h"
#include "camapi.h"
#include "params.h"
#include "logger.h"
#include "recorder.h"
#include "storage.h"
#include "bufchan.h"
#include "mediaapi.h"
#include "camera_fsm.h"
#include "globaldata.h"

static const char *TAG = "CamApi";

int CamApi_SystemShutdown(void)
{
    /* stop recording gracefully */
    CamFsm_StopRecordVideo();

    return system("shutdown -h now");
}

int CamApi_SystemReboot(void)
{
    /* stop recording gracefully */
    CamFsm_StopRecordVideo();

    return system("reboot");
}

int CamApi_SystemFactoryReset(int hard)
{
    /* stop recording gracefully */
    CamFsm_StopRecordVideo();

    /* remove all parameters and reboot */
    unlink(DEFAULT_CONFIG_FILE);

    /* reboot after reset */
    return system("reboot");
}

int CamApi_StartUpgrade(const char *filename)
{
    return 0;
}

int CamApi_SetSystemDateTime(char *datetime)
{
    int year  = -1;
    int month = -1;
    int day   = -1;
    int hour  = -1;
    int min   = -1;
    int sec   = -1;

    sscanf(datetime, "%04d-%02d-%02dT%02d:%02d:%02d",
            &year, &month, &day, &hour, &min, &sec);

    if (year < 0 || month < 0 || day < 0 ||
        hour < 0 || min < 0   || sec < 0)
        return -1;

    struct tm t = {
        .tm_year = year - 1900,
        .tm_mon  = month - 1,
        .tm_mday = day,
        .tm_hour = hour,
        .tm_min  = min,
        .tm_sec  = sec
    };

    time_t t1 = mktime(&t);

    struct timeval tv = {
        .tv_sec = t1,
        .tv_usec = 0
    };

    return settimeofday(&tv, NULL);
}

int CamApi_GetSystemDateTime(char *datetime, int len)
{
    time_t now = time(NULL);

    struct tm tm;
    localtime_r(&now, &tm);

    snprintf(datetime, len, "%04d-%02d-%02dT%02d:%02d:%02d",
            tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
            tm.tm_hour, tm.tm_min, tm.tm_sec);

    return 0;
}

int CamApi_GetWifiConfiguration(WifiConfiguration *conf)
{
    return Params_GetWifiConfiguration(conf);
}

int CamApi_SetWifiConfiguration(WifiConfiguration *conf)
{
    Params_SetWifiConfiguration(conf);
    return 0;
}

int CamApi_GetStorageStatus(StorageStatus *status)
{
    if (Storage_GetStatus(status) < 0) {
        LogV(TAG, "Storage_GetStatus failed");
        return -1;
    }

    return 0;
}

int CamApi_FormatStorage(void)
{
    if (RecMgr_DeleteVolume(SDCARD_MOUNT_POINT) < 0) {
        LogV(TAG, "RecMgr_DeleteVolume failed");
        return -1;
    }

    if (Storage_Format() < 0) {
        LogV(TAG, "Storage_Format failed");
        return -1;
    }

    if (RecMgr_AddVolume(SDCARD_MOUNT_POINT) < 0) {
        LogV(TAG, "RecMgr_AddVolume(%s) failed", SDCARD_MOUNT_POINT);
        return -1;
    }

    return 0;
}

int CamApi_GetPhotoTakingSettings(PhotoTakingSettings *settings)
{
    return Params_GetPhotoTakingSettings(settings);
}

int CamApi_SetPhotoTakingSettings(PhotoTakingSettings *settings)
{
    return Params_SetPhotoTakingSettings(settings);
}

int CamApi_GetRecordSettings(RecordSettings *settings)
{
    return Params_GetRecordSettings(settings);
}

int CamApi_SetRecordSettings(RecordSettings *settings)
{
    RecConf conf = {
        .recfmt = settings->recfmt,
        .max_rec_size = settings->max_record_size * 1024 * 1024LL,
    };
    Recorder_SetConf(&conf);
    Params_SetRecordSettings(settings);

    return 0;
}

int CamApi_GetImageSettings(ImageSettings *settings)
{
    Params_GetImageSettings(settings);
    return 0;
}

int CamApi_SetImageSettings(ImageSettings *settings)
{
    /* TODO: set image settings here */
    Params_SetImageSettings(settings);
    return 0;
}

int CamApi_GetOsdSettings(OsdSettings *settings)
{
    Params_GetOsdSettings(settings);
    return 0;
}

int CamApi_SetOsdSettings(OsdSettings *settings)
{
    return Params_SetOsdSettings(settings);
}

int CamApi_GetVideoEncoderConfiguration(int stream, VideoEncoderConfiguration *conf)
{
    if (stream >= 2)
        return -1;

    Params_GetVideoEncoderConfiguration(stream, conf);

    return 0;
}

int CamApi_SetVideoEncoderConfiguration(int stream, VideoEncoderConfiguration *conf)
{
    if (stream >= 2)
        return -1;

    return CamFsm_SetVideoEncoderConfiguration(stream, conf);
}

int CamApi_GetAudioEncoderConfiguration(AudioEncoderConfiguration *conf)
{
    return Params_GetAudioEncoderConfiguration(conf);
}

int CamApi_SetAudioEncoderConfiguration(AudioEncoderConfiguration *conf)
{
#if 0
    if (MediaApi_SetAudioEncoderConfiguration(conf->codec_type, conf->bitrate,
                conf->sample_rate, conf->bit_width, conf->channels, conf->samples_per_frame) < 0)
        return -1;
#endif

    return Params_SetAudioEncoderConfiguration(conf);
}

int CamApi_GetCameraMode(CameraMode *mode)
{
    return CamFsm_GetMode(mode);
}

int CamApi_SetCameraMode(CameraMode mode)
{
    Params_SetCameraMode(mode);
    return CamFsm_SetMode(mode);
}

int CamApi_StartRecord(void)
{
    return CamFsm_StartRecordVideo();
}

int CamApi_StopRecord(void)
{
    return CamFsm_StopRecordVideo();
}

int CamApi_StartSnapshot(void)
{
    return CamFsm_TakePhoto();
}

int CamApi_StopSnapshot(void)
{
    return CamFsm_AbortTakePhoto();
}

int CamApi_GetFilesNum(const char *suffix)
{
    return RecMgr_GetFilesNum(suffix);
}

int CamApi_GetFilesList(int index, const char *suffix, FileItem *files, int nb_files)
{
    return RecMgr_GetFilesList(index, suffix, files, nb_files, 0);
}

int CamApi_DeleteFile(const char *filename)
{
    return RecMgr_DeleteFile(filename);
}

static JList *allsubscriber = NULL;
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
static int next_session_id = 0x20170815;

static Subscriber *event_subs = NULL;
static long event_hndl;

static pthread_t monitor_tid;

typedef struct {
    int session_id;
    time_t ttl;

    long r_handle;
} EventSubscriber;

static void *subscriber_monitor(void *arg)
{
    JListNode *n, *n_next;

    while (1) {
        pthread_mutex_lock(&lock);
        for (n = j_list_first(allsubscriber); n; n = n_next) {
            n_next = j_list_next(allsubscriber, n);

            EventSubscriber *sub = n->data;
            time_t now = time(NULL);
            if (now > sub->ttl) {
                j_list_remove(allsubscriber, sub);
                BufChan_Close(sub->r_handle);
                free(sub);
            }
        }
        pthread_mutex_unlock(&lock);
        sleep(1);
    }

    return NULL;
}

static int event_cb(Event *ev, void *priv_data)
{
    BufChan_Write(event_hndl, ev, sizeof(Event));
    return 0;
}

#define MAX_GET_EVENTS_TIMEOUT  (50)

int CamApi_Init(void)
{
    BufChan_MkFifo(EVENT_STREAM, 64 * 1024);

    allsubscriber = j_list_alloc();
    if (!allsubscriber)
        goto fail;

    event_hndl = BufChan_Open(EVENT_STREAM, AV_WRONLY);
    if (!event_hndl)
        goto fail;

    event_subs = Event_NewSubscriber(0xffffffff, event_cb, NULL);
    if (!event_subs)
        goto fail;

    pthread_create(&monitor_tid, NULL, subscriber_monitor, NULL);

    return 0;

fail:
    CamApi_Quit();
    return -1;
}

int CamApi_Quit(void)
{
    if (monitor_tid) {
        pthread_join(monitor_tid, NULL);
        monitor_tid = 0;
    }

    if (event_subs) {
        Event_DelSubscriber(event_subs);
        event_subs = NULL;
    }

    if (event_hndl) {
        BufChan_Close(event_hndl);
        event_hndl = 0;
    }

    if (allsubscriber) {
        j_list_free(allsubscriber);
        allsubscriber = NULL;
    }

    BufChan_UnLink(EVENT_STREAM);

    return 0;
}

int CamApi_CreateEventSubscriber(int *session_id)
{
    EventSubscriber *sub = malloc(sizeof(EventSubscriber));
    if (!sub)
        return -1;
    memset(sub, 0, sizeof(EventSubscriber));

    sub->ttl = time(NULL) + MAX_GET_EVENTS_TIMEOUT;
    sub->r_handle = BufChan_Open(EVENT_STREAM, AV_RDONLY);
    if (!sub->r_handle) {
        free(sub);
        return -1;
    }

    pthread_mutex_lock(&lock);
    sub->session_id = next_session_id;
    next_session_id++;

    j_list_append(allsubscriber, sub);
    pthread_mutex_unlock(&lock);

    *session_id = sub->session_id;

    return 0;
}

static EventSubscriber *get_subscriber_by_id(int session_id)
{
    JListNode *n, *n_next;

    pthread_mutex_lock(&lock);
    for (n = j_list_first(allsubscriber); n; n = n_next) {
        n_next = j_list_next(allsubscriber, n);

        EventSubscriber *sub = n->data;
        if (sub->session_id == session_id) {
            pthread_mutex_unlock(&lock);
            return sub;
        }
    }
    pthread_mutex_unlock(&lock);

    return 0;
}

int CamApi_DeleteEventSubscriber(int session_id)
{
    JListNode *n, *n_next;

    pthread_mutex_lock(&lock);
    for (n = j_list_first(allsubscriber); n; n = n_next) {
        n_next = j_list_next(allsubscriber, n);

        EventSubscriber *sub = n->data;
        if (sub->session_id == session_id) {
            j_list_remove(allsubscriber, sub);
            BufChan_Close(sub->r_handle);
            free(sub);

            pthread_mutex_unlock(&lock);
            return 0;
        }
    }
    pthread_mutex_unlock(&lock);

    return -1;
}

int CamApi_GetEvents(int session_id, Event *events, int nmemb)
{
    EventSubscriber *sub = get_subscriber_by_id(session_id);
    if (!sub)
        return -1;
    sub->ttl = time(NULL) + MAX_GET_EVENTS_TIMEOUT;

    int ret = BufChan_Read(sub->r_handle, &events[0], sizeof(Event), 5000);
    if (ret == 0)
        return 0;

    int i;
    for (i = 1; i < nmemb; i++) { /* try get more */
        ret = BufChan_Read(sub->r_handle, &events[i], sizeof(Event), 1);
        if (ret == 0)
            break;
    }

    return i;
}

int CamApi_PlaybackStart(const char *filename)
{
    return CamFsm_PlaybackStart(filename);
}

int CamApi_PlaybackStop(void)
{
    return CamFsm_PlaybackStop();
}

int CamApi_PlaybackPause(void)
{
    return CamFsm_PlaybackPause();
}

int CamApi_PlaybackResume(void)
{
    return CamFsm_PlaybackResume();
}

int CamApi_PlaybackSeek(int off_ms)
{
    return CamFsm_PlaybackSeek(off_ms);
}

int CamApi_PlaybackSetSpeed(float scale)
{
    return CamFsm_PlaybackSetSpeed(scale);
}
