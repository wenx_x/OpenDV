#ifndef __EXIF_H__
#define __EXIF_H__

#include <stdint.h>

#include "libexif/exif-loader.h"
#include "libexif/exif-data.h"

typedef struct {
    ExifLoader *loader;
    ExifData *data;

    uint8_t *ptr;
    uint8_t *end;
} ExifReader;

ExifReader *ExifReader_Open(char *filename);
int ExifReader_Close(ExifReader *r);

int ExifReader_Read(ExifReader *r, uint8_t *buf, int len);

#endif  /* __EXIF_H__ */
