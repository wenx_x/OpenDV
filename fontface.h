#ifndef __FONTFACE_H__
#define __FONTFACE_H__

typedef struct FontFace FontFace;

FontFace *FontFace_CreateByFile(char *font_path, int char_width, int char_height);
FontFace *FontFace_CreateByMemory(uint8_t *font, int size, int char_width, int char_height);

int FontFace_Destroy(FontFace *ff);

/* result must release by free() */
int FontFace_Render(FontFace *ff, char *text, uint32_t **rgba, int *width, int *height);

#endif  /* __FONTFACE_H__ */
