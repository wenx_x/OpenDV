#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "fontface.h"

struct FontFace {
    FT_Library library;
    FT_Face face;

    int char_width;
    int char_height;
};

static void draw_bitmap(FT_Bitmap *bitmap, FT_Int x, FT_Int y, uint32_t *image, int width, int height)
{
    FT_Int i, j, p, q;
    FT_Int x_max = x + bitmap->width;
    FT_Int y_max = y + bitmap->rows;

    for (i = x, p = 0; i < x_max; i++, p++) {
        for (j = y, q = 0; j < y_max; j++, q++) {
            if (i < 0 || j < 0 || i >= width || j >= height)
                continue;

            /* gray to rgba */
            uint32_t pixel = bitmap->buffer[q * bitmap->width + p];
            if (pixel < 32) {
                image[j * width + i] |= 0x00;
            } else {
                image[j * width + i] |= (pixel * 0x00010101) << 8 | 0xff;
            }
        }
    }
}

FontFace *FontFace_CreateByFile(char *font_path, int char_width, int char_height)
{
    FontFace *ff = malloc(sizeof(FontFace));
    if (!ff)
        return NULL;
    memset(ff, 0, sizeof(FontFace));

    ff->char_width = char_width;
    ff->char_height = char_height;

    /* initialize library */
    if (FT_Init_FreeType(&ff->library) != 0) {
        free(ff);
        return NULL;
    }

    /* create face object */
    if (FT_New_Face(ff->library, font_path, 0, &ff->face) != 0) {
        FT_Done_FreeType(ff->library);
        free(ff);
        return NULL;
    }

    if (FT_Set_Char_Size(ff->face, char_width, char_height * 64, 100, 0) != 0) {
        FT_Done_Face(ff->face);
        FT_Done_FreeType(ff->library);
        free(ff);
        return NULL;
    }

    return ff;
}

FontFace *FontFace_CreateByMemory(uint8_t *font, int size, int char_width, int char_height)
{
    FontFace *ff = malloc(sizeof(FontFace));
    if (!ff)
        return NULL;
    memset(ff, 0, sizeof(FontFace));

    ff->char_width = char_width;
    ff->char_height = char_height;

    /* initialize library */
    if (FT_Init_FreeType(&ff->library) != 0) {
        free(ff);
        return NULL;
    }

    /* create face object */
    if (FT_New_Memory_Face(ff->library, font, size, 0, &ff->face) != 0) {
        FT_Done_FreeType(ff->library);
        free(ff);
        return NULL;
    }

    if (FT_Set_Char_Size(ff->face, char_width, char_height * 64, 100, 0) != 0) {
        FT_Done_Face(ff->face);
        FT_Done_FreeType(ff->library);
        free(ff);
        return NULL;
    }

    return ff;
}

int FontFace_Destroy(FontFace *ff)
{
    FT_Done_Face(ff->face);
    FT_Done_FreeType(ff->library);
    free(ff);
    return 0;
}

static int get_text_dimention(FontFace *ff, char *text, int *bitmap_top, int *width, int *height)
{
    FT_Vector pen;
    pen.x = 0;
    pen.y = 0;

    FT_GlyphSlot slot = ff->face->glyph;

    int num_chars = strlen(text);

    int top = 0;
    int bottom = 0;

    int n;
    for (n = 0; n < num_chars; n++) {
        /* load glyph image into the slot (erase previous one) */
        if (FT_Load_Char(ff->face, text[n], FT_LOAD_RENDER) != 0)
            continue;                 /* ignore errors */

        top = slot->bitmap_top > top ? slot->bitmap_top : top;

        int bottom1 = slot->bitmap.rows - slot->bitmap_top;

        bottom = bottom1 > bottom ? bottom1 : bottom;

        /* increment pen position */
        pen.x += slot->advance.x >> 6;
    }

    *width  = pen.x;
    *bitmap_top = top;

    if (bottom > 0)
        *height = top + bottom;
    else
        *height = top;

    return 0;
}

int FontFace_Render(FontFace *ff, char *text, uint32_t **rgba, int *width, int *height)
{
    int bitmap_top;
    if (get_text_dimention(ff, text, &bitmap_top, width, height) < 0)
        return -1;

    int size = (*width) * (*height) * 4;

    uint32_t *buf = malloc(size);
    if (!buf)
        return -1;
    memset(buf, 0, size);

    FT_Vector pen;
    pen.x = 0;
    pen.y = bitmap_top;

    FT_GlyphSlot slot = ff->face->glyph;

    int num_chars = strlen(text);

    int n;
    for (n = 0; n < num_chars; n++) {
        /* load glyph image into the slot (erase previous one) */
        if (FT_Load_Char(ff->face, text[n], FT_LOAD_RENDER) != 0)
            continue;

        /* now, draw to our target surface (convert position) */
        draw_bitmap(&slot->bitmap, pen.x + slot->bitmap_left, pen.y - slot->bitmap_top, buf, *width, *height);

        /* increment pen position */
        pen.x += slot->advance.x >> 6;
    }

    *rgba = buf;

    return size;
}

#if 0
int main(int argc, char **argv)
{
    char *filename;
    char *text;

    if (argc != 3) {
        fprintf (stderr, "usage: %s font sample-text\n", argv[0]);
        exit(1);
    }

    filename = argv[1];                           /* first argument     */
    text = argv[2];                           /* second argument    */

    FontFace *ff = FontFace_CreateByFile(filename, 0, 64);

    int width;
    int height;
    uint32_t *result = NULL;

    int len = FontFace_Render(ff, text, &result, &width, &height);
    if (len < 0)
        return -1;

    printf("width = %d, height = %d, len = %d\n", width, height, len);

    int fd = open("pic.rgba", O_CREAT | O_TRUNC | O_WRONLY, 0644);
    write(fd, result, len);
    close(fd);

    free(result);

    FontFace_Destroy(ff);

    return 0;
}
#endif

/* EOF */
