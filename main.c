#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>

#include "vod.h"
#include "osd.h"
#include "event.h"
#include "logger.h"
#include "netsvc.h"
#include "config.h"
#include "common.h"
#include "params.h"
#include "diskmgr.h"
#include "bufchan.h"
#include "rtspsvc.h"
#include "recorder.h"
#include "httpsvc.h"
#include "avtimer.h"
#include "storage.h"
#include "mediaapi.h"
#include "uploader.h"
#include "simstream.h"
#include "camera_fsm.h"
#include "globaldata.h"
#include "livestream.h"
#include "downloader.h"

#include "h264.h"

static const char *TAG = "main";

static int liveview_describe(char *path, MediaDesc *descs, int nmemb)
{
    int count = 0;

    MediaDesc *desc = &descs[count++];
    memset(desc, 0, sizeof(MediaDesc));

    desc->codec = CODEC_TYPE_H264;
    desc->sample_rate = 90000;

    int stream_id = atoi(path + strlen("/liveview/"));

    uint32_t sps_len = sizeof(desc->sps);
    if (CamFsm_GetVideoStreamHeadInfo(stream_id, STREAM_INFO_TYPE_SPS, desc->sps, &sps_len) == 0)
        desc->sps_len = sps_len;

    uint32_t pps_len = sizeof(desc->pps);
    if (CamFsm_GetVideoStreamHeadInfo(stream_id, STREAM_INFO_TYPE_PPS, desc->pps, &pps_len) == 0)
        desc->pps_len = pps_len;

    AudioEncoderConfiguration audio_cfg;
    if (Params_GetAudioEncoderConfiguration(&audio_cfg) < 0)
        return -1;

    if (audio_cfg.enable) {
        desc = &descs[count++];
        desc->codec = audio_cfg.codec_type;
        desc->sample_rate = audio_cfg.sample_rate;
        desc->audio_channels = audio_cfg.channels;
    }

    return count;
}

static long liveview_open(char *path)
{
    int stream_id = atoi(path + strlen("/liveview/"));

    return (long)LiveStream_CreateReader(stream_id, 0);
}

static int liveview_close(long handle)
{
    return LiveStream_DestroyReader((LiveStreamReader *)handle);
}

static int liveview_read(long handle, MediaInfo *info, uint8_t *data, size_t size)
{
    StreamInfo stream_info;

    struct iovec iov[2];
    iov[0].iov_base = &stream_info;
    iov[0].iov_len = sizeof(StreamInfo);

    iov[1].iov_base = data;
    iov[1].iov_len = size;

    LiveStreamReader *reader = (LiveStreamReader *)handle;

    int ret = LiveStream_ReadV(reader, iov, 2, 100);
    if (ret == 0)
        return 0;

    memset(info, 0, sizeof(MediaInfo));

    info->codec = stream_info.codec;
    info->last_slice = 1;
    info->pts = stream_info.pts;

    return ret - sizeof(StreamInfo);
}

static RtspHandler liveview_hndl = {
    .describe = liveview_describe,
    .open     = liveview_open,
    .close    = liveview_close,
    .read     = liveview_read,
};

static int record_describe(char *path, MediaDesc *descs, int nmemb)
{
    char filename[128] = {0};
    strncpy(filename, path + strlen("/record/"), sizeof(filename) - 1);

    return VodFile_GetMediaDesc(filename, descs, nmemb);
}

static long record_open(char *path)
{
    char filename[128] = {0};
    if (strlen(path) < strlen("/record/"))
        return 0;

    strncpy(filename, path + strlen("/record/"), sizeof(filename) - 1);

    return (long)VodFile_Open(filename);
}

static int record_close(long handle)
{
    return VodFile_Close((VodFile *)handle);
}

static int record_read(long handle, MediaInfo *info, uint8_t *data, size_t size)
{
    VodFrameInfo finfo;
    int ret = VodFile_ReadFrame((VodFile *)handle, &finfo, data, size);
    if (ret <= 0)
        return ret;

    memset(info, 0, sizeof(MediaInfo));

    info->codec = finfo.codec;
    info->pts = finfo.pts;
    info->last_slice = 1;

    return ret;
}

static int record_seek(long handle, int off_ms)
{
    return VodFile_SeekByTime((VodFile *)handle, off_ms);
}

static int record_scale(long handle, float scale)
{
    return VodFile_SetScaleFactor((VodFile *)handle, scale);
}

static RtspHandler record_hndl = {
    .describe = record_describe,
    .open     = record_open,
    .close    = record_close,
    .read     = record_read,
    .seek     = record_seek,
    .scale    = record_scale,
};

int main(void)
{
    Event_Init();

    Logger_Init(LOGGER_PORT, 2 * 1024 * 1024);
    Params_Init(DEFAULT_CONFIG_FILE);

    LiveStream_Init();

    MediaApi_Init();

    if (MediaApi_StartVideoInput(VIDEO_FMT_ID_3840x2160P30) < 0) {
        LogV(TAG, "MediaApi_StartVideoInput failed, format id %d", VIDEO_FMT_ID_3840x2160P30);
        return -1;
    }

    if (MediaApi_StartAudioInput() < 0)
        return -1;

    CamFsm_Init();

    Recorder_Init();
    Storage_Init();

    Osd_Init();

    /* setup rtsp server */
    RtspSvc *rtspsvc = RtspSvc_Create(RTSP_PORT);
    if (!rtspsvc)
        return -1;

    RtspSvc_AddRoute(rtspsvc, "/record",   &record_hndl);
    RtspSvc_AddRoute(rtspsvc, "/liveview", &liveview_hndl);

    /* setup http server */
    HttpSvc *httpsvc = HttpSvc_Create(HTTP_PORT);
    if (!httpsvc)
        return -1;

    /* setup message handler */
    NetSvc_Init(httpsvc, "/camera");

    UpLoader_Init(httpsvc, "/upload");
    DownLoader_Init(httpsvc, "/download");

    LogV("main", "System Init Success");

#ifdef USE_SIMULATE_STREAM
    SimStream_Start();
#endif

    while (1)
        sleep(100);

    NetSvc_Quit();

    LiveStream_Quit();
    RtspSvc_Destroy(rtspsvc);
    HttpSvc_Destroy(httpsvc);

    Osd_Quit();
    CamFsm_Quit();
    MediaApi_Quit();

    Storage_Quit();
    Recorder_Quit();

    Logger_Quit();
    Event_Quit();

    return 0;
}
