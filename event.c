#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "list.h"
#include "queue.h"
#include "event.h"

static pthread_t tid;
static Event flush_ev;
static JQueue *eventq = NULL;
static JList *allsubscriber = NULL;

static int abort_request = 0;

struct Subscriber {
    int event_type;
    int (*event_cb)(Event *ev, void *priv_data);
    void *priv_data;
};

static void *event_task(void *arg)
{
    while (!abort_request) {
        Event *ev = j_queue_get(eventq, 1);
        if (!ev)
            continue;

        if (ev == &flush_ev)
            continue;

        JListNode *n, *n_next;
        for (n = j_list_first(allsubscriber); n; n = n_next) {
            n_next = j_list_next(allsubscriber, n);

            Subscriber *sub = n->data;

            if (sub->event_type & ev->event_type)
                sub->event_cb(ev, sub->priv_data);
        }
    }

    return NULL;
}

int Event_Init(void)
{
    eventq = j_queue_new();
    if (!eventq)
        return -1;

    allsubscriber = j_list_alloc();
    if (!allsubscriber) {
        j_queue_destroy(eventq);
        return -1;
    }

    pthread_create(&tid, NULL, event_task, NULL);

    return 0;
}

int Event_Quit(void)
{
    abort_request = 1;
    j_queue_put(eventq, &flush_ev);

    pthread_join(tid, NULL);

    j_list_free(allsubscriber);
    j_queue_destroy(eventq);

    return 0;
}

int Event_Dispatch(Event *ev)
{
    return j_queue_put(eventq, ev);
}

Subscriber *Event_NewSubscriber(uint32_t events, int (*event_cb)(Event *ev, void *priv_data), void *priv_data)
{
    if (!event_cb)
        return NULL;

    Subscriber *sub = malloc(sizeof(Subscriber));
    if (!sub)
        return NULL;
    memset(sub, 0, sizeof(Subscriber));

    sub->event_type = events;
    sub->event_cb = event_cb;

    j_list_append(allsubscriber, sub);

    return sub;
}

int Event_DelSubscriber(Subscriber *sub)
{
    j_list_remove(allsubscriber, sub);
    free(sub);
    return 0;
}
