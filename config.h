#ifndef __CONFIG_H__
#define __CONFIG_H__

#define SDCARD_MOUNT_POINT  "/dev/mmc"
#define SDCARD_DEVICE_NAME  "mmc://mmcblk0"

#define LIVESTREAM_LARGE    "livestream.large"
#define LIVESTREAM_SMALL    "livestream.small"

#define EVENT_STREAM        "system.events"

#define DEFAULT_CONFIG_FILE "./config.json"

#define RTSP_PORT   8554
#define HTTP_PORT   8888
#define LOGGER_PORT 8848

#define STORAGE_LOW_WARTER_MARK  (134217728) /* 128MB = 128 * 1024 * 1024 */

#define FILE_DELETE_IN_LOW_SPACE    "MOV,MP4,JPG"

#define UPLOAD_PATH         "/dev"

#define USE_SIMULATE_STREAM

#endif  /* __CONFIG_H__ */
